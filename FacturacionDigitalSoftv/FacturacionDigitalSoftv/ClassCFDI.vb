﻿
Imports System
Imports System.Collections.Generic
Imports System.ComponentModel
Imports System.Data
Imports System.Drawing
Imports System.Text
Imports System.Windows.Forms
Imports System.Xml
Imports System.Data.SqlClient
'Imports System.Data.OleDb
Imports MizarCFD.DAL
Imports MizarCFD.BRL
Imports MizarCFD.Reportes
Imports MizarCFDi.API.Efac
Imports CrystalDecisions.Shared
Imports CrystalDecisions.CrystalReports.Engine

'Imports Newtonsoft.Json

Public Class ClassCFDI

    Public Shared GloClv_FacturaSoftv As Long = 0
    Public Shared GloClv_FacturaCFD As Long = 0
    Public Shared RutaReportes As String = ""
    Public Shared GloContrato As Long = 0
    Public Shared MiConexion As String = ""
    Public Shared GloIdAsociado As Long = 0
    Public Shared GloOpcionPantalla As Long = 0
    Public Shared Locop As Integer = 0
    Public Shared EsTimbrePrueba As Boolean = False


    Public Shared Contrato As Long = 0
    Public Shared mensaje5 As String = "  Se Guardo con Exíto  "
    Public Shared mensaje6 As String = " Se Elimino con Exíto "
    Public Shared OpcionCli As Char = Nothing
    Public Shared LocClv_Ciudad As String = ""
    Public Shared LocGloSistema As String = "Softv"
    Public Shared GloUsuario As String = "SISTE"
    Public Shared gloOpcionCli As String = ""


    Public Shared oAlta_Datosfiscales As Boolean = True

    Public Shared GloTxtUlt4DigCheque As String = Nothing
    Public Shared GloFacGlobalclvcompania As Long = 0
    Public Shared GloNomReporteFiscal As String = Nothing
    Public Shared GloNomReporteFiscalGlobal As String = Nothing
    Public Shared GloIDFactura As Long = 0
    Public Shared GloFECHAPAGO As DateTime


    Public Shared GloMotivoCancelacionCFD As String = Nothing

    Public Shared mIva As Decimal = 0
    Public Shared mipes As Decimal = 0
    Public Shared msubtotal As Decimal = 0
    Public Shared mdetalle1 As String = Nothing
    Public Shared locID_Compania_Mizart As String = Nothing
    Public Shared locID_Sucursal_Mizart As String = Nothing
    Public Shared GloFORMADEPAGO As String = Nothing
    Public Shared GlonumCtaPago As String = ""
    Public Shared GloCONDICIONESDEPAGO As String = Nothing
    Public Shared GloMOTIVODESCUENTO As String = Nothing
    Public Shared GloMETODODEPAGO As String = Nothing
    Public Shared GloTIPODECOMPROBANTE As String = Nothing
    Public Shared GloPAGOENPARCIALIDADES As String = Nothing
    Public Shared GloLUGAREXPEDICION As String = Nothing
    Public Shared GloREGIMEN As String = Nothing
    Public Shared GloMONEDA As String = Nothing
    Public Shared GloVERSON As String = Nothing

    Public Shared GloTIPOFACTOR As String = Nothing
    Public Shared GloFORMAPAGO As String = Nothing
    Public Shared GloFECHAVIGENCIA As String = Nothing
    Public Shared GloTIPOCAMBIO As String = Nothing
    Public Shared GloUSOCFD As String = Nothing
    Public Shared GloUUIDRel As String = Nothing
    Public Shared GloOBSERVACION As String = Nothing
    Public Shared GloSERIE As String = Nothing
    Public Shared GloFOLIO As String = Nothing
    Public Shared GloNUMPARCIALIDAD As String = Nothing
    Public Shared GloSALDOANTERIOR As String = Nothing
    Public Shared GloSALDONUEVO As String = Nothing
    Public Shared GloID_CFD As String = Nothing
    Public Shared GloMONTOPAGO As String = Nothing
    Public Shared GloMONTOORIGINAL As String = Nothing


    'Public Sub Imprime_Factura_Digital(ByVal oid_CFD As Integer)
    '    '    Using Cnx As New DAConexion("HL", "sa", "sa")
    '    '        Dim oRreportes As New MizarCFD.Reportes.CFD
    '    '        Dim oCFD As New MizarCFD.BRL.CFD
    '    '        oCFD.IdCFD = "1"
    '    '        oCFD.Consultar(Cnx)
    '    '        Dim document As New System.Xml.XmlDocument
    '    '        oCFD.ObtenerXML()
    '    '        document.Value = oCFD.CadenaOriginal
    '    '    End Using

    'End Sub
    Public Shared Sub Dime_Aque_Compania_FacturarleMaestro(ByVal oClv_Session As Long, ByVal locconexion As String)
        locID_Compania_Mizart = ""
        locID_Sucursal_Mizart = ""

        Dim con As New SqlConnection(locconexion)
        Dim com As New SqlCommand("Dime_Aque_Compania_FacturarleMaestro", con)
        com.CommandType = CommandType.StoredProcedure
        com.CommandTimeout = 0
        '@Numero numeric(20,2),@moneda int,@letra varchar(max) output
        'Dame la fecha del Servidor
        Dim prm1 As New SqlParameter("@Clv_SessionMaestro", SqlDbType.BigInt)
        prm1.Direction = ParameterDirection.Input
        prm1.Value = oClv_Session
        com.Parameters.Add(prm1)

        Dim prm2 As New SqlParameter("@ID_Compania_Mizart", SqlDbType.VarChar, 50)
        prm2.Direction = ParameterDirection.Output
        prm2.Value = 0
        com.Parameters.Add(prm2)

        Dim prm3 As New SqlParameter("@ID_Sucursal_Mizart", SqlDbType.VarChar, 50)
        prm3.Direction = ParameterDirection.Output
        prm3.Value = ""
        com.Parameters.Add(prm3)


        Try
            con.Open()
            com.ExecuteNonQuery()
            locID_Compania_Mizart = prm2.Value
            locID_Sucursal_Mizart = prm3.Value

        Catch ex As Exception
            'Llenalog(ex.Source.ToString & " " & ex.Message)
            If Graba = "0" Then
                Llenalog(ex.Source.ToString & " " & ex.Message)
            Else

                Llenalog(ex.Source.ToString & " " & ex.Message)
            End If
        Finally
            con.Close()
        End Try
    End Sub

    Public Shared Sub UPS_Inserta_Rel_FacturasCFDMaestro(ByVal oCLV_Session As Long, ByVal oClv_FacturaCFD As Long, ByVal oSerie As String, ByVal oTipo As String, ByVal locconexion As String)
        Dim CONEXION As New SqlConnection(locconexion)
        Dim COMANDO As New SqlCommand("UPS_Inserta_Rel_FacturasCFDMaestro", CONEXION)
        COMANDO.CommandType = CommandType.StoredProcedure
        COMANDO.CommandTimeout = 0
        '@Clv_Factura Bigint,@Clv_FacturaCFD bigint,@Serie varchar(5)
        Dim PARAMETRO1 As New SqlParameter("@Clv_Factura", SqlDbType.BigInt)
        PARAMETRO1.Direction = ParameterDirection.Input
        PARAMETRO1.Value = oCLV_Session
        COMANDO.Parameters.Add(PARAMETRO1)

        Dim PARAMETRO2 As New SqlParameter("@Clv_FacturaCFD", SqlDbType.BigInt)
        PARAMETRO2.Direction = ParameterDirection.Input
        PARAMETRO2.Value = oClv_FacturaCFD
        COMANDO.Parameters.Add(PARAMETRO2)

        Dim PARAMETRO3 As New SqlParameter("@Serie", SqlDbType.VarChar, 5)
        PARAMETRO3.Direction = ParameterDirection.Input
        PARAMETRO3.Value = oSerie
        COMANDO.Parameters.Add(PARAMETRO3)

        Dim PARAMETRO4 As New SqlParameter("@tipo", SqlDbType.VarChar, 1)
        PARAMETRO4.Direction = ParameterDirection.Input
        PARAMETRO4.Value = oTipo
        COMANDO.Parameters.Add(PARAMETRO4)

        Try
            CONEXION.Open()
            Dim i As Integer = COMANDO.ExecuteNonQuery
        Catch ex As Exception
            'MsgBox(ex.Message, MsgBoxStyle.Exclamation)
            If Graba = "0" Then
                Llenalog(ex.Source.ToString & " " & ex.Message)
            Else

                Llenalog(ex.Source.ToString & " " & ex.Message)
            End If
        Finally
            CONEXION.Close()
            CONEXION.Dispose()
        End Try
    End Sub

    Public Shared Function MZ_SPASOCIOADOSMIZAR(ByVal oRazonSocial As String,
       ByVal oRFC As String,
       ByVal oCalle As String,
       ByVal oClaveAsociado As String,
       ByVal oCodigoPostal As String,
       ByVal oColonia As String,
       ByVal oEMail As String,
       ByVal oEntreCalles As String,
       ByVal oEstado As String,
       ByVal oIdAsociado As String,
       ByVal oLocalidad As String,
       ByVal oMunicipio As String,
       ByVal oNumeroExterior As String,
       ByVal oNumeroInterior As String,
       ByVal oPais As String,
       ByVal oTel As String,
       ByVal oFax As String,
       ByVal oReferencia As String,
       ByVal oContrato As Long,
       ByVal ometodo_de_pago As String, ByVal locconexion As String) As String
        Try
            '@Contrato Bigint,
            '@BndError int OUTPUT,
            '@BndMsj varchar(800) OUTPUT,
            '@razon_social	varchar(255),
            '@RFC varchar(15),
            '@metodo_de_pago	varchar(30),
            '@calle	varchar(255),
            '@numero_exterior	varchar(15),
            '@numero_interior	varchar(15),
            '@entre_calles	varchar(255),
            '@colonia	varchar(255),
            '@localidad	varchar(255),
            '@referencia	varchar(255),
            '@municipio	varchar(255),
            '@estado	varchar(255),
            '@pais	varchar(255),
            '@codigo_postal	varchar(5),
            '@telefono	varchar(255),
            '@fax	varchar (255),
            '@email	varchar(255)
            Dim oNumError As Integer = 0

            Class1.limpiaParametros()
            Class1.CreateMyParameter("@Contrato", SqlDbType.Int, oContrato)
            'Ouput
            Class1.CreateMyParameter("@BndError", ParameterDirection.Output, SqlDbType.Int)
            Class1.CreateMyParameter("@BndMsj", ParameterDirection.Output, SqlDbType.VarChar, 800)

            Class1.CreateMyParameter("@razon_social", SqlDbType.VarChar, 255, ParameterDirection.Input, oRazonSocial)
            Class1.CreateMyParameter("@RFC", SqlDbType.VarChar, 15, ParameterDirection.Input, oRFC)
            Class1.CreateMyParameter("@metodo_de_pago", SqlDbType.VarChar, 30, ParameterDirection.Input, ometodo_de_pago)
            Class1.CreateMyParameter("@calle", SqlDbType.VarChar, 250, ParameterDirection.Input, oCalle)
            Class1.CreateMyParameter("@numero_exterior", SqlDbType.VarChar, 250, ParameterDirection.Input, oNumeroExterior)
            Class1.CreateMyParameter("@numero_interior", SqlDbType.VarChar, 250, ParameterDirection.Input, oNumeroInterior)
            Class1.CreateMyParameter("@entre_calles", SqlDbType.VarChar, 250, ParameterDirection.Input, oEntreCalles)
            Class1.CreateMyParameter("@colonia", SqlDbType.VarChar, 250, ParameterDirection.Input, oColonia)
            Class1.CreateMyParameter("@localidad", SqlDbType.VarChar, 250, ParameterDirection.Input, oLocalidad)
            Class1.CreateMyParameter("@referencia", SqlDbType.VarChar, 250, ParameterDirection.Input, oReferencia)
            Class1.CreateMyParameter("@municipio", SqlDbType.VarChar, 250, ParameterDirection.Input, oMunicipio)
            Class1.CreateMyParameter("@estado", SqlDbType.VarChar, 250, ParameterDirection.Input, oEstado)
            Class1.CreateMyParameter("@pais", SqlDbType.VarChar, 250, ParameterDirection.Input, oPais)
            Class1.CreateMyParameter("@codigo_postal", SqlDbType.VarChar, 250, ParameterDirection.Input, oCodigoPostal)
            Class1.CreateMyParameter("@telefono", SqlDbType.VarChar, 250, ParameterDirection.Input, oTel)
            Class1.CreateMyParameter("@fax", SqlDbType.VarChar, 250, ParameterDirection.Input, oFax)
            Class1.CreateMyParameter("@email", SqlDbType.VarChar, 250, ParameterDirection.Input, oEMail)
            Class1.ProcedimientoOutPut2("MZ_SPASOCIOADOSMIZAR", locconexion)
            oNumError = Class1.dicoPar("@BndError").ToString()
            If oNumError = 0 Then
                MZ_SPASOCIOADOSMIZAR = ""
            Else
                MZ_SPASOCIOADOSMIZAR = Class1.dicoPar("@BndMsj").ToString()
            End If
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Public Shared Sub Usp_Ed_DameDatosFacDigMaestro(ByVal oCLV_Session As Long, ByVal oCompania As Integer, ByVal locconexion As String)
        GloFORMADEPAGO = ""
        GloCONDICIONESDEPAGO = ""
        GloMOTIVODESCUENTO = ""
        GloMETODODEPAGO = ""
        GloTIPODECOMPROBANTE = ""
        GloPAGOENPARCIALIDADES = ""
        GloLUGAREXPEDICION = ""
        GloREGIMEN = ""
        GloMONEDA = ""
        GloVERSON = ""
        GloTIPOFACTOR = ""
        GloFORMAPAGO = ""
        GloTIPOCAMBIO = ""
        GloUSOCFD = ""
        GloFECHAVIGENCIA = ""

        Dim CONEXION As New SqlConnection(locconexion)
        Dim COMANDO As New SqlCommand("Usp_Ed_DameDatosFacDigMaestro", CONEXION)
        COMANDO.CommandType = CommandType.StoredProcedure
        CoManDo.CommandTimeout = 0
        '@Clv_Factura Bigint,@Clv_FacturaCFD bigint,@Serie varchar(5)

        Dim PARAMETRO1 As New SqlParameter("@Clv_SessionMaestro", SqlDbType.BigInt)
        PARAMETRO1.Direction = ParameterDirection.Input
        PARAMETRO1.Value = oCLV_Session
        COMANDO.Parameters.Add(PARAMETRO1)

        Dim PARAMETRO2 As New SqlParameter("@IDCOMPANIA", SqlDbType.Int)
        PARAMETRO2.Direction = ParameterDirection.Input
        PARAMETRO2.Value = oCompania
        COMANDO.Parameters.Add(PARAMETRO2)

        '@FORMADEPAGO VARCHAR(250) OUTPUT
        ',@CONDICIONESDEPAGO VARCHAR(250)OUTPUT,@MOTIVODESCUENTO VARCHAR(250)OUTPUT,@METODODEPAGO VARCHAR(250)OUTPUT,
        '@TIPODECOMPROBANTE VARCHAR(50) OUTPUT,@PAGOENPARCIALIDADES VARCHAR(50) OUTPUT,@LUGAREXPEDICION VARCHAR(250) OUTPUT
        ',@REGIMEN VARCHAR(400) OUTPUT,@MONEDA VARCHAR(15) OUTPUT,@VERSON

        Dim PARAMETRO3 As New SqlParameter("@FORMADEPAGO", SqlDbType.VarChar, 250)
        PARAMETRO3.Direction = ParameterDirection.Output
        PARAMETRO3.Value = ""
        COMANDO.Parameters.Add(PARAMETRO3)

        Dim PARAMETRO4 As New SqlParameter("@CONDICIONESDEPAGO", SqlDbType.VarChar, 250)
        PARAMETRO4.Direction = ParameterDirection.Output
        PARAMETRO4.Value = ""
        COMANDO.Parameters.Add(PARAMETRO4)

        Dim PARAMETRO5 As New SqlParameter("@MOTIVODESCUENTO", SqlDbType.VarChar, 250)
        PARAMETRO5.Direction = ParameterDirection.Output
        PARAMETRO5.Value = ""
        COMANDO.Parameters.Add(PARAMETRO5)

        Dim PARAMETRO6 As New SqlParameter("@METODODEPAGO", SqlDbType.VarChar, 250)
        PARAMETRO6.Direction = ParameterDirection.Output
        PARAMETRO6.Value = ""
        COMANDO.Parameters.Add(PARAMETRO6)

        Dim PARAMETRO7 As New SqlParameter("@TIPODECOMPROBANTE", SqlDbType.VarChar, 250)
        PARAMETRO7.Direction = ParameterDirection.Output
        PARAMETRO7.Value = ""
        COMANDO.Parameters.Add(PARAMETRO7)

        Dim PARAMETRO8 As New SqlParameter("@PAGOENPARCIALIDADES", SqlDbType.VarChar, 250)
        PARAMETRO8.Direction = ParameterDirection.Output
        PARAMETRO8.Value = ""
        COMANDO.Parameters.Add(PARAMETRO8)

        Dim PARAMETRO9 As New SqlParameter("@LUGAREXPEDICION", SqlDbType.VarChar, 250)
        PARAMETRO9.Direction = ParameterDirection.Output
        PARAMETRO9.Value = ""
        COMANDO.Parameters.Add(PARAMETRO9)

        Dim PARAMETRO10 As New SqlParameter("@REGIMEN", SqlDbType.VarChar, 400)
        PARAMETRO10.Direction = ParameterDirection.Output
        PARAMETRO10.Value = ""
        COMANDO.Parameters.Add(PARAMETRO10)

        Dim PARAMETRO11 As New SqlParameter("@MONEDA", SqlDbType.VarChar, 15)
        PARAMETRO11.Direction = ParameterDirection.Output
        PARAMETRO11.Value = ""
        COMANDO.Parameters.Add(PARAMETRO11)

        Dim PARAMETRO12 As New SqlParameter("@VERSON", SqlDbType.VarChar, 15)
        PARAMETRO12.Direction = ParameterDirection.Output
        PARAMETRO12.Value = ""
        COMANDO.Parameters.Add(PARAMETRO12)

        Dim PARAMETRO13 As New SqlParameter("@numCtaPago", SqlDbType.VarChar, 4)
        PARAMETRO13.Direction = ParameterDirection.Output
        PARAMETRO13.Value = ""
        COMANDO.Parameters.Add(PARAMETRO13)

        Dim PARAMETRO14 As New SqlParameter("@TIPOFACTOR", SqlDbType.VarChar, 4)
        PARAMETRO14.Direction = ParameterDirection.Output
        PARAMETRO14.Value = ""
        COMANDO.Parameters.Add(PARAMETRO14)

        Dim PARAMETRO15 As New SqlParameter("@USOCFD", SqlDbType.VarChar, 4)
        PARAMETRO15.Direction = ParameterDirection.Output
        PARAMETRO15.Value = ""
        COMANDO.Parameters.Add(PARAMETRO15)

        Dim PARAMETRO16 As New SqlParameter("@FECHAFACTURA", SqlDbType.VarChar, 20)
        PARAMETRO16.Direction = ParameterDirection.Output
        PARAMETRO16.Value = ""
        COMANDO.Parameters.Add(PARAMETRO16)

        Dim PARAMETRO17 As New SqlParameter("@TIPOCAMBIO", SqlDbType.VarChar, 4)
        PARAMETRO17.Direction = ParameterDirection.Output
        PARAMETRO17.Value = ""
        COMANDO.Parameters.Add(PARAMETRO17)

        Try
            '@FORMADEPAGO VARCHAR(250) OUTPUT
            ',@CONDICIONESDEPAGO VARCHAR(250)OUTPUT,@MOTIVODESCUENTO VARCHAR(250)OUTPUT,@METODODEPAGO VARCHAR(250)OUTPUT,
            '@TIPODECOMPROBANTE VARCHAR(50) OUTPUT,@PAGOENPARCIALIDADES VARCHAR(50) OUTPUT,@LUGAREXPEDICION VARCHAR(250) OUTPUT
            ',@REGIMEN VARCHAR(400) OUTPUT,@MONEDA VARCHAR(15) OUTPUT,@VERSON  

            CONEXION.Open()
            Dim i As Integer = COMANDO.ExecuteNonQuery
            GloFORMADEPAGO = PARAMETRO3.Value
            GloCONDICIONESDEPAGO = PARAMETRO4.Value
            GloMOTIVODESCUENTO = PARAMETRO5.Value
            GloMETODODEPAGO = PARAMETRO6.Value
            GloTIPODECOMPROBANTE = PARAMETRO7.Value
            GloPAGOENPARCIALIDADES = PARAMETRO8.Value
            GloLUGAREXPEDICION = PARAMETRO9.Value
            GloREGIMEN = PARAMETRO10.Value
            GloMONEDA = PARAMETRO11.Value
            GloVERSON = PARAMETRO12.Value
            GlonumCtaPago = PARAMETRO13.Value
            GloTIPOFACTOR = PARAMETRO14.Value
            GloUSOCFD = PARAMETRO15.Value
            GloFECHAVIGENCIA = PARAMETRO16.Value
            GloTIPOCAMBIO = PARAMETRO17.Value
        Catch ex As Exception
            Llenalog(ex.Source.ToString & " " & ex.Message)
        Finally
            CONEXION.Close()
            CONEXION.Dispose()
        End Try
    End Sub

   
    Public Shared Sub Graba_Factura_DigitalMaestro(ByVal oClv_Session As Long, ByVal oIden As Integer, ByVal locconexion As String)

        Dim ClvFacturaCfd As Long
        ClvFacturaCfd = SP_DIMESIEXISTEFACTURADIGITAL(oClv_Session, "N", locconexion)
        If ClvFacturaCfd = 0 Then '0 Es si no Existe

            Dim oTotalConPuntos As String = Nothing
            Dim oSubTotal As String = Nothing
            Dim oTotalSinPuntos As String = Nothing
            Dim oDescuento As String = Nothing
            Dim oiva As String = Nothing
            Dim oieps As String = Nothing
            Dim oTasaIva As String = Nothing
            Dim oTasaIeps As String = Nothing
            Dim oFecha As String = Nothing
            Dim oTotalImpuestos As String = Nothing

            ''
            Dim mfecha As DateTime
            Dim con As New SqlConnection(locconexion)
            Dim com As New SqlCommand("DAMEFECHADELSERVIDORFACTURADIGITAL", con)
            com.CommandType = CommandType.StoredProcedure
            Com.CommandTimeout = 0
            'Dame la fecha del Servidor
            Dim prmFechaObtebida As New SqlParameter("@FECHA", SqlDbType.DateTime)
            prmFechaObtebida.Direction = ParameterDirection.Output
            prmFechaObtebida.Value = ""
            com.Parameters.Add(prmFechaObtebida)
            Try
                con.Open()
                com.ExecuteNonQuery()
                mfecha = prmFechaObtebida.Value
            Catch ex As Exception
                Llenalog(ex.Source.ToString & " " & ex.Message)
            Finally
                con.Close()
            End Try
            ''

            Class1.limpiaParametros()
            Class1.CreateMyParameter("@FECHA", ParameterDirection.Output, SqlDbType.DateTime)
            Class1.ProcedimientoOutPut("DAMEFECHADELSERVIDORFACTURADIGITAL", locconexion)
            mfecha = DateTime.Parse(Class1.dicoPar("@FECHA").ToString())


            Dim CONEXION As New SqlConnection(locconexion)
            Dim COMANDO As New SqlCommand("DameFacDig_Parte_1Maestro", CONEXION)
            COMANDO.CommandType = CommandType.StoredProcedure
            CoMando.CommandTimeout = 0

            Dim parametro As New SqlParameter("@Clv_SessionMaestro", SqlDbType.BigInt)
            parametro.Direction = ParameterDirection.Input
            parametro.Value = oClv_Session
            COMANDO.Parameters.Add(parametro)

            Dim parametro1 As New SqlParameter("@TotalConPuntos", SqlDbType.Money)
            parametro1.Direction = ParameterDirection.Output
            parametro1.Value = 0
            COMANDO.Parameters.Add(parametro1)

            Dim parametro2 As New SqlParameter("@SubTotal", SqlDbType.Money)
            parametro2.Direction = ParameterDirection.Output
            parametro2.Value = 0
            COMANDO.Parameters.Add(parametro2)

            Dim parametro3 As New SqlParameter("@TotalSinPuntos", SqlDbType.Money)
            parametro3.Direction = ParameterDirection.Output
            parametro3.Value = 0
            COMANDO.Parameters.Add(parametro3)

            Dim parametro4 As New SqlParameter("@Descuento", SqlDbType.Money)
            parametro4.Direction = ParameterDirection.Output
            parametro4.Value = 0
            COMANDO.Parameters.Add(parametro4)

            Dim parametro5 As New SqlParameter("@iva", SqlDbType.Money)
            parametro5.Direction = ParameterDirection.Output
            parametro5.Value = 0
            COMANDO.Parameters.Add(parametro5)

            Dim parametro6 As New SqlParameter("@ieps", SqlDbType.Money)
            parametro6.Direction = ParameterDirection.Output
            parametro6.Value = 0
            COMANDO.Parameters.Add(parametro6)

            Dim parametro7 As New SqlParameter("@TasaIva", SqlDbType.Money)
            parametro7.Direction = ParameterDirection.Output
            parametro7.Value = 0
            COMANDO.Parameters.Add(parametro7)

            Dim parametro8 As New SqlParameter("@TasaIeps", SqlDbType.Money)
            parametro8.Direction = ParameterDirection.Output
            parametro8.Value = 0
            COMANDO.Parameters.Add(parametro8)

            Dim parametro9 As New SqlParameter("@Fecha", SqlDbType.DateTime)
            parametro9.Direction = ParameterDirection.Output
            parametro9.Value = 0
            COMANDO.Parameters.Add(parametro9)

            Dim parametro10 As New SqlParameter("@TotalImpuestos", SqlDbType.Money)
            parametro10.Direction = ParameterDirection.Output
            parametro10.Value = 0
            COMANDO.Parameters.Add(parametro10)

            Try
                CONEXION.Open()
                Dim i As Integer = COMANDO.ExecuteNonQuery
                oTotalConPuntos = CStr(parametro1.Value)
                oSubTotal = CStr(parametro2.Value)
                oTotalSinPuntos = CStr(parametro3.Value)
                oDescuento = CStr(parametro4.Value)
                oiva = CStr(parametro5.Value)
                oieps = CStr(parametro6.Value)
                oTasaIva = "0.1600" 'CStr(parametro7.Value)
                oTasaIeps = "0.0300" 'CStr(parametro8.Value)
                oFecha = CStr(parametro9.Value)
                oTotalImpuestos = CStr(parametro10.Value)
                'Cnx.DbConnection.Close()
            Catch ex As Exception
                Llenalog(ex.Source.ToString & " " & ex.Message)
            Finally
                CONEXION.Close()
                CONEXION.Dispose()
            End Try

            Usp_Ed_DameDatosFacDigMaestro(oClv_Session, locID_Compania_Mizart, locconexion)

            Dim oCFD As MizarCFD.BRL.CFD

            Dim oId_AsociadoLlave As Long = 0
            oId_AsociadoLlave = oIden 'Existe_DevuelveValor(" Select id_asociado from asociados  where id_asociado = " + CStr(oIden))


            Using Cnx As New DAConexion("HL", "sa", "sa")
                Try
                    'oIden = "1"

                    oCFD = New MizarCFD.BRL.CFD
                    oCFD.Inicializar()
                    oCFD.TipoCFD = MizarCFD.BRL.CFD.TiposCFD.Emision
                    ''Ojo 
                    'locID_Sucursal_Mizart = 0
                    'oCFD.PrepararDatosEmisorPorId(Cnx, locID_Compania_Mizart, locID_Sucursal_Mizart)
                    If locID_Sucursal_Mizart.Length > 0 And locID_Sucursal_Mizart <> "0" Then
                        oCFD.PrepararDatosEmisorPorId(Cnx, locID_Compania_Mizart, locID_Sucursal_Mizart)
                    Else
                        oCFD.PrepararDatosEmisorPorId(Cnx, locID_Compania_Mizart)
                    End If
                    oCFD.PrepararDatosReceptorPorId(Cnx, CStr(oId_AsociadoLlave), 1, 0)


                    'oCFD.EsTimbrePrueba = EsTimbrePrueba
                    'oCFD.Timbrador = CFD.PAC.Mizar

                    'oCFD.Version = GloVERSON
                    'oCFD.IdMoneda = GloMONEDA  '//1 = Pesos , Pesos = 2 USD
                    ''oFecha
                    ''oCFD.Fecha = mfecha '//Date.Now
                    'oCFD.FormaDePago = GloFORMADEPAGO
                    ''oCFD.Certificado = ""
                    'oCFD.CondicionesDePago = GloCONDICIONESDEPAGO
                    'oCFD.SubTotal = oSubTotal
                    'oCFD.Descuento = oDescuento
                    'oCFD.MotivoDescuento = GloMOTIVODESCUENTO
                    'oCFD.Total = oTotalSinPuntos
                    'oCFD.MetodoDePago = GloMETODODEPAGO
                    'oCFD.TipoDeComprobante = GloTIPODECOMPROBANTE
                    'oCFD.PagoEnParcialiades = GloPAGOENPARCIALIDADES
                    'oCFD.LugarExpedicion = GloPAGOENPARCIALIDADES
                    'oCFD.CuentaPago = GlonumCtaPago

                    'Dim oRegimen As New MizarCFD.BRL.CFDRegimenesFiscales
                    'oRegimen.IdRegimen = 0
                    'oRegimen.IdCFD = 0
                    'oRegimen.Regimen = GloREGIMEN
                    'oRegimen.Insertar(Cnx)


                    oCFD.EsTimbrePrueba = EsTimbrePrueba

                    oCFD.Timbrador = CFD.PAC.Mizar

                    oCFD.Version = GloVERSON
                    oCFD.IdMoneda = 1  '//1 = Pesos , Pesos = 2 USD
                    'oFecha
                    'oCFD.Fecha = mfecha
                    oCFD.FormaDePago = GloFORMADEPAGO
                    'oCFD.Certificado = ""
                    oCFD.CondicionesDePago = GloCONDICIONESDEPAGO
                    oCFD.SubTotal = oSubTotal
                    oCFD.Descuento = oDescuento
                    oCFD.MotivoDescuento = GloMOTIVODESCUENTO
                    oCFD.Total = oTotalSinPuntos
                    oCFD.MetodoDePago = GloMETODODEPAGO
                    oCFD.TipoDeComprobante = GloTIPODECOMPROBANTE
                    oCFD.TipoComprobante33 = GloTIPODECOMPROBANTE
                    oCFD.PagoEnParcialiades = GloPAGOENPARCIALIDADES
                    oCFD.LugarExpedicion = GloPAGOENPARCIALIDADES
                    oCFD.CuentaPago = GlonumCtaPago
                    oCFD.Moneda = GloMONEDA

                    oCFD.UsoCFDI = GloUSOCFD
                    oCFD.EsNotaCredito = 0
                    oCFD.TipoRelacion = Nothing
                    oCFD.UUIDRelacionados = Nothing
                    'oCFD.FechaVencimiento = ""
                    'oCFD.CondicionesDePago = ""
                    oCFD.TipoCambio = GloTIPOCAMBIO



                    Dim oRegimen As New MizarCFD.BRL.CFDRegimenesFiscales
                    oRegimen.IdRegimen = 0
                    oRegimen.IdCFD = 0
                    oRegimen.Regimen = GloREGIMEN

                    oCFD.RegimenesFiscales.Add(oRegimen)

                    'oCFD.Complemento.XMLComplemento = "<raiz>Caelli Enarrant Gloria Dei </raiz>"
                    'oCFD.Addenda.XMLAddenda = "<edi>Edit dididididididi</edi>"

                    oCFD.Impuestos.TotalImpuestosRetenidos = 0
                    oCFD.Impuestos.TotalImpuestosTrasladados = oTotalImpuestos
                    'Dim oimpuestosRetenciones As CFDImpuestosRetenciones
                    'oimpuestosRetenciones = New CFDImpuestosRetenciones()
                    'oimpuestosRetenciones.Impuesto = "IVA"
                    'oimpuestosRetenciones.Importe = oiva
                    'oCFD.Impuestos.Retenciones.Add(oimpuestosRetenciones)

                    'Dim oImpuestosTraslados As CFDImpuestosTraslados

                    'oImpuestosTraslados = New CFDImpuestosTraslados()
                    'oImpuestosTraslados.Impuesto = "IVA"
                    'oImpuestosTraslados.Importe = oiva
                    'oImpuestosTraslados.Tasa = oTasaIva

                    'oCFD.Impuestos.Traslados.Add(oImpuestosTraslados)

                    'oImpuestosTraslados = New CFDImpuestosTraslados()
                    'oImpuestosTraslados.Impuesto = "IEPS"
                    'oImpuestosTraslados.Importe = oieps
                    'oImpuestosTraslados.Tasa = oTasaIeps

                    'oCFD.Impuestos.Traslados.Add(oImpuestosTraslados)
                    Dim oImpuestosTraslados As CFDImpuestosTraslados
                    oImpuestosTraslados = New CFDImpuestosTraslados()

                    Dim oConcepto As CFDConceptos
                    Dim oContrador As Integer = 1

                    Dim CONEXION2 As New SqlConnection(locconexion)
                    Dim COMANDO2 As New SqlCommand("exec DameFacDig_Parte_2Maestro " + CStr(oClv_Session), CONEXION2)
                    COMANDO2.CommandType = CommandType.Text
                    Try
                        CONEXION2.Open()
                        Dim reader As SqlDataReader = COMANDO2.ExecuteReader
                        '    'Recorremos los Cargos obtenidos desde el Procedimiento
                        While (reader.Read())
                            'oConcepto = New CFDConceptos()
                            'oConcepto.Cantidad = reader(0).ToString
                            'oConcepto.UnidadMedida = "No Aplica"
                            'oConcepto.NumeroIdentificacion = reader(1).ToString
                            'oConcepto.Descripcion = reader(2).ToString
                            'oConcepto.ValorUnitario = reader(3).ToString
                            'oConcepto.Importe = reader(4).ToString
                            'oConcepto.TrasladarIVA = "1"
                            'oCFD.Conceptos.Add(oConcepto)

                            oConcepto = New CFDConceptos()
                            oConcepto.Cantidad = reader(0).ToString
                            'oConcepto.UnidadMedida = "No Aplica"
                            oConcepto.NumeroIdentificacion = reader(1).ToString
                            oConcepto.Descripcion = reader(2).ToString
                            oConcepto.ValorUnitario = reader(3).ToString
                            oConcepto.Importe = reader(4).ToString
                            oConcepto.TrasladarIVA = "1"

                            'Los nuevos
                            oConcepto.Descuento = 0 'Valor Nuevo
                            ' oConcepto.ClaveProdServ = reader(5).ToString ' "83111801" 'Campo nuevo
                            oConcepto.idClaveProdServ = reader(5).ToString
                            'Iva
                            'oConcepto.IVATRAS_TipoFactor = reader(6).ToString
                            oConcepto.TipoFactor = reader(6).ToString
                            oConcepto.IVATRAS_Importe = reader(8).ToString 'Iva del ticket
                            'oConcepto.IVATRAS_TasaOCuota = reader(7).ToString '"0.160000"


                            oConcepto.TasaIVA = reader(7).ToString
                            oConcepto.TrasladarIVA = "1"
                            'Ieps
                            'oConcepto.IEPSTRAS_TipoFactor = reader(9).ToString '"Tasa"
                            'oConcepto.IEPSTRAS_TasaOCuota = reader(10).ToString '"0.030000"
                            If Double.Parse(reader(11).ToString) > 0 Then
                                oConcepto.BaseIEPS = reader(4).ToString 'Cantida Subtotal 

                                oConcepto.BaseIVA = CDec(reader(4).ToString) + CDec(reader(11).ToString)

                                '+ CDec(reader(11).ToString)

                                'Pemex Descomentado
                                'PozaRica Descomentado
                                'Charcas Descomentado
                                'teziutlan Descomentado
                                'Colotlan Descomentado
                                'Comalcalco Descomentado

                                'Bacalar Comentado
                                'Playa Comentado
                                'Mty solo Iva Comentado

                                oConcepto.IEPSTRAS_Importe = reader(11).ToString 'Total del ieps 'Ieps del ticket
                                oConcepto.TasaIEPS = reader(10).ToString

                                oConcepto.FactorIEPS = reader(9).ToString
                                oConcepto.TrasladarIEPS = "1"
                            Else
                                oConcepto.BaseIEPS = 0 'Cantida Subtotal 
                                oConcepto.BaseIVA = reader(4).ToString
                                oConcepto.IEPSTRAS_Importe = "0" 'Total del ieps 'Ieps del ticket
                                oConcepto.TrasladarIEPS = "0"
                            End If
                            oConcepto.idUnidadMedida = reader(13).ToString '"E48" "Unidad de servicio" '"No Aplica" 'Cambio  Id que corresponde
                            oConcepto.UnidadMedida = reader(12).ToString '"Unidad de servicio" ' Pruebo poner una al gusto

                            oConcepto.IVARET_Importe = "0"
                            oConcepto.ISRRET_Importe = "0"

                            'Trasladados
                            If Double.Parse(reader(8).ToString) > 0 Then
                                oImpuestosTraslados = New CFDImpuestosTraslados()
                                oImpuestosTraslados.IdRegistroTraslado = oContrador
                                oImpuestosTraslados.Impuesto = "IVA"
                                oImpuestosTraslados.Importe = reader(8).ToString
                                oImpuestosTraslados.Tasa = reader(7).ToString 'oTasaIva
                                'oImpuestosTraslados.TipoFactor = GloTIPOFACTOR
                                oImpuestosTraslados.TasaOCuota = GloTIPOFACTOR
                                oImpuestosTraslados.IEPSTRAS_Importe = "0"
                                oImpuestosTraslados.IVATRAS_Importe = "0"

                                oCFD.Impuestos.Traslados.Add(oImpuestosTraslados)
                            End If
                            If Double.Parse(reader(11).ToString) > 0 Then
                                oImpuestosTraslados = New CFDImpuestosTraslados()
                                oImpuestosTraslados.IdRegistroTraslado = oContrador + 1
                                oImpuestosTraslados.Impuesto = "IEPS" '"003" '"IEPS"
                                oImpuestosTraslados.Importe = reader(11).ToString
                                oImpuestosTraslados.Tasa = reader(10).ToString 'oTasaIeps
                                oImpuestosTraslados.TasaOCuota = GloTIPOFACTOR
                                oImpuestosTraslados.FactorIepsTras = GloTIPOFACTOR
                                oImpuestosTraslados.IEPSTRAS_Importe = "0"
                                oImpuestosTraslados.IVATRAS_Importe = "0"
                                'oImpuestosTraslados.TipoFactor = GloTIPOFACTOR

                                oCFD.Impuestos.Traslados.Add(oImpuestosTraslados)
                            End If


                            oConcepto.RetenerIVA = "0"
                            oConcepto.RetenerISR = "0"

                            oCFD.Conceptos.Add(oConcepto)
                            oContrador = oContrador + 2

                        End While
                        'Cnx.DbConnection.Close()
                    Catch ex As Exception
                        Llenalog(ex.Source.ToString & " " & ex.Message)
                    Finally
                        CONEXION2.Close()
                        CONEXION2.Dispose()
                    End Try
                    Dim bnd As Boolean = False
                    Dim Mensaje1 As String = ""
                    If Not oCFD.Insertar(Cnx, True, False) Then
                        For Each oMensaje As DAMensajesSistema.RegistroMensaje In oCFD.MensajesSistema.ListaMensajes
                            Mensaje1 = oMensaje.TextoMensaje + " " + oMensaje.Contexto
                            Inserta_Tbl_FacturasNoGeneradas(oClv_Session, "M", Mensaje1, locconexion)
                            'MessageBox.Show(oMensaje.TextoMensaje + " " + oMensaje.Contexto)
                            Bnd = True
                        Next
                        Return
                    End If

                    'If Not oCFD.Insertar(Cnx, True) Then
                    '    For Each oMensaje As DAMensajesSistema.RegistroMensaje In oCFD.MensajesSistema.ListaMensajes
                    '        MessageBox.Show(oMensaje.TextoMensaje + " " + oMensaje.Contexto)
                    '    Next
                    '    Return
                    'End If



                    GloClv_FacturaCFD = 0
                    GloClv_FacturaCFD = oCFD.IdCFD
                    UPS_Inserta_Rel_FacturasCFDMaestro(oClv_Session, GloClv_FacturaCFD, "", "N", locconexion)
                    GloTxtUlt4DigCheque = ""

                    Try
                        ' oCFD.EnviarEmail(Cnx)
                        'ENVIAR_Factura(GloClv_FacturaCFD)
                        If GloClv_FacturaCFD > 0 And bnd = False Then
                            ENVIAR_Factura(GloClv_FacturaCFD)
                        End If
                    Catch ex As Exception

                    End Try
                Catch ex As Exception
                    Llenalog(ex.Source.ToString & " " & ex.Message)

                Finally
                    CONEXION.Close()
                    CONEXION.Dispose()
                End Try

            End Using
        Else
            GloClv_FacturaCFD = ClvFacturaCfd
        End If
    End Sub


    Public Shared Function BusFacFiscalOledbMaestro(ByVal oClv_Session As Long, ByVal LocConexion As String) As Integer
        BusFacFiscalOledbMaestro = 0
        Dim CONEXION As New SqlConnection(LocConexion)
        Dim COMANDO As New SqlCommand("BusFacFiscalMaestro", CONEXION)
        COMANDO.CommandType = CommandType.StoredProcedure
        COMANDO.CommandTimeout = 0

        Dim parametro As New SqlParameter("@Clv_SessionMaestro", SqlDbType.BigInt)
        parametro.Direction = ParameterDirection.Input
        parametro.Value = oClv_Session
        COMANDO.Parameters.Add(parametro)

        Dim parametro1 As New SqlParameter("@ident", SqlDbType.Int)
        parametro1.Direction = ParameterDirection.Output
        parametro1.Value = 0
        COMANDO.Parameters.Add(parametro1)

        Try
            CONEXION.Open()
            Dim i As Integer = COMANDO.ExecuteNonQuery
            BusFacFiscalOledbMaestro = CStr(parametro1.Value)

            'Cnx.DbConnection.Close()
        Catch ex As Exception
            'MsgBox(ex.Message, MsgBoxStyle.Exclamation)
            If Graba = "0" Then
                Llenalog(ex.Source.ToString & " " & ex.Message)
            Else

                Llenalog(ex.Source.ToString & " " & ex.Message)
            End If
        Finally
            CONEXION.Close()
            CONEXION.Dispose()
        End Try
    End Function




    Public Shared Sub Alta_Datosfiscales(ByVal oRazonSocial As String,
        ByVal oRFC As String,
        ByVal oCalle As String,
        ByVal oClaveAsociado As String,
        ByVal oCodigoPostal As String,
        ByVal oColonia As String,
        ByVal oEMail As String,
        ByVal oEntreCalles As String,
        ByVal oEstado As String,
        ByVal oIdAsociado As String,
        ByVal oLocalidad As String,
        ByVal oMunicipio As String,
        ByVal oNumeroExterior As String,
            ByVal oPais As String)



        If String.IsNullOrEmpty(oRazonSocial) = True Then oRazonSocial = ""
        If String.IsNullOrEmpty(oRFC) = True Then oRFC = ""
        If String.IsNullOrEmpty(oCalle) = True Then oCalle = ""
        If String.IsNullOrEmpty(oClaveAsociado) = True Then oClaveAsociado = ""
        If String.IsNullOrEmpty(oCodigoPostal) = True Then oCodigoPostal = ""
        If String.IsNullOrEmpty(oColonia) = True Then oColonia = ""
        If String.IsNullOrEmpty(oEMail) = True Then oEMail = ""
        If String.IsNullOrEmpty(oEntreCalles) = True Then oEntreCalles = ""
        If String.IsNullOrEmpty(oEstado) = True Then oEstado = ""
        If String.IsNullOrEmpty(oIdAsociado) = True Then oIdAsociado = ""
        If String.IsNullOrEmpty(oLocalidad) = True Then oLocalidad = ""
        If String.IsNullOrEmpty(oMunicipio) = True Then oMunicipio = ""
        If String.IsNullOrEmpty(oNumeroExterior) = True Then oNumeroExterior = ""
        If String.IsNullOrEmpty(oPais) = True Then oPais = ""

        If Existe_Checalo(" Select * from asociados  where id_asociado = " + oIdAsociado) = False Then

            Dim oAsociados As MizarCFD.BRL.Asociados
            Using Cnx As New DAConexion("HL", "sa", "sa")

                oAsociados = New MizarCFD.BRL.Asociados
                oAsociados.Inicializar()
                oAsociados.RazonSocial = oRazonSocial
                oAsociados.RFC = oRFC
                'oAsociados.Calle = oCalle
                oAsociados.ClaveAsociado = oIdAsociado
                'oAsociados.CodigoPostal = oCodigoPostal
                'oAsociados.Colonia = oColonia
                'oAsociados.EMail = oEMail
                'oAsociados.EntreCalles = oEntreCalles
                'oAsociados.Estado = oEstado
                oAsociados.IdAsociado = oIdAsociado
                'oAsociados.Localidad = oLocalidad
                'oAsociados.Municipio = oMunicipio
                'oAsociados.NumeroExterior = oNumeroExterior
                'oAsociados.Pais = oPais
                'oAsociados.IdTipoAsociado = Asociados.TipoAsociado.Cliente
                'If oAsociados.Validar(Cnx, TipoAfectacionBD.Insertar) = True Then
                '    If Not oAsociados.Insertar(Cnx) Then
                '        For Each oMensaje As DAMensajesSistema.RegistroMensaje In oAsociados.MensajesSistema.ListaMensajes
                '            MessageBox.Show(oMensaje.TextoMensaje + " " + oMensaje.Contexto)
                '        Next
                '        Return
                '    End If
                'Else
                '    For Each oMensaje As DAMensajesSistema.RegistroMensaje In oAsociados.MensajesSistema.ListaMensajes
                '        MessageBox.Show(oMensaje.TextoMensaje + " " + oMensaje.Contexto)
                '    Next
                '    Return
                'End If





            End Using
        Else

        End If

    End Sub
    Public Shared Sub Graba_Factura_Digital_GlobalPorDirrecion(ByVal oClv_Factura As Long, ByVal oId_Compania As String, ByVal oId_Sucursal As String, ByVal locconexion As String, LocIdDireccion As Integer)
        Try


            Dim ClvFacturaCfd As Long
            ClvFacturaCfd = SP_DIMESIEXISTEFACTURADIGITAL(oClv_Factura, "G", locconexion)
            If ClvFacturaCfd = 0 Then '0 Es si no Existe

                Dim oTotalConPuntos As String = Nothing
                Dim oSubTotal As String = Nothing
                Dim oTotalSinPuntos As String = Nothing
                Dim oDescuento As String = Nothing
                Dim oiva As String = Nothing
                Dim oieps As String = Nothing
                Dim oTasaIva As String = Nothing
                Dim oTasaIeps As String = Nothing
                Dim oFecha As String = Nothing
                Dim oTotalImpuestos As String = Nothing

                Dim mfecha As DateTime

                Class1.limpiaParametros()
                Class1.CreateMyParameter("@FECHA", ParameterDirection.Output, SqlDbType.DateTime)
                Class1.ProcedimientoOutPut("DAMEFECHADELSERVIDORFACTURADIGITAL", locconexion)
                mfecha = DateTime.Parse(Class1.dicoPar("@FECHA").ToString())

                Dim CONEXION As New SqlConnection(locconexion)
                Dim COMANDO As New SqlCommand("DameFacDig_Parte_1_Global", CONEXION)
                COMANDO.CommandType = CommandType.StoredProcedure
                COMANDO.CommandTimeout = 0

                Dim parametro As New SqlParameter("@Clv_Factura", SqlDbType.BigInt)
                parametro.Direction = ParameterDirection.Input
                parametro.Value = oClv_Factura
                COMANDO.Parameters.Add(parametro)

                Dim parametro1 As New SqlParameter("@TotalConPuntos", SqlDbType.Money)
                parametro1.Direction = ParameterDirection.Output
                parametro1.Value = 0
                COMANDO.Parameters.Add(parametro1)

                Dim parametro2 As New SqlParameter("@SubTotal", SqlDbType.Money)
                parametro2.Direction = ParameterDirection.Output
                parametro2.Value = 0
                COMANDO.Parameters.Add(parametro2)

                Dim parametro3 As New SqlParameter("@TotalSinPuntos", SqlDbType.Money)
                parametro3.Direction = ParameterDirection.Output
                parametro3.Value = 0
                COMANDO.Parameters.Add(parametro3)

                Dim parametro4 As New SqlParameter("@Descuento", SqlDbType.Money)
                parametro4.Direction = ParameterDirection.Output
                parametro4.Value = 0
                COMANDO.Parameters.Add(parametro4)

                Dim parametro5 As New SqlParameter("@iva", SqlDbType.Money)
                parametro5.Direction = ParameterDirection.Output
                parametro5.Value = 0
                COMANDO.Parameters.Add(parametro5)

                Dim parametro6 As New SqlParameter("@ieps", SqlDbType.Money)
                parametro6.Direction = ParameterDirection.Output
                parametro6.Value = 0
                COMANDO.Parameters.Add(parametro6)

                Dim parametro7 As New SqlParameter("@TasaIva", SqlDbType.Money)
                parametro7.Direction = ParameterDirection.Output
                parametro7.Value = 0
                COMANDO.Parameters.Add(parametro7)

                Dim parametro8 As New SqlParameter("@TasaIeps", SqlDbType.Money)
                parametro8.Direction = ParameterDirection.Output
                parametro8.Value = 0
                COMANDO.Parameters.Add(parametro8)

                Dim parametro9 As New SqlParameter("@Fecha", SqlDbType.DateTime)
                parametro9.Direction = ParameterDirection.Output
                parametro9.Value = 0
                COMANDO.Parameters.Add(parametro9)

                Dim parametro10 As New SqlParameter("@TotalImpuestos", SqlDbType.Money)
                parametro10.Direction = ParameterDirection.Output
                parametro10.Value = 0
                COMANDO.Parameters.Add(parametro10)

                Try
                    CONEXION.Open()
                    Dim i As Integer = COMANDO.ExecuteNonQuery
                    oTotalConPuntos = CStr(parametro1.Value)
                    oSubTotal = CStr(parametro2.Value)
                    oTotalSinPuntos = CStr(parametro3.Value)
                    oDescuento = CStr(parametro4.Value)
                    oiva = CStr(parametro5.Value)
                    oieps = CStr(parametro6.Value)
                    oTasaIva = CStr(parametro7.Value)
                    oTasaIeps = CStr(parametro8.Value)
                    oFecha = CStr(parametro9.Value)
                    oTotalImpuestos = CStr(parametro10.Value)
                    'Cnx.DbConnection.Close()
                Catch ex As Exception
                    'MsgBox(ex.Message & "Graba Factura Digital Global Parte 1 " & ex.Message, MsgBoxStyle.Exclamation)
                    If Graba = "0" Then
                        Llenalog(ex.Source.ToString & " " & ex.Message)
                    Else

                        Llenalog(ex.Source.ToString & " " & ex.Message)
                    End If
                Finally
                    CONEXION.Close()
                    CONEXION.Dispose()
                End Try

                Usp_Ed_DameDatosFacDigGlobal(oClv_Factura, oId_Compania, locconexion)

                Dim oId_AsociadoLlave As Long = 0
                oId_AsociadoLlave = Existe_DevuelveValor(" Select top 1 id_asociado from asociados where RFC ='XAXX010101000' order by id_asociado ")

                Dim oCFD As MizarCFD.BRL.CFD

                Using Cnx As New DAConexion("HL", "sa", "sa")
                    Try


                        oCFD = New MizarCFD.BRL.CFD
                        oCFD.Inicializar()
                        oCFD.TipoCFD = MizarCFD.BRL.CFD.TiposCFD.Emision
                        If oId_Sucursal = "0" Then oId_Sucursal = ""

                        'oCFD.PrepararDatosEmisorPorId(Cnx, locID_Compania_Mizart)
                        'oCFD.PrepararDatosReceptorPorId(Cnx, CStr(oIden), 1, 0)

                        'oCFD.PrepararDatosEmisorPorId(Cnx, oId_Compania, oId_Sucursal)
                        'Comente lo de compañia

                        If oId_Sucursal.Length > 0 And oId_Sucursal <> "0" Then
                            oCFD.PrepararDatosEmisorPorId(Cnx, oId_Compania, oId_Sucursal)
                        Else
                            oCFD.PrepararDatosEmisorPorId(Cnx, oId_Compania)
                        End If

                        oCFD.PrepararDatosReceptorPorId(Cnx, oId_AsociadoLlave, LocIdDireccion, 0)


                        oCFD.EsTimbrePrueba = EsTimbrePrueba

                        oCFD.Timbrador = CFD.PAC.Mizar

                        oCFD.Version = GloVERSON
                        oCFD.IdMoneda = 1  '//1 = Pesos , Pesos = 2 USD
                        'oFecha
                        'oCFD.Fecha = mfecha
                        oCFD.FormaDePago = GloFORMADEPAGO
                        'oCFD.Certificado = ""
                        oCFD.CondicionesDePago = GloCONDICIONESDEPAGO
                        oCFD.SubTotal = oSubTotal
                        oCFD.Descuento = oDescuento
                        oCFD.MotivoDescuento = GloMOTIVODESCUENTO
                        oCFD.Total = oTotalSinPuntos
                        oCFD.MetodoDePago = GloMETODODEPAGO
                        oCFD.TipoDeComprobante = GloTIPODECOMPROBANTE
                        oCFD.TipoComprobante33 = GloTIPODECOMPROBANTE

                        oCFD.UsoCFDI = GloUSOCFD
                        oCFD.EsNotaCredito = 0
                        oCFD.TipoRelacion = Nothing
                        oCFD.UUIDRelacionados = Nothing
                        'oCFD.FechaVencimiento = ""
                        'oCFD.CondicionesDePago = ""
                        oCFD.TipoCambio = GloTIPOCAMBIO
                        oCFD.Moneda = GloMONEDA
                        oCFD.Comentarios = GloOBSERVACION

                        oCFD.PagoEnParcialiades = GloPAGOENPARCIALIDADES
                        oCFD.LugarExpedicion = GloLUGAREXPEDICION
                        oCFD.CuentaPago = GlonumCtaPago


                        Dim oRegimen As New MizarCFD.BRL.CFDRegimenesFiscales
                        oRegimen.IdRegimen = 0
                        oRegimen.IdCFD = 0
                        oRegimen.Regimen = GloREGIMEN
                        'oRegimen.Insertar(Cnx)

                        oCFD.RegimenesFiscales.Add(oRegimen)

                        'oCFD.Complemento.XMLComplemento = "<raiz>Caelli Enarrant Gloria Dei </raiz>"
                        'oCFD.Addenda.XMLAddenda = "<edi>Edit dididididididi</edi>"

                        oCFD.Impuestos.TotalImpuestosRetenidos = 0
                        oCFD.Impuestos.TotalImpuestosTrasladados = oTotalImpuestos
                        'Dim oimpuestosRetenciones As CFDImpuestosRetenciones
                        'oimpuestosRetenciones = New CFDImpuestosRetenciones()
                        'oimpuestosRetenciones.Impuesto = "IVA"
                        'oimpuestosRetenciones.Importe = oiva
                        'oCFD.Impuestos.Retenciones.Add(oimpuestosRetenciones)

                        Dim oImpuestosTraslados As CFDImpuestosTraslados



                        'oImpuestosTraslados = New CFDImpuestosTraslados()
                        'oImpuestosTraslados.Impuesto = "IVA"
                        'oImpuestosTraslados.Importe = oiva
                        'oImpuestosTraslados.Tasa = oTasaIva

                        'oCFD.Impuestos.Traslados.Add(oImpuestosTraslados)

                        'oImpuestosTraslados = New CFDImpuestosTraslados()
                        'oImpuestosTraslados.Impuesto = "IEPS"
                        'oImpuestosTraslados.Importe = oieps
                        'oImpuestosTraslados.Tasa = oTasaIeps

                        'oCFD.Impuestos.Traslados.Add(oImpuestosTraslados)

                        Dim oConcepto As CFDConceptos
                        Dim oContrador As Integer = 1

                        Dim CONEXION2 As New SqlConnection(locconexion)
                        Dim COMANDO2 As New SqlCommand("exec DameFacDig_Parte_2_Global " + CStr(oClv_Factura), CONEXION2)
                        COMANDO2.CommandType = CommandType.Text
                        COMANDO2.CommandTimeout = 0
                        Try
                            CONEXION2.Open()
                            Dim reader As SqlDataReader = COMANDO2.ExecuteReader
                            '    'Recorremos los Cargos obtenidos desde el Procedimiento
                            While (reader.Read())
                                oConcepto = New CFDConceptos()
                                oConcepto.Cantidad = reader(0).ToString
                                'oConcepto.UnidadMedida = "No Aplica"
                                oConcepto.NumeroIdentificacion = reader(1).ToString
                                oConcepto.Descripcion = reader(2).ToString
                                oConcepto.ValorUnitario = reader(3).ToString
                                oConcepto.Importe = reader(4).ToString
                                'oConcepto.CausaIVA = "1"
                                oConcepto.TrasladarIVA = "1"

                                'Los nuevos
                                oConcepto.Descuento = 0 'Valor Nuevo
                                ' oConcepto.ClaveProdServ = reader(5).ToString ' "83111801" 'Campo nuevo
                                oConcepto.idClaveProdServ = reader(5).ToString
                                'Iva
                                'oConcepto.IVATRAS_TipoFactor = reader(6).ToString
                                oConcepto.TipoFactor = reader(6).ToString
                                oConcepto.IVATRAS_Importe = reader(8).ToString 'Iva del ticket
                                'oConcepto.IVATRAS_TasaOCuota = reader(7).ToString '"0.160000"


                                oConcepto.TasaIVA = reader(7).ToString
                                oConcepto.TrasladarIVA = "1"
                                'Ieps
                                'oConcepto.IEPSTRAS_TipoFactor = reader(9).ToString '"Tasa"
                                'oConcepto.IEPSTRAS_TasaOCuota = reader(10).ToString '"0.030000"
                                If Double.Parse(reader(11).ToString) > 0 Then
                                    oConcepto.BaseIEPS = reader(4).ToString 'Cantida Subtotal 
                                    'oConcepto.BaseIVA = CDec(reader(4).ToString) + CDec(reader(11).ToString)
                                    If SumaSubtotalYIeps = True Then
                                        oConcepto.BaseIVA = CDec(reader(4).ToString) + CDec(reader(11).ToString)
                                    Else
                                        oConcepto.BaseIVA = CDec(reader(4).ToString)
                                    End If

                                    oConcepto.IEPSTRAS_Importe = reader(11).ToString 'Total del ieps 'Ieps del ticket
                                    oConcepto.TasaIEPS = reader(10).ToString

                                    oConcepto.FactorIEPS = reader(9).ToString
                                    oConcepto.TrasladarIEPS = "1"
                                Else
                                    oConcepto.BaseIEPS = 0 'Cantida Subtotal 
                                    oConcepto.BaseIVA = reader(4).ToString
                                    oConcepto.IEPSTRAS_Importe = "0" 'Total del ieps 'Ieps del ticket
                                    oConcepto.TrasladarIEPS = "0"
                                End If
                                oConcepto.idUnidadMedida = reader(13).ToString '"E48" "Unidad de servicio" '"No Aplica" 'Cambio  Id que corresponde
                                oConcepto.UnidadMedida = reader(12).ToString '"Unidad de servicio" ' Pruebo poner una al gusto

                                oConcepto.IVARET_Importe = "0"
                                oConcepto.ISRRET_Importe = "0"

                                'Trasladados
                                If Double.Parse(reader(8).ToString) > 0 Then
                                    oImpuestosTraslados = New CFDImpuestosTraslados()
                                    oImpuestosTraslados.IdRegistroTraslado = oContrador
                                    oImpuestosTraslados.Impuesto = "IVA"
                                    oImpuestosTraslados.Importe = reader(8).ToString
                                    oImpuestosTraslados.Tasa = reader(7).ToString 'oTasaIva
                                    'oImpuestosTraslados.TipoFactor = GloTIPOFACTOR
                                    oImpuestosTraslados.TasaOCuota = GloTIPOFACTOR
                                    oImpuestosTraslados.IEPSTRAS_Importe = "0"
                                    oImpuestosTraslados.IVATRAS_Importe = "0"

                                    oCFD.Impuestos.Traslados.Add(oImpuestosTraslados)
                                End If
                                If Double.Parse(reader(11).ToString) > 0 Then
                                    oImpuestosTraslados = New CFDImpuestosTraslados()
                                    oImpuestosTraslados.IdRegistroTraslado = oContrador + 1
                                    oImpuestosTraslados.Impuesto = "IEPS" '"003" '"IEPS"
                                    oImpuestosTraslados.Importe = reader(11).ToString
                                    oImpuestosTraslados.Tasa = reader(10).ToString 'oTasaIeps
                                    oImpuestosTraslados.TasaOCuota = GloTIPOFACTOR
                                    oImpuestosTraslados.FactorIepsTras = GloTIPOFACTOR
                                    oImpuestosTraslados.IEPSTRAS_Importe = "0"
                                    oImpuestosTraslados.IVATRAS_Importe = "0"
                                    'oImpuestosTraslados.TipoFactor = GloTIPOFACTOR

                                    oCFD.Impuestos.Traslados.Add(oImpuestosTraslados)
                                End If


                                oConcepto.RetenerIVA = "0"
                                oConcepto.RetenerISR = "0"

                                oCFD.Conceptos.Add(oConcepto)
                                oContrador = oContrador + 2
                            End While
                            'Cnx.DbConnection.Close()
                        Catch ex As Exception
                            'MsgBox(ex.Message & "Graba Factura Digital Global " & ex.Message, MsgBoxStyle.Exclamation)
                            If Graba = "0" Then
                                Llenalog(ex.Source.ToString & " " & ex.Message)
                            Else

                                Llenalog(ex.Source.ToString & " " & ex.Message)
                            End If
                        Finally
                            CONEXION2.Close()
                            CONEXION2.Dispose()
                        End Try

                        'Dim a As String = JsonConvert.SerializeObject(oCFD)
                        Dim Bnd As Boolean = False
                        Dim Mensaje1 As String = ""
                        If Not oCFD.Insertar(Cnx, True, False) Then
                            For Each oMensaje As DAMensajesSistema.RegistroMensaje In oCFD.MensajesSistema.ListaMensajes
                                Mensaje1 = oMensaje.TextoMensaje + " " + oMensaje.Contexto
                                Inserta_Tbl_FacturasNoGeneradas(oClv_Factura, "G", Mensaje1, locconexion)
                                'MessageBox.Show(oMensaje.TextoMensaje + " " + oMensaje.Contexto)
                                Bnd = True
                            Next
                            Return
                        End If

                        GloClv_FacturaCFD = 0
                        GloClv_FacturaCFD = oCFD.IdCFD
                        UPS_Inserta_Rel_FacturasCFD(oClv_Factura, GloClv_FacturaCFD, "", "G", locconexion)
                        Dim r As New Globalization.CultureInfo("es-MX")

                        r.DateTimeFormat.ShortDatePattern = "dd/MM/yyyy"

                        System.Threading.Thread.CurrentThread.CurrentCulture = r
                        Try
                            '    oCFD.EnviarEmail(Cnx)
                            'ENVIAR_Factura(GloClv_FacturaCFD)
                            If GloClv_FacturaCFD > 0 And bnd = False Then
                                ENVIAR_Factura(GloClv_FacturaCFD)
                            End If
                        Catch ex As Exception

                        End Try

                    Catch ex As Exception
                        'MsgBox(ex.Message & "Graba Factura Digital Global Parte 3 " & ex.Message, MsgBoxStyle.Exclamation)
                        If Graba = "0" Then
                            Llenalog(ex.Source.ToString & " " & ex.Message)
                        Else

                            Llenalog(ex.Source.ToString & " " & ex.Message)
                        End If
                    Finally
                        CONEXION.Close()
                        CONEXION.Dispose()
                    End Try

                End Using
            Else
                GloClv_FacturaCFD = ClvFacturaCfd
            End If
        Catch ex As Exception
            'MsgBox(ex.Message & "Graba Factura Digital Por Dirección Parte 3 " & ex.Message, MsgBoxStyle.Exclamation)
            If Graba = "0" Then
                Llenalog(ex.Source.ToString & " " & ex.Message)
            Else

                Llenalog(ex.Source.ToString & " " & ex.Message)
            End If
        End Try
    End Sub


    'Public Shared Sub Graba_Factura_Digital_GlobalPorDirrecion(ByVal oClv_Factura As Long, ByVal oId_Compania As String, ByVal oId_Sucursal As String, ByVal locconexion As String, LocIdDireccion As Integer)
    '    Try


    '        Dim ClvFacturaCfd As Long
    '        ClvFacturaCfd = SP_DIMESIEXISTEFACTURADIGITAL(oClv_Factura, "G", locconexion)
    '        If ClvFacturaCfd = 0 Then '0 Es si no Existe

    '            Dim oTotalConPuntos As String = Nothing
    '            Dim oSubTotal As String = Nothing
    '            Dim oTotalSinPuntos As String = Nothing
    '            Dim oDescuento As String = Nothing
    '            Dim oiva As String = Nothing
    '            Dim oieps As String = Nothing
    '            Dim oTasaIva As String = Nothing
    '            Dim oTasaIeps As String = Nothing
    '            Dim oFecha As String = Nothing
    '            Dim oTotalImpuestos As String = Nothing

    '            Dim mfecha As DateTime

    '            Class1.limpiaParametros()
    '            Class1.CreateMyParameter("@FECHA", ParameterDirection.Output, SqlDbType.DateTime)
    '            Class1.ProcedimientoOutPut("DAMEFECHADELSERVIDORFACTURADIGITAL", locconexion)
    '            mfecha = DateTime.Parse(Class1.dicoPar("@FECHA").ToString())

    '            Dim CONEXION As New SqlConnection(locconexion)
    '            Dim COMANDO As New SqlCommand("DameFacDig_Parte_1_Global", CONEXION)
    '            COMANDO.CommandType = CommandType.StoredProcedure

    '            Dim parametro As New SqlParameter("@Clv_Factura", SqlDbType.BigInt)
    '            parametro.Direction = ParameterDirection.Input
    '            parametro.Value = oClv_Factura
    '            COMANDO.Parameters.Add(parametro)

    '            Dim parametro1 As New SqlParameter("@TotalConPuntos", SqlDbType.Money)
    '            parametro1.Direction = ParameterDirection.Output
    '            parametro1.Value = 0
    '            COMANDO.Parameters.Add(parametro1)

    '            Dim parametro2 As New SqlParameter("@SubTotal", SqlDbType.Money)
    '            parametro2.Direction = ParameterDirection.Output
    '            parametro2.Value = 0
    '            COMANDO.Parameters.Add(parametro2)

    '            Dim parametro3 As New SqlParameter("@TotalSinPuntos", SqlDbType.Money)
    '            parametro3.Direction = ParameterDirection.Output
    '            parametro3.Value = 0
    '            COMANDO.Parameters.Add(parametro3)

    '            Dim parametro4 As New SqlParameter("@Descuento", SqlDbType.Money)
    '            parametro4.Direction = ParameterDirection.Output
    '            parametro4.Value = 0
    '            COMANDO.Parameters.Add(parametro4)

    '            Dim parametro5 As New SqlParameter("@iva", SqlDbType.Money)
    '            parametro5.Direction = ParameterDirection.Output
    '            parametro5.Value = 0
    '            COMANDO.Parameters.Add(parametro5)

    '            Dim parametro6 As New SqlParameter("@ieps", SqlDbType.Money)
    '            parametro6.Direction = ParameterDirection.Output
    '            parametro6.Value = 0
    '            COMANDO.Parameters.Add(parametro6)

    '            Dim parametro7 As New SqlParameter("@TasaIva", SqlDbType.Money)
    '            parametro7.Direction = ParameterDirection.Output
    '            parametro7.Value = 0
    '            COMANDO.Parameters.Add(parametro7)

    '            Dim parametro8 As New SqlParameter("@TasaIeps", SqlDbType.Money)
    '            parametro8.Direction = ParameterDirection.Output
    '            parametro8.Value = 0
    '            COMANDO.Parameters.Add(parametro8)

    '            Dim parametro9 As New SqlParameter("@Fecha", SqlDbType.DateTime)
    '            parametro9.Direction = ParameterDirection.Output
    '            parametro9.Value = 0
    '            COMANDO.Parameters.Add(parametro9)

    '            Dim parametro10 As New SqlParameter("@TotalImpuestos", SqlDbType.Money)
    '            parametro10.Direction = ParameterDirection.Output
    '            parametro10.Value = 0
    '            COMANDO.Parameters.Add(parametro10)

    '            Try
    '                CONEXION.Open()
    '                Dim i As Integer = COMANDO.ExecuteNonQuery
    '                oTotalConPuntos = CStr(parametro1.Value)
    '                oSubTotal = CStr(parametro2.Value)
    '                oTotalSinPuntos = CStr(parametro3.Value)
    '                oDescuento = CStr(parametro4.Value)
    '                oiva = CStr(parametro5.Value)
    '                oieps = CStr(parametro6.Value)
    '                oTasaIva = CStr(parametro7.Value)
    '                oTasaIeps = CStr(parametro8.Value)
    '                oFecha = CStr(parametro9.Value)
    '                oTotalImpuestos = CStr(parametro10.Value)
    '                'Cnx.DbConnection.Close()
    '            Catch ex As Exception
    '                'MsgBox(ex.Message & "Graba Factura Digital Por Direccion Parte 1 " & ex.Message, MsgBoxStyle.Exclamation)
    '                Llenalog(ex.Source.ToString & " " & ex.Message)
    '            Finally
    '                CONEXION.Close()
    '                CONEXION.Dispose()
    '            End Try

    '            Usp_Ed_DameDatosFacDigGlobal(oClv_Factura, oId_Compania, locconexion)

    '            Dim oId_AsociadoLlave As Long = 0
    '            oId_AsociadoLlave = Existe_DevuelveValor(" Select top 1 id_asociado from asociados where RFC ='XAXX010101000' order by id_asociado ")

    '            Dim oCFD As MizarCFD.BRL.CFD

    '            Using Cnx As New DAConexion("HL", "sa", "sa")
    '                Try


    '                    oCFD = New MizarCFD.BRL.CFD
    '                    oCFD.Inicializar()
    '                    oCFD.TipoCFD = MizarCFD.BRL.CFD.TiposCFD.Emision
    '                    If oId_Sucursal = "0" Then oId_Sucursal = ""

    '                    'oCFD.PrepararDatosEmisorPorId(Cnx, locID_Compania_Mizart)
    '                    'oCFD.PrepararDatosReceptorPorId(Cnx, CStr(oIden), 1, 0)

    '                    'oCFD.PrepararDatosEmisorPorId(Cnx, oId_Compania, oId_Sucursal)
    '                    'Comente lo de compañia

    '                    If oId_Sucursal.Length > 0 And oId_Sucursal <> "0" Then
    '                        oCFD.PrepararDatosEmisorPorId(Cnx, oId_Compania, oId_Sucursal)
    '                    Else
    '                        oCFD.PrepararDatosEmisorPorId(Cnx, oId_Compania)
    '                    End If

    '                    oCFD.PrepararDatosReceptorPorId(Cnx, oId_AsociadoLlave, LocIdDireccion, 0)


    '                    oCFD.EsTimbrePrueba = EsTimbrePrueba
    '                    oCFD.Timbrador = CFD.PAC.Mizar

    '                    oCFD.Version = GloVERSON
    '                    oCFD.IdMoneda = GloMONEDA  '//1 = Pesos , Pesos = 2 USD
    '                    'oFecha
    '                    'oCFD.Fecha = mfecha
    '                    oCFD.FormaDePago = GloFORMADEPAGO
    '                    'oCFD.Certificado = ""
    '                    oCFD.CondicionesDePago = GloCONDICIONESDEPAGO
    '                    oCFD.SubTotal = oSubTotal
    '                    oCFD.Descuento = oDescuento
    '                    oCFD.MotivoDescuento = GloMOTIVODESCUENTO
    '                    oCFD.Total = oTotalSinPuntos
    '                    oCFD.MetodoDePago = GloMETODODEPAGO
    '                    oCFD.TipoDeComprobante = GloTIPODECOMPROBANTE

    '                    oCFD.PagoEnParcialiades = GloPAGOENPARCIALIDADES
    '                    oCFD.LugarExpedicion = GloLUGAREXPEDICION
    '                    oCFD.CuentaPago = GlonumCtaPago


    '                    Dim oRegimen As New MizarCFD.BRL.CFDRegimenesFiscales
    '                    oRegimen.IdRegimen = 0
    '                    oRegimen.IdCFD = 0
    '                    oRegimen.Regimen = GloREGIMEN
    '                    'oRegimen.Insertar(Cnx)

    '                    oCFD.RegimenesFiscales.Add(oRegimen)

    '                    'oCFD.Complemento.XMLComplemento = "<raiz>Caelli Enarrant Gloria Dei </raiz>"
    '                    'oCFD.Addenda.XMLAddenda = "<edi>Edit dididididididi</edi>"

    '                    oCFD.Impuestos.TotalImpuestosRetenidos = 0
    '                    oCFD.Impuestos.TotalImpuestosTrasladados = oTotalImpuestos
    '                    'Dim oimpuestosRetenciones As CFDImpuestosRetenciones
    '                    'oimpuestosRetenciones = New CFDImpuestosRetenciones()
    '                    'oimpuestosRetenciones.Impuesto = "IVA"
    '                    'oimpuestosRetenciones.Importe = oiva
    '                    'oCFD.Impuestos.Retenciones.Add(oimpuestosRetenciones)

    '                    Dim oImpuestosTraslados As CFDImpuestosTraslados



    '                    oImpuestosTraslados = New CFDImpuestosTraslados()
    '                    oImpuestosTraslados.Impuesto = "IVA"
    '                    oImpuestosTraslados.Importe = oiva
    '                    oImpuestosTraslados.Tasa = oTasaIva

    '                    oCFD.Impuestos.Traslados.Add(oImpuestosTraslados)

    '                    oImpuestosTraslados = New CFDImpuestosTraslados()
    '                    oImpuestosTraslados.Impuesto = "IEPS"
    '                    oImpuestosTraslados.Importe = oieps
    '                    oImpuestosTraslados.Tasa = oTasaIeps

    '                    oCFD.Impuestos.Traslados.Add(oImpuestosTraslados)

    '                    Dim oConcepto As CFDConceptos

    '                    Dim CONEXION2 As New SqlConnection(locconexion)
    '                    Dim COMANDO2 As New SqlCommand("exec DameFacDig_Parte_2_Global " + CStr(oClv_Factura), CONEXION2)
    '                    COMANDO2.CommandType = CommandType.Text
    '                    Try
    '                        CONEXION2.Open()
    '                        Dim reader As SqlDataReader = COMANDO2.ExecuteReader
    '                        '    'Recorremos los Cargos obtenidos desde el Procedimiento
    '                        While (reader.Read())
    '                            oConcepto = New CFDConceptos()
    '                            oConcepto.Cantidad = reader(0).ToString
    '                            oConcepto.UnidadMedida = "No Aplica"
    '                            oConcepto.NumeroIdentificacion = reader(1).ToString
    '                            oConcepto.Descripcion = reader(2).ToString
    '                            oConcepto.ValorUnitario = reader(3).ToString
    '                            oConcepto.Importe = reader(4).ToString
    '                            'oConcepto.CausaIVA = "1"
    '                            oConcepto.TrasladarIVA = "1"
    '                            oCFD.Conceptos.Add(oConcepto)
    '                        End While
    '                        'Cnx.DbConnection.Close()
    '                    Catch ex As Exception
    '                        'MsgBox(ex.Message & "Graba Factura Digital Por Direccion Parte 2 " & ex.Message, MsgBoxStyle.Exclamation)
    '                        Llenalog(ex.Source.ToString & " " & ex.Message)
    '                    Finally
    '                        CONEXION2.Close()
    '                        CONEXION2.Dispose()
    '                    End Try
    '                    Dim Bnd As Boolean = False
    '                    Dim Mensaje1 As String = ""
    '                    If Not oCFD.Insertar(Cnx, True, False) Then
    '                        For Each oMensaje As DAMensajesSistema.RegistroMensaje In oCFD.MensajesSistema.ListaMensajes
    '                            Mensaje1 = oMensaje.TextoMensaje + " " + oMensaje.Contexto
    '                            Inserta_Tbl_FacturasNoGeneradas(oClv_Factura, "G", Mensaje1, locconexion)
    '                            'MessageBox.Show(oMensaje.TextoMensaje + " " + oMensaje.Contexto)
    '                            Bnd = True
    '                        Next
    '                        Return
    '                    End If

    '                    GloClv_FacturaCFD = 0
    '                    GloClv_FacturaCFD = oCFD.IdCFD
    '                    UPS_Inserta_Rel_FacturasCFD(oClv_Factura, GloClv_FacturaCFD, "", "G", locconexion)
    '                    Dim r As New Globalization.CultureInfo("es-MX")

    '                    r.DateTimeFormat.ShortDatePattern = "dd/MM/yyyy"

    '                    System.Threading.Thread.CurrentThread.CurrentCulture = r
    '                    Try
    '                        '    oCFD.EnviarEmail(Cnx)
    '                        'ENVIAR_Factura(GloClv_FacturaCFD)
    '                        If GloClv_FacturaCFD > 0 And bnd = False Then
    '                            ENVIAR_Factura(GloClv_FacturaCFD, locconexion)
    '                        End If
    '                    Catch ex As Exception

    '                    End Try

    '                Catch ex As Exception
    '                    'MsgBox(ex.Message & "Graba Factura Digital Por Dirección Parte 3 " & ex.Message, MsgBoxStyle.Exclamation)
    '                    Llenalog(ex.Source.ToString & " " & ex.Message)
    '                Finally
    '                    CONEXION.Close()
    '                    CONEXION.Dispose()
    '                End Try

    '            End Using
    '        Else
    '            GloClv_FacturaCFD = ClvFacturaCfd
    '        End If
    '    Catch ex As Exception
    '        'MsgBox(ex.Message & "Graba Factura Digital Por Dirección Parte 3 " & ex.Message, MsgBoxStyle.Exclamation)
    '        Llenalog(ex.Source.ToString & " " & ex.Message)
    '    End Try
    'End Sub
    Public Shared Sub Graba_Factura_Digital_GlobalRefacturacion(ByVal oClv_Factura As Long, ByVal oId_Compania As String, ByVal oId_Sucursal As String, ByVal locconexion As String)
        Try


            Dim ClvFacturaCfd As Long = 0
            'ClvFacturaCfd = SP_DIMESIEXISTEFACTURADIGITAL(oClv_Factura, "G", locconexion)
            If ClvFacturaCfd = 0 Then '0 Es si no Existe

                Dim oTotalConPuntos As String = Nothing
                Dim oSubTotal As String = Nothing
                Dim oTotalSinPuntos As String = Nothing
                Dim oDescuento As String = Nothing
                Dim oiva As String = Nothing
                Dim oieps As String = Nothing
                Dim oTasaIva As String = Nothing
                Dim oTasaIeps As String = Nothing
                Dim oFecha As String = Nothing
                Dim oTotalImpuestos As String = Nothing

                Dim mfecha As DateTime

                Class1.limpiaParametros()
                Class1.CreateMyParameter("@FECHA", ParameterDirection.Output, SqlDbType.DateTime)
                Class1.ProcedimientoOutPut("DAMEFECHADELSERVIDORFACTURADIGITAL", locconexion)
                mfecha = DateTime.Parse(Class1.dicoPar("@FECHA").ToString())

                Dim CONEXION As New SqlConnection(locconexion)
                Dim COMANDO As New SqlCommand("DameFacDig_Parte_1_Global", CONEXION)
                COMANDO.CommandType = CommandType.StoredProcedure

                Dim parametro As New SqlParameter("@Clv_Factura", SqlDbType.BigInt)
                parametro.Direction = ParameterDirection.Input
                parametro.Value = oClv_Factura
                COMANDO.Parameters.Add(parametro)

                Dim parametro1 As New SqlParameter("@TotalConPuntos", SqlDbType.Money)
                parametro1.Direction = ParameterDirection.Output
                parametro1.Value = 0
                COMANDO.Parameters.Add(parametro1)

                Dim parametro2 As New SqlParameter("@SubTotal", SqlDbType.Money)
                parametro2.Direction = ParameterDirection.Output
                parametro2.Value = 0
                COMANDO.Parameters.Add(parametro2)

                Dim parametro3 As New SqlParameter("@TotalSinPuntos", SqlDbType.Money)
                parametro3.Direction = ParameterDirection.Output
                parametro3.Value = 0
                COMANDO.Parameters.Add(parametro3)

                Dim parametro4 As New SqlParameter("@Descuento", SqlDbType.Money)
                parametro4.Direction = ParameterDirection.Output
                parametro4.Value = 0
                COMANDO.Parameters.Add(parametro4)

                Dim parametro5 As New SqlParameter("@iva", SqlDbType.Money)
                parametro5.Direction = ParameterDirection.Output
                parametro5.Value = 0
                COMANDO.Parameters.Add(parametro5)

                Dim parametro6 As New SqlParameter("@ieps", SqlDbType.Money)
                parametro6.Direction = ParameterDirection.Output
                parametro6.Value = 0
                COMANDO.Parameters.Add(parametro6)

                Dim parametro7 As New SqlParameter("@TasaIva", SqlDbType.Money)
                parametro7.Direction = ParameterDirection.Output
                parametro7.Value = 0
                COMANDO.Parameters.Add(parametro7)

                Dim parametro8 As New SqlParameter("@TasaIeps", SqlDbType.Money)
                parametro8.Direction = ParameterDirection.Output
                parametro8.Value = 0
                COMANDO.Parameters.Add(parametro8)

                Dim parametro9 As New SqlParameter("@Fecha", SqlDbType.DateTime)
                parametro9.Direction = ParameterDirection.Output
                parametro9.Value = 0
                COMANDO.Parameters.Add(parametro9)

                Dim parametro10 As New SqlParameter("@TotalImpuestos", SqlDbType.Money)
                parametro10.Direction = ParameterDirection.Output
                parametro10.Value = 0
                COMANDO.Parameters.Add(parametro10)

                Try
                    CONEXION.Open()
                    Dim i As Integer = COMANDO.ExecuteNonQuery
                    oTotalConPuntos = CStr(parametro1.Value)
                    oSubTotal = CStr(parametro2.Value)
                    oTotalSinPuntos = CStr(parametro3.Value)
                    oDescuento = CStr(parametro4.Value)
                    oiva = CStr(parametro5.Value)
                    oieps = CStr(parametro6.Value)
                    oTasaIva = CStr(parametro7.Value)
                    oTasaIeps = CStr(parametro8.Value)
                    oFecha = CStr(parametro9.Value)
                    oTotalImpuestos = CStr(parametro10.Value)
                    'Cnx.DbConnection.Close()
                Catch ex As Exception
                    'MsgBox(ex.Message & "Graba Factura Digital Global Parte 1 " & ex.Message, MsgBoxStyle.Exclamation)
                    If Graba = "0" Then
                        Llenalog(ex.Source.ToString & " " & ex.Message)
                    Else

                        Llenalog(ex.Source.ToString & " " & ex.Message)
                    End If
                Finally
                    CONEXION.Close()
                    CONEXION.Dispose()
                End Try

                Usp_Ed_DameDatosFacDigGlobalReFacturacion(oClv_Factura, oId_Compania, locconexion)

                Dim oId_AsociadoLlave As Long = 0
                oId_AsociadoLlave = Existe_DevuelveValor(" Select top 1 id_asociado from asociados where RFC ='XAXX010101000' order by id_asociado ")

                Dim oCFD As MizarCFD.BRL.CFD

                Using Cnx As New DAConexion("HL", "sa", "sa")
                    Try


                        oCFD = New MizarCFD.BRL.CFD
                        oCFD.Inicializar()
                        oCFD.TipoCFD = MizarCFD.BRL.CFD.TiposCFD.Emision
                        If oId_Sucursal = "0" Then oId_Sucursal = ""

                        'oCFD.PrepararDatosEmisorPorId(Cnx, locID_Compania_Mizart)
                        'oCFD.PrepararDatosReceptorPorId(Cnx, CStr(oIden), 1, 0)

                        'oCFD.PrepararDatosEmisorPorId(Cnx, oId_Compania, oId_Sucursal)
                        'Comente lo de compañia

                        If oId_Sucursal.Length > 0 And oId_Sucursal <> "0" Then
                            oCFD.PrepararDatosEmisorPorId(Cnx, oId_Compania, oId_Sucursal)
                        Else
                            oCFD.PrepararDatosEmisorPorId(Cnx, oId_Compania)
                        End If

                        oCFD.PrepararDatosReceptorPorId(Cnx, oId_AsociadoLlave, 1, 0)


                        oCFD.EsTimbrePrueba = EsTimbrePrueba

                        oCFD.Timbrador = CFD.PAC.Mizar

                        oCFD.Version = GloVERSON
                        oCFD.IdMoneda = 1  '//1 = Pesos , Pesos = 2 USD
                        'oFecha
                        'oCFD.Fecha = mfecha
                        oCFD.FormaDePago = GloFORMADEPAGO
                        'oCFD.Certificado = ""
                        oCFD.CondicionesDePago = GloCONDICIONESDEPAGO
                        oCFD.SubTotal = oSubTotal
                        oCFD.Descuento = oDescuento
                        oCFD.MotivoDescuento = GloMOTIVODESCUENTO
                        oCFD.Total = oTotalSinPuntos
                        oCFD.MetodoDePago = GloMETODODEPAGO
                        oCFD.TipoDeComprobante = GloTIPODECOMPROBANTE
                        oCFD.TipoComprobante33 = GloTIPODECOMPROBANTE

                        oCFD.UsoCFDI = GloUSOCFD
                        oCFD.EsNotaCredito = 0
                        oCFD.TipoRelacion = "04"
                        oCFD.UUIDRelacionados = GloUUIDRel
                        oCFD.UUIDRel.Add(GloUUIDRel)
                        'oCFD.FechaVencimiento = ""
                        'oCFD.CondicionesDePago = ""
                        oCFD.TipoCambio = GloTIPOCAMBIO
                        oCFD.Moneda = GloMONEDA
                        oCFD.Comentarios = GloOBSERVACION

                        oCFD.PagoEnParcialiades = GloPAGOENPARCIALIDADES
                        oCFD.LugarExpedicion = GloLUGAREXPEDICION
                        oCFD.CuentaPago = GlonumCtaPago


                        Dim oRegimen As New MizarCFD.BRL.CFDRegimenesFiscales
                        oRegimen.IdRegimen = 0
                        oRegimen.IdCFD = 0
                        oRegimen.Regimen = GloREGIMEN
                        'oRegimen.Insertar(Cnx)

                        oCFD.RegimenesFiscales.Add(oRegimen)

                        'oCFD.Complemento.XMLComplemento = "<raiz>Caelli Enarrant Gloria Dei </raiz>"
                        'oCFD.Addenda.XMLAddenda = "<edi>Edit dididididididi</edi>"

                        oCFD.Impuestos.TotalImpuestosRetenidos = 0
                        oCFD.Impuestos.TotalImpuestosTrasladados = oTotalImpuestos
                        'Dim oimpuestosRetenciones As CFDImpuestosRetenciones
                        'oimpuestosRetenciones = New CFDImpuestosRetenciones()
                        'oimpuestosRetenciones.Impuesto = "IVA"
                        'oimpuestosRetenciones.Importe = oiva
                        'oCFD.Impuestos.Retenciones.Add(oimpuestosRetenciones)

                        Dim oImpuestosTraslados As CFDImpuestosTraslados



                        'oImpuestosTraslados = New CFDImpuestosTraslados()
                        'oImpuestosTraslados.Impuesto = "IVA"
                        'oImpuestosTraslados.Importe = oiva
                        'oImpuestosTraslados.Tasa = oTasaIva

                        'oCFD.Impuestos.Traslados.Add(oImpuestosTraslados)

                        'oImpuestosTraslados = New CFDImpuestosTraslados()
                        'oImpuestosTraslados.Impuesto = "IEPS"
                        'oImpuestosTraslados.Importe = oieps
                        'oImpuestosTraslados.Tasa = oTasaIeps

                        'oCFD.Impuestos.Traslados.Add(oImpuestosTraslados)

                        Dim oConcepto As CFDConceptos
                        Dim oContrador As Integer = 1

                        Dim CONEXION2 As New SqlConnection(locconexion)
                        Dim COMANDO2 As New SqlCommand("exec DameFacDig_Parte_2_Global " + CStr(oClv_Factura), CONEXION2)
                        COMANDO2.CommandType = CommandType.Text
                        Try
                            CONEXION2.Open()
                            Dim reader As SqlDataReader = COMANDO2.ExecuteReader
                            '    'Recorremos los Cargos obtenidos desde el Procedimiento
                            While (reader.Read())
                                oConcepto = New CFDConceptos()
                                oConcepto.Cantidad = reader(0).ToString
                                'oConcepto.UnidadMedida = "No Aplica"
                                oConcepto.NumeroIdentificacion = reader(1).ToString
                                oConcepto.Descripcion = reader(2).ToString
                                oConcepto.ValorUnitario = reader(3).ToString
                                oConcepto.Importe = reader(4).ToString
                                'oConcepto.CausaIVA = "1"
                                oConcepto.TrasladarIVA = "1"

                                'Los nuevos
                                oConcepto.Descuento = 0 'Valor Nuevo
                                ' oConcepto.ClaveProdServ = reader(5).ToString ' "83111801" 'Campo nuevo
                                oConcepto.idClaveProdServ = reader(5).ToString
                                'Iva
                                'oConcepto.IVATRAS_TipoFactor = reader(6).ToString
                                oConcepto.TipoFactor = reader(6).ToString
                                oConcepto.IVATRAS_Importe = reader(8).ToString 'Iva del ticket
                                'oConcepto.IVATRAS_TasaOCuota = reader(7).ToString '"0.160000"


                                oConcepto.TasaIVA = reader(7).ToString
                                oConcepto.TrasladarIVA = "1"
                                'Ieps
                                'oConcepto.IEPSTRAS_TipoFactor = reader(9).ToString '"Tasa"
                                'oConcepto.IEPSTRAS_TasaOCuota = reader(10).ToString '"0.030000"
                                If Double.Parse(reader(11).ToString) > 0 Then
                                    oConcepto.BaseIEPS = reader(4).ToString 'Cantida Subtotal 
                                    'oConcepto.BaseIVA = CDec(reader(4).ToString) + CDec(reader(11).ToString)
                                    If SumaSubtotalYIeps = True Then
                                        oConcepto.BaseIVA = CDec(reader(4).ToString) + CDec(reader(11).ToString)
                                    Else
                                        oConcepto.BaseIVA = CDec(reader(4).ToString)
                                    End If

                                    oConcepto.IEPSTRAS_Importe = reader(11).ToString 'Total del ieps 'Ieps del ticket
                                    oConcepto.TasaIEPS = reader(10).ToString

                                    oConcepto.FactorIEPS = reader(9).ToString
                                    oConcepto.TrasladarIEPS = "1"
                                Else
                                    oConcepto.BaseIEPS = 0 'Cantida Subtotal 
                                    oConcepto.BaseIVA = reader(4).ToString
                                    oConcepto.IEPSTRAS_Importe = "0" 'Total del ieps 'Ieps del ticket
                                    oConcepto.TrasladarIEPS = "0"
                                End If
                                oConcepto.idUnidadMedida = reader(13).ToString '"E48" "Unidad de servicio" '"No Aplica" 'Cambio  Id que corresponde
                                oConcepto.UnidadMedida = reader(12).ToString '"Unidad de servicio" ' Pruebo poner una al gusto

                                oConcepto.IVARET_Importe = "0"
                                oConcepto.ISRRET_Importe = "0"

                                'Trasladados
                                If Double.Parse(reader(8).ToString) > 0 Then
                                    oImpuestosTraslados = New CFDImpuestosTraslados()
                                    oImpuestosTraslados.IdRegistroTraslado = oContrador
                                    oImpuestosTraslados.Impuesto = "IVA"
                                    oImpuestosTraslados.Importe = reader(8).ToString
                                    oImpuestosTraslados.Tasa = reader(7).ToString 'oTasaIva
                                    'oImpuestosTraslados.TipoFactor = GloTIPOFACTOR
                                    oImpuestosTraslados.TasaOCuota = GloTIPOFACTOR
                                    oImpuestosTraslados.IEPSTRAS_Importe = "0"
                                    oImpuestosTraslados.IVATRAS_Importe = "0"

                                    oCFD.Impuestos.Traslados.Add(oImpuestosTraslados)
                                End If
                                If Double.Parse(reader(11).ToString) > 0 Then
                                    oImpuestosTraslados = New CFDImpuestosTraslados()
                                    oImpuestosTraslados.IdRegistroTraslado = oContrador + 1
                                    oImpuestosTraslados.Impuesto = "IEPS" '"003" '"IEPS"
                                    oImpuestosTraslados.Importe = reader(11).ToString
                                    oImpuestosTraslados.Tasa = reader(10).ToString 'oTasaIeps
                                    oImpuestosTraslados.TasaOCuota = GloTIPOFACTOR
                                    oImpuestosTraslados.FactorIepsTras = GloTIPOFACTOR
                                    oImpuestosTraslados.IEPSTRAS_Importe = "0"
                                    oImpuestosTraslados.IVATRAS_Importe = "0"
                                    'oImpuestosTraslados.TipoFactor = GloTIPOFACTOR

                                    oCFD.Impuestos.Traslados.Add(oImpuestosTraslados)
                                End If


                                oConcepto.RetenerIVA = "0"
                                oConcepto.RetenerISR = "0"

                                oCFD.Conceptos.Add(oConcepto)
                                oContrador = oContrador + 2
                            End While
                            'Cnx.DbConnection.Close()
                        Catch ex As Exception
                            'MsgBox(ex.Message & "Graba Factura Digital Global " & ex.Message, MsgBoxStyle.Exclamation)
                            If Graba = "0" Then
                                Llenalog(ex.Source.ToString & " " & ex.Message)
                            Else

                                Llenalog(ex.Source.ToString & " " & ex.Message)
                            End If
                        Finally
                            CONEXION2.Close()
                            CONEXION2.Dispose()
                        End Try

                        'Dim a As String = JsonConvert.SerializeObject(oCFD)
                        Dim Bnd As Boolean = False
                        Dim Mensaje1 As String = ""
                        If Not oCFD.Insertar(Cnx, True, False) Then
                            For Each oMensaje As DAMensajesSistema.RegistroMensaje In oCFD.MensajesSistema.ListaMensajes
                                Mensaje1 = oMensaje.TextoMensaje + " " + oMensaje.Contexto
                                Inserta_Tbl_FacturasNoGeneradas(oClv_Factura, "G", Mensaje1, locconexion)
                                'MessageBox.Show(oMensaje.TextoMensaje + " " + oMensaje.Contexto)
                                Bnd = True
                            Next
                            Return
                        End If

                        GloClv_FacturaCFD = 0
                        GloClv_FacturaCFD = oCFD.IdCFD
                        UPS_Inserta_Rel_FacturasCFD(oClv_Factura, GloClv_FacturaCFD, "", "G", locconexion)
                        Dim r As New Globalization.CultureInfo("es-MX")

                        r.DateTimeFormat.ShortDatePattern = "dd/MM/yyyy"

                        System.Threading.Thread.CurrentThread.CurrentCulture = r
                        Try
                            '    oCFD.EnviarEmail(Cnx)
                            'ENVIAR_Factura(GloClv_FacturaCFD)
                            If GloClv_FacturaCFD > 0 And Bnd = False Then
                                ENVIAR_Factura(GloClv_FacturaCFD)
                            End If
                        Catch ex As Exception

                        End Try

                    Catch ex As Exception
                        'MsgBox(ex.Message & "Graba Factura Digital Global Parte 3 " & ex.Message, MsgBoxStyle.Exclamation)
                        If Graba = "0" Then
                            Llenalog(ex.Source.ToString & " " & ex.Message)
                        Else

                            Llenalog(ex.Source.ToString & " " & ex.Message)
                        End If
                    Finally
                        CONEXION.Close()
                        CONEXION.Dispose()
                    End Try

                End Using
            Else
                GloClv_FacturaCFD = ClvFacturaCfd
            End If
        Catch ex As Exception
            'MsgBox(ex.Message & "Graba Factura Digital Por Dirección Parte 3 " & ex.Message, MsgBoxStyle.Exclamation)
            If Graba = "0" Then
                Llenalog(ex.Source.ToString & " " & ex.Message)
            Else

                Llenalog(ex.Source.ToString & " " & ex.Message)
            End If
        End Try
    End Sub

    Public Shared Sub Graba_Factura_Digital_GlobalSinIeps(ByVal oClv_Factura As Long, ByVal oId_Compania As String, ByVal oId_Sucursal As String, ByVal locconexion As String, ByVal oIden As Integer)
        Try


            Dim ClvFacturaCfd As Long = 0
            'ClvFacturaCfd = SP_DIMESIEXISTEFACTURADIGITAL(oClv_Factura, "G", locconexion)
            If ClvFacturaCfd = 0 Then '0 Es si no Existe

                Dim oTotalConPuntos As String = Nothing
                Dim oSubTotal As String = Nothing
                Dim oTotalSinPuntos As String = Nothing
                Dim oDescuento As String = Nothing
                Dim oiva As String = Nothing
                Dim oieps As String = Nothing
                Dim oTasaIva As String = Nothing
                Dim oTasaIeps As String = Nothing
                Dim oFecha As String = Nothing
                Dim oTotalImpuestos As String = Nothing

                Dim mfecha As DateTime

                Class1.limpiaParametros()
                Class1.CreateMyParameter("@FECHA", ParameterDirection.Output, SqlDbType.DateTime)
                Class1.ProcedimientoOutPut("DAMEFECHADELSERVIDORFACTURADIGITAL", locconexion)
                mfecha = DateTime.Parse(Class1.dicoPar("@FECHA").ToString())

                Dim CONEXION As New SqlConnection(locconexion)
                Dim COMANDO As New SqlCommand("DameFacDig_Parte_1NotaGlobalSinIeps", CONEXION)
                COMANDO.CommandType = CommandType.StoredProcedure
                COMANDO.CommandTimeout = 0

                Dim parametro As New SqlParameter("@Id_Cfd", SqlDbType.BigInt)
                parametro.Direction = ParameterDirection.Input
                parametro.Value = oClv_Factura
                COMANDO.Parameters.Add(parametro)

                Dim parametro1 As New SqlParameter("@TotalConPuntos", SqlDbType.Money)
                parametro1.Direction = ParameterDirection.Output
                parametro1.Value = 0
                COMANDO.Parameters.Add(parametro1)

                Dim parametro2 As New SqlParameter("@SubTotal", SqlDbType.Money)
                parametro2.Direction = ParameterDirection.Output
                parametro2.Value = 0
                COMANDO.Parameters.Add(parametro2)

                Dim parametro3 As New SqlParameter("@TotalSinPuntos", SqlDbType.Money)
                parametro3.Direction = ParameterDirection.Output
                parametro3.Value = 0
                COMANDO.Parameters.Add(parametro3)

                Dim parametro4 As New SqlParameter("@Descuento", SqlDbType.Money)
                parametro4.Direction = ParameterDirection.Output
                parametro4.Value = 0
                COMANDO.Parameters.Add(parametro4)

                Dim parametro5 As New SqlParameter("@iva", SqlDbType.Money)
                parametro5.Direction = ParameterDirection.Output
                parametro5.Value = 0
                COMANDO.Parameters.Add(parametro5)

                Dim parametro6 As New SqlParameter("@ieps", SqlDbType.Money)
                parametro6.Direction = ParameterDirection.Output
                parametro6.Value = 0
                COMANDO.Parameters.Add(parametro6)

                Dim parametro7 As New SqlParameter("@TasaIva", SqlDbType.VarChar, 6)
                parametro7.Direction = ParameterDirection.Output
                parametro7.Value = 0
                COMANDO.Parameters.Add(parametro7)

                Dim parametro8 As New SqlParameter("@TasaIeps", SqlDbType.VarChar, 6)
                parametro8.Direction = ParameterDirection.Output
                parametro8.Value = 0
                COMANDO.Parameters.Add(parametro8)

                Dim parametro9 As New SqlParameter("@Fecha", SqlDbType.DateTime)
                parametro9.Direction = ParameterDirection.Output
                parametro9.Value = 0
                COMANDO.Parameters.Add(parametro9)

                Dim parametro10 As New SqlParameter("@TotalImpuestos", SqlDbType.Money)
                parametro10.Direction = ParameterDirection.Output
                parametro10.Value = 0
                COMANDO.Parameters.Add(parametro10)

                Try
                    CONEXION.Open()
                    Dim i As Integer = COMANDO.ExecuteNonQuery
                    oTotalConPuntos = CStr(parametro1.Value)
                    oSubTotal = CStr(parametro2.Value)
                    oTotalSinPuntos = CStr(parametro3.Value)
                    oDescuento = CStr(parametro4.Value)
                    oiva = CStr(parametro5.Value)
                    oieps = CStr(parametro6.Value)
                    oTasaIva = CStr(parametro7.Value)
                    oTasaIeps = CStr(parametro8.Value)
                    oFecha = CStr(parametro9.Value)
                    oTotalImpuestos = CStr(parametro10.Value)
                    'Cnx.DbConnection.Close()
                Catch ex As Exception
                    'MsgBox(ex.Message & "Graba Factura Digital Global Parte 1 " & ex.Message, MsgBoxStyle.Exclamation)
                    If Graba = "0" Then
                        Llenalog(ex.Source.ToString & " " & ex.Message)
                    Else

                        Llenalog(ex.Source.ToString & " " & ex.Message)
                    End If
                Finally
                    CONEXION.Close()
                    CONEXION.Dispose()
                End Try

                Usp_Ed_DameDatosFacDigGlobalSinIeps(oClv_Factura, oId_Compania, locconexion)

                Dim oId_AsociadoLlave As Long = 0
                oId_AsociadoLlave = oIden 'Existe_DevuelveValor(" Select top 1 id_asociado from asociados where RFC ='XAXX010101000' order by id_asociado ")

                Dim oCFD As MizarCFD.BRL.CFD

                Using Cnx As New DAConexion("HL", "sa", "sa")
                    Try


                        oCFD = New MizarCFD.BRL.CFD
                        oCFD.Inicializar()
                        oCFD.TipoCFD = MizarCFD.BRL.CFD.TiposCFD.Emision
                        If oId_Sucursal = "0" Then oId_Sucursal = ""

                        'oCFD.PrepararDatosEmisorPorId(Cnx, locID_Compania_Mizart)
                        'oCFD.PrepararDatosReceptorPorId(Cnx, CStr(oIden), 1, 0)

                        'oCFD.PrepararDatosEmisorPorId(Cnx, oId_Compania, oId_Sucursal)
                        'Comente lo de compañia

                        If oId_Sucursal.Length > 0 And oId_Sucursal <> "0" Then
                            oCFD.PrepararDatosEmisorPorId(Cnx, oId_Compania, oId_Sucursal)
                        Else
                            oCFD.PrepararDatosEmisorPorId(Cnx, oId_Compania)
                        End If

                        oCFD.PrepararDatosReceptorPorId(Cnx, oId_AsociadoLlave, 1, 0)


                        oCFD.EsTimbrePrueba = EsTimbrePrueba

                        oCFD.Timbrador = CFD.PAC.Mizar

                        oCFD.Version = GloVERSON
                        oCFD.IdMoneda = 1  '//1 = Pesos , Pesos = 2 USD
                        'oFecha
                        'oCFD.Fecha = mfecha
                        oCFD.FormaDePago = GloFORMADEPAGO
                        'oCFD.Certificado = ""
                        oCFD.CondicionesDePago = GloCONDICIONESDEPAGO
                        oCFD.SubTotal = oSubTotal
                        oCFD.Descuento = oDescuento
                        oCFD.MotivoDescuento = GloMOTIVODESCUENTO
                        oCFD.Total = oTotalSinPuntos
                        oCFD.MetodoDePago = GloMETODODEPAGO
                        oCFD.TipoDeComprobante = GloTIPODECOMPROBANTE
                        oCFD.TipoComprobante33 = GloTIPODECOMPROBANTE

                        oCFD.UsoCFDI = GloUSOCFD
                        oCFD.EsNotaCredito = 0
                        oCFD.TipoRelacion = Nothing
                        oCFD.UUIDRelacionados = Nothing
                        'oCFD.FechaVencimiento = ""
                        'oCFD.CondicionesDePago = ""
                        oCFD.TipoCambio = GloTIPOCAMBIO
                        oCFD.Moneda = GloMONEDA
                        oCFD.Comentarios = GloOBSERVACION

                        oCFD.PagoEnParcialiades = GloPAGOENPARCIALIDADES
                        oCFD.LugarExpedicion = GloLUGAREXPEDICION
                        oCFD.CuentaPago = GlonumCtaPago


                        Dim oRegimen As New MizarCFD.BRL.CFDRegimenesFiscales
                        oRegimen.IdRegimen = 0
                        oRegimen.IdCFD = 0
                        oRegimen.Regimen = GloREGIMEN
                        'oRegimen.Insertar(Cnx)

                        oCFD.RegimenesFiscales.Add(oRegimen)

                        'oCFD.Complemento.XMLComplemento = "<raiz>Caelli Enarrant Gloria Dei </raiz>"
                        'oCFD.Addenda.XMLAddenda = "<edi>Edit dididididididi</edi>"

                        oCFD.Impuestos.TotalImpuestosRetenidos = 0
                        oCFD.Impuestos.TotalImpuestosTrasladados = oTotalImpuestos
                        'Dim oimpuestosRetenciones As CFDImpuestosRetenciones
                        'oimpuestosRetenciones = New CFDImpuestosRetenciones()
                        'oimpuestosRetenciones.Impuesto = "IVA"
                        'oimpuestosRetenciones.Importe = oiva
                        'oCFD.Impuestos.Retenciones.Add(oimpuestosRetenciones)

                        Dim oImpuestosTraslados As CFDImpuestosTraslados



                        'oImpuestosTraslados = New CFDImpuestosTraslados()
                        'oImpuestosTraslados.Impuesto = "IVA"
                        'oImpuestosTraslados.Importe = oiva
                        'oImpuestosTraslados.Tasa = oTasaIva

                        'oCFD.Impuestos.Traslados.Add(oImpuestosTraslados)

                        'oImpuestosTraslados = New CFDImpuestosTraslados()
                        'oImpuestosTraslados.Impuesto = "IEPS"
                        'oImpuestosTraslados.Importe = oieps
                        'oImpuestosTraslados.Tasa = oTasaIeps

                        'oCFD.Impuestos.Traslados.Add(oImpuestosTraslados)

                        Dim oConcepto As CFDConceptos
                        Dim oContrador As Integer = 1

                        Dim CONEXION2 As New SqlConnection(locconexion)
                        Dim COMANDO2 As New SqlCommand("exec DameFacDig_Parte_2NotaGlobalSinIeps " + CStr(oClv_Factura), CONEXION2)
                        COMANDO2.CommandType = CommandType.Text
                        COMANDO2.CommandTimeout = 0
                        Try
                            CONEXION2.Open()
                            Dim reader As SqlDataReader = COMANDO2.ExecuteReader
                            '    'Recorremos los Cargos obtenidos desde el Procedimiento
                            While (reader.Read())
                                oConcepto = New CFDConceptos()
                                oConcepto.Cantidad = reader(0).ToString
                                'oConcepto.UnidadMedida = "No Aplica"
                                oConcepto.NumeroIdentificacion = reader(1).ToString
                                oConcepto.Descripcion = reader(2).ToString
                                oConcepto.ValorUnitario = reader(3).ToString
                                oConcepto.Importe = reader(4).ToString
                                'oConcepto.CausaIVA = "1"
                                oConcepto.TrasladarIVA = "1"

                                'Los nuevos
                                oConcepto.Descuento = 0 'Valor Nuevo
                                ' oConcepto.ClaveProdServ = reader(5).ToString ' "83111801" 'Campo nuevo
                                oConcepto.idClaveProdServ = reader(5).ToString
                                'Iva
                                'oConcepto.IVATRAS_TipoFactor = reader(6).ToString
                                oConcepto.TipoFactor = reader(6).ToString
                                oConcepto.IVATRAS_Importe = reader(8).ToString 'Iva del ticket
                                'oConcepto.IVATRAS_TasaOCuota = reader(7).ToString '"0.160000"


                                oConcepto.TasaIVA = reader(7).ToString
                                oConcepto.TrasladarIVA = "1"
                                'Ieps
                                'oConcepto.IEPSTRAS_TipoFactor = reader(9).ToString '"Tasa"
                                'oConcepto.IEPSTRAS_TasaOCuota = reader(10).ToString '"0.030000"
                                If Double.Parse(reader(11).ToString) > 0 Then
                                    oConcepto.BaseIEPS = reader(4).ToString 'Cantida Subtotal 
                                    'oConcepto.BaseIVA = CDec(reader(4).ToString) + CDec(reader(11).ToString)
                                    If SumaSubtotalYIeps = True Then
                                        oConcepto.BaseIVA = CDec(reader(4).ToString) + CDec(reader(11).ToString)
                                    Else
                                        oConcepto.BaseIVA = CDec(reader(4).ToString)
                                    End If

                                    oConcepto.IEPSTRAS_Importe = reader(11).ToString 'Total del ieps 'Ieps del ticket
                                    oConcepto.TasaIEPS = reader(10).ToString

                                    oConcepto.FactorIEPS = reader(9).ToString
                                    oConcepto.TrasladarIEPS = "1"
                                Else
                                    oConcepto.BaseIEPS = 0 'Cantida Subtotal 
                                    oConcepto.BaseIVA = reader(4).ToString
                                    oConcepto.IEPSTRAS_Importe = "0" 'Total del ieps 'Ieps del ticket
                                    oConcepto.TrasladarIEPS = "0"
                                End If
                                oConcepto.idUnidadMedida = reader(13).ToString '"E48" "Unidad de servicio" '"No Aplica" 'Cambio  Id que corresponde
                                oConcepto.UnidadMedida = reader(12).ToString '"Unidad de servicio" ' Pruebo poner una al gusto

                                oConcepto.IVARET_Importe = "0"
                                oConcepto.ISRRET_Importe = "0"

                                'Trasladados
                                If Double.Parse(reader(8).ToString) > 0 Then
                                    oImpuestosTraslados = New CFDImpuestosTraslados()
                                    oImpuestosTraslados.IdRegistroTraslado = oContrador
                                    oImpuestosTraslados.Impuesto = "IVA"
                                    oImpuestosTraslados.Importe = reader(8).ToString
                                    oImpuestosTraslados.Tasa = reader(7).ToString 'oTasaIva
                                    'oImpuestosTraslados.TipoFactor = GloTIPOFACTOR
                                    oImpuestosTraslados.TasaOCuota = GloTIPOFACTOR
                                    oImpuestosTraslados.IEPSTRAS_Importe = "0"
                                    oImpuestosTraslados.IVATRAS_Importe = "0"

                                    oCFD.Impuestos.Traslados.Add(oImpuestosTraslados)
                                End If
                                If Double.Parse(reader(11).ToString) > 0 Then
                                    oImpuestosTraslados = New CFDImpuestosTraslados()
                                    oImpuestosTraslados.IdRegistroTraslado = oContrador + 1
                                    oImpuestosTraslados.Impuesto = "IEPS" '"003" '"IEPS"
                                    oImpuestosTraslados.Importe = reader(11).ToString
                                    oImpuestosTraslados.Tasa = reader(10).ToString 'oTasaIeps
                                    oImpuestosTraslados.TasaOCuota = GloTIPOFACTOR
                                    oImpuestosTraslados.FactorIepsTras = GloTIPOFACTOR
                                    oImpuestosTraslados.IEPSTRAS_Importe = "0"
                                    oImpuestosTraslados.IVATRAS_Importe = "0"
                                    'oImpuestosTraslados.TipoFactor = GloTIPOFACTOR

                                    oCFD.Impuestos.Traslados.Add(oImpuestosTraslados)
                                End If


                                oConcepto.RetenerIVA = "0"
                                oConcepto.RetenerISR = "0"

                                oCFD.Conceptos.Add(oConcepto)
                                oContrador = oContrador + 2
                            End While
                            'Cnx.DbConnection.Close()
                        Catch ex As Exception
                            'MsgBox(ex.Message & "Graba Factura Digital Global " & ex.Message, MsgBoxStyle.Exclamation)
                            If Graba = "0" Then
                                Llenalog(ex.Source.ToString & " " & ex.Message)
                            Else

                                Llenalog(ex.Source.ToString & " " & ex.Message)
                            End If
                        Finally
                            CONEXION2.Close()
                            CONEXION2.Dispose()
                        End Try

                        'Dim a As String = JsonConvert.SerializeObject(oCFD)
                        Dim Bnd As Boolean = False
                        Dim Mensaje1 As String = ""
                        If Not oCFD.Insertar(Cnx, True, False) Then
                            For Each oMensaje As DAMensajesSistema.RegistroMensaje In oCFD.MensajesSistema.ListaMensajes
                                Mensaje1 = oMensaje.TextoMensaje + " " + oMensaje.Contexto
                                Inserta_Tbl_FacturasNoGeneradas(oClv_Factura, "6", Mensaje1, locconexion)
                                'MessageBox.Show(oMensaje.TextoMensaje + " " + oMensaje.Contexto)
                                Bnd = True
                            Next
                            Return
                        End If

                        GloClv_FacturaCFD = 0
                        GloClv_FacturaCFD = oCFD.IdCFD
                        UPS_Inserta_Rel_FacturasCFD(oClv_Factura, GloClv_FacturaCFD, "", "6", locconexion)
                        Dim r As New Globalization.CultureInfo("es-MX")

                        r.DateTimeFormat.ShortDatePattern = "dd/MM/yyyy"

                        System.Threading.Thread.CurrentThread.CurrentCulture = r
                        Try
                            '    oCFD.EnviarEmail(Cnx)
                            'ENVIAR_Factura(GloClv_FacturaCFD)
                            If GloClv_FacturaCFD > 0 And Bnd = False Then
                                ENVIAR_Factura(GloClv_FacturaCFD)
                            End If
                        Catch ex As Exception

                        End Try

                    Catch ex As Exception
                        'MsgBox(ex.Message & "Graba Factura Digital Global Parte 3 " & ex.Message, MsgBoxStyle.Exclamation)
                        If Graba = "0" Then
                            Llenalog(ex.Source.ToString & " " & ex.Message)
                        Else

                            Llenalog(ex.Source.ToString & " " & ex.Message)
                        End If
                    Finally
                        CONEXION.Close()
                        CONEXION.Dispose()
                    End Try

                End Using
            Else
                GloClv_FacturaCFD = ClvFacturaCfd
            End If
        Catch ex As Exception
            'MsgBox(ex.Message & "Graba Factura Digital Por Dirección Parte 3 " & ex.Message, MsgBoxStyle.Exclamation)
            If Graba = "0" Then
                Llenalog(ex.Source.ToString & " " & ex.Message)
            Else

                Llenalog(ex.Source.ToString & " " & ex.Message)
            End If
        End Try
    End Sub

    Public Shared Sub Graba_Factura_Digital_Global(ByVal oClv_Factura As Long, ByVal oId_Compania As String, ByVal oId_Sucursal As String, ByVal locconexion As String)
        Try


            Dim ClvFacturaCfd As Long
            ClvFacturaCfd = SP_DIMESIEXISTEFACTURADIGITAL(oClv_Factura, "G", locconexion)
            If ClvFacturaCfd = 0 Then '0 Es si no Existe

                Dim oTotalConPuntos As String = Nothing
                Dim oSubTotal As String = Nothing
                Dim oTotalSinPuntos As String = Nothing
                Dim oDescuento As String = Nothing
                Dim oiva As String = Nothing
                Dim oieps As String = Nothing
                Dim oTasaIva As String = Nothing
                Dim oTasaIeps As String = Nothing
                Dim oFecha As String = Nothing
                Dim oTotalImpuestos As String = Nothing

                Dim mfecha As DateTime

                Class1.limpiaParametros()
                Class1.CreateMyParameter("@FECHA", ParameterDirection.Output, SqlDbType.DateTime)
                Class1.ProcedimientoOutPut("DAMEFECHADELSERVIDORFACTURADIGITAL", locconexion)
                mfecha = DateTime.Parse(Class1.dicoPar("@FECHA").ToString())

                Dim CONEXION As New SqlConnection(locconexion)
                Dim COMANDO As New SqlCommand("DameFacDig_Parte_1_Global", CONEXION)
                COMANDO.CommandType = CommandType.StoredProcedure
                COMANDO.CommandTimeout = 0

                Dim parametro As New SqlParameter("@Clv_Factura", SqlDbType.BigInt)
                parametro.Direction = ParameterDirection.Input
                parametro.Value = oClv_Factura
                COMANDO.Parameters.Add(parametro)

                Dim parametro1 As New SqlParameter("@TotalConPuntos", SqlDbType.Money)
                parametro1.Direction = ParameterDirection.Output
                parametro1.Value = 0
                COMANDO.Parameters.Add(parametro1)

                Dim parametro2 As New SqlParameter("@SubTotal", SqlDbType.Money)
                parametro2.Direction = ParameterDirection.Output
                parametro2.Value = 0
                COMANDO.Parameters.Add(parametro2)

                Dim parametro3 As New SqlParameter("@TotalSinPuntos", SqlDbType.Money)
                parametro3.Direction = ParameterDirection.Output
                parametro3.Value = 0
                COMANDO.Parameters.Add(parametro3)

                Dim parametro4 As New SqlParameter("@Descuento", SqlDbType.Money)
                parametro4.Direction = ParameterDirection.Output
                parametro4.Value = 0
                COMANDO.Parameters.Add(parametro4)

                Dim parametro5 As New SqlParameter("@iva", SqlDbType.Money)
                parametro5.Direction = ParameterDirection.Output
                parametro5.Value = 0
                COMANDO.Parameters.Add(parametro5)

                Dim parametro6 As New SqlParameter("@ieps", SqlDbType.Money)
                parametro6.Direction = ParameterDirection.Output
                parametro6.Value = 0
                COMANDO.Parameters.Add(parametro6)

                Dim parametro7 As New SqlParameter("@TasaIva", SqlDbType.Money)
                parametro7.Direction = ParameterDirection.Output
                parametro7.Value = 0
                COMANDO.Parameters.Add(parametro7)

                Dim parametro8 As New SqlParameter("@TasaIeps", SqlDbType.Money)
                parametro8.Direction = ParameterDirection.Output
                parametro8.Value = 0
                COMANDO.Parameters.Add(parametro8)

                Dim parametro9 As New SqlParameter("@Fecha", SqlDbType.DateTime)
                parametro9.Direction = ParameterDirection.Output
                parametro9.Value = 0
                COMANDO.Parameters.Add(parametro9)

                Dim parametro10 As New SqlParameter("@TotalImpuestos", SqlDbType.Money)
                parametro10.Direction = ParameterDirection.Output
                parametro10.Value = 0
                COMANDO.Parameters.Add(parametro10)

                Try
                    CONEXION.Open()
                    Dim i As Integer = COMANDO.ExecuteNonQuery
                    oTotalConPuntos = CStr(parametro1.Value)
                    oSubTotal = CStr(parametro2.Value)
                    oTotalSinPuntos = CStr(parametro3.Value)
                    oDescuento = CStr(parametro4.Value)
                    oiva = CStr(parametro5.Value)
                    oieps = CStr(parametro6.Value)
                    oTasaIva = CStr(parametro7.Value)
                    oTasaIeps = CStr(parametro8.Value)
                    oFecha = CStr(parametro9.Value)
                    oTotalImpuestos = CStr(parametro10.Value)
                    'Cnx.DbConnection.Close()
                Catch ex As Exception
                    'MsgBox(ex.Message & "Graba Factura Digital Global Parte 1 " & ex.Message, MsgBoxStyle.Exclamation)
                    If Graba = "0" Then
                        Llenalog(ex.Source.ToString & " " & ex.Message)
                    Else

                        Llenalog(ex.Source.ToString & " " & ex.Message)
                    End If
                Finally
                    CONEXION.Close()
                    CONEXION.Dispose()
                End Try

                Usp_Ed_DameDatosFacDigGlobal(oClv_Factura, oId_Compania, locconexion)

                Dim oId_AsociadoLlave As Long = 0
                oId_AsociadoLlave = Existe_DevuelveValor(" Select top 1 id_asociado from asociados where RFC ='XAXX010101000' order by id_asociado ")

                Dim oCFD As MizarCFD.BRL.CFD

                Using Cnx As New DAConexion("HL", "sa", "sa")
                    Try


                        oCFD = New MizarCFD.BRL.CFD
                        oCFD.Inicializar()
                        oCFD.TipoCFD = MizarCFD.BRL.CFD.TiposCFD.Emision
                        If oId_Sucursal = "0" Then oId_Sucursal = ""

                        'oCFD.PrepararDatosEmisorPorId(Cnx, locID_Compania_Mizart)
                        'oCFD.PrepararDatosReceptorPorId(Cnx, CStr(oIden), 1, 0)

                        'oCFD.PrepararDatosEmisorPorId(Cnx, oId_Compania, oId_Sucursal)
                        'Comente lo de compañia

                        If oId_Sucursal.Length > 0 And oId_Sucursal <> "0" Then
                            oCFD.PrepararDatosEmisorPorId(Cnx, oId_Compania, oId_Sucursal)
                        Else
                            oCFD.PrepararDatosEmisorPorId(Cnx, oId_Compania)
                        End If

                        oCFD.PrepararDatosReceptorPorId(Cnx, oId_AsociadoLlave, 1, 0)


                        oCFD.EsTimbrePrueba = EsTimbrePrueba

                        oCFD.Timbrador = CFD.PAC.Mizar

                        oCFD.Version = GloVERSON
                        oCFD.IdMoneda = 1  '//1 = Pesos , Pesos = 2 USD
                        'oFecha
                        'oCFD.Fecha = mfecha
                        oCFD.FormaDePago = GloFORMADEPAGO
                        'oCFD.Certificado = ""
                        oCFD.CondicionesDePago = GloCONDICIONESDEPAGO
                        oCFD.SubTotal = oSubTotal
                        oCFD.Descuento = oDescuento
                        oCFD.MotivoDescuento = GloMOTIVODESCUENTO
                        oCFD.Total = oTotalSinPuntos
                        oCFD.MetodoDePago = GloMETODODEPAGO
                        oCFD.TipoDeComprobante = GloTIPODECOMPROBANTE
                        oCFD.TipoComprobante33 = GloTIPODECOMPROBANTE

                        oCFD.UsoCFDI = GloUSOCFD
                        oCFD.EsNotaCredito = 0
                        oCFD.TipoRelacion = Nothing
                        oCFD.UUIDRelacionados = Nothing
                        'oCFD.FechaVencimiento = ""
                        'oCFD.CondicionesDePago = ""
                        oCFD.TipoCambio = GloTIPOCAMBIO
                        oCFD.Moneda = GloMONEDA
                        oCFD.Comentarios = GloOBSERVACION

                        oCFD.PagoEnParcialiades = GloPAGOENPARCIALIDADES
                        oCFD.LugarExpedicion = GloLUGAREXPEDICION
                        oCFD.CuentaPago = GlonumCtaPago


                        Dim oRegimen As New MizarCFD.BRL.CFDRegimenesFiscales
                        oRegimen.IdRegimen = 0
                        oRegimen.IdCFD = 0
                        oRegimen.Regimen = GloREGIMEN
                        'oRegimen.Insertar(Cnx)

                        oCFD.RegimenesFiscales.Add(oRegimen)

                        'oCFD.Complemento.XMLComplemento = "<raiz>Caelli Enarrant Gloria Dei </raiz>"
                        'oCFD.Addenda.XMLAddenda = "<edi>Edit dididididididi</edi>"

                        oCFD.Impuestos.TotalImpuestosRetenidos = 0
                        oCFD.Impuestos.TotalImpuestosTrasladados = oTotalImpuestos
                        'Dim oimpuestosRetenciones As CFDImpuestosRetenciones
                        'oimpuestosRetenciones = New CFDImpuestosRetenciones()
                        'oimpuestosRetenciones.Impuesto = "IVA"
                        'oimpuestosRetenciones.Importe = oiva
                        'oCFD.Impuestos.Retenciones.Add(oimpuestosRetenciones)

                        Dim oImpuestosTraslados As CFDImpuestosTraslados



                        'oImpuestosTraslados = New CFDImpuestosTraslados()
                        'oImpuestosTraslados.Impuesto = "IVA"
                        'oImpuestosTraslados.Importe = oiva
                        'oImpuestosTraslados.Tasa = oTasaIva

                        'oCFD.Impuestos.Traslados.Add(oImpuestosTraslados)

                        'oImpuestosTraslados = New CFDImpuestosTraslados()
                        'oImpuestosTraslados.Impuesto = "IEPS"
                        'oImpuestosTraslados.Importe = oieps
                        'oImpuestosTraslados.Tasa = oTasaIeps

                        'oCFD.Impuestos.Traslados.Add(oImpuestosTraslados)

                        Dim oConcepto As CFDConceptos
                        Dim oContrador As Integer = 1

                        Dim CONEXION2 As New SqlConnection(locconexion)
                        Dim COMANDO2 As New SqlCommand("exec DameFacDig_Parte_2_Global " + CStr(oClv_Factura), CONEXION2)
                        COMANDO2.CommandType = CommandType.Text
                        COMANDO2.CommandTimeout = 0
                        Try
                            CONEXION2.Open()
                            Dim reader As SqlDataReader = COMANDO2.ExecuteReader
                            '    'Recorremos los Cargos obtenidos desde el Procedimiento
                            While (reader.Read())
                                oConcepto = New CFDConceptos()
                                oConcepto.Cantidad = reader(0).ToString
                                'oConcepto.UnidadMedida = "No Aplica"
                                oConcepto.NumeroIdentificacion = reader(1).ToString
                                oConcepto.Descripcion = reader(2).ToString
                                oConcepto.ValorUnitario = reader(3).ToString
                                oConcepto.Importe = reader(4).ToString
                                'oConcepto.CausaIVA = "1"
                                oConcepto.TrasladarIVA = "1"

                                'Los nuevos
                                oConcepto.Descuento = 0 'Valor Nuevo
                                ' oConcepto.ClaveProdServ = reader(5).ToString ' "83111801" 'Campo nuevo
                                oConcepto.idClaveProdServ = reader(5).ToString
                                'Iva
                                'oConcepto.IVATRAS_TipoFactor = reader(6).ToString
                                oConcepto.TipoFactor = reader(6).ToString
                                oConcepto.IVATRAS_Importe = reader(8).ToString 'Iva del ticket
                                'oConcepto.IVATRAS_TasaOCuota = reader(7).ToString '"0.160000"


                                oConcepto.TasaIVA = reader(7).ToString
                                oConcepto.TrasladarIVA = "1"
                                'Ieps
                                'oConcepto.IEPSTRAS_TipoFactor = reader(9).ToString '"Tasa"
                                'oConcepto.IEPSTRAS_TasaOCuota = reader(10).ToString '"0.030000"
                                If Double.Parse(reader(11).ToString) > 0 Then
                                    oConcepto.BaseIEPS = reader(4).ToString 'Cantida Subtotal 
                                    'oConcepto.BaseIVA = CDec(reader(4).ToString) + CDec(reader(11).ToString)
                                    If SumaSubtotalYIeps = True Then
                                        oConcepto.BaseIVA = CDec(reader(4).ToString) + CDec(reader(11).ToString)
                                    Else
                                        oConcepto.BaseIVA = CDec(reader(4).ToString)
                                    End If

                                    oConcepto.IEPSTRAS_Importe = reader(11).ToString 'Total del ieps 'Ieps del ticket
                                    oConcepto.TasaIEPS = reader(10).ToString

                                    oConcepto.FactorIEPS = reader(9).ToString
                                    oConcepto.TrasladarIEPS = "1"
                                Else
                                    oConcepto.BaseIEPS = 0 'Cantida Subtotal 
                                    oConcepto.BaseIVA = reader(4).ToString
                                    oConcepto.IEPSTRAS_Importe = "0" 'Total del ieps 'Ieps del ticket
                                    oConcepto.TrasladarIEPS = "0"
                                End If
                                oConcepto.idUnidadMedida = reader(13).ToString '"E48" "Unidad de servicio" '"No Aplica" 'Cambio  Id que corresponde
                                oConcepto.UnidadMedida = reader(12).ToString '"Unidad de servicio" ' Pruebo poner una al gusto

                                oConcepto.IVARET_Importe = "0"
                                oConcepto.ISRRET_Importe = "0"

                                'Trasladados
                                If Double.Parse(reader(8).ToString) > 0 Then
                                    oImpuestosTraslados = New CFDImpuestosTraslados()
                                    oImpuestosTraslados.IdRegistroTraslado = oContrador
                                    oImpuestosTraslados.Impuesto = "IVA"
                                    oImpuestosTraslados.Importe = reader(8).ToString
                                    oImpuestosTraslados.Tasa = reader(7).ToString 'oTasaIva
                                    'oImpuestosTraslados.TipoFactor = GloTIPOFACTOR
                                    oImpuestosTraslados.TasaOCuota = GloTIPOFACTOR
                                    oImpuestosTraslados.IEPSTRAS_Importe = "0"
                                    oImpuestosTraslados.IVATRAS_Importe = "0"

                                    oCFD.Impuestos.Traslados.Add(oImpuestosTraslados)
                                End If
                                If Double.Parse(reader(11).ToString) > 0 Then
                                    oImpuestosTraslados = New CFDImpuestosTraslados()
                                    oImpuestosTraslados.IdRegistroTraslado = oContrador + 1
                                    oImpuestosTraslados.Impuesto = "IEPS" '"003" '"IEPS"
                                    oImpuestosTraslados.Importe = reader(11).ToString
                                    oImpuestosTraslados.Tasa = reader(10).ToString 'oTasaIeps
                                    oImpuestosTraslados.TasaOCuota = GloTIPOFACTOR
                                    oImpuestosTraslados.FactorIepsTras = GloTIPOFACTOR
                                    oImpuestosTraslados.IEPSTRAS_Importe = "0"
                                    oImpuestosTraslados.IVATRAS_Importe = "0"
                                    'oImpuestosTraslados.TipoFactor = GloTIPOFACTOR

                                    oCFD.Impuestos.Traslados.Add(oImpuestosTraslados)
                                End If


                                oConcepto.RetenerIVA = "0"
                                oConcepto.RetenerISR = "0"

                                oCFD.Conceptos.Add(oConcepto)
                                oContrador = oContrador + 2
                            End While
                            'Cnx.DbConnection.Close()
                        Catch ex As Exception
                            'MsgBox(ex.Message & "Graba Factura Digital Global " & ex.Message, MsgBoxStyle.Exclamation)
                            If Graba = "0" Then
                                Llenalog(ex.Source.ToString & " " & ex.Message)
                            Else

                                Llenalog(ex.Source.ToString & " " & ex.Message)
                            End If
                        Finally
                            CONEXION2.Close()
                            CONEXION2.Dispose()
                        End Try

                        'Dim a As String = JsonConvert.SerializeObject(oCFD)
                        Dim Bnd As Boolean = False
                        Dim Mensaje1 As String = ""
                        If Not oCFD.Insertar(Cnx, True, False) Then
                            For Each oMensaje As DAMensajesSistema.RegistroMensaje In oCFD.MensajesSistema.ListaMensajes
                                Mensaje1 = oMensaje.TextoMensaje + " " + oMensaje.Contexto
                                Inserta_Tbl_FacturasNoGeneradas(oClv_Factura, "G", Mensaje1, locconexion)
                                'MessageBox.Show(oMensaje.TextoMensaje + " " + oMensaje.Contexto)
                                Bnd = True
                            Next
                            Return
                        End If

                        GloClv_FacturaCFD = 0
                        GloClv_FacturaCFD = oCFD.IdCFD
                        UPS_Inserta_Rel_FacturasCFD(oClv_Factura, GloClv_FacturaCFD, "", "G", locconexion)
                        Dim r As New Globalization.CultureInfo("es-MX")

                        r.DateTimeFormat.ShortDatePattern = "dd/MM/yyyy"

                        System.Threading.Thread.CurrentThread.CurrentCulture = r
                        Try
                            '    oCFD.EnviarEmail(Cnx)
                            'ENVIAR_Factura(GloClv_FacturaCFD)
                            If GloClv_FacturaCFD > 0 And bnd = False Then
                                ENVIAR_Factura(GloClv_FacturaCFD)
                            End If
                        Catch ex As Exception

                        End Try

                    Catch ex As Exception
                        'MsgBox(ex.Message & "Graba Factura Digital Global Parte 3 " & ex.Message, MsgBoxStyle.Exclamation)
                        If Graba = "0" Then
                            Llenalog(ex.Source.ToString & " " & ex.Message)
                        Else

                            Llenalog(ex.Source.ToString & " " & ex.Message)
                        End If
                    Finally
                        CONEXION.Close()
                        CONEXION.Dispose()
                    End Try

                End Using
            Else
                GloClv_FacturaCFD = ClvFacturaCfd
            End If
        Catch ex As Exception
            'MsgBox(ex.Message & "Graba Factura Digital Por Dirección Parte 3 " & ex.Message, MsgBoxStyle.Exclamation)
            If Graba = "0" Then
                Llenalog(ex.Source.ToString & " " & ex.Message)
            Else

                Llenalog(ex.Source.ToString & " " & ex.Message)
            End If
        End Try
    End Sub

    Public Shared Sub Graba_Factura_Digital_Almacen(ByVal oIdCompra As Long, ByVal oIden As Integer, ByVal locconexion As String)

        Dim ClvFacturaCfd As Long
        ClvFacturaCfd = SP_DIMESIEXISTEFACTURADIGITAL(oIdCompra, "A", locconexion)
        If ClvFacturaCfd = 0 Then '0 Es si no Existe


            Dim oTotalConPuntos As String = Nothing
            Dim oSubTotal As String = Nothing
            Dim oTotalSinPuntos As String = Nothing
            Dim oDescuento As String = Nothing
            Dim oiva As String = Nothing
            Dim oieps As String = Nothing
            Dim oTasaIva As String = Nothing
            Dim oTasaIeps As String = Nothing
            Dim oFecha As String = Nothing
            Dim oTotalImpuestos As String = Nothing

            ''
            Dim mfecha As DateTime
            Dim con As New SqlConnection(locconexion)
            Dim com As New SqlCommand("DAMEFECHADELSERVIDORFACTURADIGITAL", con)
            com.CommandType = CommandType.StoredProcedure
            com.CommandTimeout = 0
            'Dame la fecha del Servidor
            Dim prmFechaObtebida As New SqlParameter("@FECHA", SqlDbType.DateTime)
            prmFechaObtebida.Direction = ParameterDirection.Output
            prmFechaObtebida.Value = ""
            com.Parameters.Add(prmFechaObtebida)
            Try
                con.Open()
                com.ExecuteNonQuery()
                mfecha = prmFechaObtebida.Value
            Catch ex As Exception
                'MsgBox("Error: No se pudo obtener la fecha válida del Servidor.", MsgBoxStyle.Critical)
                ''MsgBox("Descripción:  Procedimiento [DAMEFECHADELSERVIDOR] // Función [DameFechaServidor] // Form: FrmAdelantarCargos", MsgBoxStyle.Information)
                ''Llenalog(ex.Source.ToString & " " & ex.Message)
                If Graba = "0" Then
                    Llenalog(ex.Source.ToString & " " & ex.Message)
                Else

                    Llenalog(ex.Source.ToString & " " & ex.Message)
                End If
            Finally
                con.Close()
            End Try
            ''

            Class1.limpiaParametros()
            Class1.CreateMyParameter("@FECHA", ParameterDirection.Output, SqlDbType.DateTime)
            Class1.ProcedimientoOutPut("DAMEFECHADELSERVIDORFACTURADIGITAL", locconexion)
            mfecha = DateTime.Parse(Class1.dicoPar("@FECHA").ToString())


            Dim CONEXION As New SqlConnection(locconexion)
            Dim COMANDO As New SqlCommand("DameFacDig_Parte_1_Almacen", CONEXION)
            COMANDO.CommandType = CommandType.StoredProcedure
            COMANDO.CommandTimeout = 0

            Dim parametro As New SqlParameter("@Clv_Factura", SqlDbType.BigInt)
            parametro.Direction = ParameterDirection.Input
            parametro.Value = oIdCompra
            COMANDO.Parameters.Add(parametro)

            Dim parametro1 As New SqlParameter("@TotalConPuntos", SqlDbType.Money)
            parametro1.Direction = ParameterDirection.Output
            parametro1.Value = 0
            COMANDO.Parameters.Add(parametro1)

            Dim parametro2 As New SqlParameter("@SubTotal", SqlDbType.Money)
            parametro2.Direction = ParameterDirection.Output
            parametro2.Value = 0
            COMANDO.Parameters.Add(parametro2)

            Dim parametro3 As New SqlParameter("@TotalSinPuntos", SqlDbType.Money)
            parametro3.Direction = ParameterDirection.Output
            parametro3.Value = 0
            COMANDO.Parameters.Add(parametro3)

            Dim parametro4 As New SqlParameter("@Descuento", SqlDbType.Money)
            parametro4.Direction = ParameterDirection.Output
            parametro4.Value = 0
            COMANDO.Parameters.Add(parametro4)

            Dim parametro5 As New SqlParameter("@iva", SqlDbType.Money)
            parametro5.Direction = ParameterDirection.Output
            parametro5.Value = 0
            COMANDO.Parameters.Add(parametro5)

            Dim parametro6 As New SqlParameter("@ieps", SqlDbType.Money)
            parametro6.Direction = ParameterDirection.Output
            parametro6.Value = 0
            COMANDO.Parameters.Add(parametro6)

            Dim parametro7 As New SqlParameter("@TasaIva", SqlDbType.Money)
            parametro7.Direction = ParameterDirection.Output
            parametro7.Value = 0
            COMANDO.Parameters.Add(parametro7)

            Dim parametro8 As New SqlParameter("@TasaIeps", SqlDbType.Money)
            parametro8.Direction = ParameterDirection.Output
            parametro8.Value = 0
            COMANDO.Parameters.Add(parametro8)

            Dim parametro9 As New SqlParameter("@Fecha", SqlDbType.DateTime)
            parametro9.Direction = ParameterDirection.Output
            parametro9.Value = 0
            COMANDO.Parameters.Add(parametro9)

            Dim parametro10 As New SqlParameter("@TotalImpuestos", SqlDbType.Money)
            parametro10.Direction = ParameterDirection.Output
            parametro10.Value = 0
            COMANDO.Parameters.Add(parametro10)

            Try
                CONEXION.Open()
                Dim i As Integer = COMANDO.ExecuteNonQuery
                oTotalConPuntos = CStr(parametro1.Value)
                oSubTotal = CStr(parametro2.Value)
                oTotalSinPuntos = CStr(parametro3.Value)
                oDescuento = CStr(parametro4.Value)
                oiva = CStr(parametro5.Value)
                oieps = CStr(parametro6.Value)
                oTasaIva = CStr(parametro7.Value)
                oTasaIeps = CStr(parametro8.Value)
                oFecha = CStr(parametro9.Value)
                oTotalImpuestos = CStr(parametro10.Value)
                'Cnx.DbConnection.Close()
            Catch ex As Exception
                ''MsgBox(ex.Message & "Graba Factura Digital Almacen Parte 1 " & ex.Message, MsgBoxStyle.Exclamation)
                If Graba = "0" Then
                    Llenalog(ex.Source.ToString & " " & ex.Message)
                Else

                    Llenalog(ex.Source.ToString & " " & ex.Message)
                End If
            Finally
                CONEXION.Close()
                CONEXION.Dispose()
            End Try

            Usp_Ed_DameDatosFacDig_Almacen(oIdCompra, locID_Compania_Mizart, locconexion)

            Dim oCFD As MizarCFD.BRL.CFD

            Dim oId_AsociadoLlave As Long = 0
            oId_AsociadoLlave = oIden 'Existe_DevuelveValor(" Select id_asociado from asociados  where id_asociado = " + CStr(oIden))


            Using Cnx As New DAConexion("HL", "sa", "sa")
                Try
                    'oIden = "1"

                    oCFD = New MizarCFD.BRL.CFD
                    oCFD.Inicializar()
                    oCFD.TipoCFD = MizarCFD.BRL.CFD.TiposCFD.Emision
                    ''Ojo 
                    'locID_Sucursal_Mizart = 0
                    'oCFD.PrepararDatosEmisorPorId(Cnx, locID_Compania_Mizart, locID_Sucursal_Mizart)
                    If locID_Sucursal_Mizart.Length > 0 And locID_Sucursal_Mizart <> "0" Then
                        oCFD.PrepararDatosEmisorPorId(Cnx, locID_Compania_Mizart, locID_Sucursal_Mizart)
                    Else
                        oCFD.PrepararDatosEmisorPorId(Cnx, locID_Compania_Mizart)
                    End If
                    oCFD.PrepararDatosReceptorPorId(Cnx, CStr(oId_AsociadoLlave), 1, 0)


                    oCFD.EsTimbrePrueba = EsTimbrePrueba
                    oCFD.Timbrador = CFD.PAC.Mizar

                    oCFD.Version = GloVERSON
                    If GloMONEDA = "USD" Then
                        oCFD.IdMoneda = 2
                    Else
                        oCFD.IdMoneda = 1  '//1 = Pesos , Pesos = 2 USD
                    End If

                    'oFecha
                    'oCFD.Fecha = mfecha '//Date.Now
                    oCFD.FormaDePago = GloFORMADEPAGO
                    'oCFD.Certificado = ""
                    oCFD.CondicionesDePago = GloCONDICIONESDEPAGO
                    oCFD.SubTotal = oSubTotal
                    oCFD.Descuento = oDescuento
                    oCFD.MotivoDescuento = GloMOTIVODESCUENTO
                    oCFD.Total = oTotalSinPuntos
                    oCFD.MetodoDePago = GloMETODODEPAGO
                    oCFD.TipoDeComprobante = GloTIPODECOMPROBANTE
                    oCFD.TipoComprobante33 = GloTIPODECOMPROBANTE
                    oCFD.PagoEnParcialiades = GloPAGOENPARCIALIDADES
                    oCFD.LugarExpedicion = GloPAGOENPARCIALIDADES
                    oCFD.CuentaPago = GlonumCtaPago

                    oCFD.Moneda = GloMONEDA
                    oCFD.UsoCFDI = GloUSOCFD
                    oCFD.EsNotaCredito = 0
                    oCFD.TipoRelacion = Nothing
                    oCFD.UUIDRelacionados = Nothing
                    'oCFD.FechaVencimiento = Date.Parse(GloFECHAVIGENCIA)
                    oCFD.CondicionesDePago = ""
                    oCFD.TipoCambio = GloTIPOCAMBIO


                    Dim oRegimen As New MizarCFD.BRL.CFDRegimenesFiscales
                    oRegimen.IdRegimen = 0
                    oRegimen.IdCFD = 0
                    oRegimen.Regimen = GloREGIMEN
                    'oRegimen.Insertar(Cnx)

                    oCFD.RegimenesFiscales.Add(oRegimen)

                    'oCFD.Complemento.XMLComplemento = "<raiz>Caelli Enarrant Gloria Dei </raiz>"
                    'oCFD.Addenda.XMLAddenda = "<edi>Edit dididididididi</edi>"

                    oCFD.Impuestos.TotalImpuestosRetenidos = 0
                    oCFD.Impuestos.TotalImpuestosTrasladados = oTotalImpuestos
                    'Dim oimpuestosRetenciones As CFDImpuestosRetenciones
                    'oimpuestosRetenciones = New CFDImpuestosRetenciones()
                    'oimpuestosRetenciones.Impuesto = "IVA"
                    'oimpuestosRetenciones.Importe = oiva
                    'oCFD.Impuestos.Retenciones.Add(oimpuestosRetenciones)

                    Dim oImpuestosTraslados As CFDImpuestosTraslados

                    'oImpuestosTraslados = New CFDImpuestosTraslados()
                    'oImpuestosTraslados.Impuesto = "IVA"
                    'oImpuestosTraslados.Importe = oiva
                    'oImpuestosTraslados.Tasa = oTasaIva

                    'oCFD.Impuestos.Traslados.Add(oImpuestosTraslados)

                    'oImpuestosTraslados = New CFDImpuestosTraslados()
                    'oImpuestosTraslados.Impuesto = "IEPS"
                    'oImpuestosTraslados.Importe = oieps
                    'oImpuestosTraslados.Tasa = oTasaIeps

                    'oCFD.Impuestos.Traslados.Add(oImpuestosTraslados)

                    Dim oConcepto As CFDConceptos
                    Dim oContrador As Integer = 1

                    Dim CONEXION2 As New SqlConnection(locconexion)
                    Dim COMANDO2 As New SqlCommand("exec DameFacDig_Parte_2_Almacen " + CStr(oIdCompra), CONEXION2)
                    COMANDO2.CommandType = CommandType.Text
                    COMANDO2.CommandTimeout = 0
                    Try
                        CONEXION2.Open()
                        Dim reader As SqlDataReader = COMANDO2.ExecuteReader
                        '    'Recorremos los Cargos obtenidos desde el Procedimiento
                        While (reader.Read())
                            oConcepto = New CFDConceptos()
                            oConcepto.Cantidad = reader(0).ToString
                            'oConcepto.UnidadMedida = "No Aplica"
                            oConcepto.NumeroIdentificacion = reader(1).ToString
                            oConcepto.Descripcion = reader(2).ToString
                            oConcepto.ValorUnitario = reader(3).ToString
                            oConcepto.Importe = reader(4).ToString
                            oConcepto.TrasladarIVA = "1"

                            'Los nuevos
                            oConcepto.Descuento = 0 'Valor Nuevo
                            ' oConcepto.ClaveProdServ = reader(5).ToString ' "83111801" 'Campo nuevo
                            oConcepto.idClaveProdServ = reader(5).ToString
                            'Iva
                            'oConcepto.IVATRAS_TipoFactor = reader(6).ToString
                            oConcepto.TipoFactor = reader(6).ToString
                            oConcepto.IVATRAS_Importe = reader(8).ToString 'Iva del ticket
                            'oConcepto.IVATRAS_TasaOCuota = reader(7).ToString '"0.160000"


                            oConcepto.TasaIVA = reader(7).ToString
                            oConcepto.TrasladarIVA = "1"
                            'Ieps
                            'oConcepto.IEPSTRAS_TipoFactor = reader(9).ToString '"Tasa"
                            'oConcepto.IEPSTRAS_TasaOCuota = reader(10).ToString '"0.030000"
                            If Double.Parse(reader(11).ToString) > 0 Then
                                oConcepto.BaseIEPS = reader(4).ToString 'Cantida Subtotal 
                                'oConcepto.BaseIVA = CDec(reader(4).ToString) + CDec(reader(11).ToString)
                                If SumaSubtotalYIeps = True Then
                                    oConcepto.BaseIVA = CDec(reader(4).ToString) + CDec(reader(11).ToString)
                                Else
                                    oConcepto.BaseIVA = CDec(reader(4).ToString)
                                End If

                                oConcepto.IEPSTRAS_Importe = reader(11).ToString 'Total del ieps 'Ieps del ticket
                                oConcepto.TasaIEPS = reader(10).ToString

                                oConcepto.FactorIEPS = reader(9).ToString
                                oConcepto.TrasladarIEPS = "1"
                            Else
                                oConcepto.BaseIEPS = 0 'Cantida Subtotal 
                                oConcepto.BaseIVA = reader(4).ToString
                                oConcepto.IEPSTRAS_Importe = "0" 'Total del ieps 'Ieps del ticket
                                oConcepto.TrasladarIEPS = "0"
                            End If
                            oConcepto.idUnidadMedida = reader(13).ToString '"E48" "Unidad de servicio" '"No Aplica" 'Cambio  Id que corresponde
                            oConcepto.UnidadMedida = reader(12).ToString '"Unidad de servicio" ' Pruebo poner una al gusto

                            oConcepto.IVARET_Importe = "0"
                            oConcepto.ISRRET_Importe = "0"

                            'Trasladados
                            If Double.Parse(reader(8).ToString) > 0 Then
                                oImpuestosTraslados = New CFDImpuestosTraslados()
                                oImpuestosTraslados.IdRegistroTraslado = oContrador
                                oImpuestosTraslados.Impuesto = "IVA"
                                oImpuestosTraslados.Importe = reader(8).ToString
                                oImpuestosTraslados.Tasa = reader(7).ToString 'oTasaIva
                                'oImpuestosTraslados.TipoFactor = GloTIPOFACTOR
                                oImpuestosTraslados.TasaOCuota = GloTIPOFACTOR
                                oImpuestosTraslados.IEPSTRAS_Importe = "0"
                                oImpuestosTraslados.IVATRAS_Importe = "0"

                                oCFD.Impuestos.Traslados.Add(oImpuestosTraslados)
                            End If
                            If Double.Parse(reader(11).ToString) > 0 Then
                                oImpuestosTraslados = New CFDImpuestosTraslados()
                                oImpuestosTraslados.IdRegistroTraslado = oContrador + 1
                                oImpuestosTraslados.Impuesto = "IEPS" '"003" '"IEPS"
                                oImpuestosTraslados.Importe = reader(11).ToString
                                oImpuestosTraslados.Tasa = reader(10).ToString 'oTasaIeps
                                oImpuestosTraslados.TasaOCuota = GloTIPOFACTOR
                                oImpuestosTraslados.FactorIepsTras = GloTIPOFACTOR
                                oImpuestosTraslados.IEPSTRAS_Importe = "0"
                                oImpuestosTraslados.IVATRAS_Importe = "0"
                                'oImpuestosTraslados.TipoFactor = GloTIPOFACTOR

                                oCFD.Impuestos.Traslados.Add(oImpuestosTraslados)
                            End If

                            oConcepto.RetenerIVA = "0"
                            oConcepto.RetenerISR = "0"

                            oCFD.Conceptos.Add(oConcepto)
                            oContrador = oContrador + 2
                        End While
                        'Cnx.DbConnection.Close()
                    Catch ex As Exception
                        ''MsgBox(ex.Message & "Graba Factura Digital Global Parte 2 " & ex.Message, MsgBoxStyle.Exclamation)
                        If Graba = "0" Then
                            Llenalog(ex.Source.ToString & " " & ex.Message)
                        Else

                            Llenalog(ex.Source.ToString & " " & ex.Message)
                        End If
                    Finally
                        CONEXION2.Close()
                        CONEXION2.Dispose()
                    End Try
                    Dim bnd As Boolean = False
                    Dim Mensaje1 As String = ""
                    If Not oCFD.Insertar(Cnx, True, False) Then
                        For Each oMensaje As DAMensajesSistema.RegistroMensaje In oCFD.MensajesSistema.ListaMensajes
                            Mensaje1 = oMensaje.TextoMensaje + " " + oMensaje.Contexto
                            Inserta_Tbl_FacturasNoGeneradas(oIdCompra, "A", Mensaje1, locconexion)
                            'MessageBox.Show(oMensaje.TextoMensaje + " " + oMensaje.Contexto)
                            bnd = True
                        Next
                        Return
                    End If
                    'If Not oCFD.Insertar(Cnx, True) Then
                    '    For Each oMensaje As DAMensajesSistema.RegistroMensaje In oCFD.MensajesSistema.ListaMensajes
                    '        MessageBox.Show(oMensaje.TextoMensaje + " " + oMensaje.Contexto)
                    '    Next
                    '    Return
                    'End If



                    GloClv_FacturaCFD = 0
                    GloClv_FacturaCFD = oCFD.IdCFD
                    UPS_Inserta_Rel_FacturasCFD(oIdCompra, GloClv_FacturaCFD, "", "A", locconexion)
                    GloTxtUlt4DigCheque = ""
                    Dim r As New Globalization.CultureInfo("es-MX")

                    r.DateTimeFormat.ShortDatePattern = "dd/MM/yyyy"

                    System.Threading.Thread.CurrentThread.CurrentCulture = r
                    Try
                        ' oCFD.EnviarEmail(Cnx)
                        If GloClv_FacturaCFD > 0 And bnd = False Then
                            ENVIAR_Factura(GloClv_FacturaCFD)
                        End If

                    Catch ex As Exception

                    End Try

                Catch ex As Exception
                    ''MsgBox(ex.Message & "Graba Factura Digital Almacen Parte 3 " & ex.Message, MsgBoxStyle.Exclamation)
                    If Graba = "0" Then
                        Llenalog(ex.Source.ToString & " " & ex.Message)
                    Else

                        Llenalog(ex.Source.ToString & " " & ex.Message)
                    End If
                Finally
                    CONEXION.Close()
                    CONEXION.Dispose()
                End Try

            End Using
        Else
            GloClv_FacturaCFD = ClvFacturaCfd
        End If

    End Sub

    Public Shared Sub Graba_Factura_NotaGlobal(ByVal oClv_Nota As Long, ByVal oIden As Integer, ByVal locconexion As String)

        Dim ClvFacturaCfd As Long = 0
        'ClvFacturaCfd = SP_DIMESIEXISTEFACTURADIGITAL(oClv_Nota, "O", locconexion)
        If ClvFacturaCfd = 0 Then '0 Es si no Existe

            Dim oTotalConPuntos As String = Nothing
            Dim oSubTotal As String = Nothing
            Dim oTotalSinPuntos As String = Nothing
            Dim oDescuento As String = Nothing
            Dim oiva As String = Nothing
            Dim oieps As String = Nothing
            Dim oTasaIva As String = Nothing
            Dim oTasaIeps As String = Nothing
            Dim oFecha As String = Nothing
            Dim oTotalImpuestos As String = Nothing

            ''
            Dim mfecha As DateTime
            Dim con As New SqlConnection(locconexion)
            Dim com As New SqlCommand("DAMEFECHADELSERVIDORFACTURADIGITAL", con)
            com.CommandType = CommandType.StoredProcedure
            'Dame la fecha del Servidor
            Dim prmFechaObtebida As New SqlParameter("@FECHA", SqlDbType.DateTime)
            prmFechaObtebida.Direction = ParameterDirection.Output
            prmFechaObtebida.Value = ""
            com.Parameters.Add(prmFechaObtebida)
            Try
                con.Open()
                com.ExecuteNonQuery()
                mfecha = prmFechaObtebida.Value
            Catch ex As Exception
                ''MsgBox("Error: No se pudo obtener la fecha válida del Servidor.", MsgBoxStyle.Critical)
                If Graba = "0" Then
                    Llenalog(ex.Source.ToString & " " & ex.Message)
                Else

                    Llenalog(ex.Source.ToString & " " & ex.Message)
                End If
                ''MsgBox("Descripción:  Procedimiento [DAMEFECHADELSERVIDOR] // Función [DameFechaServidor] // Form: FrmAdelantarCargos", MsgBoxStyle.Information)
                ''Llenalog(ex.Source.ToString & " " & ex.Message)
            Finally
                con.Close()
            End Try
            ''

            Class1.limpiaParametros()
            Class1.CreateMyParameter("@FECHA", ParameterDirection.Output, SqlDbType.DateTime)
            Class1.ProcedimientoOutPut("DAMEFECHADELSERVIDORFACTURADIGITAL", locconexion)
            mfecha = DateTime.Parse(Class1.dicoPar("@FECHA").ToString())


            Dim CONEXION As New SqlConnection(locconexion)
            Dim COMANDO As New SqlCommand("DameFacDig_Parte_1NotaGlobal", CONEXION)
            COMANDO.CommandType = CommandType.StoredProcedure
            COMANDO.CommandTimeout = 0
            Dim parametro As New SqlParameter("@Id_Cfd", SqlDbType.BigInt)
            parametro.Direction = ParameterDirection.Input
            parametro.Value = oClv_Nota
            COMANDO.Parameters.Add(parametro)

            Dim parametro1 As New SqlParameter("@TotalConPuntos", SqlDbType.Money)
            parametro1.Direction = ParameterDirection.Output
            parametro1.Value = 0
            COMANDO.Parameters.Add(parametro1)

            Dim parametro2 As New SqlParameter("@SubTotal", SqlDbType.Money)
            parametro2.Direction = ParameterDirection.Output
            parametro2.Value = 0
            COMANDO.Parameters.Add(parametro2)

            Dim parametro3 As New SqlParameter("@TotalSinPuntos", SqlDbType.Money)
            parametro3.Direction = ParameterDirection.Output
            parametro3.Value = 0
            COMANDO.Parameters.Add(parametro3)

            Dim parametro4 As New SqlParameter("@Descuento", SqlDbType.Money)
            parametro4.Direction = ParameterDirection.Output
            parametro4.Value = 0
            COMANDO.Parameters.Add(parametro4)

            Dim parametro5 As New SqlParameter("@iva", SqlDbType.Money)
            parametro5.Direction = ParameterDirection.Output
            parametro5.Value = 0
            COMANDO.Parameters.Add(parametro5)

            Dim parametro6 As New SqlParameter("@ieps", SqlDbType.Money)
            parametro6.Direction = ParameterDirection.Output
            parametro6.Value = 0
            COMANDO.Parameters.Add(parametro6)

            Dim parametro7 As New SqlParameter("@TasaIva", SqlDbType.VarChar, 6)
            parametro7.Direction = ParameterDirection.Output
            parametro7.Value = 0
            COMANDO.Parameters.Add(parametro7)

            Dim parametro8 As New SqlParameter("@TasaIeps", SqlDbType.VarChar, 6)
            parametro8.Direction = ParameterDirection.Output
            parametro8.Value = 0
            COMANDO.Parameters.Add(parametro8)

            Dim parametro9 As New SqlParameter("@Fecha", SqlDbType.DateTime)
            parametro9.Direction = ParameterDirection.Output
            parametro9.Value = 0
            COMANDO.Parameters.Add(parametro9)

            Dim parametro10 As New SqlParameter("@TotalImpuestos", SqlDbType.Money)
            parametro10.Direction = ParameterDirection.Output
            parametro10.Value = 0
            COMANDO.Parameters.Add(parametro10)

            Try
                CONEXION.Open()
                Dim i As Integer = COMANDO.ExecuteNonQuery
                oTotalConPuntos = CStr(parametro1.Value)
                oSubTotal = CStr(parametro2.Value)
                oTotalSinPuntos = CStr(parametro3.Value)
                oDescuento = CStr(parametro4.Value)
                oiva = CStr(parametro5.Value)
                oieps = CStr(parametro6.Value)
                oTasaIva = CStr(parametro7.Value)
                oTasaIeps = CStr(parametro8.Value)
                oFecha = CStr(parametro9.Value)
                oTotalImpuestos = CStr(parametro10.Value)
                'Cnx.DbConnection.Close()
            Catch ex As Exception
                'MsgBox(ex.Message & "Nota Parte 1 " & ex.Message, MsgBoxStyle.Exclamation)
                If Graba = "0" Then
                    Llenalog(ex.Source.ToString & " " & ex.Message)
                Else

                    Llenalog(ex.Source.ToString & " " & ex.Message)
                End If
            Finally
                CONEXION.Close()
                CONEXION.Dispose()
            End Try

            Usp_Ed_DameDatosFacDigNotaGlobal(oClv_Nota, locID_Compania_Mizart, locconexion)

            Dim oCFD As MizarCFD.BRL.CFD

            Dim oId_AsociadoLlave As Long = 0
            oId_AsociadoLlave = oIden 'Existe_DevuelveValor(" Select id_asociado from asociados  where id_asociado = " + CStr(oIden))


            Using Cnx As New DAConexion("HL", "sa", "sa")
                Try
                    'oIden = "1"

                    oCFD = New MizarCFD.BRL.CFD
                    oCFD.Inicializar()
                    oCFD.TipoCFD = MizarCFD.BRL.CFD.TiposCFD.Emision
                    ''Ojo 
                    'locID_Sucursal_Mizart = 0
                    'oCFD.PrepararDatosEmisorPorId(Cnx, locID_Compania_Mizart, locID_Sucursal_Mizart)
                    If locID_Sucursal_Mizart.Length > 0 And locID_Sucursal_Mizart <> "0" Then
                        oCFD.PrepararDatosEmisorPorId(Cnx, locID_Compania_Mizart, locID_Sucursal_Mizart)
                    Else
                        oCFD.PrepararDatosEmisorPorId(Cnx, locID_Compania_Mizart)
                    End If
                    oCFD.PrepararDatosReceptorPorId(Cnx, CStr(oId_AsociadoLlave), 1, 0)

                    oCFD.EsNotaCredito = "1"

                    oCFD.EsTimbrePrueba = EsTimbrePrueba
                    oCFD.Timbrador = CFD.PAC.Mizar

                    oCFD.Version = GloVERSON
                    oCFD.IdMoneda = 1  '//1 = Pesos , Pesos = 2 USD
                    'oFecha
                    'oCFD.Fecha = mfecha '//Date.Now
                    oCFD.FormaDePago = GloFORMADEPAGO
                    'oCFD.Certificado = ""
                    oCFD.CondicionesDePago = GloCONDICIONESDEPAGO
                    oCFD.SubTotal = oSubTotal
                    oCFD.Descuento = oDescuento
                    oCFD.MotivoDescuento = GloMOTIVODESCUENTO
                    oCFD.Total = oTotalSinPuntos
                    oCFD.MetodoDePago = GloMETODODEPAGO
                    oCFD.TipoDeComprobante = GloTIPODECOMPROBANTE
                    oCFD.TipoComprobante33 = GloTIPODECOMPROBANTE
                    oCFD.PagoEnParcialiades = GloPAGOENPARCIALIDADES
                    oCFD.LugarExpedicion = GloPAGOENPARCIALIDADES
                    oCFD.CuentaPago = GlonumCtaPago

                    'Parte especial de las notas
                    oCFD.Moneda = GloMONEDA
                    oCFD.UsoCFDI = GloUSOCFD
                    oCFD.TipoRelacion = "01"
                    oCFD.UUIDRelacionados = GloUUIDRel
                    oCFD.UUIDRel.Add(GloUUIDRel)

                    'oCFD.FechaVencimiento = Date.Parse(GloFECHAVIGENCIA)
                    'oCFD.CondicionesDePago = ""
                    oCFD.TipoCambio = GloTIPOCAMBIO

                    Dim oRegimen As New MizarCFD.BRL.CFDRegimenesFiscales
                    oRegimen.IdRegimen = 0
                    oRegimen.IdCFD = 0
                    oRegimen.Regimen = GloREGIMEN
                    'oRegimen.Insertar(Cnx)

                    oCFD.RegimenesFiscales.Add(oRegimen)

                    'oCFD.Complemento.XMLComplemento = "<raiz>Caelli Enarrant Gloria Dei </raiz>"
                    'oCFD.Addenda.XMLAddenda = "<edi>Edit dididididididi</edi>"

                    oCFD.Impuestos.TotalImpuestosRetenidos = 0
                    oCFD.Impuestos.TotalImpuestosTrasladados = oTotalImpuestos
                    'Dim oimpuestosRetenciones As CFDImpuestosRetenciones
                    'oimpuestosRetenciones = New CFDImpuestosRetenciones()
                    'oimpuestosRetenciones.Impuesto = "IVA"
                    'oimpuestosRetenciones.Importe = oiva
                    'oCFD.Impuestos.Retenciones.Add(oimpuestosRetenciones)

                    Dim oImpuestosTraslados As CFDImpuestosTraslados

                    'oImpuestosTraslados = New CFDImpuestosTraslados()
                    'oImpuestosTraslados.Impuesto = "IVA"
                    'oImpuestosTraslados.Importe = oiva
                    'oImpuestosTraslados.Tasa = oTasaIva

                    'oCFD.Impuestos.Traslados.Add(oImpuestosTraslados)

                    'oImpuestosTraslados = New CFDImpuestosTraslados()
                    'oImpuestosTraslados.Impuesto = "IEPS"
                    'oImpuestosTraslados.Importe = oieps
                    'oImpuestosTraslados.Tasa = oTasaIeps

                    'oCFD.Impuestos.Traslados.Add(oImpuestosTraslados)

                    Dim oConcepto As CFDConceptos
                    Dim oContrador As Integer = 1

                    Dim CONEXION2 As New SqlConnection(locconexion)
                    Dim COMANDO2 As New SqlCommand("exec DameFacDig_Parte_2NotaGlobal " + CStr(oClv_Nota), CONEXION2)
                    COMANDO2.CommandType = CommandType.Text
                    Try
                        CONEXION2.Open()
                        Dim reader As SqlDataReader = COMANDO2.ExecuteReader
                        '    'Recorremos los Cargos obtenidos desde el Procedimiento
                        While (reader.Read())
                            oConcepto = New CFDConceptos()
                            oConcepto.Cantidad = reader(0).ToString
                            oConcepto.UnidadMedida = "No Aplica"
                            oConcepto.NumeroIdentificacion = reader(1).ToString
                            oConcepto.Descripcion = reader(2).ToString
                            oConcepto.ValorUnitario = reader(3).ToString
                            oConcepto.Importe = reader(4).ToString
                            oConcepto.TrasladarIVA = "1"

                            'Los nuevos
                            oConcepto.Descuento = 0 'Valor Nuevo
                            ' oConcepto.ClaveProdServ = reader(5).ToString ' "83111801" 'Campo nuevo
                            oConcepto.idClaveProdServ = reader(5).ToString
                            'Iva
                            'oConcepto.IVATRAS_TipoFactor = reader(6).ToString
                            oConcepto.TipoFactor = reader(6).ToString
                            oConcepto.IVATRAS_Importe = reader(8).ToString 'Iva del ticket
                            'oConcepto.IVATRAS_TasaOCuota = reader(7).ToString '"0.160000"


                            oConcepto.TasaIVA = reader(7).ToString
                            oConcepto.TrasladarIVA = "1"
                            'Ieps
                            'oConcepto.IEPSTRAS_TipoFactor = reader(9).ToString '"Tasa"
                            'oConcepto.IEPSTRAS_TasaOCuota = reader(10).ToString '"0.030000"
                            If Double.Parse(reader(11).ToString) > 0 Then
                                oConcepto.BaseIEPS = reader(4).ToString 'Cantida Subtotal 
                                'oConcepto.BaseIVA = CDec(reader(4).ToString) + CDec(reader(11).ToString)
                                If SumaSubtotalYIeps = True Then
                                    oConcepto.BaseIVA = CDec(reader(4).ToString) + CDec(reader(11).ToString)
                                Else
                                    oConcepto.BaseIVA = CDec(reader(4).ToString)
                                End If

                                oConcepto.IEPSTRAS_Importe = reader(11).ToString 'Total del ieps 'Ieps del ticket
                                oConcepto.TasaIEPS = reader(10).ToString

                                oConcepto.FactorIEPS = reader(9).ToString
                                oConcepto.TrasladarIEPS = "1"
                            Else
                                oConcepto.BaseIEPS = 0 'Cantida Subtotal 
                                oConcepto.BaseIVA = reader(4).ToString
                                oConcepto.IEPSTRAS_Importe = "0" 'Total del ieps 'Ieps del ticket
                                oConcepto.TrasladarIEPS = "0"
                            End If
                            oConcepto.idUnidadMedida = reader(13).ToString '"E48" "Unidad de servicio" '"No Aplica" 'Cambio  Id que corresponde
                            'oConcepto.UnidadDesc = reader(12).ToString '"Unidad de servicio" ' Pruebo poner una al gusto

                            oConcepto.IVARET_Importe = "0"
                            oConcepto.ISRRET_Importe = "0"

                            'Trasladados
                            If Double.Parse(reader(8).ToString) > 0 Then
                                oImpuestosTraslados = New CFDImpuestosTraslados()
                                oImpuestosTraslados.IdRegistroTraslado = oContrador
                                oImpuestosTraslados.Impuesto = "IVA"
                                oImpuestosTraslados.Importe = reader(8).ToString
                                oImpuestosTraslados.Tasa = reader(7).ToString 'oTasaIva
                                'oImpuestosTraslados.TipoFactor = GloTIPOFACTOR
                                oImpuestosTraslados.TasaOCuota = GloTIPOFACTOR
                                oImpuestosTraslados.IEPSTRAS_Importe = "0"
                                oImpuestosTraslados.IVATRAS_Importe = "0"

                                oCFD.Impuestos.Traslados.Add(oImpuestosTraslados)
                            End If
                            If Double.Parse(reader(11).ToString) > 0 Then
                                oImpuestosTraslados = New CFDImpuestosTraslados()
                                oImpuestosTraslados.IdRegistroTraslado = oContrador + 1
                                oImpuestosTraslados.Impuesto = "IEPS" '"003" '"IEPS"
                                oImpuestosTraslados.Importe = reader(11).ToString
                                oImpuestosTraslados.Tasa = reader(10).ToString 'oTasaIeps
                                oImpuestosTraslados.TasaOCuota = GloTIPOFACTOR
                                oImpuestosTraslados.FactorIepsTras = GloTIPOFACTOR
                                oImpuestosTraslados.IEPSTRAS_Importe = "0"
                                oImpuestosTraslados.IVATRAS_Importe = "0"
                                'oImpuestosTraslados.TipoFactor = GloTIPOFACTOR

                                oCFD.Impuestos.Traslados.Add(oImpuestosTraslados)
                            End If



                            oConcepto.RetenerIVA = "0"
                            oConcepto.RetenerISR = "0"

                            oCFD.Conceptos.Add(oConcepto)
                            oContrador = oContrador + 2
                        End While
                        'Cnx.DbConnection.Close()
                    Catch ex As Exception
                        'MsgBox(ex.Message & "Nota Parte 2 " & ex.Message, MsgBoxStyle.Exclamation)
                        If Graba = "0" Then
                            Llenalog(ex.Source.ToString & " " & ex.Message)
                        Else

                            Llenalog(ex.Source.ToString & " " & ex.Message)
                        End If
                    Finally
                        CONEXION2.Close()
                        CONEXION2.Dispose()
                    End Try
                    'Dim a As String = JsonConvert.SerializeObject(oCFD)
                    Dim Bnd As Boolean = False
                    Dim Mensaje1 As String = ""
                    If Not oCFD.Insertar(Cnx, True, False) Then
                        For Each oMensaje As DAMensajesSistema.RegistroMensaje In oCFD.MensajesSistema.ListaMensajes
                            Mensaje1 = oMensaje.TextoMensaje + " " + oMensaje.Contexto
                            Inserta_Tbl_FacturasNoGeneradas(oClv_Nota, "5", Mensaje1, locconexion)
                            'MessageBox.Show(oMensaje.TextoMensaje + " " + oMensaje.Contexto)
                            Bnd = True
                        Next
                        Return
                    End If
                    'If Not oCFD.Insertar(Cnx, True) Then
                    '    For Each oMensaje As DAMensajesSistema.RegistroMensaje In oCFD.MensajesSistema.ListaMensajes
                    '        MessageBox.Show(oMensaje.TextoMensaje + " " + oMensaje.Contexto)
                    '    Next
                    '    Return
                    'End If



                    GloClv_FacturaCFD = 0
                    GloClv_FacturaCFD = oCFD.IdCFD
                    UPS_Inserta_Rel_FacturasCFD(oClv_Nota, GloClv_FacturaCFD, "", "5", locconexion)
                    GloTxtUlt4DigCheque = ""
                    Dim r As New Globalization.CultureInfo("es-MX")

                    r.DateTimeFormat.ShortDatePattern = "dd/MM/yyyy"

                    System.Threading.Thread.CurrentThread.CurrentCulture = r
                    Try
                        'oCFD.EnviarEmail(Cnx)
                        'ENVIAR_Factura(GloClv_FacturaCFD)
                        If GloClv_FacturaCFD > 0 And Bnd = False Then
                            ENVIAR_Factura(GloClv_FacturaCFD)
                        End If
                    Catch ex As Exception

                    End Try

                Catch ex As Exception
                    'MsgBox(ex.Message & "Nota Parte 3 " & ex.Message, MsgBoxStyle.Exclamation)
                    If Graba = "0" Then
                        Llenalog(ex.Source.ToString & " " & ex.Message)
                    Else

                        Llenalog(ex.Source.ToString & " " & ex.Message)
                    End If
                Finally
                    CONEXION.Close()
                    CONEXION.Dispose()
                End Try

            End Using
        Else
            GloClv_FacturaCFD = ClvFacturaCfd
        End If
    End Sub

    Public Shared Sub Graba_Factura_Nota(ByVal oClv_Nota As Long, ByVal oIden As Integer, ByVal locconexion As String)

        Dim ClvFacturaCfd As Long
        ClvFacturaCfd = SP_DIMESIEXISTEFACTURADIGITAL(oClv_Nota, "O", locconexion)
        If ClvFacturaCfd = 0 Then '0 Es si no Existe

            Dim oTotalConPuntos As String = Nothing
            Dim oSubTotal As String = Nothing
            Dim oTotalSinPuntos As String = Nothing
            Dim oDescuento As String = Nothing
            Dim oiva As String = Nothing
            Dim oieps As String = Nothing
            Dim oTasaIva As String = Nothing
            Dim oTasaIeps As String = Nothing
            Dim oFecha As String = Nothing
            Dim oTotalImpuestos As String = Nothing

            ''
            Dim mfecha As DateTime
            Dim con As New SqlConnection(locconexion)
            Dim com As New SqlCommand("DAMEFECHADELSERVIDORFACTURADIGITAL", con)
            com.CommandType = CommandType.StoredProcedure
            com.CommandTimeout = 0
            'Dame la fecha del Servidor
            Dim prmFechaObtebida As New SqlParameter("@FECHA", SqlDbType.DateTime)
            prmFechaObtebida.Direction = ParameterDirection.Output
            prmFechaObtebida.Value = ""
            com.Parameters.Add(prmFechaObtebida)
            Try
                con.Open()
                com.ExecuteNonQuery()
                mfecha = prmFechaObtebida.Value
            Catch ex As Exception
                ''MsgBox("Error: No se pudo obtener la fecha válida del Servidor.", MsgBoxStyle.Critical)
                If Graba = "0" Then
                    Llenalog(ex.Source.ToString & " " & ex.Message)
                Else

                    Llenalog(ex.Source.ToString & " " & ex.Message)
                End If
                ''MsgBox("Descripción:  Procedimiento [DAMEFECHADELSERVIDOR] // Función [DameFechaServidor] // Form: FrmAdelantarCargos", MsgBoxStyle.Information)
                ''Llenalog(ex.Source.ToString & " " & ex.Message)
            Finally
                con.Close()
            End Try
            ''

            Class1.limpiaParametros()
            Class1.CreateMyParameter("@FECHA", ParameterDirection.Output, SqlDbType.DateTime)
            Class1.ProcedimientoOutPut("DAMEFECHADELSERVIDORFACTURADIGITAL", locconexion)
            mfecha = DateTime.Parse(Class1.dicoPar("@FECHA").ToString())


            Dim CONEXION As New SqlConnection(locconexion)
            Dim COMANDO As New SqlCommand("DameFacDig_Parte_1Nota", CONEXION)
            COMANDO.CommandType = CommandType.StoredProcedure
            COMANDO.CommandTimeout = 0

            Dim parametro As New SqlParameter("@Clv_Nota", SqlDbType.BigInt)
            parametro.Direction = ParameterDirection.Input
            parametro.Value = oClv_Nota
            COMANDO.Parameters.Add(parametro)

            Dim parametro1 As New SqlParameter("@TotalConPuntos", SqlDbType.Money)
            parametro1.Direction = ParameterDirection.Output
            parametro1.Value = 0
            COMANDO.Parameters.Add(parametro1)

            Dim parametro2 As New SqlParameter("@SubTotal", SqlDbType.Money)
            parametro2.Direction = ParameterDirection.Output
            parametro2.Value = 0
            COMANDO.Parameters.Add(parametro2)

            Dim parametro3 As New SqlParameter("@TotalSinPuntos", SqlDbType.Money)
            parametro3.Direction = ParameterDirection.Output
            parametro3.Value = 0
            COMANDO.Parameters.Add(parametro3)

            Dim parametro4 As New SqlParameter("@Descuento", SqlDbType.Money)
            parametro4.Direction = ParameterDirection.Output
            parametro4.Value = 0
            COMANDO.Parameters.Add(parametro4)

            Dim parametro5 As New SqlParameter("@iva", SqlDbType.Money)
            parametro5.Direction = ParameterDirection.Output
            parametro5.Value = 0
            COMANDO.Parameters.Add(parametro5)

            Dim parametro6 As New SqlParameter("@ieps", SqlDbType.Money)
            parametro6.Direction = ParameterDirection.Output
            parametro6.Value = 0
            COMANDO.Parameters.Add(parametro6)

            Dim parametro7 As New SqlParameter("@TasaIva", SqlDbType.Money)
            parametro7.Direction = ParameterDirection.Output
            parametro7.Value = 0
            COMANDO.Parameters.Add(parametro7)

            Dim parametro8 As New SqlParameter("@TasaIeps", SqlDbType.Money)
            parametro8.Direction = ParameterDirection.Output
            parametro8.Value = 0
            COMANDO.Parameters.Add(parametro8)

            Dim parametro9 As New SqlParameter("@Fecha", SqlDbType.DateTime)
            parametro9.Direction = ParameterDirection.Output
            parametro9.Value = 0
            COMANDO.Parameters.Add(parametro9)

            Dim parametro10 As New SqlParameter("@TotalImpuestos", SqlDbType.Money)
            parametro10.Direction = ParameterDirection.Output
            parametro10.Value = 0
            COMANDO.Parameters.Add(parametro10)

            Try
                CONEXION.Open()
                Dim i As Integer = COMANDO.ExecuteNonQuery
                oTotalConPuntos = CStr(parametro1.Value)
                oSubTotal = CStr(parametro2.Value)
                oTotalSinPuntos = CStr(parametro3.Value)
                oDescuento = CStr(parametro4.Value)
                oiva = CStr(parametro5.Value)
                oieps = CStr(parametro6.Value)
                oTasaIva = CStr(parametro7.Value)
                oTasaIeps = CStr(parametro8.Value)
                oFecha = CStr(parametro9.Value)
                oTotalImpuestos = CStr(parametro10.Value)
                'Cnx.DbConnection.Close()
            Catch ex As Exception
                'MsgBox(ex.Message & "Nota Parte 1 " & ex.Message, MsgBoxStyle.Exclamation)
                If Graba = "0" Then
                    Llenalog(ex.Source.ToString & " " & ex.Message)
                Else

                    Llenalog(ex.Source.ToString & " " & ex.Message)
                End If
            Finally
                CONEXION.Close()
                CONEXION.Dispose()
            End Try

            Usp_Ed_DameDatosFacDigNota(oClv_Nota, locID_Compania_Mizart, locconexion)

            Dim oCFD As MizarCFD.BRL.CFD

            Dim oId_AsociadoLlave As Long = 0
            oId_AsociadoLlave = oIden 'Existe_DevuelveValor(" Select id_asociado from asociados  where id_asociado = " + CStr(oIden))


            Using Cnx As New DAConexion("HL", "sa", "sa")
                Try
                    'oIden = "1"

                    oCFD = New MizarCFD.BRL.CFD
                    oCFD.Inicializar()
                    oCFD.TipoCFD = MizarCFD.BRL.CFD.TiposCFD.Emision
                    ''Ojo 
                    'locID_Sucursal_Mizart = 0
                    'oCFD.PrepararDatosEmisorPorId(Cnx, locID_Compania_Mizart, locID_Sucursal_Mizart)
                    If locID_Sucursal_Mizart.Length > 0 And locID_Sucursal_Mizart <> "0" Then
                        oCFD.PrepararDatosEmisorPorId(Cnx, locID_Compania_Mizart, locID_Sucursal_Mizart)
                    Else
                        oCFD.PrepararDatosEmisorPorId(Cnx, locID_Compania_Mizart)
                    End If
                    oCFD.PrepararDatosReceptorPorId(Cnx, CStr(oId_AsociadoLlave), 1, 0)

                    oCFD.EsNotaCredito = "1"

                    oCFD.EsTimbrePrueba = EsTimbrePrueba
                    oCFD.Timbrador = CFD.PAC.Mizar

                    oCFD.Version = GloVERSON
                    oCFD.IdMoneda = 1  '//1 = Pesos , Pesos = 2 USD
                    'oFecha
                    'oCFD.Fecha = mfecha '//Date.Now
                    oCFD.FormaDePago = GloFORMADEPAGO
                    'oCFD.Certificado = ""
                    oCFD.CondicionesDePago = GloCONDICIONESDEPAGO
                    oCFD.SubTotal = oSubTotal
                    oCFD.Descuento = oDescuento
                    oCFD.MotivoDescuento = GloMOTIVODESCUENTO
                    oCFD.Total = oTotalSinPuntos
                    oCFD.MetodoDePago = GloMETODODEPAGO
                    oCFD.TipoDeComprobante = GloTIPODECOMPROBANTE
                    oCFD.TipoComprobante33 = GloTIPODECOMPROBANTE
                    oCFD.PagoEnParcialiades = GloPAGOENPARCIALIDADES
                    oCFD.LugarExpedicion = GloPAGOENPARCIALIDADES
                    oCFD.CuentaPago = GlonumCtaPago

                    'Parte especial de las notas
                    oCFD.Moneda = GloMONEDA
                    oCFD.UsoCFDI = GloUSOCFD
                    oCFD.TipoRelacion = "01"
                    oCFD.UUIDRelacionados = GloUUIDRel
                    oCFD.UUIDRel.Add(GloUUIDRel)

                    'oCFD.FechaVencimiento = Date.Parse(GloFECHAVIGENCIA)
                    'oCFD.CondicionesDePago = ""
                    oCFD.TipoCambio = GloTIPOCAMBIO

                    Dim oRegimen As New MizarCFD.BRL.CFDRegimenesFiscales
                    oRegimen.IdRegimen = 0
                    oRegimen.IdCFD = 0
                    oRegimen.Regimen = GloREGIMEN
                    'oRegimen.Insertar(Cnx)

                    oCFD.RegimenesFiscales.Add(oRegimen)

                    'oCFD.Complemento.XMLComplemento = "<raiz>Caelli Enarrant Gloria Dei </raiz>"
                    'oCFD.Addenda.XMLAddenda = "<edi>Edit dididididididi</edi>"

                    oCFD.Impuestos.TotalImpuestosRetenidos = 0
                    oCFD.Impuestos.TotalImpuestosTrasladados = oTotalImpuestos
                    'Dim oimpuestosRetenciones As CFDImpuestosRetenciones
                    'oimpuestosRetenciones = New CFDImpuestosRetenciones()
                    'oimpuestosRetenciones.Impuesto = "IVA"
                    'oimpuestosRetenciones.Importe = oiva
                    'oCFD.Impuestos.Retenciones.Add(oimpuestosRetenciones)

                    Dim oImpuestosTraslados As CFDImpuestosTraslados

                    'oImpuestosTraslados = New CFDImpuestosTraslados()
                    'oImpuestosTraslados.Impuesto = "IVA"
                    'oImpuestosTraslados.Importe = oiva
                    'oImpuestosTraslados.Tasa = oTasaIva

                    'oCFD.Impuestos.Traslados.Add(oImpuestosTraslados)

                    'oImpuestosTraslados = New CFDImpuestosTraslados()
                    'oImpuestosTraslados.Impuesto = "IEPS"
                    'oImpuestosTraslados.Importe = oieps
                    'oImpuestosTraslados.Tasa = oTasaIeps

                    'oCFD.Impuestos.Traslados.Add(oImpuestosTraslados)

                    Dim oConcepto As CFDConceptos
                    Dim oContrador As Integer = 1

                    Dim CONEXION2 As New SqlConnection(locconexion)
                    Dim COMANDO2 As New SqlCommand("exec DameFacDig_Parte_2Nota " + CStr(oClv_Nota), CONEXION2)
                    COMANDO2.CommandType = CommandType.Text
                    COMANDO2.CommandTimeout = 0
                    Try
                        CONEXION2.Open()
                        Dim reader As SqlDataReader = COMANDO2.ExecuteReader
                        '    'Recorremos los Cargos obtenidos desde el Procedimiento
                        While (reader.Read())
                            oConcepto = New CFDConceptos()
                            oConcepto.Cantidad = reader(0).ToString
                            oConcepto.UnidadMedida = "No Aplica"
                            oConcepto.NumeroIdentificacion = reader(1).ToString
                            oConcepto.Descripcion = reader(2).ToString
                            oConcepto.ValorUnitario = reader(3).ToString
                            oConcepto.Importe = reader(4).ToString
                            oConcepto.TrasladarIVA = "1"

                            'Los nuevos
                            oConcepto.Descuento = 0 'Valor Nuevo
                            ' oConcepto.ClaveProdServ = reader(5).ToString ' "83111801" 'Campo nuevo
                            oConcepto.idClaveProdServ = reader(5).ToString
                            'Iva
                            'oConcepto.IVATRAS_TipoFactor = reader(6).ToString
                            oConcepto.TipoFactor = reader(6).ToString
                            oConcepto.IVATRAS_Importe = reader(8).ToString 'Iva del ticket
                            'oConcepto.IVATRAS_TasaOCuota = reader(7).ToString '"0.160000"


                            oConcepto.TasaIVA = reader(7).ToString
                            oConcepto.TrasladarIVA = "1"
                            'Ieps
                            'oConcepto.IEPSTRAS_TipoFactor = reader(9).ToString '"Tasa"
                            'oConcepto.IEPSTRAS_TasaOCuota = reader(10).ToString '"0.030000"
                            If Double.Parse(reader(11).ToString) > 0 Then
                                oConcepto.BaseIEPS = reader(4).ToString 'Cantida Subtotal 
                                'oConcepto.BaseIVA = CDec(reader(4).ToString) + CDec(reader(11).ToString)
                                If SumaSubtotalYIeps = True Then
                                    oConcepto.BaseIVA = CDec(reader(4).ToString) + CDec(reader(11).ToString)
                                Else
                                    oConcepto.BaseIVA = CDec(reader(4).ToString)
                                End If

                                oConcepto.IEPSTRAS_Importe = reader(11).ToString 'Total del ieps 'Ieps del ticket
                                oConcepto.TasaIEPS = reader(10).ToString

                                oConcepto.FactorIEPS = reader(9).ToString
                                oConcepto.TrasladarIEPS = "1"
                            Else
                                oConcepto.BaseIEPS = 0 'Cantida Subtotal 
                                oConcepto.BaseIVA = reader(4).ToString
                                oConcepto.IEPSTRAS_Importe = "0" 'Total del ieps 'Ieps del ticket
                                oConcepto.TrasladarIEPS = "0"
                            End If
                            oConcepto.idUnidadMedida = reader(13).ToString '"E48" "Unidad de servicio" '"No Aplica" 'Cambio  Id que corresponde
                            'oConcepto.UnidadDesc = reader(12).ToString '"Unidad de servicio" ' Pruebo poner una al gusto

                            oConcepto.IVARET_Importe = "0"
                            oConcepto.ISRRET_Importe = "0"

                            'Trasladados
                            If Double.Parse(reader(8).ToString) > 0 Then
                                oImpuestosTraslados = New CFDImpuestosTraslados()
                                oImpuestosTraslados.IdRegistroTraslado = oContrador
                                oImpuestosTraslados.Impuesto = "IVA"
                                oImpuestosTraslados.Importe = reader(8).ToString
                                oImpuestosTraslados.Tasa = reader(7).ToString 'oTasaIva
                                'oImpuestosTraslados.TipoFactor = GloTIPOFACTOR
                                oImpuestosTraslados.TasaOCuota = GloTIPOFACTOR
                                oImpuestosTraslados.IEPSTRAS_Importe = "0"
                                oImpuestosTraslados.IVATRAS_Importe = "0"

                                oCFD.Impuestos.Traslados.Add(oImpuestosTraslados)
                            End If
                            If Double.Parse(reader(11).ToString) > 0 Then
                                oImpuestosTraslados = New CFDImpuestosTraslados()
                                oImpuestosTraslados.IdRegistroTraslado = oContrador + 1
                                oImpuestosTraslados.Impuesto = "IEPS" '"003" '"IEPS"
                                oImpuestosTraslados.Importe = reader(11).ToString
                                oImpuestosTraslados.Tasa = reader(10).ToString 'oTasaIeps
                                oImpuestosTraslados.TasaOCuota = GloTIPOFACTOR
                                oImpuestosTraslados.FactorIepsTras = GloTIPOFACTOR
                                oImpuestosTraslados.IEPSTRAS_Importe = "0"
                                oImpuestosTraslados.IVATRAS_Importe = "0"
                                'oImpuestosTraslados.TipoFactor = GloTIPOFACTOR

                                oCFD.Impuestos.Traslados.Add(oImpuestosTraslados)
                            End If



                            oConcepto.RetenerIVA = "0"
                            oConcepto.RetenerISR = "0"

                            oCFD.Conceptos.Add(oConcepto)
                            oContrador = oContrador + 2
                        End While
                        'Cnx.DbConnection.Close()
                    Catch ex As Exception
                        'MsgBox(ex.Message & "Nota Parte 2 " & ex.Message, MsgBoxStyle.Exclamation)
                        If Graba = "0" Then
                            Llenalog(ex.Source.ToString & " " & ex.Message)
                        Else

                            Llenalog(ex.Source.ToString & " " & ex.Message)
                        End If
                    Finally
                        CONEXION2.Close()
                        CONEXION2.Dispose()
                    End Try
                    'Dim a As String = JsonConvert.SerializeObject(oCFD)
                    Dim Bnd As Boolean = False
                    Dim Mensaje1 As String = ""
                    If Not oCFD.Insertar(Cnx, True, False) Then
                        For Each oMensaje As DAMensajesSistema.RegistroMensaje In oCFD.MensajesSistema.ListaMensajes
                            Mensaje1 = oMensaje.TextoMensaje + " " + oMensaje.Contexto
                            Inserta_Tbl_FacturasNoGeneradas(oClv_Nota, "O", Mensaje1, locconexion)
                            'MessageBox.Show(oMensaje.TextoMensaje + " " + oMensaje.Contexto)
                            Bnd = True
                        Next
                        Return
                    End If
                    'If Not oCFD.Insertar(Cnx, True) Then
                    '    For Each oMensaje As DAMensajesSistema.RegistroMensaje In oCFD.MensajesSistema.ListaMensajes
                    '        MessageBox.Show(oMensaje.TextoMensaje + " " + oMensaje.Contexto)
                    '    Next
                    '    Return
                    'End If



                    GloClv_FacturaCFD = 0
                    GloClv_FacturaCFD = oCFD.IdCFD
                    UPS_Inserta_Rel_FacturasCFD(oClv_Nota, GloClv_FacturaCFD, "", "O", locconexion)
                    GloTxtUlt4DigCheque = ""
                    Dim r As New Globalization.CultureInfo("es-MX")

                    r.DateTimeFormat.ShortDatePattern = "dd/MM/yyyy"

                    System.Threading.Thread.CurrentThread.CurrentCulture = r
                    Try
                        'oCFD.EnviarEmail(Cnx)
                        'ENVIAR_Factura(GloClv_FacturaCFD)
                        If GloClv_FacturaCFD > 0 And Bnd = False Then
                            ENVIAR_Factura(GloClv_FacturaCFD)
                        End If
                    Catch ex As Exception

                    End Try

                Catch ex As Exception
                    'MsgBox(ex.Message & "Nota Parte 3 " & ex.Message, MsgBoxStyle.Exclamation)
                    If Graba = "0" Then
                        Llenalog(ex.Source.ToString & " " & ex.Message)
                    Else

                        Llenalog(ex.Source.ToString & " " & ex.Message)
                    End If
                Finally
                    CONEXION.Close()
                    CONEXION.Dispose()
                End Try

            End Using
        Else
            GloClv_FacturaCFD = ClvFacturaCfd
        End If
    End Sub

    Public Shared Sub Graba_Factura_NotaMaestro(ByVal oClv_Nota As Long, ByVal oIden As Integer, ByVal locconexion As String)
        oIden = BusFacFiscalOledbMaestroNota(oClv_Nota, locconexion)
        If oIden > 0 Then
            Dime_Aque_Compania_FacturarleNOTAMaestro(oClv_Nota, locconexion)

            Dim ClvFacturaCfd As Long
            ClvFacturaCfd = SP_DIMESIEXISTEFACTURADIGITAL(oClv_Nota, "T", locconexion)
            If ClvFacturaCfd = 0 Then '0 Es si no Existe

                Dim oTotalConPuntos As String = Nothing
                Dim oSubTotal As String = Nothing
                Dim oTotalSinPuntos As String = Nothing
                Dim oDescuento As String = Nothing
                Dim oiva As String = Nothing
                Dim oieps As String = Nothing
                Dim oTasaIva As String = Nothing
                Dim oTasaIeps As String = Nothing
                Dim oFecha As String = Nothing
                Dim oTotalImpuestos As String = Nothing

                Dim CONEXION As New SqlConnection(locconexion)
                Dim COMANDO As New SqlCommand("DameFacDig_Parte_1NotaMaestro", CONEXION)
                COMANDO.CommandType = CommandType.StoredProcedure
                COMANDO.CommandTimeout = 0

                Dim parametro As New SqlParameter("@Clv_Nota", SqlDbType.BigInt)
                parametro.Direction = ParameterDirection.Input
                parametro.Value = oClv_Nota
                COMANDO.Parameters.Add(parametro)

                Dim parametro1 As New SqlParameter("@TotalConPuntos", SqlDbType.Money)
                parametro1.Direction = ParameterDirection.Output
                parametro1.Value = 0
                COMANDO.Parameters.Add(parametro1)

                Dim parametro2 As New SqlParameter("@SubTotal", SqlDbType.Money)
                parametro2.Direction = ParameterDirection.Output
                parametro2.Value = 0
                COMANDO.Parameters.Add(parametro2)

                Dim parametro3 As New SqlParameter("@TotalSinPuntos", SqlDbType.Money)
                parametro3.Direction = ParameterDirection.Output
                parametro3.Value = 0
                COMANDO.Parameters.Add(parametro3)

                Dim parametro4 As New SqlParameter("@Descuento", SqlDbType.Money)
                parametro4.Direction = ParameterDirection.Output
                parametro4.Value = 0
                COMANDO.Parameters.Add(parametro4)

                Dim parametro5 As New SqlParameter("@iva", SqlDbType.Money)
                parametro5.Direction = ParameterDirection.Output
                parametro5.Value = 0
                COMANDO.Parameters.Add(parametro5)

                Dim parametro6 As New SqlParameter("@ieps", SqlDbType.Money)
                parametro6.Direction = ParameterDirection.Output
                parametro6.Value = 0
                COMANDO.Parameters.Add(parametro6)

                Dim parametro7 As New SqlParameter("@TasaIva", SqlDbType.Money)
                parametro7.Direction = ParameterDirection.Output
                parametro7.Value = 0
                COMANDO.Parameters.Add(parametro7)

                Dim parametro8 As New SqlParameter("@TasaIeps", SqlDbType.Money)
                parametro8.Direction = ParameterDirection.Output
                parametro8.Value = 0
                COMANDO.Parameters.Add(parametro8)

                Dim parametro9 As New SqlParameter("@Fecha", SqlDbType.DateTime)
                parametro9.Direction = ParameterDirection.Output
                parametro9.Value = 0
                COMANDO.Parameters.Add(parametro9)

                Dim parametro10 As New SqlParameter("@TotalImpuestos", SqlDbType.Money)
                parametro10.Direction = ParameterDirection.Output
                parametro10.Value = 0
                COMANDO.Parameters.Add(parametro10)

                Try
                    CONEXION.Open()
                    Dim i As Integer = COMANDO.ExecuteNonQuery
                    oTotalConPuntos = CStr(parametro1.Value)
                    oSubTotal = CStr(parametro2.Value)
                    oTotalSinPuntos = CStr(parametro3.Value)
                    oDescuento = CStr(parametro4.Value)
                    oiva = CStr(parametro5.Value)
                    oieps = CStr(parametro6.Value)
                    oTasaIva = CStr(parametro7.Value)
                    oTasaIeps = CStr(parametro8.Value)
                    oFecha = CStr(parametro9.Value)
                    oTotalImpuestos = CStr(parametro10.Value)
                    'Cnx.DbConnection.Close()
                Catch ex As Exception
                    'MsgBox(ex.Message & "Nota Parte 1 " & ex.Message, MsgBoxStyle.Exclamation)
                    If Graba = "0" Then
                        MsgBox(ex.Source.ToString & " " & ex.Message)
                    Else

                        Llenalog(ex.Source.ToString & " " & ex.Message)
                    End If
                Finally
                    CONEXION.Close()
                    CONEXION.Dispose()
                End Try

                Usp_Ed_DameDatosFacDigNotaMaestro(oClv_Nota, locID_Compania_Mizart, locconexion)

                Dim oCFD As MizarCFD.BRL.CFD

                Dim oId_AsociadoLlave As Long = 0
                oId_AsociadoLlave = oIden 'Existe_DevuelveValor(" Select id_asociado from asociados  where id_asociado = " + CStr(oIden))


                Using Cnx As New DAConexion("HL", "sa", "sa")
                    Try
                        'oIden = "1"

                        oCFD = New MizarCFD.BRL.CFD
                        oCFD.Inicializar()
                        oCFD.TipoCFD = MizarCFD.BRL.CFD.TiposCFD.Emision
                        ''Ojo 
                        'locID_Sucursal_Mizart = 0
                        'oCFD.PrepararDatosEmisorPorId(Cnx, locID_Compania_Mizart, locID_Sucursal_Mizart)
                        If locID_Sucursal_Mizart.Length > 0 And locID_Sucursal_Mizart <> "0" Then
                            oCFD.PrepararDatosEmisorPorId(Cnx, locID_Compania_Mizart, locID_Sucursal_Mizart)
                        Else
                            oCFD.PrepararDatosEmisorPorId(Cnx, locID_Compania_Mizart)
                        End If
                        oCFD.PrepararDatosReceptorPorId(Cnx, CStr(oId_AsociadoLlave), 1, 0)

                        oCFD.EsNotaCredito = "1"

                        oCFD.EsTimbrePrueba = EsTimbrePrueba
                        oCFD.Timbrador = CFD.PAC.Mizar

                        oCFD.Version = GloVERSON
                        If (GloMONEDA = "USD") Then
                            oCFD.IdMoneda = "2"
                        Else
                            oCFD.IdMoneda = "1" '//1 = Pesos , Pesos = 2 USD  'oFecha
                        End If
                        'oFecha
                        'oCFD.Fecha = mfecha '//Date.Now
                        oCFD.FormaDePago = GloFORMADEPAGO
                        'oCFD.Certificado = ""
                        oCFD.CondicionesDePago = GloCONDICIONESDEPAGO
                        oCFD.SubTotal = oSubTotal
                        oCFD.Descuento = oDescuento
                        oCFD.MotivoDescuento = GloMOTIVODESCUENTO
                        oCFD.Total = oTotalSinPuntos
                        oCFD.MetodoDePago = GloMETODODEPAGO
                        oCFD.TipoDeComprobante = GloTIPODECOMPROBANTE
                        oCFD.TipoComprobante33 = GloTIPODECOMPROBANTE
                        oCFD.PagoEnParcialiades = GloPAGOENPARCIALIDADES
                        oCFD.LugarExpedicion = GloPAGOENPARCIALIDADES
                        oCFD.CuentaPago = GlonumCtaPago

                        'Parte especial de las notas
                        oCFD.Moneda = GloMONEDA
                        oCFD.UsoCFDI = GloUSOCFD
                        oCFD.TipoRelacion = "01"
                        oCFD.UUIDRelacionados = GloUUIDRel
                        oCFD.UUIDRel.Add(GloUUIDRel)
                        oCFD.TipoCambio = GloTIPOCAMBIO

                        Dim oRegimen As New MizarCFD.BRL.CFDRegimenesFiscales
                        oRegimen.IdRegimen = 0
                        oRegimen.IdCFD = 0
                        oRegimen.Regimen = GloREGIMEN
                        'oRegimen.Insertar(Cnx)

                        oCFD.RegimenesFiscales.Add(oRegimen)
                        oCFD.Impuestos.TotalImpuestosRetenidos = 0
                        oCFD.Impuestos.TotalImpuestosTrasladados = oTotalImpuestos


                        Dim oImpuestosTraslados As CFDImpuestosTraslados

                        Dim oConcepto As CFDConceptos
                        Dim oContrador As Integer = 1

                        Dim CONEXION2 As New SqlConnection(locconexion)
                        Dim COMANDO2 As New SqlCommand("exec DameFacDig_Parte_2NotaMaestro " + CStr(oClv_Nota), CONEXION2)
                        COMANDO2.CommandType = CommandType.Text
                        COMANDO2.CommandTimeout = 0
                        Try
                            CONEXION2.Open()
                            Dim reader As SqlDataReader = COMANDO2.ExecuteReader
                            '    'Recorremos los Cargos obtenidos desde el Procedimiento
                            While (reader.Read())
                                oConcepto = New CFDConceptos()
                                oConcepto.Cantidad = reader(0).ToString
                                oConcepto.UnidadMedida = "No Aplica"
                                oConcepto.NumeroIdentificacion = reader(1).ToString
                                oConcepto.Descripcion = reader(2).ToString
                                oConcepto.ValorUnitario = reader(3).ToString
                                oConcepto.Importe = reader(4).ToString
                                oConcepto.TrasladarIVA = "1"

                                'Los nuevos
                                oConcepto.Descuento = 0 'Valor Nuevo
                                ' oConcepto.ClaveProdServ = reader(5).ToString ' "83111801" 'Campo nuevo
                                oConcepto.idClaveProdServ = reader(5).ToString
                                'Iva
                                'oConcepto.IVATRAS_TipoFactor = reader(6).ToString
                                oConcepto.TipoFactor = reader(6).ToString
                                oConcepto.IVATRAS_Importe = reader(8).ToString 'Iva del ticket
                                'oConcepto.IVATRAS_TasaOCuota = reader(7).ToString '"0.160000"


                                oConcepto.TasaIVA = reader(7).ToString
                                oConcepto.TrasladarIVA = "1"
                                'Ieps
                                'oConcepto.IEPSTRAS_TipoFactor = reader(9).ToString '"Tasa"
                                'oConcepto.IEPSTRAS_TasaOCuota = reader(10).ToString '"0.030000"
                                If Double.Parse(reader(11).ToString) > 0 Then
                                    oConcepto.BaseIEPS = reader(4).ToString 'Cantida Subtotal 
                                    'oConcepto.BaseIVA = CDec(reader(4).ToString) + CDec(reader(11).ToString)
                                    If SumaSubtotalYIeps = True Then
                                        oConcepto.BaseIVA = CDec(reader(4).ToString) + CDec(reader(11).ToString)
                                    Else
                                        oConcepto.BaseIVA = CDec(reader(4).ToString)
                                    End If

                                    oConcepto.IEPSTRAS_Importe = reader(11).ToString 'Total del ieps 'Ieps del ticket
                                    oConcepto.TasaIEPS = reader(10).ToString

                                    oConcepto.FactorIEPS = reader(9).ToString
                                    oConcepto.TrasladarIEPS = "1"
                                Else
                                    oConcepto.BaseIEPS = 0 'Cantida Subtotal 
                                    oConcepto.BaseIVA = reader(4).ToString
                                    oConcepto.IEPSTRAS_Importe = "0" 'Total del ieps 'Ieps del ticket
                                    oConcepto.TrasladarIEPS = "0"
                                End If
                                oConcepto.idUnidadMedida = reader(13).ToString '"E48" "Unidad de servicio" '"No Aplica" 'Cambio  Id que corresponde
                                'oConcepto.UnidadDesc = reader(12).ToString '"Unidad de servicio" ' Pruebo poner una al gusto

                                oConcepto.IVARET_Importe = "0"
                                oConcepto.ISRRET_Importe = "0"

                                'Trasladados
                                If Double.Parse(reader(8).ToString) > 0 Then
                                    oImpuestosTraslados = New CFDImpuestosTraslados()
                                    oImpuestosTraslados.IdRegistroTraslado = oContrador
                                    oImpuestosTraslados.Impuesto = "IVA"
                                    oImpuestosTraslados.Importe = reader(8).ToString
                                    oImpuestosTraslados.Tasa = reader(7).ToString 'oTasaIva
                                    'oImpuestosTraslados.TipoFactor = GloTIPOFACTOR
                                    oImpuestosTraslados.TasaOCuota = GloTIPOFACTOR
                                    oImpuestosTraslados.IEPSTRAS_Importe = "0"
                                    oImpuestosTraslados.IVATRAS_Importe = "0"

                                    oCFD.Impuestos.Traslados.Add(oImpuestosTraslados)
                                End If
                                If Double.Parse(reader(11).ToString) > 0 Then
                                    oImpuestosTraslados = New CFDImpuestosTraslados()
                                    oImpuestosTraslados.IdRegistroTraslado = oContrador + 1
                                    oImpuestosTraslados.Impuesto = "IEPS" '"003" '"IEPS"
                                    oImpuestosTraslados.Importe = reader(11).ToString
                                    oImpuestosTraslados.Tasa = reader(10).ToString 'oTasaIeps
                                    oImpuestosTraslados.TasaOCuota = GloTIPOFACTOR
                                    oImpuestosTraslados.FactorIepsTras = GloTIPOFACTOR
                                    oImpuestosTraslados.IEPSTRAS_Importe = "0"
                                    oImpuestosTraslados.IVATRAS_Importe = "0"
                                    'oImpuestosTraslados.TipoFactor = GloTIPOFACTOR

                                    oCFD.Impuestos.Traslados.Add(oImpuestosTraslados)
                                End If



                                oConcepto.RetenerIVA = "0"
                                oConcepto.RetenerISR = "0"

                                oCFD.Conceptos.Add(oConcepto)
                                oContrador = oContrador + 2
                            End While
                            'Cnx.DbConnection.Close()
                        Catch ex As Exception
                            'MsgBox(ex.Message & "Nota Parte 2 " & ex.Message, MsgBoxStyle.Exclamation)
                            If Graba = "0" Then
                                MsgBox(ex.Source.ToString & " " & ex.Message)
                            Else

                                Llenalog(ex.Source.ToString & " " & ex.Message)
                            End If
                        Finally
                            CONEXION2.Close()
                            CONEXION2.Dispose()
                        End Try
                        'Dim a As String = JsonConvert.SerializeObject(oCFD)
                        Dim Bnd As Boolean = False
                        Dim Mensaje1 As String = ""
                        If Not oCFD.Insertar(Cnx, True, False) Then
                            For Each oMensaje As DAMensajesSistema.RegistroMensaje In oCFD.MensajesSistema.ListaMensajes
                                Mensaje1 = oMensaje.TextoMensaje + " " + oMensaje.Contexto
                                Inserta_Tbl_FacturasNoGeneradas(oClv_Nota, "T", Mensaje1, locconexion)
                                'MessageBox.Show(oMensaje.TextoMensaje + " " + oMensaje.Contexto)
                                Bnd = True
                            Next
                            Return
                        End If
                        'If Not oCFD.Insertar(Cnx, True) Then
                        '    For Each oMensaje As DAMensajesSistema.RegistroMensaje In oCFD.MensajesSistema.ListaMensajes
                        '        MessageBox.Show(oMensaje.TextoMensaje + " " + oMensaje.Contexto)
                        '    Next
                        '    Return
                        'End If



                        GloClv_FacturaCFD = 0
                        GloClv_FacturaCFD = oCFD.IdCFD
                        UPS_Inserta_Rel_FacturasCFD(oClv_Nota, GloClv_FacturaCFD, "", "T", locconexion)
                        GloTxtUlt4DigCheque = ""
                        Dim r As New Globalization.CultureInfo("es-MX")

                        r.DateTimeFormat.ShortDatePattern = "dd/MM/yyyy"

                        System.Threading.Thread.CurrentThread.CurrentCulture = r
                        Try
                            'oCFD.EnviarEmail(Cnx)
                            'ENVIAR_Factura(GloClv_FacturaCFD)
                            If GloClv_FacturaCFD > 0 And Bnd = False Then
                                ENVIAR_Factura(GloClv_FacturaCFD)
                            End If
                        Catch ex As Exception

                        End Try

                    Catch ex As Exception
                        'MsgBox(ex.Message & "Nota Parte 3 " & ex.Message, MsgBoxStyle.Exclamation)
                        If Graba = "0" Then
                            MsgBox(ex.Source.ToString & " " & ex.Message)
                        Else

                            Llenalog(ex.Source.ToString & " " & ex.Message)
                        End If
                    Finally
                        CONEXION.Close()
                        CONEXION.Dispose()
                    End Try

                End Using
            Else
                GloClv_FacturaCFD = ClvFacturaCfd
            End If
        End If
    End Sub

    'Public Shared Sub Graba_Factura_Digital(ByVal oClv_Factura As Long, ByVal oIden As Integer, ByVal locconexion As String) ' MiConexion

    '    Dim ClvFacturaCfd As Long = 0
    '    'ClvFacturaCfd = SP_DIMESIEXISTEFACTURADIGITAL(oClv_Factura, "N", locconexion)
    '    If ClvFacturaCfd = 0 Then '0 Es si no Existe y si debe de Genera la Factura


    '        Dim oTotalConPuntos As String = Nothing
    '        Dim oSubTotal As String = Nothing
    '        Dim oTotalSinPuntos As String = Nothing
    '        Dim oDescuento As String = Nothing
    '        Dim oiva As String = Nothing
    '        Dim oieps As String = Nothing
    '        Dim oTasaIva As String = Nothing
    '        Dim oTasaIeps As String = Nothing
    '        Dim oFecha As String = Nothing
    '        Dim oTotalImpuestos As String = Nothing


    '        Dim mfecha As DateTime
    '        Dim con As New SqlConnection(locconexion)
    '        Dim com As New SqlCommand("DAMEFECHADELSERVIDORFACTURADIGITAL", con)
    '        com.CommandType = CommandType.StoredProcedure
    '        'Dame la fecha del Servidor
    '        Dim prmFechaObtebida As New SqlParameter("@FECHA", SqlDbType.DateTime)
    '        prmFechaObtebida.Direction = ParameterDirection.Output
    '        prmFechaObtebida.Value = ""
    '        com.Parameters.Add(prmFechaObtebida)
    '        Try
    '            con.Open()
    '            com.ExecuteNonQuery()
    '            mfecha = prmFechaObtebida.Value
    '        Catch ex As Exception
    '            'MsgBox("Error: No se pudo obtener la fecha válida del Servidor.", MsgBoxStyle.Critical)
    '            'MsgBox("Descripción:  Procedimiento [DAMEFECHADELSERVIDOR] // Función [DameFechaServidor] // Form: FrmAdelantarCargos", MsgBoxStyle.Information)
    '            'Llenalog(ex.Source.ToString & " " & ex.Message)
    '        Finally
    '            con.Close()
    '        End Try
    '        ''

    '        Class1.limpiaParametros()
    '        Class1.CreateMyParameter("@FECHA", ParameterDirection.Output, SqlDbType.DateTime)
    '        Class1.ProcedimientoOutPut("DAMEFECHADELSERVIDORFACTURADIGITAL", locconexion)
    '        mfecha = DateTime.Parse(Class1.dicoPar("@FECHA").ToString())


    '        Dim CONEXION As New SqlConnection(locconexion)
    '        Dim COMANDO As New SqlCommand("DameFacDig_Parte_1", CONEXION)
    '        COMANDO.CommandType = CommandType.StoredProcedure

    '        Dim parametro As New SqlParameter("@Clv_Factura", SqlDbType.BigInt)
    '        parametro.Direction = ParameterDirection.Input
    '        parametro.Value = oClv_Factura
    '        COMANDO.Parameters.Add(parametro)

    '        Dim parametro1 As New SqlParameter("@TotalConPuntos", SqlDbType.Money)
    '        parametro1.Direction = ParameterDirection.Output
    '        parametro1.Value = 0
    '        COMANDO.Parameters.Add(parametro1)

    '        Dim parametro2 As New SqlParameter("@SubTotal", SqlDbType.Money)
    '        parametro2.Direction = ParameterDirection.Output
    '        parametro2.Value = 0
    '        COMANDO.Parameters.Add(parametro2)

    '        Dim parametro3 As New SqlParameter("@TotalSinPuntos", SqlDbType.Money)
    '        parametro3.Direction = ParameterDirection.Output
    '        parametro3.Value = 0
    '        COMANDO.Parameters.Add(parametro3)

    '        Dim parametro4 As New SqlParameter("@Descuento", SqlDbType.Money)
    '        parametro4.Direction = ParameterDirection.Output
    '        parametro4.Value = 0
    '        COMANDO.Parameters.Add(parametro4)

    '        Dim parametro5 As New SqlParameter("@iva", SqlDbType.Money)
    '        parametro5.Direction = ParameterDirection.Output
    '        parametro5.Value = 0
    '        COMANDO.Parameters.Add(parametro5)

    '        Dim parametro6 As New SqlParameter("@ieps", SqlDbType.Money)
    '        parametro6.Direction = ParameterDirection.Output
    '        parametro6.Value = 0
    '        COMANDO.Parameters.Add(parametro6)

    '        Dim parametro7 As New SqlParameter("@TasaIva", SqlDbType.Money)
    '        parametro7.Direction = ParameterDirection.Output
    '        parametro7.Value = 0
    '        COMANDO.Parameters.Add(parametro7)

    '        Dim parametro8 As New SqlParameter("@TasaIeps", SqlDbType.Money)
    '        parametro8.Direction = ParameterDirection.Output
    '        parametro8.Value = 0
    '        COMANDO.Parameters.Add(parametro8)

    '        Dim parametro9 As New SqlParameter("@Fecha", SqlDbType.DateTime)
    '        parametro9.Direction = ParameterDirection.Output
    '        parametro9.Value = 0
    '        COMANDO.Parameters.Add(parametro9)

    '        Dim parametro10 As New SqlParameter("@TotalImpuestos", SqlDbType.Money)
    '        parametro10.Direction = ParameterDirection.Output
    '        parametro10.Value = 0
    '        COMANDO.Parameters.Add(parametro10)

    '        Try
    '            CONEXION.Open()
    '            Dim i As Integer = COMANDO.ExecuteNonQuery
    '            oTotalConPuntos = CStr(parametro1.Value)
    '            oSubTotal = CStr(parametro2.Value)
    '            oTotalSinPuntos = CStr(parametro3.Value)
    '            oDescuento = CStr(parametro4.Value)
    '            oiva = CStr(parametro5.Value)
    '            oieps = CStr(parametro6.Value)
    '            oTasaIva = "0.16000" 'CStr(parametro7.Value)
    '            oTasaIeps = "0.0300" 'CStr(parametro8.Value)
    '            oFecha = CStr(parametro9.Value)
    '            oTotalImpuestos = CStr(parametro10.Value)
    '            'Cnx.DbConnection.Close()
    '        Catch ex As Exception
    '            'MsgBox(ex.Message & " Parte 1 " & ex.Source, MsgBoxStyle.Exclamation)
    '        Finally
    '            CONEXION.Close()
    '            CONEXION.Dispose()
    '        End Try

    '        Usp_Ed_DameDatosFacDig(oClv_Factura, locID_Compania_Mizart, locconexion)

    '        Dim oCFD As MizarCFD.BRL.CFD

    '        Dim oId_AsociadoLlave As Long = 0
    '        oId_AsociadoLlave = oIden 'Existe_DevuelveValor(" Select id_asociado from asociados  where id_asociado = " + CStr(oIden))


    '        Using Cnx As New DAConexion("HL", "sa", "sa")
    '            Try
    '                'oIden = "1"

    '                oCFD = New MizarCFD.BRL.CFD
    '                oCFD.Inicializar()
    '                oCFD.TipoCFD = MizarCFD.BRL.CFD.TiposCFD.Emision
    '                ''Ojo 
    '                'locID_Sucursal_Mizart = 0
    '                'oCFD.PrepararDatosEmisorPorId(Cnx, locID_Compania_Mizart, locID_Sucursal_Mizart)
    '                If locID_Sucursal_Mizart.Length > 0 And locID_Sucursal_Mizart <> "0" Then
    '                    oCFD.PrepararDatosEmisorPorId(Cnx, locID_Compania_Mizart, locID_Sucursal_Mizart)
    '                Else
    '                    oCFD.PrepararDatosEmisorPorId(Cnx, locID_Compania_Mizart)
    '                End If
    '                oCFD.PrepararDatosReceptorPorId(Cnx, CStr(oId_AsociadoLlave), 1, 0)


    '                oCFD.EsTimbrePrueba = True 'EsTimbrePrueba
    '                oCFD.Timbrador = CFD.PAC.Mizar

    '                oCFD.Version = "3.3" 'GloVERSON
    '                oCFD.IdMoneda = 1  '//1 = Pesos , Pesos = 2 USD
    '                'oFecha
    '                'oCFD.Fecha = mfecha '//Date.Now
    '                oCFD.FormaDePago = GloFORMADEPAGO
    '                'oCFD.Certificado = ""
    '                oCFD.CondicionesDePago = GloCONDICIONESDEPAGO
    '                oCFD.SubTotal = oSubTotal
    '                oCFD.Descuento = oDescuento
    '                oCFD.MotivoDescuento = GloMOTIVODESCUENTO
    '                oCFD.Total = oTotalSinPuntos
    '                oCFD.MetodoDePago = GloMETODODEPAGO

    '                oCFD.TipoDeComprobante = GloTIPODECOMPROBANTE
    '                oCFD.TipoComprobante33 = GloTIPODECOMPROBANTE
    '                oCFD.PagoEnParcialiades = GloPAGOENPARCIALIDADES
    '                oCFD.LugarExpedicion = GloPAGOENPARCIALIDADES
    '                oCFD.CuentaPago = GlonumCtaPago
    '                oCFD.Moneda = GloMONEDA

    '                oCFD.UsoCFDI = "G03"
    '                oCFD.EsNotaCredito = 0
    '                oCFD.TipoRelacion = Nothing
    '                oCFD.UUIDRelacionados = Nothing
    '                oCFD.FechaVencimiento = Date.Parse("17/11/2017")
    '                oCFD.CondicionesDePago = ""
    '                oCFD.TipoCambio = 1

    '                'oCFD.IdEstatus = 1

    '                Dim oRegimen As New MizarCFD.BRL.CFDRegimenesFiscales
    '                oRegimen.IdRegimen = 0
    '                oRegimen.IdCFD = 0
    '                oRegimen.Regimen = GloREGIMEN
    '                'oRegimen.Insertar(Cnx)

    '                oCFD.RegimenesFiscales.Add(oRegimen)

    '                'oCFD.Complemento.XMLComplemento = "<raiz>Caelli Enarrant Gloria Dei </raiz>"
    '                'oCFD.Addenda.XMLAddenda = "<edi>Edit dididididididi</edi>"

    '                oCFD.Impuestos.TotalImpuestosRetenidos = 0
    '                oCFD.Impuestos.TotalImpuestosTrasladados = oTotalImpuestos
    '                'Dim oimpuestosRetenciones As CFDImpuestosRetenciones
    '                'oimpuestosRetenciones = New CFDImpuestosRetenciones()
    '                'oimpuestosRetenciones.Impuesto = "IVA"
    '                'oimpuestosRetenciones.Importe = oiva
    '                'oCFD.Impuestos.Retenciones.Add(oimpuestosRetenciones)



    '                'oImpuestosTraslados = New CFDImpuestosTraslados()
    '                'oImpuestosTraslados.Impuesto = "002" '"IVA"
    '                'oImpuestosTraslados.Importe = oiva
    '                'oImpuestosTraslados.Tasa = oTasaIva
    '                ''oImpuestosTraslados.TipoFactor = GloTIPOFACTOR
    '                'oImpuestosTraslados.TasaOCuota = GloTIPOFACTOR

    '                'oCFD.Impuestos.Traslados.Add(oImpuestosTraslados)

    '                'oImpuestosTraslados = New CFDImpuestosTraslados()
    '                'oImpuestosTraslados.Impuesto = "003" '"IEPS"
    '                'oImpuestosTraslados.Importe = oieps
    '                'oImpuestosTraslados.Tasa = oTasaIeps
    '                'oImpuestosTraslados.TasaOCuota = GloTIPOFACTOR
    '                'oImpuestosTraslados.FactorIepsTras = GloTIPOFACTOR
    '                ''oImpuestosTraslados.TipoFactor = GloTIPOFACTOR

    '                'oCFD.Impuestos.Traslados.Add(oImpuestosTraslados)
    '                Dim oImpuestosTraslados As CFDImpuestosTraslados
    '                oImpuestosTraslados = New CFDImpuestosTraslados()

    '                Dim oConcepto As CFDConceptos
    '                Dim oContrador As Integer = 1

    '                Dim CONEXION2 As New SqlConnection(locconexion)
    '                Dim COMANDO2 As New SqlCommand("exec DameFacDig_Parte_2 " + CStr(oClv_Factura), CONEXION2)
    '                COMANDO2.CommandType = CommandType.Text
    '                Try
    '                    CONEXION2.Open()
    '                    Dim reader As SqlDataReader = COMANDO2.ExecuteReader
    '                    '    'Recorremos los Cargos obtenidos desde el Procedimiento
    '                    While (reader.Read())
    '                        oConcepto = New CFDConceptos()
    '                        oConcepto.Cantidad = reader(0).ToString
    '                        oConcepto.UnidadMedida = "No Aplica"
    '                        oConcepto.NumeroIdentificacion = reader(1).ToString
    '                        oConcepto.Descripcion = reader(2).ToString
    '                        oConcepto.ValorUnitario = reader(3).ToString
    '                        oConcepto.Importe = reader(4).ToString
    '                        oConcepto.TrasladarIVA = "1"

    '                        'Los nuevos
    '                        oConcepto.Descuento = 0 'Valor Nuevo
    '                        ' oConcepto.ClaveProdServ = reader(5).ToString ' "83111801" 'Campo nuevo
    '                        oConcepto.idClaveProdServ = reader(5).ToString
    '                        'Iva
    '                        'oConcepto.IVATRAS_TipoFactor = reader(6).ToString
    '                        oConcepto.TipoFactor = reader(6).ToString
    '                        oConcepto.IVATRAS_Importe = reader(8).ToString 'Iva del ticket
    '                        'oConcepto.IVATRAS_TasaOCuota = reader(7).ToString '"0.160000"
    '                        oConcepto.Base = reader(4).ToString 'Cantida Subtotal 
    '                        oConcepto.TasaIVA = reader(7).ToString
    '                        oConcepto.TrasladarIVA = "1"
    '                        'Ieps
    '                        'oConcepto.IEPSTRAS_TipoFactor = reader(9).ToString '"Tasa"
    '                        'oConcepto.IEPSTRAS_TasaOCuota = reader(10).ToString '"0.030000"
    '                        If Double.Parse(reader(11).ToString) > 0 Then
    '                            oConcepto.IEPSTRAS_Importe = reader(11).ToString 'Total del ieps 'Ieps del ticket
    '                            oConcepto.TasaIEPS = reader(10).ToString

    '                            oConcepto.FactorIEPS = reader(9).ToString
    '                            oConcepto.TrasladarIEPS = "1"
    '                        Else
    '                            oConcepto.IEPSTRAS_Importe = "0" 'Total del ieps 'Ieps del ticket
    '                            oConcepto.TrasladarIEPS = "0"
    '                        End If
    '                        oConcepto.idUnidadMedida = reader(13).ToString '"E48" "Unidad de servicio" '"No Aplica" 'Cambio  Id que corresponde
    '                        'oConcepto.UnidadDesc = reader(12).ToString '"Unidad de servicio" ' Pruebo poner una al gusto

    '                        oConcepto.IVARET_Importe = "0"
    '                        oConcepto.ISRRET_Importe = "0"

    '                        'Dim oprueba As CFDImpuestos
    '                        'oprueba = New CFDImpuestos

    '                        'oprueba.Traslados.Item(0).Tasa = oTasaIva
    '                        'oprueba.Traslados.Item(0).TasaOCuota = GloTIPOFACTOR
    '                        'oprueba.Traslados.Item(0).Importe = reader(8).ToString
    '                        'oprueba.Traslados.Add(oprueba)

    '                        'Trasladados
    '                        If Double.Parse(reader(8).ToString) > 0 Then
    '                            oImpuestosTraslados.IdRegistroTraslado = oContrador
    '                            oImpuestosTraslados.Impuesto = "IVA" '"002"
    '                            oImpuestosTraslados.Importe = reader(8).ToString
    '                            oImpuestosTraslados.Tasa = "0.1600" 'oTasaIva
    '                            'oImpuestosTraslados.TipoFactor = GloTIPOFACTOR
    '                            oImpuestosTraslados.TasaOCuota = GloTIPOFACTOR
    '                            oImpuestosTraslados.IEPSTRAS_Importe = "0"
    '                            oImpuestosTraslados.IVATRAS_Importe = "0"

    '                            oCFD.Impuestos.Traslados.Add(oImpuestosTraslados)
    '                        End If
    '                        If Double.Parse(reader(11).ToString) > 0 Then
    '                            oImpuestosTraslados.IdRegistroTraslado = oContrador + 1
    '                            oImpuestosTraslados.Impuesto = "IEPS" '"003" '"IEPS"
    '                            oImpuestosTraslados.Importe = reader(11).ToString
    '                            oImpuestosTraslados.Tasa = "0.0300" 'oTasaIeps
    '                            oImpuestosTraslados.TasaOCuota = GloTIPOFACTOR
    '                            oImpuestosTraslados.FactorIepsTras = GloTIPOFACTOR
    '                            oImpuestosTraslados.IEPSTRAS_Importe = "0"
    '                            oImpuestosTraslados.IVATRAS_Importe = "0"
    '                            'oImpuestosTraslados.TipoFactor = GloTIPOFACTOR

    '                            oCFD.Impuestos.Traslados.Add(oImpuestosTraslados)
    '                        End If



    '                        oConcepto.RetenerIVA = "0"
    '                        oConcepto.RetenerISR = "0"

    '                        oCFD.Conceptos.Add(oConcepto)
    '                        oContrador = oContrador + 2
    '                    End While
    '                    'Cnx.DbConnection.Close()
    '                Catch ex As Exception
    '                    'MsgBox(ex.Message & " Parte 2 " & ex.Source, MsgBoxStyle.Exclamation)
    '                Finally
    '                    CONEXION2.Close()
    '                    CONEXION2.Dispose()
    '                End Try

    '                'Dim a As String = JsonConvert.SerializeObject(oCFD)

    '                Dim Mensaje1 As String = ""
    '                'oCFD.Validar(Cnx, )
    '                If Not oCFD.Insertar(Cnx, True, False) Then
    '                    For Each oMensaje As DAMensajesSistema.RegistroMensaje In oCFD.MensajesSistema.ListaMensajes
    '                        Mensaje1 = oMensaje.TextoMensaje + " " + oMensaje.Contexto
    '                        'Inserta_Tbl_FacturasNoGeneradas(oClv_Factura, "N", Mensaje1, locconexion)
    '                        MessageBox.Show(oMensaje.TextoMensaje + " " + oMensaje.Contexto)
    '                    Next
    '                    Return
    '                End If
    '                'If Not oCFD.Insertar(Cnx, True) Then
    '                '    For Each oMensaje As DAMensajesSistema.RegistroMensaje In oCFD.MensajesSistema.ListaMensajes
    '                '        MessageBox.Show(oMensaje.TextoMensaje + " " + oMensaje.Contexto)
    '                '    Next
    '                '    Return
    '                'End If




    '                GloClv_FacturaCFD = oCFD.IdCFD
    '                UPS_Inserta_Rel_FacturasCFD(oClv_Factura, GloClv_FacturaCFD, "", "N", locconexion)
    '                GloTxtUlt4DigCheque = ""

    '                Try
    '                    'oCFD.EnviarEmail(Cnx)
    '                    ENVIAR_Factura(GloClv_FacturaCFD)
    '                Catch ex As Exception

    '                End Try


    '            Catch ex As Exception
    '                'MsgBox(ex.Message & " Parte 3 " & ex.Source, MsgBoxStyle.Exclamation)
    '            Finally
    '                CONEXION.Close()
    '                CONEXION.Dispose()
    '            End Try

    '        End Using
    '    Else
    '        GloClv_FacturaCFD = ClvFacturaCfd
    '    End If

    'End Sub
    Public Shared Sub Graba_Factura_Digital(ByVal oClv_Factura As Long, ByVal oIden As Integer, ByVal locconexion As String) ' MiConexion

        Dim ClvFacturaCfd As Long = 0
        ClvFacturaCfd = SP_DIMESIEXISTEFACTURADIGITAL(oClv_Factura, "N", locconexion)
        If ClvFacturaCfd = 0 Then '0 Es si no Existe y si debe de Genera la Factura


            Dim oTotalConPuntos As String = Nothing
            Dim oSubTotal As String = Nothing
            Dim oTotalSinPuntos As String = Nothing
            Dim oDescuento As String = Nothing
            Dim oiva As String = Nothing
            Dim oieps As String = Nothing
            Dim oTasaIva As String = Nothing
            Dim oTasaIeps As String = Nothing
            Dim oFecha As String = Nothing
            Dim oTotalImpuestos As String = Nothing


            Dim mfecha As DateTime
            Dim con As New SqlConnection(locconexion)
            Dim com As New SqlCommand("DAMEFECHADELSERVIDORFACTURADIGITAL", con)
            com.CommandType = CommandType.StoredProcedure
            com.CommandTimeout = 0
            'Dame la fecha del Servidor
            Dim prmFechaObtebida As New SqlParameter("@FECHA", SqlDbType.DateTime)
            prmFechaObtebida.Direction = ParameterDirection.Output
            prmFechaObtebida.Value = ""
            com.Parameters.Add(prmFechaObtebida)
            Try
                con.Open()
                com.ExecuteNonQuery()
                mfecha = prmFechaObtebida.Value
            Catch ex As Exception
                'MsgBox("Error: No se pudo obtener la fecha válida del Servidor.", MsgBoxStyle.Critical)
                If Graba = "0" Then
                    Llenalog(ex.Source.ToString & " " & ex.Message)
                Else

                    Llenalog(ex.Source.ToString & " " & ex.Message)
                End If
                'MsgBox("Descripción:  Procedimiento [DAMEFECHADELSERVIDOR] // Función [DameFechaServidor] // Form: FrmAdelantarCargos", MsgBoxStyle.Information)
                'Llenalog(ex.Source.ToString & " " & ex.Message)
            Finally
                con.Close()
            End Try
            ''

            Class1.limpiaParametros()
            Class1.CreateMyParameter("@FECHA", ParameterDirection.Output, SqlDbType.DateTime)
            Class1.ProcedimientoOutPut("DAMEFECHADELSERVIDORFACTURADIGITAL", locconexion)
            mfecha = DateTime.Parse(Class1.dicoPar("@FECHA").ToString())


            Dim CONEXION As New SqlConnection(locconexion)
            Dim COMANDO As New SqlCommand("DameFacDig_Parte_1", CONEXION)
            COMANDO.CommandType = CommandType.StoredProcedure
            COMANDO.CommandTimeout = 0

            Dim parametro As New SqlParameter("@Clv_Factura", SqlDbType.BigInt)
            parametro.Direction = ParameterDirection.Input
            parametro.Value = oClv_Factura
            COMANDO.Parameters.Add(parametro)

            Dim parametro1 As New SqlParameter("@TotalConPuntos", SqlDbType.Money)
            parametro1.Direction = ParameterDirection.Output
            parametro1.Value = 0
            COMANDO.Parameters.Add(parametro1)

            Dim parametro2 As New SqlParameter("@SubTotal", SqlDbType.Money)
            parametro2.Direction = ParameterDirection.Output
            parametro2.Value = 0
            COMANDO.Parameters.Add(parametro2)

            Dim parametro3 As New SqlParameter("@TotalSinPuntos", SqlDbType.Money)
            parametro3.Direction = ParameterDirection.Output
            parametro3.Value = 0
            COMANDO.Parameters.Add(parametro3)

            Dim parametro4 As New SqlParameter("@Descuento", SqlDbType.Money)
            parametro4.Direction = ParameterDirection.Output
            parametro4.Value = 0
            COMANDO.Parameters.Add(parametro4)

            Dim parametro5 As New SqlParameter("@iva", SqlDbType.Money)
            parametro5.Direction = ParameterDirection.Output
            parametro5.Value = 0
            COMANDO.Parameters.Add(parametro5)

            Dim parametro6 As New SqlParameter("@ieps", SqlDbType.Money)
            parametro6.Direction = ParameterDirection.Output
            parametro6.Value = 0
            COMANDO.Parameters.Add(parametro6)

            Dim parametro7 As New SqlParameter("@TasaIva", SqlDbType.Money)
            parametro7.Direction = ParameterDirection.Output
            parametro7.Value = 0
            COMANDO.Parameters.Add(parametro7)

            Dim parametro8 As New SqlParameter("@TasaIeps", SqlDbType.Money)
            parametro8.Direction = ParameterDirection.Output
            parametro8.Value = 0
            COMANDO.Parameters.Add(parametro8)

            Dim parametro9 As New SqlParameter("@Fecha", SqlDbType.DateTime)
            parametro9.Direction = ParameterDirection.Output
            parametro9.Value = 0
            COMANDO.Parameters.Add(parametro9)

            Dim parametro10 As New SqlParameter("@TotalImpuestos", SqlDbType.Money)
            parametro10.Direction = ParameterDirection.Output
            parametro10.Value = 0
            COMANDO.Parameters.Add(parametro10)

            Try
                CONEXION.Open()
                Dim i As Integer = COMANDO.ExecuteNonQuery
                oTotalConPuntos = CStr(parametro1.Value)
                oSubTotal = CStr(parametro2.Value)
                oTotalSinPuntos = CStr(parametro3.Value)
                oDescuento = CStr(parametro4.Value)
                oiva = CStr(parametro5.Value)
                oieps = CStr(parametro6.Value)
                oTasaIva = "0.1600" 'CStr(parametro7.Value)
                oTasaIeps = "0.0300" 'CStr(parametro8.Value)
                oFecha = CStr(parametro9.Value)
                oTotalImpuestos = CStr(parametro10.Value)
                'Cnx.DbConnection.Close()
            Catch ex As Exception
                'MsgBox(ex.Message & " Parte 1 " & ex.Source, MsgBoxStyle.Exclamation)
                If Graba = "0" Then
                    Llenalog(ex.Source.ToString & " " & ex.Message)
                Else

                    Llenalog(ex.Source.ToString & " " & ex.Message)
                End If
            Finally
                CONEXION.Close()
                CONEXION.Dispose()
            End Try

            Usp_Ed_DameDatosFacDig(oClv_Factura, locID_Compania_Mizart, locconexion)

            Dim oCFD As MizarCFD.BRL.CFD

            'oIden = "2226"
            Dim oId_AsociadoLlave As Long = 0
            oId_AsociadoLlave = oIden 'Existe_DevuelveValor(" Select id_asociado from asociados  where id_asociado = " + CStr(oIden))


            Using Cnx As New DAConexion("HL", "sa", "sa")
                Try
                    'oIden = "2226"

                    oCFD = New MizarCFD.BRL.CFD
                    oCFD.Inicializar()
                    oCFD.TipoCFD = MizarCFD.BRL.CFD.TiposCFD.Emision
                    ''Ojo 
                    'locID_Sucursal_Mizart = 0
                    'oCFD.PrepararDatosEmisorPorId(Cnx, locID_Compania_Mizart, locID_Sucursal_Mizart)
                    If locID_Sucursal_Mizart.Length > 0 And locID_Sucursal_Mizart <> "0" Then
                        oCFD.PrepararDatosEmisorPorId(Cnx, locID_Compania_Mizart, locID_Sucursal_Mizart)
                    Else
                        oCFD.PrepararDatosEmisorPorId(Cnx, locID_Compania_Mizart)
                    End If
                    oCFD.PrepararDatosReceptorPorId(Cnx, CStr(oId_AsociadoLlave), 1, 0)


                    oCFD.EsTimbrePrueba = EsTimbrePrueba
                    oCFD.Timbrador = CFD.PAC.Mizar

                    oCFD.Version = GloVERSON
                    oCFD.IdMoneda = 1  '//1 = Pesos , Pesos = 2 USD
                    'oFecha
                    'oCFD.Fecha = mfecha '//Date.Now
                    oCFD.FormaDePago = GloFORMADEPAGO
                    'oCFD.Certificado = ""
                    oCFD.CondicionesDePago = GloCONDICIONESDEPAGO
                    oCFD.SubTotal = oSubTotal
                    oCFD.Descuento = oDescuento
                    oCFD.MotivoDescuento = GloMOTIVODESCUENTO
                    oCFD.Total = oTotalSinPuntos
                    oCFD.MetodoDePago = GloMETODODEPAGO

                    oCFD.TipoDeComprobante = GloTIPODECOMPROBANTE
                    oCFD.TipoComprobante33 = GloTIPODECOMPROBANTE
                    oCFD.PagoEnParcialiades = GloPAGOENPARCIALIDADES
                    oCFD.LugarExpedicion = GloPAGOENPARCIALIDADES
                    oCFD.CuentaPago = GlonumCtaPago
                    oCFD.Moneda = GloMONEDA

                    oCFD.UsoCFDI = GloUSOCFD
                    oCFD.EsNotaCredito = 0
                    oCFD.TipoRelacion = Nothing
                    oCFD.UUIDRelacionados = Nothing
                    'oCFD.FechaVencimiento = "" 'Date.Parse(GloFECHAVIGENCIA)
                    'oCFD.CondicionesDePago = ""
                    oCFD.TipoCambio = GloTIPOCAMBIO

                    'oCFD.IdEstatus = 1

                    Dim oRegimen As New MizarCFD.BRL.CFDRegimenesFiscales
                    oRegimen.IdRegimen = 0
                    oRegimen.IdCFD = 0
                    oRegimen.Regimen = GloREGIMEN
                    'oRegimen.Insertar(Cnx)

                    oCFD.RegimenesFiscales.Add(oRegimen)

                    'oCFD.Complemento.XMLComplemento = "<raiz>Caelli Enarrant Gloria Dei </raiz>"
                    'oCFD.Addenda.XMLAddenda = "<edi>Edit dididididididi</edi>"

                    oCFD.Impuestos.TotalImpuestosRetenidos = 0
                    oCFD.Impuestos.TotalImpuestosTrasladados = oTotalImpuestos
                    'Dim oimpuestosRetenciones As CFDImpuestosRetenciones
                    'oimpuestosRetenciones = New CFDImpuestosRetenciones()
                    'oimpuestosRetenciones.Impuesto = "IVA"
                    'oimpuestosRetenciones.Importe = oiva
                    'oCFD.Impuestos.Retenciones.Add(oimpuestosRetenciones)



                    'oImpuestosTraslados = New CFDImpuestosTraslados()
                    'oImpuestosTraslados.Impuesto = "002" '"IVA"
                    'oImpuestosTraslados.Importe = oiva
                    'oImpuestosTraslados.Tasa = oTasaIva
                    ''oImpuestosTraslados.TipoFactor = GloTIPOFACTOR
                    'oImpuestosTraslados.TasaOCuota = GloTIPOFACTOR

                    'oCFD.Impuestos.Traslados.Add(oImpuestosTraslados)

                    'oImpuestosTraslados = New CFDImpuestosTraslados()
                    'oImpuestosTraslados.Impuesto = "003" '"IEPS"
                    'oImpuestosTraslados.Importe = oieps
                    'oImpuestosTraslados.Tasa = oTasaIeps
                    'oImpuestosTraslados.TasaOCuota = GloTIPOFACTOR
                    'oImpuestosTraslados.FactorIepsTras = GloTIPOFACTOR
                    ''oImpuestosTraslados.TipoFactor = GloTIPOFACTOR

                    'oCFD.Impuestos.Traslados.Add(oImpuestosTraslados)
                    Dim oImpuestosTraslados As CFDImpuestosTraslados
                    oImpuestosTraslados = New CFDImpuestosTraslados()

                    Dim oConcepto As CFDConceptos
                    Dim oContrador As Integer = 1

                    Dim CONEXION2 As New SqlConnection(locconexion)
                    Dim COMANDO2 As New SqlCommand("exec DameFacDig_Parte_2 " + CStr(oClv_Factura), CONEXION2)
                    COMANDO2.CommandType = CommandType.Text
                    COMANDO2.CommandTimeout = 0
                    Try
                        CONEXION2.Open()
                        Dim reader As SqlDataReader = COMANDO2.ExecuteReader
                        '    'Recorremos los Cargos obtenidos desde el Procedimiento
                        While (reader.Read())
                            oConcepto = New CFDConceptos()
                            oConcepto.Cantidad = reader(0).ToString
                            'oConcepto.UnidadMedida = "No Aplica"
                            oConcepto.NumeroIdentificacion = reader(1).ToString
                            oConcepto.Descripcion = reader(2).ToString
                            oConcepto.ValorUnitario = reader(3).ToString
                            oConcepto.Importe = reader(4).ToString
                            oConcepto.TrasladarIVA = "1"

                            'Los nuevos
                            oConcepto.Descuento = 0 'Valor Nuevo
                            ' oConcepto.ClaveProdServ = reader(5).ToString ' "83111801" 'Campo nuevo
                            oConcepto.idClaveProdServ = reader(5).ToString
                            'Iva
                            'oConcepto.IVATRAS_TipoFactor = reader(6).ToString
                            oConcepto.TipoFactor = reader(6).ToString
                            oConcepto.IVATRAS_Importe = reader(8).ToString 'Iva del ticket
                            'oConcepto.IVATRAS_TasaOCuota = reader(7).ToString '"0.160000"


                            oConcepto.TasaIVA = reader(7).ToString
                            oConcepto.TrasladarIVA = "1"
                            'Ieps
                            'oConcepto.IEPSTRAS_TipoFactor = reader(9).ToString '"Tasa"
                            'oConcepto.IEPSTRAS_TasaOCuota = reader(10).ToString '"0.030000"
                            If Double.Parse(reader(11).ToString) > 0 Then
                                oConcepto.BaseIEPS = reader(4).ToString 'Cantida Subtotal 
                                If SumaSubtotalYIeps = True Then
                                    oConcepto.BaseIVA = CDec(reader(4).ToString) + CDec(reader(11).ToString)
                                Else
                                    oConcepto.BaseIVA = CDec(reader(4).ToString)
                                End If


                                oConcepto.IEPSTRAS_Importe = reader(11).ToString 'Total del ieps 'Ieps del ticket
                                oConcepto.TasaIEPS = reader(10).ToString

                                oConcepto.FactorIEPS = reader(9).ToString
                                oConcepto.TrasladarIEPS = "1"
                            Else
                                oConcepto.BaseIEPS = 0 'Cantida Subtotal 
                                oConcepto.BaseIVA = reader(4).ToString
                                oConcepto.IEPSTRAS_Importe = "0" 'Total del ieps 'Ieps del ticket
                                oConcepto.TrasladarIEPS = "0"
                            End If
                            oConcepto.idUnidadMedida = reader(13).ToString '"E48" "Unidad de servicio" '"No Aplica" 'Cambio  Id que corresponde
                            oConcepto.UnidadMedida = reader(12).ToString '"Unidad de servicio" ' Pruebo poner una al gusto

                            oConcepto.IVARET_Importe = "0"
                            oConcepto.ISRRET_Importe = "0"

                            'Trasladados
                            If Double.Parse(reader(8).ToString) > 0 Then
                                oImpuestosTraslados = New CFDImpuestosTraslados()
                                oImpuestosTraslados.IdRegistroTraslado = oContrador
                                oImpuestosTraslados.Impuesto = "IVA"
                                oImpuestosTraslados.Importe = reader(8).ToString
                                oImpuestosTraslados.Tasa = reader(7).ToString 'oTasaIva
                                'oImpuestosTraslados.TipoFactor = GloTIPOFACTOR
                                oImpuestosTraslados.TasaOCuota = GloTIPOFACTOR
                                oImpuestosTraslados.IEPSTRAS_Importe = "0"
                                oImpuestosTraslados.IVATRAS_Importe = "0"

                                oCFD.Impuestos.Traslados.Add(oImpuestosTraslados)
                            End If
                            If Double.Parse(reader(11).ToString) > 0 Then
                                oImpuestosTraslados = New CFDImpuestosTraslados()
                                oImpuestosTraslados.IdRegistroTraslado = oContrador + 1
                                oImpuestosTraslados.Impuesto = "IEPS" '"003" '"IEPS"
                                oImpuestosTraslados.Importe = reader(11).ToString
                                oImpuestosTraslados.Tasa = reader(10).ToString 'oTasaIeps
                                oImpuestosTraslados.TasaOCuota = GloTIPOFACTOR
                                oImpuestosTraslados.FactorIepsTras = GloTIPOFACTOR
                                oImpuestosTraslados.IEPSTRAS_Importe = "0"
                                oImpuestosTraslados.IVATRAS_Importe = "0"
                                'oImpuestosTraslados.TipoFactor = GloTIPOFACTOR

                                oCFD.Impuestos.Traslados.Add(oImpuestosTraslados)
                            End If



                            oConcepto.RetenerIVA = "0"
                            oConcepto.RetenerISR = "0"

                            oCFD.Conceptos.Add(oConcepto)
                            oContrador = oContrador + 2
                        End While
                        'Cnx.DbConnection.Close()
                    Catch ex As Exception
                        If Graba = "0" Then
                            Llenalog(ex.Source.ToString & " " & ex.Message)
                        Else

                            Llenalog(ex.Source.ToString & " " & ex.Message)
                        End If
                    Finally
                        CONEXION2.Close()
                        CONEXION2.Dispose()
                    End Try

                    ' Dim a As String = JsonConvert.SerializeObject(oCFD)
                    Dim bnd As Boolean = False

                    Dim Mensaje1 As String = ""
                    'oCFD.Validar(Cnx, )
                    If Not oCFD.Insertar(Cnx, True, False) Then
                        For Each oMensaje As DAMensajesSistema.RegistroMensaje In oCFD.MensajesSistema.ListaMensajes
                            Mensaje1 = oMensaje.TextoMensaje + " " + oMensaje.Contexto
                            Inserta_Tbl_FacturasNoGeneradas(oClv_Factura, "N", Mensaje1, locconexion)
                            bnd = True
                            'MessageBox.Show(oMensaje.TextoMensaje + " " + oMensaje.Contexto)
                        Next
                        Return
                    End If
                    GloClv_FacturaCFD = 0
                    GloClv_FacturaCFD = oCFD.IdCFD
                    UPS_Inserta_Rel_FacturasCFD(oClv_Factura, GloClv_FacturaCFD, "", "N", locconexion)
                    GloTxtUlt4DigCheque = ""
                    Dim r As New Globalization.CultureInfo("es-MX")

                    r.DateTimeFormat.ShortDatePattern = "dd/MM/yyyy"

                    System.Threading.Thread.CurrentThread.CurrentCulture = r
                    Try
                        'oCFD.EnviarEmail(Cnx)
                        If GloClv_FacturaCFD > 0 And bnd = False Then
                            ENVIAR_Factura(GloClv_FacturaCFD)
                        End If

                    Catch ex As Exception

                    End Try



                Catch ex As Exception
                    If Graba = "0" Then
                        Llenalog(ex.Source.ToString & " " & ex.Message)
                    Else

                        Llenalog(ex.Source.ToString & " " & ex.Message)
                    End If
                Finally
                    CONEXION.Close()
                    CONEXION.Dispose()
                End Try

            End Using
        Else
            GloClv_FacturaCFD = ClvFacturaCfd
        End If

    End Sub

    Public Shared Sub Graba_Factura_DigitalRefacturacion(ByVal oClv_Factura As Long, ByVal oIden As Integer, ByVal locconexion As String) ' MiConexion

        Dim ClvFacturaCfd As Long = 0
        'ClvFacturaCfd = SP_DIMESIEXISTEFACTURADIGITAL(oClv_Factura, "N", locconexion)

        If ClvFacturaCfd = 0 Then '0 Es si no Existe y si debe de Genera la Factura


            Dim oTotalConPuntos As String = Nothing
            Dim oSubTotal As String = Nothing
            Dim oTotalSinPuntos As String = Nothing
            Dim oDescuento As String = Nothing
            Dim oiva As String = Nothing
            Dim oieps As String = Nothing
            Dim oTasaIva As String = Nothing
            Dim oTasaIeps As String = Nothing
            Dim oFecha As String = Nothing
            Dim oTotalImpuestos As String = Nothing


            Dim mfecha As DateTime
            Dim con As New SqlConnection(locconexion)
            Dim com As New SqlCommand("DAMEFECHADELSERVIDORFACTURADIGITAL", con)
            com.CommandType = CommandType.StoredProcedure
            com.CommandTimeout = 0
            'Dame la fecha del Servidor
            Dim prmFechaObtebida As New SqlParameter("@FECHA", SqlDbType.DateTime)
            prmFechaObtebida.Direction = ParameterDirection.Output
            prmFechaObtebida.Value = ""
            com.Parameters.Add(prmFechaObtebida)
            Try
                con.Open()
                com.ExecuteNonQuery()
                mfecha = prmFechaObtebida.Value
            Catch ex As Exception
                If Graba = "0" Then
                    Llenalog(ex.Source.ToString & " " & ex.Message)
                Else

                    Llenalog(ex.Source.ToString & " " & ex.Message)
                End If
            Finally
                con.Close()
            End Try
            ''

            Class1.limpiaParametros()
            Class1.CreateMyParameter("@FECHA", ParameterDirection.Output, SqlDbType.DateTime)
            Class1.ProcedimientoOutPut("DAMEFECHADELSERVIDORFACTURADIGITAL", locconexion)
            mfecha = DateTime.Parse(Class1.dicoPar("@FECHA").ToString())


            Dim CONEXION As New SqlConnection(locconexion)
            Dim COMANDO As New SqlCommand("DameFacDig_Parte_1", CONEXION)
            COMANDO.CommandType = CommandType.StoredProcedure
            COMANDO.CommandTimeout = 0

            Dim parametro As New SqlParameter("@Clv_Factura", SqlDbType.BigInt)
            parametro.Direction = ParameterDirection.Input
            parametro.Value = oClv_Factura
            COMANDO.Parameters.Add(parametro)

            Dim parametro1 As New SqlParameter("@TotalConPuntos", SqlDbType.Money)
            parametro1.Direction = ParameterDirection.Output
            parametro1.Value = 0
            COMANDO.Parameters.Add(parametro1)

            Dim parametro2 As New SqlParameter("@SubTotal", SqlDbType.Money)
            parametro2.Direction = ParameterDirection.Output
            parametro2.Value = 0
            COMANDO.Parameters.Add(parametro2)

            Dim parametro3 As New SqlParameter("@TotalSinPuntos", SqlDbType.Money)
            parametro3.Direction = ParameterDirection.Output
            parametro3.Value = 0
            COMANDO.Parameters.Add(parametro3)

            Dim parametro4 As New SqlParameter("@Descuento", SqlDbType.Money)
            parametro4.Direction = ParameterDirection.Output
            parametro4.Value = 0
            COMANDO.Parameters.Add(parametro4)

            Dim parametro5 As New SqlParameter("@iva", SqlDbType.Money)
            parametro5.Direction = ParameterDirection.Output
            parametro5.Value = 0
            COMANDO.Parameters.Add(parametro5)

            Dim parametro6 As New SqlParameter("@ieps", SqlDbType.Money)
            parametro6.Direction = ParameterDirection.Output
            parametro6.Value = 0
            COMANDO.Parameters.Add(parametro6)

            Dim parametro7 As New SqlParameter("@TasaIva", SqlDbType.Money)
            parametro7.Direction = ParameterDirection.Output
            parametro7.Value = 0
            COMANDO.Parameters.Add(parametro7)

            Dim parametro8 As New SqlParameter("@TasaIeps", SqlDbType.Money)
            parametro8.Direction = ParameterDirection.Output
            parametro8.Value = 0
            COMANDO.Parameters.Add(parametro8)

            Dim parametro9 As New SqlParameter("@Fecha", SqlDbType.DateTime)
            parametro9.Direction = ParameterDirection.Output
            parametro9.Value = 0
            COMANDO.Parameters.Add(parametro9)

            Dim parametro10 As New SqlParameter("@TotalImpuestos", SqlDbType.Money)
            parametro10.Direction = ParameterDirection.Output
            parametro10.Value = 0
            COMANDO.Parameters.Add(parametro10)

            Try
                CONEXION.Open()
                Dim i As Integer = COMANDO.ExecuteNonQuery
                oTotalConPuntos = CStr(parametro1.Value)
                oSubTotal = CStr(parametro2.Value)
                oTotalSinPuntos = CStr(parametro3.Value)
                oDescuento = CStr(parametro4.Value)
                oiva = CStr(parametro5.Value)
                oieps = CStr(parametro6.Value)
                oTasaIva = "0.1600" 'CStr(parametro7.Value)
                oTasaIeps = "0.0300" 'CStr(parametro8.Value)
                oFecha = CStr(parametro9.Value)
                oTotalImpuestos = CStr(parametro10.Value)
                'Cnx.DbConnection.Close()
            Catch ex As Exception
                If Graba = "0" Then
                    Llenalog(ex.Source.ToString & " " & ex.Message)
                Else

                    Llenalog(ex.Source.ToString & " " & ex.Message)
                End If
            Finally
                CONEXION.Close()
                CONEXION.Dispose()
            End Try

            Usp_Ed_DameDatosFacDigRefacturacion(oClv_Factura, locID_Compania_Mizart, locconexion)

            Dim oCFD As MizarCFD.BRL.CFD

            Dim oId_AsociadoLlave As Long = 0
            oId_AsociadoLlave = oIden 'Existe_DevuelveValor(" Select id_asociado from asociados  where id_asociado = " + CStr(oIden))


            Using Cnx As New DAConexion("HL", "sa", "sa")
                Try
                    oIden = "1"

                    oCFD = New MizarCFD.BRL.CFD
                    oCFD.Inicializar()
                    oCFD.TipoCFD = MizarCFD.BRL.CFD.TiposCFD.Emision
                    ''Ojo 
                    'locID_Sucursal_Mizart = 0
                    'oCFD.PrepararDatosEmisorPorId(Cnx, locID_Compania_Mizart, locID_Sucursal_Mizart)
                    If locID_Sucursal_Mizart.Length > 0 And locID_Sucursal_Mizart <> "0" Then
                        oCFD.PrepararDatosEmisorPorId(Cnx, locID_Compania_Mizart, locID_Sucursal_Mizart)
                    Else
                        oCFD.PrepararDatosEmisorPorId(Cnx, locID_Compania_Mizart)
                    End If
                    oCFD.PrepararDatosReceptorPorId(Cnx, CStr(oId_AsociadoLlave), 1, 0)


                    oCFD.EsTimbrePrueba = EsTimbrePrueba
                    oCFD.Timbrador = CFD.PAC.Mizar

                    oCFD.Version = GloVERSON
                    oCFD.IdMoneda = 1  '//1 = Pesos , Pesos = 2 USD
                    'oFecha
                    'oCFD.Fecha = mfecha '//Date.Now
                    oCFD.FormaDePago = GloFORMADEPAGO
                    'oCFD.Certificado = ""
                    oCFD.CondicionesDePago = GloCONDICIONESDEPAGO
                    oCFD.SubTotal = oSubTotal
                    oCFD.Descuento = oDescuento
                    oCFD.MotivoDescuento = GloMOTIVODESCUENTO
                    oCFD.Total = oTotalSinPuntos
                    oCFD.MetodoDePago = GloMETODODEPAGO

                    oCFD.TipoDeComprobante = GloTIPODECOMPROBANTE
                    oCFD.TipoComprobante33 = GloTIPODECOMPROBANTE
                    oCFD.PagoEnParcialiades = GloPAGOENPARCIALIDADES
                    oCFD.LugarExpedicion = GloPAGOENPARCIALIDADES
                    oCFD.CuentaPago = GlonumCtaPago
                    oCFD.Moneda = GloMONEDA

                    oCFD.UsoCFDI = GloUSOCFD
                    oCFD.EsNotaCredito = 0
                    oCFD.TipoRelacion = "04"
                    oCFD.UUIDRelacionados = GloUUIDRel
                    oCFD.UUIDRel.Add(GloUUIDRel)
                    'oCFD.FechaVencimiento = Date.Parse(GloFECHAVIGENCIA)
                    'oCFD.CondicionesDePago = ""
                    oCFD.TipoCambio = GloTIPOCAMBIO

                    'oCFD.IdEstatus = 1

                    Dim oRegimen As New MizarCFD.BRL.CFDRegimenesFiscales
                    oRegimen.IdRegimen = 0
                    oRegimen.IdCFD = 0
                    oRegimen.Regimen = GloREGIMEN
                    'oRegimen.Insertar(Cnx)

                    oCFD.RegimenesFiscales.Add(oRegimen)

                    'oCFD.Complemento.XMLComplemento = "<raiz>Caelli Enarrant Gloria Dei </raiz>"
                    'oCFD.Addenda.XMLAddenda = "<edi>Edit dididididididi</edi>"

                    oCFD.Impuestos.TotalImpuestosRetenidos = 0
                    oCFD.Impuestos.TotalImpuestosTrasladados = oTotalImpuestos
                    'Dim oimpuestosRetenciones As CFDImpuestosRetenciones
                    'oimpuestosRetenciones = New CFDImpuestosRetenciones()
                    'oimpuestosRetenciones.Impuesto = "IVA"
                    'oimpuestosRetenciones.Importe = oiva
                    'oCFD.Impuestos.Retenciones.Add(oimpuestosRetenciones)



                    'oImpuestosTraslados = New CFDImpuestosTraslados()
                    'oImpuestosTraslados.Impuesto = "002" '"IVA"
                    'oImpuestosTraslados.Importe = oiva
                    'oImpuestosTraslados.Tasa = oTasaIva
                    ''oImpuestosTraslados.TipoFactor = GloTIPOFACTOR
                    'oImpuestosTraslados.TasaOCuota = GloTIPOFACTOR

                    'oCFD.Impuestos.Traslados.Add(oImpuestosTraslados)

                    'oImpuestosTraslados = New CFDImpuestosTraslados()
                    'oImpuestosTraslados.Impuesto = "003" '"IEPS"
                    'oImpuestosTraslados.Importe = oieps
                    'oImpuestosTraslados.Tasa = oTasaIeps
                    'oImpuestosTraslados.TasaOCuota = GloTIPOFACTOR
                    'oImpuestosTraslados.FactorIepsTras = GloTIPOFACTOR
                    ''oImpuestosTraslados.TipoFactor = GloTIPOFACTOR

                    'oCFD.Impuestos.Traslados.Add(oImpuestosTraslados)
                    Dim oImpuestosTraslados As CFDImpuestosTraslados
                    oImpuestosTraslados = New CFDImpuestosTraslados()

                    Dim oConcepto As CFDConceptos
                    Dim oContrador As Integer = 1

                    Dim CONEXION2 As New SqlConnection(locconexion)
                    Dim COMANDO2 As New SqlCommand("exec DameFacDig_Parte_2 " + CStr(oClv_Factura), CONEXION2)
                    COMANDO2.CommandType = CommandType.Text
                    COMANDO2.CommandTimeout = 0
                    Try
                        CONEXION2.Open()
                        Dim reader As SqlDataReader = COMANDO2.ExecuteReader
                        '    'Recorremos los Cargos obtenidos desde el Procedimiento
                        While (reader.Read())
                            oConcepto = New CFDConceptos()
                            oConcepto.Cantidad = reader(0).ToString
                            'oConcepto.UnidadMedida = "No Aplica"
                            oConcepto.NumeroIdentificacion = reader(1).ToString
                            oConcepto.Descripcion = reader(2).ToString
                            oConcepto.ValorUnitario = reader(3).ToString
                            oConcepto.Importe = reader(4).ToString
                            oConcepto.TrasladarIVA = "1"

                            'Los nuevos
                            oConcepto.Descuento = 0 'Valor Nuevo
                            ' oConcepto.ClaveProdServ = reader(5).ToString ' "83111801" 'Campo nuevo
                            oConcepto.idClaveProdServ = reader(5).ToString
                            'Iva
                            'oConcepto.IVATRAS_TipoFactor = reader(6).ToString
                            oConcepto.TipoFactor = reader(6).ToString
                            oConcepto.IVATRAS_Importe = reader(8).ToString 'Iva del ticket
                            'oConcepto.IVATRAS_TasaOCuota = reader(7).ToString '"0.160000"


                            oConcepto.TasaIVA = reader(7).ToString
                            oConcepto.TrasladarIVA = "1"
                            'Ieps
                            'oConcepto.IEPSTRAS_TipoFactor = reader(9).ToString '"Tasa"
                            'oConcepto.IEPSTRAS_TasaOCuota = reader(10).ToString '"0.030000"
                            If Double.Parse(reader(11).ToString) > 0 Then
                                oConcepto.BaseIEPS = reader(4).ToString 'Cantida Subtotal 
                                'oConcepto.BaseIVA = CDec(reader(4).ToString) + CDec(reader(11).ToString)
                                If SumaSubtotalYIeps = True Then
                                    oConcepto.BaseIVA = CDec(reader(4).ToString) + CDec(reader(11).ToString)
                                Else
                                    oConcepto.BaseIVA = CDec(reader(4).ToString)
                                End If

                                oConcepto.IEPSTRAS_Importe = reader(11).ToString 'Total del ieps 'Ieps del ticket
                                oConcepto.TasaIEPS = reader(10).ToString

                                oConcepto.FactorIEPS = reader(9).ToString
                                oConcepto.TrasladarIEPS = "1"
                            Else
                                oConcepto.BaseIEPS = 0 'Cantida Subtotal 
                                oConcepto.BaseIVA = reader(4).ToString
                                oConcepto.IEPSTRAS_Importe = "0" 'Total del ieps 'Ieps del ticket
                                oConcepto.TrasladarIEPS = "0"
                            End If
                            oConcepto.idUnidadMedida = reader(13).ToString '"E48" "Unidad de servicio" '"No Aplica" 'Cambio  Id que corresponde
                            oConcepto.UnidadMedida = reader(12).ToString '"Unidad de servicio" ' Pruebo poner una al gusto

                            oConcepto.IVARET_Importe = "0"
                            oConcepto.ISRRET_Importe = "0"

                            'Trasladados
                            If Double.Parse(reader(8).ToString) > 0 Then
                                oImpuestosTraslados = New CFDImpuestosTraslados()
                                oImpuestosTraslados.IdRegistroTraslado = oContrador
                                oImpuestosTraslados.Impuesto = "IVA"
                                oImpuestosTraslados.Importe = reader(8).ToString
                                oImpuestosTraslados.Tasa = reader(7).ToString 'oTasaIva
                                'oImpuestosTraslados.TipoFactor = GloTIPOFACTOR
                                oImpuestosTraslados.TasaOCuota = GloTIPOFACTOR
                                oImpuestosTraslados.IEPSTRAS_Importe = "0"
                                oImpuestosTraslados.IVATRAS_Importe = "0"

                                oCFD.Impuestos.Traslados.Add(oImpuestosTraslados)
                            End If
                            If Double.Parse(reader(11).ToString) > 0 Then
                                oImpuestosTraslados = New CFDImpuestosTraslados()
                                oImpuestosTraslados.IdRegistroTraslado = oContrador + 1
                                oImpuestosTraslados.Impuesto = "IEPS" '"003" '"IEPS"
                                oImpuestosTraslados.Importe = reader(11).ToString
                                oImpuestosTraslados.Tasa = reader(10).ToString 'oTasaIeps
                                oImpuestosTraslados.TasaOCuota = GloTIPOFACTOR
                                oImpuestosTraslados.FactorIepsTras = GloTIPOFACTOR
                                oImpuestosTraslados.IEPSTRAS_Importe = "0"
                                oImpuestosTraslados.IVATRAS_Importe = "0"
                                'oImpuestosTraslados.TipoFactor = GloTIPOFACTOR

                                oCFD.Impuestos.Traslados.Add(oImpuestosTraslados)
                            End If



                            oConcepto.RetenerIVA = "0"
                            oConcepto.RetenerISR = "0"

                            oCFD.Conceptos.Add(oConcepto)
                            oContrador = oContrador + 2
                        End While
                        'Cnx.DbConnection.Close()
                    Catch ex As Exception
                        If Graba = "0" Then
                            Llenalog(ex.Source.ToString & " " & ex.Message)
                        Else

                            Llenalog(ex.Source.ToString & " " & ex.Message)
                        End If
                    Finally
                        CONEXION2.Close()
                        CONEXION2.Dispose()
                    End Try

                    'Dim a As String = JsonConvert.SerializeObject(oCFD)
                    Dim Bnd As Boolean = False
                    Dim Mensaje1 As String = ""
                    'oCFD.Validar(Cnx, )
                    If Not oCFD.Insertar(Cnx, True, False) Then
                        For Each oMensaje As DAMensajesSistema.RegistroMensaje In oCFD.MensajesSistema.ListaMensajes
                            Mensaje1 = oMensaje.TextoMensaje + " " + oMensaje.Contexto
                            Inserta_Tbl_FacturasNoGeneradas(oClv_Factura, "N", Mensaje1, locconexion)
                            'MessageBox.Show(oMensaje.TextoMensaje + " " + oMensaje.Contexto)
                            Bnd = True
                        Next
                        Return
                    End If
                    GloClv_FacturaCFD = 0
                    GloClv_FacturaCFD = oCFD.IdCFD

                    UPS_Inserta_Rel_FacturasCFD(oClv_Factura, GloClv_FacturaCFD, "", "N", locconexion)
                    GloTxtUlt4DigCheque = ""

                    Try
                        'oCFD.EnviarEmail(Cnx)
                        'ENVIAR_Factura(GloClv_FacturaCFD)
                        If GloClv_FacturaCFD > 0 And bnd = False Then
                            ENVIAR_Factura(GloClv_FacturaCFD)
                        End If
                    Catch ex As Exception

                    End Try


                Catch ex As Exception
                    If Graba = "0" Then
                        Llenalog(ex.Source.ToString & " " & ex.Message)
                    Else

                        Llenalog(ex.Source.ToString & " " & ex.Message)
                    End If
                Finally
                    CONEXION.Close()
                    CONEXION.Dispose()
                End Try

            End Using
        Else
            GloClv_FacturaCFD = ClvFacturaCfd
        End If

    End Sub

    Public Shared Sub Graba_Factura_DigitalPago(ByVal oClv_Factura As Long, ByVal oIden As Integer, ByVal locconexion As String) ' MiConexion
        oIden = BusFacFiscalOledbPago(oClv_Factura, locconexion)

        If oIden > 0 Then

            Dime_Aque_Compania_FacturarlePago(oClv_Factura, locconexion)

            Dim ClvFacturaCfd As Long = 0
            ClvFacturaCfd = SP_DIMESIEXISTEFACTURADIGITAL(oClv_Factura, "P", locconexion)
            If ClvFacturaCfd = 0 Then '0 Es si no Existe y si debe de Genera la Factura


                Dim oTotalConPuntos As String = Nothing
                Dim oSubTotal As String = Nothing
                Dim oTotalSinPuntos As String = Nothing
                Dim oDescuento As String = Nothing
                Dim oiva As String = Nothing
                Dim oieps As String = Nothing
                Dim oTasaIva As String = Nothing
                Dim oTasaIeps As String = Nothing
                Dim oFecha As String = Nothing
                Dim oTotalImpuestos As String = Nothing


                Usp_Ed_DameDatosFacDigPago(oClv_Factura, locID_Compania_Mizart, locconexion)

                Dim oCFD As MizarCFD.BRL.CFD

                'oIden = "2226"
                Dim oId_AsociadoLlave As Long = 0
                oId_AsociadoLlave = oIden 'Existe_DevuelveValor(" Select id_asociado from asociados  where id_asociado = " + CStr(oIden))


                Using Cnx As New DAConexion("HL", "sa", "sa")
                    Try
                        'oIden = "2226"

                        oCFD = New MizarCFD.BRL.CFD
                        oCFD.Inicializar()
                        oCFD.TipoCFD = MizarCFD.BRL.CFD.TiposCFD.Emision

                        'Consultamos los datos de la factura original
                        Dim oCFDFacturaOriginal As New MizarCFD.BRL.CFD
                        oCFDFacturaOriginal.IdCFD = GloID_CFD
                        oCFDFacturaOriginal.Consultar(Cnx)

                        ''Ojo 
                        'locID_Sucursal_Mizart = 0
                        'oCFD.PrepararDatosEmisorPorId(Cnx, locID_Compania_Mizart, locID_Sucursal_Mizart)
                        If locID_Sucursal_Mizart.Length > 0 And locID_Sucursal_Mizart <> "0" Then
                            oCFD.PrepararDatosEmisorPorId(Cnx, locID_Compania_Mizart, locID_Sucursal_Mizart)
                        Else
                            oCFD.PrepararDatosEmisorPorId(Cnx, locID_Compania_Mizart)
                        End If
                        oCFD.PrepararDatosReceptorPorId(Cnx, CStr(oId_AsociadoLlave), 1, 0)

                        'Pagos
                        Dim oPagos As New MizarCFD.BRL.CFDPagos
                        oPagos.Descripcion = "Pago"
                        oPagos.TipoPago = GloFORMAPAGO
                        oPagos.idMoneda = GloMONEDA

                        'Dólares - Dólares no se captura y se pone el de la original
                        If oCFDFacturaOriginal.Moneda.Contains("mericano") And GloMONEDA = "USD" Then

                            oPagos.TipoCambio = oCFDFacturaOriginal.TipoCambio 'GloTIPOCAMBIO;//(1 / decimal.Parse(GloTIPOCAMBIO)).ToString();

                        ElseIf oCFDFacturaOriginal.Moneda.Contains("exicano") And GloMONEDA = "MXN" Then

                            oPagos.TipoCambio = "1.00"
                        End If

                        oPagos.MontoTotalOriginal = GloMONTOPAGO
                        oPagos.FechaTransaccion = GloFECHAPAGO
                        oPagos.Version = "1.0"
                        oPagos.MontoPago = GloMONTOPAGO

                        'Pago Complemento
                        Dim oPagoComp As New MizarCFD.BRL.CFDPagosComplemento
                        oPagoComp.IDCFD = oCFDFacturaOriginal.IdCFD
                        oPagoComp.IdDocumento = GloUUIDRel
                        oPagoComp.Serie = oCFDFacturaOriginal.Serie
                        oPagoComp.Folio = oCFDFacturaOriginal.Folio
                        oPagoComp.MonedaDR = If(oCFDFacturaOriginal.Moneda.Contains("Peso"), "MXN", "USD") 'GloMONEDA

                        'TipoCambioDR= NO SE DEBE LLENAR CUANDO MonedaP y MonedaDR son iguales
                        If ((oCFDFacturaOriginal.Moneda = "Dolar americano" And GloMONEDA = "MXN")) Then
                            oPagoComp.TipoCambioDR = GloTIPOCAMBIO
                        End If

                        oPagoComp.MetodoPagoDR = oCFDFacturaOriginal.MetodoDePago
                        oPagoComp.SaldoAnterior = GloSALDOANTERIOR
                        oPagoComp.SaldoNuevo = GloSALDONUEVO
                        oPagoComp.MontoPagoDR = GloMONTOORIGINAL
                        oPagoComp.NumParcialidad = GloNUMPARCIALIDAD
                        oPagoComp.MontoOriginal = GloMONTOPAGO
                        oPagos.DoctoRel.Add(oPagoComp)


                        oCFD.IdCFD = DAUtileriasSQL.ObtenerSiguienteId(Cnx, "id_cfd", "cfd").ToString()
                        oPagos.IdCFDGenerado = oCFD.IdCFD
                        oCFD.Pagos.Add(oPagos)
                        'oCFD.NumeroCertificado = oCFDFacturaOriginal.NumeroCertificado

                        oCFD.EsTimbrePrueba = EsTimbrePrueba
                        oCFD.Timbrador = CFD.PAC.Mizar
                        oCFD.Version = GloVERSON
                        If (GloMONEDA = "USD") Then
                            oCFD.IdMoneda = "2"
                        Else
                            oCFD.IdMoneda = "1" '//1 = Pesos , Pesos = 2 USD  'oFecha
                        End If
                        oCFD.FormaDePago = GloFORMAPAGO
                        'oCFD.CondicionesDePago = GloCONDICIONESDEPAGO
                        oCFD.SubTotal = "0.00"
                        oCFD.Descuento = "0.00"
                        oCFD.MotivoDescuento = GloMOTIVODESCUENTO
                        oCFD.Total = "0.00"
                        'oCFD.MetodoDePago = GloMETODODEPAGO
                        oCFD.TipoDeComprobante = GloTIPODECOMPROBANTE
                        oCFD.TipoComprobante33 = GloTIPODECOMPROBANTE
                        oCFD.PagoEnParcialiades = "0"
                        oCFD.LugarExpedicion = oCFDFacturaOriginal.LugarExpedicion
                        oCFD.CuentaPago = GlonumCtaPago
                        oCFD.Moneda = "XXX"
                        oCFD.UsoCFDI = GloUSOCFD
                        oCFD.EsNotaCredito = 2

                        Dim ComplementoPago = oPagos.ObtenerXMLCFDi()
                        oCFD.Complemento.XMLPlantilla = ComplementoPago.OuterXml
                        oCFD.Complemento.IdPlantillaXSD = "0"

                        Dim oConcepto As New CFDConceptos
                        oConcepto.idClaveProdServ = "84111506"
                        oConcepto.Cantidad = "1"
                        oConcepto.idUnidadMedida = "ACT"
                        oConcepto.Descripcion = "Pago"
                        oConcepto.ValorUnitario = "0"
                        oConcepto.Importe = "0"

                        oConcepto.IVARET_Importe = "0"
                        oConcepto.IVATRAS_Importe = "0"
                        oConcepto.IEPSTRAS_Importe = "0"
                        oConcepto.ISRRET_Importe = "0"

                        oCFD.Conceptos.Add(oConcepto)


                        'Dim a As String = JsonConvert.SerializeObject(oCFD)

                        Dim bnd As Boolean = False

                        Dim Mensaje1 As String = ""
                        'oCFD.Validar(Cnx, )
                        If Not oCFD.Insertar(Cnx, True, True) Then
                            For Each oMensaje As DAMensajesSistema.RegistroMensaje In oCFD.MensajesSistema.ListaMensajes
                                Mensaje1 = oMensaje.TextoMensaje + " " + oMensaje.Contexto
                                Inserta_Tbl_FacturasNoGeneradas(oClv_Factura, "P", Mensaje1, locconexion)
                                bnd = True
                                'MessageBox.Show(oMensaje.TextoMensaje + " " + oMensaje.Contexto)
                            Next
                            Return
                        End If
                        GloClv_FacturaCFD = 0
                        GloClv_FacturaCFD = oCFD.IdCFD
                        UPS_Inserta_Rel_FacturasCFD(oClv_Factura, GloClv_FacturaCFD, "", "P", locconexion)
                        GloTxtUlt4DigCheque = ""
                        Dim r As New Globalization.CultureInfo("es-MX")

                        r.DateTimeFormat.ShortDatePattern = "dd/MM/yyyy"

                        System.Threading.Thread.CurrentThread.CurrentCulture = r
                        Try
                            'oCFD.EnviarEmail(Cnx)
                            If GloClv_FacturaCFD > 0 And bnd = False Then
                                ENVIAR_Factura(GloClv_FacturaCFD)
                            End If

                        Catch ex As Exception

                        End Try



                    Catch ex As Exception
                        If Graba = "0" Then
                            'MsgBox(ex.Source.ToString & " 4 " & ex.Message)
                        Else

                            Llenalog(ex.Source.ToString & " " & ex.Message)
                        End If
                    Finally

                    End Try

                End Using
            Else
                GloClv_FacturaCFD = ClvFacturaCfd
            End If
        End If

    End Sub

    Public Shared Sub Graba_Factura_DigitalMaestrotvzac(ByVal oClv_Factura As Long, ByVal oIden As Integer, ByVal locconexion As String)

        oIden = BusFacFiscalOledbMaestrotvzac(oClv_Factura, locconexion)
        If oIden > 0 Then

            Dime_Aque_Compania_FacturarleMaestrotvzac(oClv_Factura, locconexion)

            Dim ClvFacturaCfd As Long = 0
            ClvFacturaCfd = SP_DIMESIEXISTEFACTURADIGITAL(oClv_Factura, "M", locconexion)
            If ClvFacturaCfd = 0 Then '0 Es si no Existe y si debe de Genera la Factura


                Dim oTotalConPuntos As String = Nothing
                Dim oSubTotal As String = Nothing
                Dim oTotalSinPuntos As String = Nothing
                Dim oDescuento As String = Nothing
                Dim oiva As String = Nothing
                Dim oieps As String = Nothing
                Dim oTasaIva As String = Nothing
                Dim oTasaIeps As String = Nothing
                Dim oFecha As String = Nothing
                Dim oTotalImpuestos As String = Nothing


                Dim CONEXION As New SqlConnection(locconexion)
                Dim COMANDO As New SqlCommand("DameFacDig_Parte_1_Maestro2", CONEXION)
                COMANDO.CommandType = CommandType.StoredProcedure
                COMANDO.CommandTimeout = 0

                Dim parametro As New SqlParameter("@Clv_FacturaMaestro", SqlDbType.BigInt)
                parametro.Direction = ParameterDirection.Input
                parametro.Value = oClv_Factura
                COMANDO.Parameters.Add(parametro)

                Dim parametro1 As New SqlParameter("@TotalConPuntos", SqlDbType.Money)
                parametro1.Direction = ParameterDirection.Output
                parametro1.Value = 0
                COMANDO.Parameters.Add(parametro1)

                Dim parametro2 As New SqlParameter("@SubTotal", SqlDbType.Money)
                parametro2.Direction = ParameterDirection.Output
                parametro2.Value = 0
                COMANDO.Parameters.Add(parametro2)

                Dim parametro3 As New SqlParameter("@TotalSinPuntos", SqlDbType.Money)
                parametro3.Direction = ParameterDirection.Output
                parametro3.Value = 0
                COMANDO.Parameters.Add(parametro3)

                Dim parametro4 As New SqlParameter("@Descuento", SqlDbType.Money)
                parametro4.Direction = ParameterDirection.Output
                parametro4.Value = 0
                COMANDO.Parameters.Add(parametro4)

                Dim parametro5 As New SqlParameter("@iva", SqlDbType.Money)
                parametro5.Direction = ParameterDirection.Output
                parametro5.Value = 0
                COMANDO.Parameters.Add(parametro5)

                Dim parametro6 As New SqlParameter("@ieps", SqlDbType.Money)
                parametro6.Direction = ParameterDirection.Output
                parametro6.Value = 0
                COMANDO.Parameters.Add(parametro6)

                Dim parametro7 As New SqlParameter("@TasaIva", SqlDbType.Money)
                parametro7.Direction = ParameterDirection.Output
                parametro7.Value = 0
                COMANDO.Parameters.Add(parametro7)

                Dim parametro8 As New SqlParameter("@TasaIeps", SqlDbType.Money)
                parametro8.Direction = ParameterDirection.Output
                parametro8.Value = 0
                COMANDO.Parameters.Add(parametro8)

                Dim parametro9 As New SqlParameter("@Fecha", SqlDbType.DateTime)
                parametro9.Direction = ParameterDirection.Output
                parametro9.Value = 0
                COMANDO.Parameters.Add(parametro9)

                Dim parametro10 As New SqlParameter("@TotalImpuestos", SqlDbType.Money)
                parametro10.Direction = ParameterDirection.Output
                parametro10.Value = 0
                COMANDO.Parameters.Add(parametro10)

                Try
                    CONEXION.Open()
                    Dim i As Integer = COMANDO.ExecuteNonQuery
                    oTotalConPuntos = CStr(parametro1.Value)
                    oSubTotal = CStr(parametro2.Value)
                    oTotalSinPuntos = CStr(parametro3.Value)
                    oDescuento = CStr(parametro4.Value)
                    oiva = CStr(parametro5.Value)
                    oieps = CStr(parametro6.Value)
                    oTasaIva = CStr(parametro7.Value)
                    oTasaIeps = CStr(parametro8.Value)
                    oFecha = CStr(parametro9.Value)
                    oTotalImpuestos = CStr(parametro10.Value)
                    'Cnx.DbConnection.Close()
                Catch ex As Exception
                    'MsgBox(ex.Message & " Parte 1 " & ex.Source, MsgBoxStyle.Exclamation)
                    If Graba = "0" Then
                        MsgBox(ex.Source.ToString & "2 " & ex.Message)
                    Else

                        Llenalog(ex.Source.ToString & " " & ex.Message)
                    End If
                Finally
                    CONEXION.Close()
                    CONEXION.Dispose()
                End Try

                Usp_Ed_DameDatosFacDigMaestrotvzac(oClv_Factura, locID_Compania_Mizart, locconexion)

                Dim oCFD As MizarCFD.BRL.CFD

                'oIden = "2226"
                Dim oId_AsociadoLlave As Long = 0
                oId_AsociadoLlave = oIden 'Existe_DevuelveValor(" Select id_asociado from asociados  where id_asociado = " + CStr(oIden))


                Using Cnx As New DAConexion("HL", "sa", "sa")
                    Try
                        'oIden = "2226"

                        oCFD = New MizarCFD.BRL.CFD
                        oCFD.Inicializar()
                        oCFD.TipoCFD = MizarCFD.BRL.CFD.TiposCFD.Emision
                        ''Ojo 
                        'locID_Sucursal_Mizart = 0
                        'oCFD.PrepararDatosEmisorPorId(Cnx, locID_Compania_Mizart, locID_Sucursal_Mizart)
                        If locID_Sucursal_Mizart.Length > 0 And locID_Sucursal_Mizart <> "0" Then
                            oCFD.PrepararDatosEmisorPorId(Cnx, locID_Compania_Mizart, locID_Sucursal_Mizart)
                        Else
                            oCFD.PrepararDatosEmisorPorId(Cnx, locID_Compania_Mizart)
                        End If
                        oCFD.PrepararDatosReceptorPorId(Cnx, CStr(oId_AsociadoLlave), 1, 0)


                        oCFD.EsTimbrePrueba = EsTimbrePrueba
                        oCFD.Timbrador = CFD.PAC.Mizar

                        oCFD.Version = GloVERSON
                        If GloMONEDA = "USD" Then

                            oCFD.IdMoneda = "2"

                        Else

                            oCFD.IdMoneda = "1" '1 = Pesos , Pesos = 2 USD  'oFecha
                        End If
                        oCFD.FormaDePago = GloFORMADEPAGO
                        oCFD.CondicionesDePago = GloCONDICIONESDEPAGO
                        oCFD.SubTotal = oSubTotal
                        oCFD.Descuento = oDescuento
                        oCFD.MotivoDescuento = GloMOTIVODESCUENTO
                        oCFD.Total = oTotalSinPuntos
                        oCFD.MetodoDePago = GloMETODODEPAGO

                        oCFD.TipoDeComprobante = GloTIPODECOMPROBANTE
                        oCFD.TipoComprobante33 = GloTIPODECOMPROBANTE
                        oCFD.PagoEnParcialiades = GloPAGOENPARCIALIDADES
                        oCFD.LugarExpedicion = GloPAGOENPARCIALIDADES
                        oCFD.CuentaPago = GlonumCtaPago
                        oCFD.Moneda = GloMONEDA

                        oCFD.UsoCFDI = GloUSOCFD
                        oCFD.EsNotaCredito = 0
                        oCFD.TipoCambio = GloTIPOCAMBIO

                        Dim oRegimen As New MizarCFD.BRL.CFDRegimenesFiscales
                        oRegimen.IdRegimen = 0
                        oRegimen.IdCFD = 0
                        oRegimen.Regimen = GloREGIMEN
                        oCFD.RegimenesFiscales.Add(oRegimen)
                        oCFD.Impuestos.TotalImpuestosRetenidos = 0
                        oCFD.Impuestos.TotalImpuestosTrasladados = oTotalImpuestos

                        Dim oImpuestosTraslados As CFDImpuestosTraslados
                        oImpuestosTraslados = New CFDImpuestosTraslados()

                        Dim oConcepto As CFDConceptos
                        Dim oContrador As Integer = 1

                        Dim CONEXION2 As New SqlConnection(locconexion)
                        Dim COMANDO2 As New SqlCommand("exec DameFacDig_Parte_2_Maestro2 " + CStr(oClv_Factura), CONEXION2)
                        COMANDO2.CommandType = CommandType.Text
                        COMANDO2.CommandTimeout = 0
                        Try
                            CONEXION2.Open()
                            Dim reader As SqlDataReader = COMANDO2.ExecuteReader
                            '    'Recorremos los Cargos obtenidos desde el Procedimiento
                            While (reader.Read())
                                oConcepto = New CFDConceptos()
                                oConcepto.Cantidad = reader(0).ToString
                                oConcepto.NumeroIdentificacion = reader(1).ToString
                                oConcepto.Descripcion = reader(2).ToString
                                oConcepto.ValorUnitario = reader(3).ToString
                                oConcepto.Importe = reader(4).ToString
                                oConcepto.TrasladarIVA = "1"

                                'Los nuevos
                                oConcepto.Descuento = 0 'Valor Nuevo
                                oConcepto.idClaveProdServ = reader(5).ToString
                                oConcepto.TipoFactor = reader(6).ToString
                                oConcepto.IVATRAS_Importe = reader(8).ToString 'Iva del ticket
                                oConcepto.TasaIVA = reader(7).ToString

                                If Double.Parse(reader(11).ToString) > 0 Then
                                    oConcepto.BaseIEPS = reader(4).ToString 'Cantida Subtotal 
                                    If SumaSubtotalYIeps = True Then
                                        oConcepto.BaseIVA = CDec(reader(4).ToString) + CDec(reader(11).ToString)
                                    Else
                                        oConcepto.BaseIVA = CDec(reader(4).ToString)
                                    End If
                                    oConcepto.IEPSTRAS_Importe = reader(11).ToString 'Total del ieps 'Ieps del ticket
                                    oConcepto.TasaIEPS = reader(10).ToString

                                    oConcepto.FactorIEPS = reader(9).ToString
                                    oConcepto.TrasladarIEPS = "1"
                                Else
                                    oConcepto.BaseIEPS = 0 'Cantida Subtotal 
                                    oConcepto.BaseIVA = reader(4).ToString
                                    oConcepto.IEPSTRAS_Importe = "0" 'Total del ieps 'Ieps del ticket
                                    oConcepto.TrasladarIEPS = "0"
                                End If
                                oConcepto.idUnidadMedida = reader(13).ToString '"E48" "Unidad de servicio" '"No Aplica" 'Cambio  Id que corresponde
                                oConcepto.UnidadMedida = reader(12).ToString '"Unidad de servicio" ' Pruebo poner una al gusto

                                oConcepto.IVARET_Importe = "0"
                                oConcepto.ISRRET_Importe = "0"

                                'Trasladados
                                If Double.Parse(reader(8).ToString) > 0 Then
                                    oImpuestosTraslados = New CFDImpuestosTraslados()
                                    oImpuestosTraslados.IdRegistroTraslado = oContrador
                                    oImpuestosTraslados.Impuesto = "IVA"
                                    oImpuestosTraslados.Importe = reader(8).ToString
                                    oImpuestosTraslados.Tasa = reader(7).ToString 'oTasaIva
                                    'oImpuestosTraslados.TipoFactor = GloTIPOFACTOR
                                    oImpuestosTraslados.TasaOCuota = GloTIPOFACTOR
                                    oImpuestosTraslados.IEPSTRAS_Importe = "0"
                                    oImpuestosTraslados.IVATRAS_Importe = "0"

                                    oCFD.Impuestos.Traslados.Add(oImpuestosTraslados)
                                End If
                                If Double.Parse(reader(11).ToString) > 0 Then
                                    oImpuestosTraslados = New CFDImpuestosTraslados()
                                    oImpuestosTraslados.IdRegistroTraslado = oContrador + 1
                                    oImpuestosTraslados.Impuesto = "IEPS" '"003" '"IEPS"
                                    oImpuestosTraslados.Importe = reader(11).ToString
                                    oImpuestosTraslados.Tasa = reader(10).ToString 'oTasaIeps
                                    oImpuestosTraslados.TasaOCuota = GloTIPOFACTOR
                                    oImpuestosTraslados.FactorIepsTras = GloTIPOFACTOR
                                    oImpuestosTraslados.IEPSTRAS_Importe = "0"
                                    oImpuestosTraslados.IVATRAS_Importe = "0"
                                    'oImpuestosTraslados.TipoFactor = GloTIPOFACTOR

                                    oCFD.Impuestos.Traslados.Add(oImpuestosTraslados)
                                End If



                                oConcepto.RetenerIVA = "0"
                                oConcepto.RetenerISR = "0"

                                oCFD.Conceptos.Add(oConcepto)
                                oContrador = oContrador + 2
                            End While
                            'Cnx.DbConnection.Close()
                        Catch ex As Exception
                            If Graba = "0" Then
                                MsgBox(ex.Source.ToString & "3 " & ex.Message)
                            Else

                                Llenalog(ex.Source.ToString & " " & ex.Message)
                            End If
                        Finally
                            CONEXION2.Close()
                            CONEXION2.Dispose()
                        End Try

                        ' Dim a As String = JsonConvert.SerializeObject(oCFD)
                        Dim bnd As Boolean = False

                        Dim Mensaje1 As String = ""
                        'oCFD.Validar(Cnx, )
                        If Not oCFD.Insertar(Cnx, True, False) Then
                            For Each oMensaje As DAMensajesSistema.RegistroMensaje In oCFD.MensajesSistema.ListaMensajes
                                Mensaje1 = oMensaje.TextoMensaje + " " + oMensaje.Contexto
                                Inserta_Tbl_FacturasNoGeneradas(oClv_Factura, "M", Mensaje1, locconexion)
                                bnd = True
                                MessageBox.Show(oMensaje.TextoMensaje + " " + oMensaje.Contexto)
                            Next
                            Return
                        End If
                        GloClv_FacturaCFD = 0
                        GloClv_FacturaCFD = oCFD.IdCFD
                        UPS_Inserta_Rel_FacturasCFD(oClv_Factura, GloClv_FacturaCFD, "", "M", locconexion)
                        GloTxtUlt4DigCheque = ""
                        Dim r As New Globalization.CultureInfo("es-MX")

                        r.DateTimeFormat.ShortDatePattern = "dd/MM/yyyy"

                        System.Threading.Thread.CurrentThread.CurrentCulture = r
                        Try
                            'oCFD.EnviarEmail(Cnx)
                            If GloClv_FacturaCFD > 0 And bnd = False Then
                                ENVIAR_Factura(GloClv_FacturaCFD)
                            End If

                        Catch ex As Exception

                        End Try



                    Catch ex As Exception
                        If Graba = "0" Then
                            MsgBox(ex.Source.ToString & " 4 " & ex.Message)
                        Else

                            Llenalog(ex.Source.ToString & " " & ex.Message)
                        End If
                    Finally
                        CONEXION.Close()
                        CONEXION.Dispose()
                    End Try

                End Using
            Else
                GloClv_FacturaCFD = ClvFacturaCfd
            End If
        End If
    End Sub
    

    Public Shared Sub Dime_Aque_Compania_FacturarleMaestrotvzac(ByVal oClv_Factura As Long, ByVal locconexion As String)
        locID_Compania_Mizart = ""
        locID_Sucursal_Mizart = ""

        Dim con As New SqlConnection(locconexion)
        Dim com As New SqlCommand("Dime_Aque_Compania_FacturarleMaestro", con)
        com.CommandType = CommandType.StoredProcedure
        com.CommandTimeout = 0
        '@Numero numeric(20,2),@moneda int,@letra varchar(max) output
        'Dame la fecha del Servidor
        Dim prm1 As New SqlParameter("@Clv_FacturaMaestro", SqlDbType.BigInt)
        prm1.Direction = ParameterDirection.Input
        prm1.Value = oClv_Factura
        com.Parameters.Add(prm1)

        Dim prm2 As New SqlParameter("@ID_Compania_Mizart", SqlDbType.VarChar, 50)
        prm2.Direction = ParameterDirection.Output
        prm2.Value = 0
        com.Parameters.Add(prm2)

        Dim prm3 As New SqlParameter("@ID_Sucursal_Mizart", SqlDbType.VarChar, 50)
        prm3.Direction = ParameterDirection.Output
        prm3.Value = ""
        com.Parameters.Add(prm3)


        Try
            con.Open()
            com.ExecuteNonQuery()
            locID_Compania_Mizart = prm2.Value
            locID_Sucursal_Mizart = prm3.Value

        Catch ex As Exception
            'Llenalog(ex.Source.ToString & " " & ex.Message)
            If Graba = "0" Then
                Llenalog(ex.Source.ToString & " " & ex.Message)
            Else

                Llenalog(ex.Source.ToString & " " & ex.Message)
            End If
        Finally
            con.Close()
        End Try
    End Sub

    Public Shared Sub Dime_Aque_Compania_FacturarlePago(ByVal oClv_Factura As Long, ByVal locconexion As String)
        locID_Compania_Mizart = ""
        locID_Sucursal_Mizart = ""

        Dim con As New SqlConnection(locconexion)
        Dim com As New SqlCommand("Dime_Aque_Compania_FacturarlePago", con)
        com.CommandType = CommandType.StoredProcedure
        com.CommandTimeout = 0
        '@Numero numeric(20,2),@moneda int,@letra varchar(max) output
        'Dame la fecha del Servidor
        Dim prm1 As New SqlParameter("@Clv_Pago", SqlDbType.BigInt)
        prm1.Direction = ParameterDirection.Input
        prm1.Value = oClv_Factura
        com.Parameters.Add(prm1)

        Dim prm2 As New SqlParameter("@ID_Compania_Mizart", SqlDbType.VarChar, 50)
        prm2.Direction = ParameterDirection.Output
        prm2.Value = 0
        com.Parameters.Add(prm2)

        Dim prm3 As New SqlParameter("@ID_Sucursal_Mizart", SqlDbType.VarChar, 50)
        prm3.Direction = ParameterDirection.Output
        prm3.Value = ""
        com.Parameters.Add(prm3)


        Try
            con.Open()
            com.ExecuteNonQuery()
            locID_Compania_Mizart = prm2.Value
            locID_Sucursal_Mizart = prm3.Value

        Catch ex As Exception
            'Llenalog(ex.Source.ToString & " " & ex.Message)
            If Graba = "0" Then
                Llenalog(ex.Source.ToString & " " & ex.Message)
            Else

                Llenalog(ex.Source.ToString & " " & ex.Message)
            End If
        Finally
            con.Close()
        End Try
    End Sub

    Public Shared Function BusFacFiscalOledbMaestrotvzac(ByVal oClv_Factura As Long, ByVal LocConexion As String) As Integer
        BusFacFiscalOledbMaestrotvzac = 0
        Dim CONEXION As New SqlConnection(LocConexion)
        Dim COMANDO As New SqlCommand("BusFacFiscalMaestro", CONEXION)
        COMANDO.CommandType = CommandType.StoredProcedure
        COMANDO.CommandTimeout = 0

        Dim parametro As New SqlParameter("@Clv_FacturaMaestro", SqlDbType.BigInt)
        parametro.Direction = ParameterDirection.Input
        parametro.Value = oClv_Factura
        COMANDO.Parameters.Add(parametro)

        Dim parametro1 As New SqlParameter("@ident", SqlDbType.Int)
        parametro1.Direction = ParameterDirection.Output
        parametro1.Value = 0
        COMANDO.Parameters.Add(parametro1)

        Try
            CONEXION.Open()
            Dim i As Integer = COMANDO.ExecuteNonQuery
            BusFacFiscalOledbMaestrotvzac = CStr(parametro1.Value)

            'Cnx.DbConnection.Close()
        Catch ex As Exception
            'MsgBox(ex.Message, MsgBoxStyle.Exclamation)
            If Graba = "0" Then
                Llenalog(ex.Source.ToString & " " & ex.Message)
            Else

                Llenalog(ex.Source.ToString & " " & ex.Message)
            End If
        Finally
            CONEXION.Close()
            CONEXION.Dispose()
        End Try
    End Function

    Public Shared Function BusFacFiscalOledbMaestroNota(ByVal oClv_Nota As Long, ByVal LocConexion As String) As Integer
        BusFacFiscalOledbMaestroNota = 0
        Dim CONEXION As New SqlConnection(LocConexion)
        Dim COMANDO As New SqlCommand("BusFacFiscalMaestroNota", CONEXION)
        COMANDO.CommandType = CommandType.StoredProcedure
        COMANDO.CommandTimeout = 0

        Dim parametro As New SqlParameter("@CLV_NOTA", SqlDbType.BigInt)
        parametro.Direction = ParameterDirection.Input
        parametro.Value = oClv_Nota
        COMANDO.Parameters.Add(parametro)

        Dim parametro1 As New SqlParameter("@ident", SqlDbType.Int)
        parametro1.Direction = ParameterDirection.Output
        parametro1.Value = 0
        COMANDO.Parameters.Add(parametro1)

        Try
            CONEXION.Open()
            Dim i As Integer = COMANDO.ExecuteNonQuery
            BusFacFiscalOledbMaestroNota = CStr(parametro1.Value)

            'Cnx.DbConnection.Close()
        Catch ex As Exception
            'MsgBox(ex.Message, MsgBoxStyle.Exclamation)
            If Graba = "0" Then
                Llenalog(ex.Source.ToString & " " & ex.Message)
            Else

                Llenalog(ex.Source.ToString & " " & ex.Message)
            End If
        Finally
            CONEXION.Close()
            CONEXION.Dispose()
        End Try
    End Function

    Public Shared Function BusFacFiscalOledbPago(ByVal oClv_Factura As Long, ByVal LocConexion As String) As Integer
        BusFacFiscalOledbPago = 0
        Dim CONEXION As New SqlConnection(LocConexion)
        Dim COMANDO As New SqlCommand("BusFacFiscalPago", CONEXION)
        COMANDO.CommandType = CommandType.StoredProcedure
        COMANDO.CommandTimeout = 0

        Dim parametro As New SqlParameter("@Clv_Pago", SqlDbType.BigInt)
        parametro.Direction = ParameterDirection.Input
        parametro.Value = oClv_Factura
        COMANDO.Parameters.Add(parametro)

        Dim parametro1 As New SqlParameter("@ident", SqlDbType.Int)
        parametro1.Direction = ParameterDirection.Output
        parametro1.Value = 0
        COMANDO.Parameters.Add(parametro1)

        Try
            CONEXION.Open()
            Dim i As Integer = COMANDO.ExecuteNonQuery
            BusFacFiscalOledbPago = CStr(parametro1.Value)

            'Cnx.DbConnection.Close()
        Catch ex As Exception
            'MsgBox(ex.Message, MsgBoxStyle.Exclamation)
            If Graba = "0" Then
                Llenalog(ex.Source.ToString & " " & ex.Message)
            Else

                Llenalog(ex.Source.ToString & " " & ex.Message)
            End If
        Finally
            CONEXION.Close()
            CONEXION.Dispose()
        End Try
    End Function

    Public Shared Sub Usp_Ed_DameDatosFacDigMaestrotvzac(ByVal oCLV_FActura As Long, ByVal oCompania As Integer, ByVal LocConexion As String)
        GloFORMADEPAGO = ""
        GloCONDICIONESDEPAGO = ""
        GloMOTIVODESCUENTO = ""
        GloMETODODEPAGO = ""
        GloTIPODECOMPROBANTE = ""
        GloPAGOENPARCIALIDADES = ""
        GloLUGAREXPEDICION = ""
        GloREGIMEN = ""
        GloMONEDA = ""
        GloVERSON = ""
        GloTIPOFACTOR = ""
        GloFORMAPAGO = ""
        GloTIPOCAMBIO = ""
        GloUSOCFD = ""
        GloFECHAVIGENCIA = ""

        Dim CONEXION As New SqlConnection(LocConexion)
        Dim COMANDO As New SqlCommand("Usp_Ed_DameDatosFacDigMaestro", CONEXION)
        COMANDO.CommandType = CommandType.StoredProcedure
        '@Clv_Factura Bigint,@Clv_FacturaCFD bigint,@Serie varchar(5)

        Dim PARAMETRO1 As New SqlParameter("@CLV_FACTURA", SqlDbType.BigInt)
        PARAMETRO1.Direction = ParameterDirection.Input
        PARAMETRO1.Value = oCLV_FActura
        COMANDO.Parameters.Add(PARAMETRO1)

        Dim PARAMETRO2 As New SqlParameter("@IDCOMPANIA", SqlDbType.Int)
        PARAMETRO2.Direction = ParameterDirection.Input
        PARAMETRO2.Value = oCompania
        COMANDO.Parameters.Add(PARAMETRO2)

        '@FORMADEPAGO VARCHAR(250) OUTPUT
        ',@CONDICIONESDEPAGO VARCHAR(250)OUTPUT,@MOTIVODESCUENTO VARCHAR(250)OUTPUT,@METODODEPAGO VARCHAR(250)OUTPUT,
        '@TIPODECOMPROBANTE VARCHAR(50) OUTPUT,@PAGOENPARCIALIDADES VARCHAR(50) OUTPUT,@LUGAREXPEDICION VARCHAR(250) OUTPUT
        ',@REGIMEN VARCHAR(400) OUTPUT,@MONEDA VARCHAR(15) OUTPUT,@VERSON

        Dim PARAMETRO3 As New SqlParameter("@FORMADEPAGO", SqlDbType.VarChar, 250)
        PARAMETRO3.Direction = ParameterDirection.Output
        PARAMETRO3.Value = ""
        COMANDO.Parameters.Add(PARAMETRO3)

        Dim PARAMETRO4 As New SqlParameter("@CONDICIONESDEPAGO", SqlDbType.VarChar, 250)
        PARAMETRO4.Direction = ParameterDirection.Output
        PARAMETRO4.Value = ""
        COMANDO.Parameters.Add(PARAMETRO4)

        Dim PARAMETRO5 As New SqlParameter("@MOTIVODESCUENTO", SqlDbType.VarChar, 250)
        PARAMETRO5.Direction = ParameterDirection.Output
        PARAMETRO5.Value = ""
        COMANDO.Parameters.Add(PARAMETRO5)

        Dim PARAMETRO6 As New SqlParameter("@METODODEPAGO", SqlDbType.VarChar, 250)
        PARAMETRO6.Direction = ParameterDirection.Output
        PARAMETRO6.Value = ""
        COMANDO.Parameters.Add(PARAMETRO6)

        Dim PARAMETRO7 As New SqlParameter("@TIPODECOMPROBANTE", SqlDbType.VarChar, 250)
        PARAMETRO7.Direction = ParameterDirection.Output
        PARAMETRO7.Value = ""
        COMANDO.Parameters.Add(PARAMETRO7)

        Dim PARAMETRO8 As New SqlParameter("@PAGOENPARCIALIDADES", SqlDbType.VarChar, 250)
        PARAMETRO8.Direction = ParameterDirection.Output
        PARAMETRO8.Value = ""
        COMANDO.Parameters.Add(PARAMETRO8)

        Dim PARAMETRO9 As New SqlParameter("@LUGAREXPEDICION", SqlDbType.VarChar, 250)
        PARAMETRO9.Direction = ParameterDirection.Output
        PARAMETRO9.Value = ""
        COMANDO.Parameters.Add(PARAMETRO9)

        Dim PARAMETRO10 As New SqlParameter("@REGIMEN", SqlDbType.VarChar, 400)
        PARAMETRO10.Direction = ParameterDirection.Output
        PARAMETRO10.Value = ""
        COMANDO.Parameters.Add(PARAMETRO10)

        Dim PARAMETRO11 As New SqlParameter("@MONEDA", SqlDbType.VarChar, 15)
        PARAMETRO11.Direction = ParameterDirection.Output
        PARAMETRO11.Value = ""
        COMANDO.Parameters.Add(PARAMETRO11)

        Dim PARAMETRO12 As New SqlParameter("@VERSON", SqlDbType.VarChar, 15)
        PARAMETRO12.Direction = ParameterDirection.Output
        PARAMETRO12.Value = ""
        COMANDO.Parameters.Add(PARAMETRO12)

        Dim PARAMETRO13 As New SqlParameter("@numCtaPago", SqlDbType.VarChar, 4)
        PARAMETRO13.Direction = ParameterDirection.Output
        PARAMETRO13.Value = ""
        COMANDO.Parameters.Add(PARAMETRO13)

        Dim PARAMETRO14 As New SqlParameter("@TIPOFACTOR", SqlDbType.VarChar, 4)
        PARAMETRO14.Direction = ParameterDirection.Output
        PARAMETRO14.Value = ""
        COMANDO.Parameters.Add(PARAMETRO14)

        Dim PARAMETRO15 As New SqlParameter("@USOCFD", SqlDbType.VarChar, 4)
        PARAMETRO15.Direction = ParameterDirection.Output
        PARAMETRO15.Value = ""
        COMANDO.Parameters.Add(PARAMETRO15)

        Dim PARAMETRO16 As New SqlParameter("@FECHAFACTURA", SqlDbType.VarChar, 20)
        PARAMETRO16.Direction = ParameterDirection.Output
        PARAMETRO16.Value = ""
        COMANDO.Parameters.Add(PARAMETRO16)

        Dim PARAMETRO17 As New SqlParameter("@TIPOCAMBIO", SqlDbType.VarChar, 4)
        PARAMETRO17.Direction = ParameterDirection.Output
        PARAMETRO17.Value = ""
        COMANDO.Parameters.Add(PARAMETRO17)

        Try
            '@FORMADEPAGO VARCHAR(250) OUTPUT
            ',@CONDICIONESDEPAGO VARCHAR(250)OUTPUT,@MOTIVODESCUENTO VARCHAR(250)OUTPUT,@METODODEPAGO VARCHAR(250)OUTPUT,
            '@TIPODECOMPROBANTE VARCHAR(50) OUTPUT,@PAGOENPARCIALIDADES VARCHAR(50) OUTPUT,@LUGAREXPEDICION VARCHAR(250) OUTPUT
            ',@REGIMEN VARCHAR(400) OUTPUT,@MONEDA VARCHAR(15) OUTPUT,@VERSON  

            CONEXION.Open()
            Dim i As Integer = COMANDO.ExecuteNonQuery
            GloFORMADEPAGO = PARAMETRO3.Value
            GloCONDICIONESDEPAGO = PARAMETRO4.Value
            GloMOTIVODESCUENTO = PARAMETRO5.Value
            GloMETODODEPAGO = PARAMETRO6.Value
            GloTIPODECOMPROBANTE = PARAMETRO7.Value
            GloPAGOENPARCIALIDADES = PARAMETRO8.Value
            GloLUGAREXPEDICION = PARAMETRO9.Value
            GloREGIMEN = PARAMETRO10.Value
            GloMONEDA = PARAMETRO11.Value
            GloVERSON = PARAMETRO12.Value
            GlonumCtaPago = PARAMETRO13.Value
            GloTIPOFACTOR = PARAMETRO14.Value
            GloUSOCFD = PARAMETRO15.Value
            GloFECHAVIGENCIA = PARAMETRO16.Value
            GloTIPOCAMBIO = PARAMETRO17.Value
        Catch ex As Exception
            If Graba = "0" Then
                MsgBox(ex.Source.ToString & " " & ex.Message)
            Else

                Llenalog(ex.Source.ToString & " " & ex.Message)
            End If
        Finally
            CONEXION.Close()
            CONEXION.Dispose()
        End Try
    End Sub

    Public Shared Sub Usp_Ed_DameDatosFacDigPago(ByVal oCLV_FActura As Long, ByVal oCompania As Integer, ByVal LocConexion As String)
        GloFORMADEPAGO = ""
        GloCONDICIONESDEPAGO = ""
        GloMOTIVODESCUENTO = ""
        GloMETODODEPAGO = ""
        GloTIPODECOMPROBANTE = ""
        GloPAGOENPARCIALIDADES = ""
        GloLUGAREXPEDICION = ""
        GloREGIMEN = ""
        GloMONEDA = ""
        GloVERSON = ""
        GloTIPOFACTOR = ""
        GloFORMAPAGO = ""
        GloTIPOCAMBIO = ""
        GloUSOCFD = ""
        GloFECHAVIGENCIA = ""
        GloSERIE = ""
        GloFOLIO = ""
        GloNUMPARCIALIDAD = ""
        GloSALDOANTERIOR = ""
        GloSALDONUEVO = ""
        GloUUIDRel = ""
        GloID_CFD = ""
        GloMONTOPAGO = ""
        GloMONTOORIGINAL = ""

        Dim CONEXION As New SqlConnection(LocConexion)
        Dim COMANDO As New SqlCommand("Usp_Ed_DameDatosFacDigPago", CONEXION)
        COMANDO.CommandType = CommandType.StoredProcedure
        '@Clv_Factura Bigint,@Clv_FacturaCFD bigint,@Serie varchar(5)

        Dim PARAMETRO1 As New SqlParameter("@Clv_Pago", SqlDbType.BigInt)
        PARAMETRO1.Direction = ParameterDirection.Input
        PARAMETRO1.Value = oCLV_FActura
        COMANDO.Parameters.Add(PARAMETRO1)

        Dim PARAMETRO2 As New SqlParameter("@IDCOMPANIA", SqlDbType.Int)
        PARAMETRO2.Direction = ParameterDirection.Input
        PARAMETRO2.Value = oCompania
        COMANDO.Parameters.Add(PARAMETRO2)

        '@FORMADEPAGO VARCHAR(250) OUTPUT
        ',@CONDICIONESDEPAGO VARCHAR(250)OUTPUT,@MOTIVODESCUENTO VARCHAR(250)OUTPUT,@METODODEPAGO VARCHAR(250)OUTPUT,
        '@TIPODECOMPROBANTE VARCHAR(50) OUTPUT,@PAGOENPARCIALIDADES VARCHAR(50) OUTPUT,@LUGAREXPEDICION VARCHAR(250) OUTPUT
        ',@REGIMEN VARCHAR(400) OUTPUT,@MONEDA VARCHAR(15) OUTPUT,@VERSON

        Dim PARAMETRO3 As New SqlParameter("@FORMADEPAGO", SqlDbType.VarChar, 250)
        PARAMETRO3.Direction = ParameterDirection.Output
        PARAMETRO3.Value = ""
        COMANDO.Parameters.Add(PARAMETRO3)

        Dim PARAMETRO4 As New SqlParameter("@CONDICIONESDEPAGO", SqlDbType.VarChar, 250)
        PARAMETRO4.Direction = ParameterDirection.Output
        PARAMETRO4.Value = ""
        COMANDO.Parameters.Add(PARAMETRO4)

        Dim PARAMETRO5 As New SqlParameter("@MOTIVODESCUENTO", SqlDbType.VarChar, 250)
        PARAMETRO5.Direction = ParameterDirection.Output
        PARAMETRO5.Value = ""
        COMANDO.Parameters.Add(PARAMETRO5)

        Dim PARAMETRO6 As New SqlParameter("@METODODEPAGO", SqlDbType.VarChar, 250)
        PARAMETRO6.Direction = ParameterDirection.Output
        PARAMETRO6.Value = ""
        COMANDO.Parameters.Add(PARAMETRO6)

        Dim PARAMETRO7 As New SqlParameter("@TIPODECOMPROBANTE", SqlDbType.VarChar, 250)
        PARAMETRO7.Direction = ParameterDirection.Output
        PARAMETRO7.Value = ""
        COMANDO.Parameters.Add(PARAMETRO7)

        Dim PARAMETRO8 As New SqlParameter("@PAGOENPARCIALIDADES", SqlDbType.VarChar, 250)
        PARAMETRO8.Direction = ParameterDirection.Output
        PARAMETRO8.Value = ""
        COMANDO.Parameters.Add(PARAMETRO8)

        Dim PARAMETRO9 As New SqlParameter("@LUGAREXPEDICION", SqlDbType.VarChar, 250)
        PARAMETRO9.Direction = ParameterDirection.Output
        PARAMETRO9.Value = ""
        COMANDO.Parameters.Add(PARAMETRO9)

        Dim PARAMETRO10 As New SqlParameter("@REGIMEN", SqlDbType.VarChar, 400)
        PARAMETRO10.Direction = ParameterDirection.Output
        PARAMETRO10.Value = ""
        COMANDO.Parameters.Add(PARAMETRO10)

        Dim PARAMETRO11 As New SqlParameter("@MONEDA", SqlDbType.VarChar, 15)
        PARAMETRO11.Direction = ParameterDirection.Output
        PARAMETRO11.Value = ""
        COMANDO.Parameters.Add(PARAMETRO11)

        Dim PARAMETRO12 As New SqlParameter("@VERSON", SqlDbType.VarChar, 15)
        PARAMETRO12.Direction = ParameterDirection.Output
        PARAMETRO12.Value = ""
        COMANDO.Parameters.Add(PARAMETRO12)

        Dim PARAMETRO13 As New SqlParameter("@numCtaPago", SqlDbType.VarChar, 4)
        PARAMETRO13.Direction = ParameterDirection.Output
        PARAMETRO13.Value = ""
        COMANDO.Parameters.Add(PARAMETRO13)

        Dim PARAMETRO14 As New SqlParameter("@TIPOFACTOR", SqlDbType.VarChar, 4)
        PARAMETRO14.Direction = ParameterDirection.Output
        PARAMETRO14.Value = ""
        COMANDO.Parameters.Add(PARAMETRO14)

        Dim PARAMETRO15 As New SqlParameter("@USOCFD", SqlDbType.VarChar, 4)
        PARAMETRO15.Direction = ParameterDirection.Output
        PARAMETRO15.Value = ""
        COMANDO.Parameters.Add(PARAMETRO15)

        Dim PARAMETRO16 As New SqlParameter("@FECHAFACTURA", SqlDbType.VarChar, 20)
        PARAMETRO16.Direction = ParameterDirection.Output
        PARAMETRO16.Value = ""
        COMANDO.Parameters.Add(PARAMETRO16)

        Dim PARAMETRO17 As New SqlParameter("@TIPOCAMBIO", SqlDbType.Money)
        PARAMETRO17.Direction = ParameterDirection.Output
        PARAMETRO17.Value = ""
        COMANDO.Parameters.Add(PARAMETRO17)

        Dim PARAMETRO18 As New SqlParameter("@SERIE", SqlDbType.VarChar, 4)
        PARAMETRO18.Direction = ParameterDirection.Output
        PARAMETRO18.Value = ""
        COMANDO.Parameters.Add(PARAMETRO18)

        Dim PARAMETRO19 As New SqlParameter("@FOLIO", SqlDbType.BigInt)
        PARAMETRO19.Direction = ParameterDirection.Output
        PARAMETRO19.Value = ""
        COMANDO.Parameters.Add(PARAMETRO19)

        Dim PARAMETRO20 As New SqlParameter("@NUMPARCIALIDAD", SqlDbType.Int)
        PARAMETRO20.Direction = ParameterDirection.Output
        PARAMETRO20.Value = ""
        COMANDO.Parameters.Add(PARAMETRO20)

        Dim PARAMETRO21 As New SqlParameter("@SALDOANTERIOR", SqlDbType.Money)
        PARAMETRO21.Direction = ParameterDirection.Output
        PARAMETRO21.Value = ""
        COMANDO.Parameters.Add(PARAMETRO21)

        Dim PARAMETRO22 As New SqlParameter("@SALDONUEVO", SqlDbType.Money)
        PARAMETRO22.Direction = ParameterDirection.Output
        PARAMETRO22.Value = ""
        COMANDO.Parameters.Add(PARAMETRO22)

        Dim PARAMETRO23 As New SqlParameter("@UUIDRel", SqlDbType.VarChar, 150)
        PARAMETRO23.Direction = ParameterDirection.Output
        PARAMETRO23.Value = ""
        COMANDO.Parameters.Add(PARAMETRO23)

        Dim PARAMETRO24 As New SqlParameter("@ID_CFD", SqlDbType.BigInt)
        PARAMETRO24.Direction = ParameterDirection.Output
        PARAMETRO24.Value = ""
        COMANDO.Parameters.Add(PARAMETRO24)

        Dim PARAMETRO25 As New SqlParameter("@MONTOPAGO", SqlDbType.Money)
        PARAMETRO25.Direction = ParameterDirection.Output
        PARAMETRO25.Value = ""
        COMANDO.Parameters.Add(PARAMETRO25)

        Dim PARAMETRO26 As New SqlParameter("@MONTOORIGINAL", SqlDbType.Money)
        PARAMETRO26.Direction = ParameterDirection.Output
        PARAMETRO26.Value = ""
        COMANDO.Parameters.Add(PARAMETRO26)

        Dim PARAMETRO27 As New SqlParameter("@FechaPago", SqlDbType.DateTime)
        PARAMETRO27.Direction = ParameterDirection.Output
        PARAMETRO27.Value = "20180605"
        COMANDO.Parameters.Add(PARAMETRO27)

        Try
            '@FORMADEPAGO VARCHAR(250) OUTPUT
            ',@CONDICIONESDEPAGO VARCHAR(250)OUTPUT,@MOTIVODESCUENTO VARCHAR(250)OUTPUT,@METODODEPAGO VARCHAR(250)OUTPUT,
            '@TIPODECOMPROBANTE VARCHAR(50) OUTPUT,@PAGOENPARCIALIDADES VARCHAR(50) OUTPUT,@LUGAREXPEDICION VARCHAR(250) OUTPUT
            ',@REGIMEN VARCHAR(400) OUTPUT,@MONEDA VARCHAR(15) OUTPUT,@VERSON  

            CONEXION.Open()
            Dim i As Integer = COMANDO.ExecuteNonQuery
            GloFORMAPAGO = PARAMETRO3.Value
            GloCONDICIONESDEPAGO = PARAMETRO4.Value
            GloMOTIVODESCUENTO = PARAMETRO5.Value
            GloMETODODEPAGO = PARAMETRO6.Value
            GloTIPODECOMPROBANTE = PARAMETRO7.Value
            GloPAGOENPARCIALIDADES = PARAMETRO8.Value
            GloLUGAREXPEDICION = PARAMETRO9.Value
            GloREGIMEN = PARAMETRO10.Value
            GloMONEDA = PARAMETRO11.Value
            GloVERSON = PARAMETRO12.Value
            GlonumCtaPago = PARAMETRO13.Value
            GloTIPOFACTOR = PARAMETRO14.Value
            GloUSOCFD = PARAMETRO15.Value
            GloFECHAVIGENCIA = PARAMETRO16.Value
            GloTIPOCAMBIO = Decimal.Parse(PARAMETRO17.Value.ToString()).ToString("0.0000")
            GloSERIE = PARAMETRO18.Value
            GloFOLIO = PARAMETRO19.Value
            GloNUMPARCIALIDAD = PARAMETRO20.Value
            GloSALDOANTERIOR = Decimal.Parse(PARAMETRO21.Value.ToString()).ToString("0.00")
            GloSALDONUEVO = Decimal.Parse(PARAMETRO22.Value.ToString()).ToString("0.00")
            GloUUIDRel = PARAMETRO23.Value
            GloID_CFD = PARAMETRO24.Value
            GloMONTOPAGO = Decimal.Parse(PARAMETRO25.Value.ToString()).ToString("0.00")
            GloMONTOORIGINAL = Decimal.Parse(PARAMETRO26.Value.ToString()).ToString("0.00")
            GloFECHAPAGO = DateTime.Parse(PARAMETRO27.Value.ToString())
        Catch ex As Exception
            If Graba = "0" Then
                MsgBox(ex.Source.ToString & " " & ex.Message)
            Else

                Llenalog(ex.Source.ToString & " " & ex.Message)
            End If
        Finally
            CONEXION.Close()
            CONEXION.Dispose()
        End Try
    End Sub

    Public Shared Sub Cancelacion_FacturaCFD(ByVal oClv_FacturaCFD As Long, ByVal locconexion As String)
        Dim ofecha As Date
        Dim con As New SqlConnection(locconexion)
        Dim com As New SqlCommand("DAMEFECHADELSERVIDOR", con)
        com.CommandType = CommandType.StoredProcedure
        com.CommandTimeout = 0

        'Dame la fecha del Servidor
        Dim prmFechaObtebida As New SqlParameter("@FECHA", SqlDbType.DateTime)
        prmFechaObtebida.Direction = ParameterDirection.Output
        prmFechaObtebida.Value = ""
        com.Parameters.Add(prmFechaObtebida)
        Dim listComprobantes As New List(Of String)
        listComprobantes.Add(oClv_FacturaCFD)
        Try
            con.Open()
            com.ExecuteNonQuery()

            ofecha = prmFechaObtebida.Value

        Catch ex As Exception
            If Graba = "0" Then
                Llenalog(ex.Source.ToString & " " & ex.Message)
            Else

                Llenalog(ex.Source.ToString & " " & ex.Message)
            End If
        Finally
            con.Close()
        End Try
        'Dim oEFAC As New Miza
        'Prueba

        Dim oCFD As MizarCFD.BRL.CFD
        Using Cnx As New DAConexion("HL", "sa", "sa")
            oCFD = New MizarCFD.BRL.CFD
            oCFD.Inicializar()
            oCFD.IdCFD = CStr(oClv_FacturaCFD)
            If Len(GloMotivoCancelacionCFD) = 0 Then GloMotivoCancelacionCFD = "CANCELACION"
            oCFD.MotivoCancelacion = GloMotivoCancelacionCFD
            oCFD.FechaCancelacion = ofecha
            oCFD.EstaCancelado = 1
            Try

                If Not oCFD.CancelarCFDi(Cnx, locID_Compania_Mizart, GloMotivoCancelacionCFD, listComprobantes.ToArray(), False) = False Then
                    For Each oMensaje As DAMensajesSistema.RegistroMensaje In oCFD.MensajesSistema.ListaMensajes
                        MessageBox.Show(oMensaje.TextoMensaje + " " + oMensaje.Contexto)

                    Next
                    Return
                End If
            Catch ex As Exception

            End Try
            Dim MiError As Integer = 0

            If Not oCFD.ModificarEstatusCancelado(Cnx) = False Then
                MiError = 1
                For Each oMensaje As DAMensajesSistema.RegistroMensaje In oCFD.MensajesSistema.ListaMensajes
                    MessageBox.Show(oMensaje.TextoMensaje + " " + oMensaje.Contexto)

                Next
                Return
            End If
            Try
                UPS_Modificar_Rel_FacturasCFD(oClv_FacturaCFD, locconexion)
            Catch ex As Exception

            End Try


        End Using
    End Sub

    Public Shared Sub Dame_Detalle1Global(ByVal oClv_FacturaCFD As Long, ByVal locconexion As String)
        mIva = 0
        mipes = 0
        msubtotal = 0
        mdetalle1 = ""

        Dim con As New SqlConnection(locconexion)
        Dim com As New SqlCommand("Dame_Detalle1", con)
        com.CommandType = CommandType.StoredProcedure
        com.CommandTimeout = 0
        '@Numero numeric(20,2),@moneda int,@letra varchar(max) output
        'Dame la fecha del Servidor
        Dim prm1 As New SqlParameter("@Clv_FacturaCFD", SqlDbType.BigInt)
        prm1.Direction = ParameterDirection.Input
        prm1.Value = oClv_FacturaCFD
        com.Parameters.Add(prm1)

        Dim prm2 As New SqlParameter("@Iva", SqlDbType.Money)
        prm2.Direction = ParameterDirection.Output
        prm2.Value = 0
        com.Parameters.Add(prm2)

        Dim prm3 As New SqlParameter("@Ieps", SqlDbType.Money)
        prm3.Direction = ParameterDirection.Output
        prm3.Value = ""
        com.Parameters.Add(prm3)

        Dim prm4 As New SqlParameter("@Subtotal", SqlDbType.Money)
        prm4.Direction = ParameterDirection.Output
        prm4.Value = ""
        com.Parameters.Add(prm4)

        Dim prm5 As New SqlParameter("@Detalle1", SqlDbType.VarChar, 8000)
        prm5.Direction = ParameterDirection.Output
        prm5.Value = ""
        com.Parameters.Add(prm5)

        Try
            con.Open()
            com.ExecuteNonQuery()
            mIva = prm2.Value
            mipes = prm3.Value
            msubtotal = prm4.Value
            mdetalle1 = prm5.Value

        Catch ex As Exception
            If Graba = "0" Then
                Llenalog(ex.Source.ToString & " " & ex.Message)
            Else

                Llenalog(ex.Source.ToString & " " & ex.Message)
            End If
        Finally
            con.Close()
        End Try
    End Sub

    Public Shared Sub Dime_Aque_Compania_FacturarleNOTAGlobal(ByVal oClv_NOTA As Long, ByVal locconexion As String)
        locID_Compania_Mizart = ""
        locID_Sucursal_Mizart = ""

        Dim con As New SqlConnection(locconexion)
        Dim com As New SqlCommand("Dime_Aque_Compania_FacturarleNOTAGlobal", con)
        com.CommandType = CommandType.StoredProcedure
        '@Numero numeric(20,2),@moneda int,@letra varchar(max) output
        'Dame la fecha del Servidor
        Dim prm1 As New SqlParameter("@IdCfd", SqlDbType.BigInt)
        prm1.Direction = ParameterDirection.Input
        prm1.Value = oClv_NOTA
        com.Parameters.Add(prm1)

        Dim prm2 As New SqlParameter("@ID_Compania_Mizart", SqlDbType.VarChar, 50)
        prm2.Direction = ParameterDirection.Output
        prm2.Value = 0
        com.Parameters.Add(prm2)

        Dim prm3 As New SqlParameter("@ID_Sucursal_Mizart", SqlDbType.VarChar, 50)
        prm3.Direction = ParameterDirection.Output
        prm3.Value = ""
        com.Parameters.Add(prm3)


        Try
            con.Open()
            com.ExecuteNonQuery()
            locID_Compania_Mizart = prm2.Value
            locID_Sucursal_Mizart = prm3.Value

        Catch ex As Exception
            If Graba = "0" Then
                Llenalog(ex.Source.ToString & " " & ex.Message)
            Else

                Llenalog(ex.Source.ToString & " " & ex.Message)
            End If
        Finally
            con.Close()
        End Try
    End Sub

    Public Shared Sub Dime_Aque_Compania_FacturarleNOTA(ByVal oClv_NOTA As Long, ByVal locconexion As String)
        locID_Compania_Mizart = ""
        locID_Sucursal_Mizart = ""

        Dim con As New SqlConnection(locconexion)
        Dim com As New SqlCommand("Dime_Aque_Compania_FacturarleNOTA", con)
        com.CommandType = CommandType.StoredProcedure
        com.CommandTimeout = 0
        '@Numero numeric(20,2),@moneda int,@letra varchar(max) output
        'Dame la fecha del Servidor
        Dim prm1 As New SqlParameter("@Clv_NOTA", SqlDbType.BigInt)
        prm1.Direction = ParameterDirection.Input
        prm1.Value = oClv_NOTA
        com.Parameters.Add(prm1)

        Dim prm2 As New SqlParameter("@ID_Compania_Mizart", SqlDbType.VarChar, 50)
        prm2.Direction = ParameterDirection.Output
        prm2.Value = 0
        com.Parameters.Add(prm2)

        Dim prm3 As New SqlParameter("@ID_Sucursal_Mizart", SqlDbType.VarChar, 50)
        prm3.Direction = ParameterDirection.Output
        prm3.Value = ""
        com.Parameters.Add(prm3)


        Try
            con.Open()
            com.ExecuteNonQuery()
            locID_Compania_Mizart = prm2.Value
            locID_Sucursal_Mizart = prm3.Value

        Catch ex As Exception
            If Graba = "0" Then
                Llenalog(ex.Source.ToString & " " & ex.Message)
            Else

                Llenalog(ex.Source.ToString & " " & ex.Message)
            End If
        Finally
            con.Close()
        End Try
    End Sub

    Public Shared Sub Dime_Aque_Compania_FacturarleNOTAMaestro(ByVal oClv_NOTA As Long, ByVal locconexion As String)
        locID_Compania_Mizart = ""
        locID_Sucursal_Mizart = ""

        Dim con As New SqlConnection(locconexion)
        Dim com As New SqlCommand("Dime_Aque_Compania_FacturarleNOTAMaestro", con)
        com.CommandType = CommandType.StoredProcedure
        com.CommandTimeout = 0
        '@Numero numeric(20,2),@moneda int,@letra varchar(max) output
        'Dame la fecha del Servidor
        Dim prm1 As New SqlParameter("@Clv_NOTA", SqlDbType.BigInt)
        prm1.Direction = ParameterDirection.Input
        prm1.Value = oClv_NOTA
        com.Parameters.Add(prm1)

        Dim prm2 As New SqlParameter("@ID_Compania_Mizart", SqlDbType.VarChar, 50)
        prm2.Direction = ParameterDirection.Output
        prm2.Value = 0
        com.Parameters.Add(prm2)

        Dim prm3 As New SqlParameter("@ID_Sucursal_Mizart", SqlDbType.VarChar, 50)
        prm3.Direction = ParameterDirection.Output
        prm3.Value = ""
        com.Parameters.Add(prm3)


        Try
            con.Open()
            com.ExecuteNonQuery()
            locID_Compania_Mizart = prm2.Value
            locID_Sucursal_Mizart = prm3.Value

        Catch ex As Exception
            If Graba = "0" Then
                Llenalog(ex.Source.ToString & " " & ex.Message)
            Else

                Llenalog(ex.Source.ToString & " " & ex.Message)
            End If
        Finally
            con.Close()
        End Try
    End Sub

    Public Shared Sub Dime_Aque_Compania_Facturarle(ByVal oClv_Factura As Long, ByVal locconexion As String)
        locID_Compania_Mizart = ""
        locID_Sucursal_Mizart = ""

        Dim con As New SqlConnection(locconexion)
        Dim com As New SqlCommand("Dime_Aque_Compania_Facturarle", con)
        com.CommandType = CommandType.StoredProcedure
        com.CommandTimeout = 0
        '@Numero numeric(20,2),@moneda int,@letra varchar(max) output
        'Dame la fecha del Servidor
        Dim prm1 As New SqlParameter("@Clv_Factura", SqlDbType.BigInt)
        prm1.Direction = ParameterDirection.Input
        prm1.Value = oClv_Factura
        com.Parameters.Add(prm1)

        Dim prm2 As New SqlParameter("@ID_Compania_Mizart", SqlDbType.VarChar, 50)
        prm2.Direction = ParameterDirection.Output
        prm2.Value = 0
        com.Parameters.Add(prm2)

        Dim prm3 As New SqlParameter("@ID_Sucursal_Mizart", SqlDbType.VarChar, 50)
        prm3.Direction = ParameterDirection.Output
        prm3.Value = ""
        com.Parameters.Add(prm3)


        Try
            con.Open()
            com.ExecuteNonQuery()
            locID_Compania_Mizart = prm2.Value
            locID_Sucursal_Mizart = prm3.Value

        Catch ex As Exception
            If Graba = "0" Then
                Llenalog(ex.Source.ToString & " " & ex.Message)
            Else

                Llenalog(ex.Source.ToString & " " & ex.Message)
            End If
        Finally
            con.Close()
        End Try
    End Sub


    Public Shared Function Dime_Aque_Compania_Facturarle_Almacen(ByVal oIdCompra As Long, ByVal locconexion As String)
        locID_Compania_Mizart = ""
        locID_Sucursal_Mizart = ""
        Dime_Aque_Compania_Facturarle_Almacen = 0
        Dim con As New SqlConnection(locconexion)
        Dim com As New SqlCommand("Dime_Aque_Compania_Facturarle_Almacen", con)
        com.CommandType = CommandType.StoredProcedure
        com.CommandTimeout = 0
        '@Numero numeric(20,2),@moneda int,@letra varchar(max) output
        'Dame la fecha del Servidor
        Dim prm1 As New SqlParameter("@IdCompra", SqlDbType.BigInt)
        prm1.Direction = ParameterDirection.Input
        prm1.Value = oIdCompra
        com.Parameters.Add(prm1)

        Dim prm2 As New SqlParameter("@ID_Compania_Mizart", SqlDbType.VarChar, 50)
        prm2.Direction = ParameterDirection.Output
        prm2.Value = 0
        com.Parameters.Add(prm2)

        Dim prm3 As New SqlParameter("@ID_Sucursal_Mizart", SqlDbType.VarChar, 50)
        prm3.Direction = ParameterDirection.Output
        prm3.Value = ""
        com.Parameters.Add(prm3)

        Dim prm4 As New SqlParameter("@IdCFD", SqlDbType.BigInt)
        prm4.Direction = ParameterDirection.Output
        prm4.Value = ""
        com.Parameters.Add(prm4)


        Try
            con.Open()
            com.ExecuteNonQuery()
            locID_Compania_Mizart = prm2.Value
            locID_Sucursal_Mizart = prm3.Value
            Dime_Aque_Compania_Facturarle_Almacen = prm4.Value
        Catch ex As Exception
            If Graba = "0" Then
                Llenalog(ex.Source.ToString & " " & ex.Message)
            Else

                Llenalog(ex.Source.ToString & " " & ex.Message)
            End If
        Finally
            con.Close()
        End Try
    End Function
    Public Shared Sub Dime_Aque_Compania_FacturarleGlobal(ByVal oClv_Factura As Long, ByVal LocConexion As String)
        locID_Compania_Mizart = ""
        locID_Sucursal_Mizart = ""

        Dim con As New SqlConnection(LocConexion)
        Dim com As New SqlCommand("Dime_Aque_Compania_FacturarleGlobal", con)
        com.CommandType = CommandType.StoredProcedure
        com.CommandTimeout = 0
        '@Numero numeric(20,2),@moneda int,@letra varchar(max) output
        'Dame la fecha del Servidor
        Dim prm1 As New SqlParameter("@Clv_Facturaglobal", SqlDbType.BigInt)
        prm1.Direction = ParameterDirection.Input
        prm1.Value = oClv_Factura
        com.Parameters.Add(prm1)

        Dim prm2 As New SqlParameter("@ID_Compania_Mizart", SqlDbType.VarChar, 50)
        prm2.Direction = ParameterDirection.Output
        prm2.Value = 0
        com.Parameters.Add(prm2)

        Dim prm3 As New SqlParameter("@ID_Sucursal_Mizart", SqlDbType.VarChar, 50)
        prm3.Direction = ParameterDirection.Output
        prm3.Value = ""
        com.Parameters.Add(prm3)


        Try
            con.Open()
            com.ExecuteNonQuery()
            locID_Compania_Mizart = prm2.Value
            locID_Sucursal_Mizart = prm3.Value

        Catch ex As Exception
            If Graba = "0" Then
                Llenalog(ex.Source.ToString & " " & ex.Message)
            Else

                Llenalog(ex.Source.ToString & " " & ex.Message)
            End If
        Finally
            con.Close()
        End Try
    End Sub

    Public Shared Function Dame_Cantidad_Con_Letra(ByVal oCantidad As Long, ByVal LocConexion As String) As String
        Dame_Cantidad_Con_Letra = ""

        Dim con As New SqlConnection(LocConexion)
        Dim com As New SqlCommand("DameCantidadALetra", con)
        com.CommandType = CommandType.StoredProcedure
        com.CommandTimeout = 0
        '@Numero numeric(20,2),@moneda int,@letra varchar(max) output
        'Dame la fecha del Servidor
        If IsNumeric(oCantidad) = False Then oCantidad = 0
        Dim prm1 As New SqlParameter("@Numero", SqlDbType.Decimal)
        prm1.Direction = ParameterDirection.Input
        prm1.Value = oCantidad
        com.Parameters.Add(prm1)

        Dim prm2 As New SqlParameter("@moneda", SqlDbType.Int)
        prm2.Direction = ParameterDirection.Input
        prm2.Value = 0
        com.Parameters.Add(prm2)

        Dim prm3 As New SqlParameter("@letra", SqlDbType.VarChar, 400)
        prm3.Direction = ParameterDirection.Output
        prm3.Value = ""
        com.Parameters.Add(prm3)

        Try
            con.Open()
            com.ExecuteNonQuery()

            Dame_Cantidad_Con_Letra = prm3.Value

        Catch ex As Exception
            If Graba = "0" Then
                Llenalog(ex.Source.ToString & " " & ex.Message)
            Else

                Llenalog(ex.Source.ToString & " " & ex.Message)
            End If
        Finally
            con.Close()
        End Try
    End Function

    Public Shared Function BusFacFiscalOledb_Almacen(ByVal oClv_Factura As Long, ByVal LocConexion As String) As Integer
        BusFacFiscalOledb_Almacen = 0
        Dim CONEXION As New SqlConnection(LocConexion)
        Dim COMANDO As New SqlCommand("BusFacFiscalAlmacen", CONEXION)
        COMANDO.CommandType = CommandType.StoredProcedure
        COMANDO.CommandTimeout = 0

        Dim parametro As New SqlParameter("@IDFACTURA", SqlDbType.BigInt)
        parametro.Direction = ParameterDirection.Input
        parametro.Value = oClv_Factura
        COMANDO.Parameters.Add(parametro)

        Dim parametro1 As New SqlParameter("@IdReceptor", SqlDbType.Int)
        parametro1.Direction = ParameterDirection.Output
        parametro1.Value = 0
        COMANDO.Parameters.Add(parametro1)

        Try
            CONEXION.Open()
            Dim i As Integer = COMANDO.ExecuteNonQuery
            BusFacFiscalOledb_Almacen = CStr(parametro1.Value)

            'Cnx.DbConnection.Close()
        Catch ex As Exception
            If Graba = "0" Then
                Llenalog(ex.Source.ToString & " " & ex.Message)
            Else

                Llenalog(ex.Source.ToString & " " & ex.Message)
            End If
        Finally
            CONEXION.Close()
            CONEXION.Dispose()
        End Try
    End Function

    Public Shared Sub DameId_NotaCDF(ByVal oClv_Nota As Long, ByVal oTipo As String, ByVal LocConexion As String)
        GloClv_FacturaCFD = 0
        Dim CONEXION As New SqlConnection(LocConexion)
        Dim COMANDO As New SqlCommand("upsDameFacDig_Clv_FacturaCDF", CONEXION)
        COMANDO.CommandType = CommandType.StoredProcedure
        COMANDO.CommandTimeout = 0

        Dim parametro As New SqlParameter("@Clv_Factura", SqlDbType.BigInt)
        parametro.Direction = ParameterDirection.Input
        parametro.Value = oClv_Nota
        COMANDO.Parameters.Add(parametro)

        Dim parametro1 As New SqlParameter("@Clv_FacturaCDF", SqlDbType.Money)
        parametro1.Direction = ParameterDirection.Output
        parametro1.Value = 0
        COMANDO.Parameters.Add(parametro1)

        Dim parametro2 As New SqlParameter("@tipo", SqlDbType.VarChar, 1)
        parametro2.Direction = ParameterDirection.Input
        parametro2.Value = oTipo
        COMANDO.Parameters.Add(parametro2)

        Try
            CONEXION.Open()
            Dim i As Integer = COMANDO.ExecuteNonQuery
            GloClv_FacturaCFD = CStr(parametro1.Value)

            'Cnx.DbConnection.Close()
        Catch ex As Exception
            If Graba = "0" Then
                Llenalog(ex.Source.ToString & " " & ex.Message)
            Else

                Llenalog(ex.Source.ToString & " " & ex.Message)
            End If
        Finally
            CONEXION.Close()
            CONEXION.Dispose()
        End Try
    End Sub

    Public Shared Sub DameId_FacturaCDF(ByVal oClv_Factura As Long, ByVal oTipo As String, ByVal LocConexion As String)
        GloClv_FacturaCFD = 0
        Dim CONEXION As New SqlConnection(LocConexion)
        Dim COMANDO As New SqlCommand("upsDameFacDig_Clv_FacturaCDF", CONEXION)
        COMANDO.CommandType = CommandType.StoredProcedure
        COMANDO.CommandTimeout = 0

        Dim parametro As New SqlParameter("@Clv_Factura", SqlDbType.BigInt)
        parametro.Direction = ParameterDirection.Input
        parametro.Value = oClv_Factura
        COMANDO.Parameters.Add(parametro)

        Dim parametro1 As New SqlParameter("@Clv_FacturaCDF", SqlDbType.Money)
        parametro1.Direction = ParameterDirection.Output
        parametro1.Value = 0
        COMANDO.Parameters.Add(parametro1)

        Dim parametro2 As New SqlParameter("@tipo", SqlDbType.VarChar, 1)
        parametro2.Direction = ParameterDirection.Input
        parametro2.Value = oTipo
        COMANDO.Parameters.Add(parametro2)

        Try
            CONEXION.Open()
            Dim i As Integer = COMANDO.ExecuteNonQuery
            GloClv_FacturaCFD = CStr(parametro1.Value)

            'Cnx.DbConnection.Close()
        Catch ex As Exception
            If Graba = "0" Then
                Llenalog(ex.Source.ToString & " " & ex.Message)
            Else

                Llenalog(ex.Source.ToString & " " & ex.Message)
            End If
        Finally
            CONEXION.Close()
            CONEXION.Dispose()
        End Try
    End Sub

    Public Shared Function BusFacFiscalNotaGlobal(ByVal oClv_Nota As Long, ByVal LocConexion As String) As Integer
        BusFacFiscalNotaGlobal = 0
        Dim CONEXION As New SqlConnection(LocConexion)
        Dim COMANDO As New SqlCommand("BusFacFiscalNotaGlobal", CONEXION)
        COMANDO.CommandType = CommandType.StoredProcedure
        COMANDO.CommandTimeout = 0

        Dim parametro As New SqlParameter("@CLV_NOTA", SqlDbType.BigInt)
        parametro.Direction = ParameterDirection.Input
        parametro.Value = oClv_Nota
        COMANDO.Parameters.Add(parametro)

        Dim parametro1 As New SqlParameter("@IDENT", SqlDbType.Int)
        parametro1.Direction = ParameterDirection.Output
        parametro1.Value = 0
        COMANDO.Parameters.Add(parametro1)

        Try
            CONEXION.Open()
            Dim i As Integer = COMANDO.ExecuteNonQuery
            BusFacFiscalNotaGlobal = CStr(parametro1.Value)

            'Cnx.DbConnection.Close()
        Catch ex As Exception
            If Graba = "0" Then
                Llenalog(ex.Source.ToString & " " & ex.Message)
            Else

                Llenalog(ex.Source.ToString & " " & ex.Message)
            End If
        Finally
            CONEXION.Close()
            CONEXION.Dispose()
        End Try
    End Function

    Public Shared Function BusFacFiscalNota(ByVal oClv_Nota As Long, ByVal LocConexion As String) As Integer
        BusFacFiscalNota = 0
        Dim CONEXION As New SqlConnection(LocConexion)
        Dim COMANDO As New SqlCommand("BusFacFiscalNota", CONEXION)
        COMANDO.CommandType = CommandType.StoredProcedure
        COMANDO.CommandTimeout = 0

        Dim parametro As New SqlParameter("@CLV_NOTA", SqlDbType.BigInt)
        parametro.Direction = ParameterDirection.Input
        parametro.Value = oClv_Nota
        COMANDO.Parameters.Add(parametro)

        Dim parametro1 As New SqlParameter("@IDENT", SqlDbType.Int)
        parametro1.Direction = ParameterDirection.Output
        parametro1.Value = 0
        COMANDO.Parameters.Add(parametro1)

        Try
            CONEXION.Open()
            Dim i As Integer = COMANDO.ExecuteNonQuery
            BusFacFiscalNota = CStr(parametro1.Value)

            'Cnx.DbConnection.Close()
        Catch ex As Exception
            If Graba = "0" Then
                Llenalog(ex.Source.ToString & " " & ex.Message)
            Else

                Llenalog(ex.Source.ToString & " " & ex.Message)
            End If
        Finally
            CONEXION.Close()
            CONEXION.Dispose()
        End Try
    End Function

    Public Shared Function SP_DIMESIEXISTEFACTURADIGITAL(ByVal oClv_Factura As Long, oTipoFact As String, ByVal LocConexion As String) As Long
        SP_DIMESIEXISTEFACTURADIGITAL = 0
        Dim CONEXION As New SqlConnection(LocConexion)
        Dim COMANDO As New SqlCommand("SP_DIMESIEXISTEFACTURADIGITAL", CONEXION)
        COMANDO.CommandType = CommandType.StoredProcedure
        COMANDO.CommandTimeout = 0

        Dim parametro As New SqlParameter("@Clv_Factura", SqlDbType.BigInt)
        parametro.Direction = ParameterDirection.Input
        parametro.Value = oClv_Factura
        COMANDO.Parameters.Add(parametro)

        Dim parametro1 As New SqlParameter("@TipoFact", SqlDbType.VarChar, 10)
        parametro1.Direction = ParameterDirection.Input
        parametro1.Value = oTipoFact
        COMANDO.Parameters.Add(parametro1)

        Dim parametro2 As New SqlParameter("@Clv_FacturaCFD", SqlDbType.BigInt)
        parametro2.Direction = ParameterDirection.Output
        parametro2.Value = 0
        COMANDO.Parameters.Add(parametro2)

        Try
            CONEXION.Open()
            Dim i As Integer = COMANDO.ExecuteNonQuery
            SP_DIMESIEXISTEFACTURADIGITAL = CStr(parametro2.Value)

            'Cnx.DbConnection.Close()
        Catch ex As Exception
            If Graba = "0" Then
                Llenalog(ex.Source.ToString & " " & ex.Message)
            Else

                Llenalog(ex.Source.ToString & " " & ex.Message)
            End If
        Finally
            CONEXION.Close()
            CONEXION.Dispose()
        End Try
    End Function

    Public Shared Function BusFacFiscalOledb(ByVal oClv_Factura As Long, ByVal LocConexion As String) As Integer
        BusFacFiscalOledb = 0
        Dim CONEXION As New SqlConnection(LocConexion)
        Dim COMANDO As New SqlCommand("BusFacFiscal", CONEXION)
        COMANDO.CommandType = CommandType.StoredProcedure
        COMANDO.CommandTimeout = 0

        Dim parametro As New SqlParameter("@CLV_FACTURA", SqlDbType.BigInt)
        parametro.Direction = ParameterDirection.Input
        parametro.Value = oClv_Factura
        COMANDO.Parameters.Add(parametro)

        Dim parametro1 As New SqlParameter("@IDENT", SqlDbType.Int)
        parametro1.Direction = ParameterDirection.Output
        parametro1.Value = 0
        COMANDO.Parameters.Add(parametro1)

        Try
            CONEXION.Open()
            Dim i As Integer = COMANDO.ExecuteNonQuery
            BusFacFiscalOledb = CStr(parametro1.Value)

            'Cnx.DbConnection.Close()
        Catch ex As Exception
            If Graba = "0" Then
                Llenalog(ex.Source.ToString & " " & ex.Message)
            Else

                Llenalog(ex.Source.ToString & " " & ex.Message)
            End If
        Finally
            CONEXION.Close()
            CONEXION.Dispose()
        End Try
    End Function





    'Public Sub Graba_Factura_Digital(ByVal oClv_Factura As Long)
    '    Dim oTotalConPuntos As String = Nothing
    '    Dim oSubTotal As String = Nothing
    '    Dim oTotalSinPuntos As String = Nothing
    '    Dim oDescuento As String = Nothing
    '    Dim oiva As String = Nothing
    '    Dim oieps As String = Nothing
    '    Dim oTasaIva As String = Nothing
    '    Dim oTasaIeps As String = Nothing
    '    Dim oFecha As String = Nothing

    '    Dim CONEXION As New SqlConnection(conexion)
    '    Dim COMANDO As New SqlCommand("DameFacDig_Parte_1", CONEXION)
    '    COMANDO.CommandType = CommandType.StoredProcedure

    '    Dim parametro As New SqlParameter("@Clv_Factura", SqlDbType.BigInt)
    '    parametro.Direction = ParameterDirection.Input
    '    parametro.Value = oClv_Factura
    '    COMANDO.Parameters.Add(parametro)

    '    Dim parametro1 As New SqlParameter("@TotalConPuntos", SqlDbType.Money)
    '    parametro1.Direction = ParameterDirection.Output
    '    parametro1.Value = 0
    '    COMANDO.Parameters.Add(parametro1)

    '    Dim parametro2 As New SqlParameter("@SubTotal", SqlDbType.Money)
    '    parametro2.Direction = ParameterDirection.Output
    '    parametro2.Value = 0
    '    COMANDO.Parameters.Add(parametro2)

    '    Dim parametro3 As New SqlParameter("@TotalSinPuntos", SqlDbType.Money)
    '    parametro3.Direction = ParameterDirection.Output
    '    parametro3.Value = 0
    '    COMANDO.Parameters.Add(parametro3)

    '    Dim parametro4 As New SqlParameter("@Descuento", SqlDbType.Money)
    '    parametro4.Direction = ParameterDirection.Output
    '    parametro4.Value = 0
    '    COMANDO.Parameters.Add(parametro4)

    '    Dim parametro5 As New SqlParameter("@iva", SqlDbType.Money)
    '    parametro5.Direction = ParameterDirection.Output
    '    parametro5.Value = 0
    '    COMANDO.Parameters.Add(parametro5)

    '    Dim parametro6 As New SqlParameter("@ieps", SqlDbType.Money)
    '    parametro6.Direction = ParameterDirection.Output
    '    parametro6.Value = 0
    '    COMANDO.Parameters.Add(parametro6)

    '    Dim parametro7 As New SqlParameter("@TasaIva", SqlDbType.Money)
    '    parametro7.Direction = ParameterDirection.Output
    '    parametro7.Value = 0
    '    COMANDO.Parameters.Add(parametro7)

    '    Dim parametro8 As New SqlParameter("@TasaIeps", SqlDbType.Money)
    '    parametro8.Direction = ParameterDirection.Output
    '    parametro8.Value = 0
    '    COMANDO.Parameters.Add(parametro8)

    '    Dim parametro9 As New SqlParameter("@Fecha", SqlDbType.DateTime)
    '    parametro9.Direction = ParameterDirection.Output
    '    parametro9.Value = 0
    '    COMANDO.Parameters.Add(parametro9)


    '    Try
    '        CONEXION.Open()
    '        Dim i As Integer = COMANDO.ExecuteNonQuery
    '        oTotalConPuntos = CStr(parametro1.Value)
    '        oSubTotal = CStr(parametro2.Value)
    '        oTotalSinPuntos = CStr(parametro3.Value)
    '        oDescuento = CStr(parametro4.Value)
    '        oiva = CStr(parametro5.Value)
    '        oieps = CStr(parametro6.Value)
    '        oTasaIva = CStr(parametro7.Value)
    '        oTasaIeps = CStr(parametro8.Value)
    '        oFecha = CStr(parametro9.Value)
    '        'Cnx.DbConnection.Close()
    '    Catch ex As Exception
    '        'MsgBox(ex.Message, MsgBoxStyle.Exclamation)

    '    Finally
    '        CONEXION.Close()
    '        CONEXION.Dispose()
    '    End Try


    '    Dim oCFD As MizarCFD.BRL.CFD

    '    Using Cnx As New DAConexion("HL", "sa", "sa")
    '        oCFD = New MizarCFD.BRL.CFD
    '        oCFD.Inicializar()
    '        oCFD.TipoCFD = MizarCFD.BRL.CFD.TiposCFD.Emision

    '        oCFD.PrepararDatosEmisorPorId(Cnx, "1", "1")
    '        oCFD.PrepararDatosReceptorPorId(Cnx, "3")

    '        oCFD.Version = "2.0"
    '        oCFD.IdMoneda = "1" '//1 = Pesos , Pesos = 2 USD
    '        oCFD.Fecha = oFecha
    '        oCFD.FormaDePago = "Forma de Pago"
    '        oCFD.Certificado = ""
    '        oCFD.CondicionesDePago = "Condiciones de Pago"
    '        oCFD.SubTotal = oSubTotal
    '        oCFD.Descuento = oDescuento
    '        oCFD.MotivoDescuento = "Pronto Pago"
    '        oCFD.Total = oTotalSinPuntos
    '        oCFD.MetodoDePago = "Efectivo"
    '        oCFD.TipoDeComprobante = "ingreso"

    '        oCFD.Complemento.XMLComplemento = "<raiz>Caelli Enarrant Gloria Dei </raiz>"
    '        oCFD.Addenda.XMLAddenda = "<edi>Edit dididididididi</edi>"

    '        oCFD.Impuestos.TotalImpuestosRetenidos = oiva
    '        oCFD.Impuestos.TotalImpuestosTrasladados = oieps

    '        Dim oimpuestosRetenciones As CFDImpuestosRetenciones
    '        oimpuestosRetenciones = New CFDImpuestosRetenciones()
    '        oimpuestosRetenciones.Impuesto = "IVA"
    '        oimpuestosRetenciones.Importe = oiva
    '        oCFD.Impuestos.Retenciones.Add(oimpuestosRetenciones)

    '        Dim oImpuestosTraslados As CFDImpuestosTraslados

    '        oImpuestosTraslados = New CFDImpuestosTraslados()
    '        oImpuestosTraslados.Impuesto = "IEPS"
    '        oImpuestosTraslados.Importe = oieps
    '        oImpuestosTraslados.Tasa = oTasaIeps

    '        oCFD.Impuestos.Traslados.Add(oImpuestosTraslados)

    '        oImpuestosTraslados = New CFDImpuestosTraslados()
    '        oImpuestosTraslados.Impuesto = "IVA"
    '        oImpuestosTraslados.Importe = oiva
    '        oImpuestosTraslados.Tasa = oTasaIva

    '        oCFD.Impuestos.Traslados.Add(oImpuestosTraslados)

    '        Dim oConcepto As CFDConceptos
    '        oConcepto = New CFDConceptos()
    '        oConcepto.Cantidad = "10"
    '        oConcepto.UnidadMedida = "Pieza"
    '        oConcepto.NumeroIdentificacion = "ART 350"
    '        oConcepto.Descripcion = "Escoba"
    '        oConcepto.ValorUnitario = "10.50"
    '        oConcepto.Importe = "105.000"
    '        oConcepto.CausaIVA = "1"
    '        oCFD.Conceptos.Add(oConcepto)


    '        Dim oConceptoComplemento As CFDConceptosComplemento
    '        oConceptoComplemento = New CFDConceptosComplemento()
    '        oConceptoComplemento.XMLComplemento = "<rootita>Cogito Ergo Sum</rootita>"
    '        oConcepto.Complemento = oConceptoComplemento

    '        oConcepto = New CFDConceptos()
    '        oConcepto.Cantidad = "5"
    '        oConcepto.UnidadMedida = "Pieza"
    '        oConcepto.NumeroIdentificacion = "ART 5"
    '        oConcepto.Descripcion = "Escoba"
    '        oConcepto.ValorUnitario = "5.50"
    '        oConcepto.Importe = "27.50"
    '        oConcepto.CausaIVA = "1"
    '        oCFD.Conceptos.Add(oConcepto)

    '        Dim oConceptoAduanas As CFDConceptosAduanas

    '        oConceptoAduanas = New CFDConceptosAduanas()
    '        oConceptoAduanas.Numero = "Num"
    '        oConceptoAduanas.Fecha = DateTime.Now
    '        oConceptoAduanas.Aduana = "Nuevo Laredo"

    '        oConcepto.Aduanas.Add(oConceptoAduanas)

    '        oConcepto = New CFDConceptos()
    '        oConcepto.Cantidad = "20"
    '        oConcepto.UnidadMedida = "Pieza"
    '        oConcepto.NumeroIdentificacion = "ART 352"
    '        oConcepto.Descripcion = "Trapeador"
    '        oConcepto.ValorUnitario = "11.50"
    '        oConcepto.Importe = "230.0000"
    '        oConcepto.CausaIVA = "1"

    '        Dim oConceptoParte As CFDConceptosPartes

    '        oConceptoParte = New CFDConceptosPartes()
    '        oConceptoParte.Cantidad = "10"
    '        oConceptoParte.UnidadMedida = "Componente"
    '        oConceptoParte.Descripcion = "Palo"
    '        oConceptoParte.ValorUnitario = "10"
    '        oConceptoParte.NumeroIdentificacion = "N1"
    '        oConceptoParte.Importe = "100.00"

    '        oConcepto.Partes.Add(oConceptoParte)

    '        oConceptoParte = New CFDConceptosPartes()
    '        oConceptoParte.Cantidad = "10"
    '        oConceptoParte.UnidadMedida = "Componente"
    '        oConceptoParte.Descripcion = "Estopa"
    '        oConceptoParte.ValorUnitario = "13"
    '        oConceptoParte.NumeroIdentificacion = "N2"
    '        oConceptoParte.Importe = "130.00"


    '        Dim oConceptoParteAduana As CFDConceptosPartesAduanas

    '        oConceptoParteAduana = New CFDConceptosPartesAduanas()
    '        oConceptoParteAduana.Numero = "Num"
    '        oConceptoParteAduana.Fecha = DateTime.Now
    '        oConceptoParteAduana.Aduana = "King Nosa"

    '        oConceptoParte.Aduanas.Add(oConceptoParteAduana)

    '        oConcepto.Partes.Add(oConceptoParte)

    '        oCFD.Conceptos.Add(oConcepto)

    '        If Not oCFD.Insertar(Cnx, True) Then
    '            For Each oMensaje As DAMensajesSistema.RegistroMensaje In oCFD.MensajesSistema.ListaMensajes
    '                MessageBox.Show(oMensaje.TextoMensaje + " " + oMensaje.Contexto)
    '            Next
    '            Return
    '        End If


    '        MessageBox.Show("El número de cfd generadio fue : " + oCFD.IdCFD)



    '    End Using
    'End Sub
    Public Shared Sub Usp_Ed_DameDatosFacDigNotaGlobal(ByVal oCLV_Nota As Long, ByVal oCompania As Integer, ByVal LocConexion As String)
        GloFORMADEPAGO = ""
        GloCONDICIONESDEPAGO = ""
        GloMOTIVODESCUENTO = ""
        GloMETODODEPAGO = ""
        GloTIPODECOMPROBANTE = ""
        GloPAGOENPARCIALIDADES = ""
        GloLUGAREXPEDICION = ""
        GloREGIMEN = ""
        GloMONEDA = ""
        GloVERSON = ""
        GloTIPOFACTOR = ""
        GloFORMAPAGO = ""
        GloTIPOCAMBIO = ""
        GloUSOCFD = ""
        GloFECHAVIGENCIA = ""
        GloUUIDRel = ""
        GloOBSERVACION = ""

        Dim CONEXION As New SqlConnection(LocConexion)
        Dim COMANDO As New SqlCommand("Usp_Ed_DameDatosFacDigNotaGlobal", CONEXION)
        COMANDO.CommandType = CommandType.StoredProcedure
        COMANDO.CommandTimeout = 0
        '@Clv_Factura Bigint,@Clv_FacturaCFD bigint,@Serie varchar(5)

        Dim PARAMETRO1 As New SqlParameter("@Id_CFD", SqlDbType.BigInt)
        PARAMETRO1.Direction = ParameterDirection.Input
        PARAMETRO1.Value = oCLV_Nota
        COMANDO.Parameters.Add(PARAMETRO1)

        Dim PARAMETRO2 As New SqlParameter("@IDCOMPANIA", SqlDbType.Int)
        PARAMETRO2.Direction = ParameterDirection.Input
        PARAMETRO2.Value = oCompania
        COMANDO.Parameters.Add(PARAMETRO2)

        '@FORMADEPAGO VARCHAR(250) OUTPUT
        ',@CONDICIONESDEPAGO VARCHAR(250)OUTPUT,@MOTIVODESCUENTO VARCHAR(250)OUTPUT,@METODODEPAGO VARCHAR(250)OUTPUT,
        '@TIPODECOMPROBANTE VARCHAR(50) OUTPUT,@PAGOENPARCIALIDADES VARCHAR(50) OUTPUT,@LUGAREXPEDICION VARCHAR(250) OUTPUT
        ',@REGIMEN VARCHAR(400) OUTPUT,@MONEDA VARCHAR(15) OUTPUT,@VERSON

        Dim PARAMETRO3 As New SqlParameter("@FORMADEPAGO", SqlDbType.VarChar, 250)
        PARAMETRO3.Direction = ParameterDirection.Output
        PARAMETRO3.Value = ""
        COMANDO.Parameters.Add(PARAMETRO3)

        Dim PARAMETRO4 As New SqlParameter("@CONDICIONESDEPAGO", SqlDbType.VarChar, 250)
        PARAMETRO4.Direction = ParameterDirection.Output
        PARAMETRO4.Value = ""
        COMANDO.Parameters.Add(PARAMETRO4)

        Dim PARAMETRO5 As New SqlParameter("@MOTIVODESCUENTO", SqlDbType.VarChar, 250)
        PARAMETRO5.Direction = ParameterDirection.Output
        PARAMETRO5.Value = ""
        COMANDO.Parameters.Add(PARAMETRO5)

        Dim PARAMETRO6 As New SqlParameter("@METODODEPAGO", SqlDbType.VarChar, 250)
        PARAMETRO6.Direction = ParameterDirection.Output
        PARAMETRO6.Value = ""
        COMANDO.Parameters.Add(PARAMETRO6)

        Dim PARAMETRO7 As New SqlParameter("@TIPODECOMPROBANTE", SqlDbType.VarChar, 250)
        PARAMETRO7.Direction = ParameterDirection.Output
        PARAMETRO7.Value = ""
        COMANDO.Parameters.Add(PARAMETRO7)

        Dim PARAMETRO8 As New SqlParameter("@PAGOENPARCIALIDADES", SqlDbType.VarChar, 250)
        PARAMETRO8.Direction = ParameterDirection.Output
        PARAMETRO8.Value = ""
        COMANDO.Parameters.Add(PARAMETRO8)

        Dim PARAMETRO9 As New SqlParameter("@LUGAREXPEDICION", SqlDbType.VarChar, 250)
        PARAMETRO9.Direction = ParameterDirection.Output
        PARAMETRO9.Value = ""
        COMANDO.Parameters.Add(PARAMETRO9)

        Dim PARAMETRO10 As New SqlParameter("@REGIMEN", SqlDbType.VarChar, 400)
        PARAMETRO10.Direction = ParameterDirection.Output
        PARAMETRO10.Value = ""
        COMANDO.Parameters.Add(PARAMETRO10)

        Dim PARAMETRO11 As New SqlParameter("@MONEDA", SqlDbType.VarChar, 15)
        PARAMETRO11.Direction = ParameterDirection.Output
        PARAMETRO11.Value = ""
        COMANDO.Parameters.Add(PARAMETRO11)

        Dim PARAMETRO12 As New SqlParameter("@VERSON", SqlDbType.VarChar, 15)
        PARAMETRO12.Direction = ParameterDirection.Output
        PARAMETRO12.Value = ""
        COMANDO.Parameters.Add(PARAMETRO12)

        Dim PARAMETRO13 As New SqlParameter("@numCtaPago", SqlDbType.VarChar, 4)
        PARAMETRO13.Direction = ParameterDirection.Output
        PARAMETRO13.Value = ""
        COMANDO.Parameters.Add(PARAMETRO13)

        Dim PARAMETRO14 As New SqlParameter("@TIPOFACTOR", SqlDbType.VarChar, 4)
        PARAMETRO14.Direction = ParameterDirection.Output
        PARAMETRO14.Value = ""
        COMANDO.Parameters.Add(PARAMETRO14)

        Dim PARAMETRO15 As New SqlParameter("@USOCFD", SqlDbType.VarChar, 4)
        PARAMETRO15.Direction = ParameterDirection.Output
        PARAMETRO15.Value = ""
        COMANDO.Parameters.Add(PARAMETRO15)

        Dim PARAMETRO16 As New SqlParameter("@FECHAFACTURA", SqlDbType.VarChar, 20)
        PARAMETRO16.Direction = ParameterDirection.Output
        PARAMETRO16.Value = ""
        COMANDO.Parameters.Add(PARAMETRO16)

        Dim PARAMETRO17 As New SqlParameter("@TIPOCAMBIO", SqlDbType.VarChar, 4)
        PARAMETRO17.Direction = ParameterDirection.Output
        PARAMETRO17.Value = ""
        COMANDO.Parameters.Add(PARAMETRO17)

        Dim PARAMETRO18 As New SqlParameter("@UUIDRel", SqlDbType.VarChar, 150)
        PARAMETRO18.Direction = ParameterDirection.Output
        PARAMETRO18.Value = ""
        COMANDO.Parameters.Add(PARAMETRO18)

        Try
            '@FORMADEPAGO VARCHAR(250) OUTPUT
            ',@CONDICIONESDEPAGO VARCHAR(250)OUTPUT,@MOTIVODESCUENTO VARCHAR(250)OUTPUT,@METODODEPAGO VARCHAR(250)OUTPUT,
            '@TIPODECOMPROBANTE VARCHAR(50) OUTPUT,@PAGOENPARCIALIDADES VARCHAR(50) OUTPUT,@LUGAREXPEDICION VARCHAR(250) OUTPUT
            ',@REGIMEN VARCHAR(400) OUTPUT,@MONEDA VARCHAR(15) OUTPUT,@VERSON  

            CONEXION.Open()
            Dim i As Integer = COMANDO.ExecuteNonQuery
            GloFORMADEPAGO = PARAMETRO3.Value
            GloCONDICIONESDEPAGO = PARAMETRO4.Value
            GloMOTIVODESCUENTO = PARAMETRO5.Value
            GloMETODODEPAGO = PARAMETRO6.Value
            GloTIPODECOMPROBANTE = PARAMETRO7.Value
            GloPAGOENPARCIALIDADES = PARAMETRO8.Value
            GloLUGAREXPEDICION = PARAMETRO9.Value
            GloREGIMEN = PARAMETRO10.Value
            GloMONEDA = PARAMETRO11.Value
            GloVERSON = PARAMETRO12.Value
            GlonumCtaPago = PARAMETRO13.Value
            GloTIPOFACTOR = PARAMETRO14.Value
            GloUSOCFD = PARAMETRO15.Value
            GloFECHAVIGENCIA = PARAMETRO16.Value
            GloTIPOCAMBIO = PARAMETRO17.Value
            GloUUIDRel = PARAMETRO18.Value
        Catch ex As Exception
            If Graba = "0" Then
                Llenalog(ex.Source.ToString & " " & ex.Message)
            Else

                Llenalog(ex.Source.ToString & " " & ex.Message)
            End If
        Finally
            CONEXION.Close()
            CONEXION.Dispose()
        End Try
    End Sub

    Public Shared Sub Usp_Ed_DameDatosFacDigNota(ByVal oCLV_Nota As Long, ByVal oCompania As Integer, ByVal LocConexion As String)
        GloFORMADEPAGO = ""
        GloCONDICIONESDEPAGO = ""
        GloMOTIVODESCUENTO = ""
        GloMETODODEPAGO = ""
        GloTIPODECOMPROBANTE = ""
        GloPAGOENPARCIALIDADES = ""
        GloLUGAREXPEDICION = ""
        GloREGIMEN = ""
        GloMONEDA = ""
        GloVERSON = ""
        GloTIPOFACTOR = ""
        GloFORMAPAGO = ""
        GloTIPOCAMBIO = ""
        GloUSOCFD = ""
        GloFECHAVIGENCIA = ""
        GloUUIDRel = ""

        Dim CONEXION As New SqlConnection(LocConexion)
        Dim COMANDO As New SqlCommand("Usp_Ed_DameDatosFacDigNota", CONEXION)
        COMANDO.CommandType = CommandType.StoredProcedure
        COMANDO.CommandTimeout = 0
        '@Clv_Factura Bigint,@Clv_FacturaCFD bigint,@Serie varchar(5)

        Dim PARAMETRO1 As New SqlParameter("@CLV_NOTA", SqlDbType.BigInt)
        PARAMETRO1.Direction = ParameterDirection.Input
        PARAMETRO1.Value = oCLV_Nota
        COMANDO.Parameters.Add(PARAMETRO1)

        Dim PARAMETRO2 As New SqlParameter("@IDCOMPANIA", SqlDbType.Int)
        PARAMETRO2.Direction = ParameterDirection.Input
        PARAMETRO2.Value = oCompania
        COMANDO.Parameters.Add(PARAMETRO2)

        '@FORMADEPAGO VARCHAR(250) OUTPUT
        ',@CONDICIONESDEPAGO VARCHAR(250)OUTPUT,@MOTIVODESCUENTO VARCHAR(250)OUTPUT,@METODODEPAGO VARCHAR(250)OUTPUT,
        '@TIPODECOMPROBANTE VARCHAR(50) OUTPUT,@PAGOENPARCIALIDADES VARCHAR(50) OUTPUT,@LUGAREXPEDICION VARCHAR(250) OUTPUT
        ',@REGIMEN VARCHAR(400) OUTPUT,@MONEDA VARCHAR(15) OUTPUT,@VERSON

        Dim PARAMETRO3 As New SqlParameter("@FORMADEPAGO", SqlDbType.VarChar, 250)
        PARAMETRO3.Direction = ParameterDirection.Output
        PARAMETRO3.Value = ""
        COMANDO.Parameters.Add(PARAMETRO3)

        Dim PARAMETRO4 As New SqlParameter("@CONDICIONESDEPAGO", SqlDbType.VarChar, 250)
        PARAMETRO4.Direction = ParameterDirection.Output
        PARAMETRO4.Value = ""
        COMANDO.Parameters.Add(PARAMETRO4)

        Dim PARAMETRO5 As New SqlParameter("@MOTIVODESCUENTO", SqlDbType.VarChar, 250)
        PARAMETRO5.Direction = ParameterDirection.Output
        PARAMETRO5.Value = ""
        COMANDO.Parameters.Add(PARAMETRO5)

        Dim PARAMETRO6 As New SqlParameter("@METODODEPAGO", SqlDbType.VarChar, 250)
        PARAMETRO6.Direction = ParameterDirection.Output
        PARAMETRO6.Value = ""
        COMANDO.Parameters.Add(PARAMETRO6)

        Dim PARAMETRO7 As New SqlParameter("@TIPODECOMPROBANTE", SqlDbType.VarChar, 250)
        PARAMETRO7.Direction = ParameterDirection.Output
        PARAMETRO7.Value = ""
        COMANDO.Parameters.Add(PARAMETRO7)

        Dim PARAMETRO8 As New SqlParameter("@PAGOENPARCIALIDADES", SqlDbType.VarChar, 250)
        PARAMETRO8.Direction = ParameterDirection.Output
        PARAMETRO8.Value = ""
        COMANDO.Parameters.Add(PARAMETRO8)

        Dim PARAMETRO9 As New SqlParameter("@LUGAREXPEDICION", SqlDbType.VarChar, 250)
        PARAMETRO9.Direction = ParameterDirection.Output
        PARAMETRO9.Value = ""
        COMANDO.Parameters.Add(PARAMETRO9)

        Dim PARAMETRO10 As New SqlParameter("@REGIMEN", SqlDbType.VarChar, 400)
        PARAMETRO10.Direction = ParameterDirection.Output
        PARAMETRO10.Value = ""
        COMANDO.Parameters.Add(PARAMETRO10)

        Dim PARAMETRO11 As New SqlParameter("@MONEDA", SqlDbType.VarChar, 15)
        PARAMETRO11.Direction = ParameterDirection.Output
        PARAMETRO11.Value = ""
        COMANDO.Parameters.Add(PARAMETRO11)

        Dim PARAMETRO12 As New SqlParameter("@VERSON", SqlDbType.VarChar, 15)
        PARAMETRO12.Direction = ParameterDirection.Output
        PARAMETRO12.Value = ""
        COMANDO.Parameters.Add(PARAMETRO12)

        Dim PARAMETRO13 As New SqlParameter("@numCtaPago", SqlDbType.VarChar, 4)
        PARAMETRO13.Direction = ParameterDirection.Output
        PARAMETRO13.Value = ""
        COMANDO.Parameters.Add(PARAMETRO13)

        Dim PARAMETRO14 As New SqlParameter("@TIPOFACTOR", SqlDbType.VarChar, 4)
        PARAMETRO14.Direction = ParameterDirection.Output
        PARAMETRO14.Value = ""
        COMANDO.Parameters.Add(PARAMETRO14)

        Dim PARAMETRO15 As New SqlParameter("@USOCFD", SqlDbType.VarChar, 4)
        PARAMETRO15.Direction = ParameterDirection.Output
        PARAMETRO15.Value = ""
        COMANDO.Parameters.Add(PARAMETRO15)

        Dim PARAMETRO16 As New SqlParameter("@FECHAFACTURA", SqlDbType.VarChar, 20)
        PARAMETRO16.Direction = ParameterDirection.Output
        PARAMETRO16.Value = ""
        COMANDO.Parameters.Add(PARAMETRO16)

        Dim PARAMETRO17 As New SqlParameter("@TIPOCAMBIO", SqlDbType.VarChar, 4)
        PARAMETRO17.Direction = ParameterDirection.Output
        PARAMETRO17.Value = ""
        COMANDO.Parameters.Add(PARAMETRO17)

        Dim PARAMETRO18 As New SqlParameter("@UUIDRel", SqlDbType.VarChar, 150)
        PARAMETRO18.Direction = ParameterDirection.Output
        PARAMETRO18.Value = ""
        COMANDO.Parameters.Add(PARAMETRO18)

        Try
            '@FORMADEPAGO VARCHAR(250) OUTPUT
            ',@CONDICIONESDEPAGO VARCHAR(250)OUTPUT,@MOTIVODESCUENTO VARCHAR(250)OUTPUT,@METODODEPAGO VARCHAR(250)OUTPUT,
            '@TIPODECOMPROBANTE VARCHAR(50) OUTPUT,@PAGOENPARCIALIDADES VARCHAR(50) OUTPUT,@LUGAREXPEDICION VARCHAR(250) OUTPUT
            ',@REGIMEN VARCHAR(400) OUTPUT,@MONEDA VARCHAR(15) OUTPUT,@VERSON  

            CONEXION.Open()
            Dim i As Integer = COMANDO.ExecuteNonQuery
            GloFORMADEPAGO = PARAMETRO3.Value
            GloCONDICIONESDEPAGO = PARAMETRO4.Value
            GloMOTIVODESCUENTO = PARAMETRO5.Value
            GloMETODODEPAGO = PARAMETRO6.Value
            GloTIPODECOMPROBANTE = PARAMETRO7.Value
            GloPAGOENPARCIALIDADES = PARAMETRO8.Value
            GloLUGAREXPEDICION = PARAMETRO9.Value
            GloREGIMEN = PARAMETRO10.Value
            GloMONEDA = PARAMETRO11.Value
            GloVERSON = PARAMETRO12.Value
            GlonumCtaPago = PARAMETRO13.Value
            GloTIPOFACTOR = PARAMETRO14.Value
            GloUSOCFD = PARAMETRO15.Value
            GloFECHAVIGENCIA = PARAMETRO16.Value
            GloTIPOCAMBIO = PARAMETRO17.Value
            GloUUIDRel = PARAMETRO18.Value
        Catch ex As Exception
            If Graba = "0" Then
                Llenalog(ex.Source.ToString & " " & ex.Message)
            Else

                Llenalog(ex.Source.ToString & " " & ex.Message)
            End If
        Finally
            CONEXION.Close()
            CONEXION.Dispose()
        End Try
    End Sub

    Public Shared Sub Usp_Ed_DameDatosFacDigNotaMaestro(ByVal oCLV_Nota As Long, ByVal oCompania As Integer, ByVal LocConexion As String)
        GloFORMADEPAGO = ""
        GloCONDICIONESDEPAGO = ""
        GloMOTIVODESCUENTO = ""
        GloMETODODEPAGO = ""
        GloTIPODECOMPROBANTE = ""
        GloPAGOENPARCIALIDADES = ""
        GloLUGAREXPEDICION = ""
        GloREGIMEN = ""
        GloMONEDA = ""
        GloVERSON = ""
        GloTIPOFACTOR = ""
        GloFORMAPAGO = ""
        GloTIPOCAMBIO = ""
        GloUSOCFD = ""
        GloFECHAVIGENCIA = ""
        GloUUIDRel = ""

        Dim CONEXION As New SqlConnection(LocConexion)
        Dim COMANDO As New SqlCommand("Usp_Ed_DameDatosFacDigNotaMaestro", CONEXION)
        COMANDO.CommandType = CommandType.StoredProcedure
        COMANDO.CommandTimeout = 0
        '@Clv_Factura Bigint,@Clv_FacturaCFD bigint,@Serie varchar(5)

        Dim PARAMETRO1 As New SqlParameter("@CLV_NOTA", SqlDbType.BigInt)
        PARAMETRO1.Direction = ParameterDirection.Input
        PARAMETRO1.Value = oCLV_Nota
        COMANDO.Parameters.Add(PARAMETRO1)

        Dim PARAMETRO2 As New SqlParameter("@IDCOMPANIA", SqlDbType.Int)
        PARAMETRO2.Direction = ParameterDirection.Input
        PARAMETRO2.Value = oCompania
        COMANDO.Parameters.Add(PARAMETRO2)

        '@FORMADEPAGO VARCHAR(250) OUTPUT
        ',@CONDICIONESDEPAGO VARCHAR(250)OUTPUT,@MOTIVODESCUENTO VARCHAR(250)OUTPUT,@METODODEPAGO VARCHAR(250)OUTPUT,
        '@TIPODECOMPROBANTE VARCHAR(50) OUTPUT,@PAGOENPARCIALIDADES VARCHAR(50) OUTPUT,@LUGAREXPEDICION VARCHAR(250) OUTPUT
        ',@REGIMEN VARCHAR(400) OUTPUT,@MONEDA VARCHAR(15) OUTPUT,@VERSON

        Dim PARAMETRO3 As New SqlParameter("@FORMADEPAGO", SqlDbType.VarChar, 250)
        PARAMETRO3.Direction = ParameterDirection.Output
        PARAMETRO3.Value = ""
        COMANDO.Parameters.Add(PARAMETRO3)

        Dim PARAMETRO4 As New SqlParameter("@CONDICIONESDEPAGO", SqlDbType.VarChar, 250)
        PARAMETRO4.Direction = ParameterDirection.Output
        PARAMETRO4.Value = ""
        COMANDO.Parameters.Add(PARAMETRO4)

        Dim PARAMETRO5 As New SqlParameter("@MOTIVODESCUENTO", SqlDbType.VarChar, 250)
        PARAMETRO5.Direction = ParameterDirection.Output
        PARAMETRO5.Value = ""
        COMANDO.Parameters.Add(PARAMETRO5)

        Dim PARAMETRO6 As New SqlParameter("@METODODEPAGO", SqlDbType.VarChar, 250)
        PARAMETRO6.Direction = ParameterDirection.Output
        PARAMETRO6.Value = ""
        COMANDO.Parameters.Add(PARAMETRO6)

        Dim PARAMETRO7 As New SqlParameter("@TIPODECOMPROBANTE", SqlDbType.VarChar, 250)
        PARAMETRO7.Direction = ParameterDirection.Output
        PARAMETRO7.Value = ""
        COMANDO.Parameters.Add(PARAMETRO7)

        Dim PARAMETRO8 As New SqlParameter("@PAGOENPARCIALIDADES", SqlDbType.VarChar, 250)
        PARAMETRO8.Direction = ParameterDirection.Output
        PARAMETRO8.Value = ""
        COMANDO.Parameters.Add(PARAMETRO8)

        Dim PARAMETRO9 As New SqlParameter("@LUGAREXPEDICION", SqlDbType.VarChar, 250)
        PARAMETRO9.Direction = ParameterDirection.Output
        PARAMETRO9.Value = ""
        COMANDO.Parameters.Add(PARAMETRO9)

        Dim PARAMETRO10 As New SqlParameter("@REGIMEN", SqlDbType.VarChar, 400)
        PARAMETRO10.Direction = ParameterDirection.Output
        PARAMETRO10.Value = ""
        COMANDO.Parameters.Add(PARAMETRO10)

        Dim PARAMETRO11 As New SqlParameter("@MONEDA", SqlDbType.VarChar, 15)
        PARAMETRO11.Direction = ParameterDirection.Output
        PARAMETRO11.Value = ""
        COMANDO.Parameters.Add(PARAMETRO11)

        Dim PARAMETRO12 As New SqlParameter("@VERSON", SqlDbType.VarChar, 15)
        PARAMETRO12.Direction = ParameterDirection.Output
        PARAMETRO12.Value = ""
        COMANDO.Parameters.Add(PARAMETRO12)

        Dim PARAMETRO13 As New SqlParameter("@numCtaPago", SqlDbType.VarChar, 4)
        PARAMETRO13.Direction = ParameterDirection.Output
        PARAMETRO13.Value = ""
        COMANDO.Parameters.Add(PARAMETRO13)

        Dim PARAMETRO14 As New SqlParameter("@TIPOFACTOR", SqlDbType.VarChar, 4)
        PARAMETRO14.Direction = ParameterDirection.Output
        PARAMETRO14.Value = ""
        COMANDO.Parameters.Add(PARAMETRO14)

        Dim PARAMETRO15 As New SqlParameter("@USOCFD", SqlDbType.VarChar, 4)
        PARAMETRO15.Direction = ParameterDirection.Output
        PARAMETRO15.Value = ""
        COMANDO.Parameters.Add(PARAMETRO15)

        Dim PARAMETRO16 As New SqlParameter("@FECHAFACTURA", SqlDbType.VarChar, 20)
        PARAMETRO16.Direction = ParameterDirection.Output
        PARAMETRO16.Value = ""
        COMANDO.Parameters.Add(PARAMETRO16)

        Dim PARAMETRO17 As New SqlParameter("@TIPOCAMBIO", SqlDbType.VarChar, 4)
        PARAMETRO17.Direction = ParameterDirection.Output
        PARAMETRO17.Value = ""
        COMANDO.Parameters.Add(PARAMETRO17)

        Dim PARAMETRO18 As New SqlParameter("@UUIDRel", SqlDbType.VarChar, 150)
        PARAMETRO18.Direction = ParameterDirection.Output
        PARAMETRO18.Value = ""
        COMANDO.Parameters.Add(PARAMETRO18)

        Try
            '@FORMADEPAGO VARCHAR(250) OUTPUT
            ',@CONDICIONESDEPAGO VARCHAR(250)OUTPUT,@MOTIVODESCUENTO VARCHAR(250)OUTPUT,@METODODEPAGO VARCHAR(250)OUTPUT,
            '@TIPODECOMPROBANTE VARCHAR(50) OUTPUT,@PAGOENPARCIALIDADES VARCHAR(50) OUTPUT,@LUGAREXPEDICION VARCHAR(250) OUTPUT
            ',@REGIMEN VARCHAR(400) OUTPUT,@MONEDA VARCHAR(15) OUTPUT,@VERSON  

            CONEXION.Open()
            Dim i As Integer = COMANDO.ExecuteNonQuery
            GloFORMADEPAGO = PARAMETRO3.Value
            GloCONDICIONESDEPAGO = PARAMETRO4.Value
            GloMOTIVODESCUENTO = PARAMETRO5.Value
            GloMETODODEPAGO = PARAMETRO6.Value
            GloTIPODECOMPROBANTE = PARAMETRO7.Value
            GloPAGOENPARCIALIDADES = PARAMETRO8.Value
            GloLUGAREXPEDICION = PARAMETRO9.Value
            GloREGIMEN = PARAMETRO10.Value
            GloMONEDA = PARAMETRO11.Value
            GloVERSON = PARAMETRO12.Value
            GlonumCtaPago = PARAMETRO13.Value
            GloTIPOFACTOR = PARAMETRO14.Value
            GloUSOCFD = PARAMETRO15.Value
            GloFECHAVIGENCIA = PARAMETRO16.Value
            GloTIPOCAMBIO = PARAMETRO17.Value
            GloUUIDRel = PARAMETRO18.Value
        Catch ex As Exception
            If Graba = "0" Then
                Llenalog(ex.Source.ToString & " " & ex.Message)
            Else

                Llenalog(ex.Source.ToString & " " & ex.Message)
            End If
        Finally
            CONEXION.Close()
            CONEXION.Dispose()
        End Try
    End Sub

    Public Shared Sub Usp_Ed_DameDatosFacDig(ByVal oCLV_FActura As Long, ByVal oCompania As Integer, ByVal LocConexion As String)
        GloFORMADEPAGO = ""
        GloCONDICIONESDEPAGO = ""
        GloMOTIVODESCUENTO = ""
        GloMETODODEPAGO = ""
        GloTIPODECOMPROBANTE = ""
        GloPAGOENPARCIALIDADES = ""
        GloLUGAREXPEDICION = ""
        GloREGIMEN = ""
        GloMONEDA = ""
        GloVERSON = ""
        GloTIPOFACTOR = ""
        GloFORMAPAGO = ""
        GloTIPOCAMBIO = ""
        GloUSOCFD = ""
        GloFECHAVIGENCIA = ""

        Dim CONEXION As New SqlConnection(LocConexion)
        Dim COMANDO As New SqlCommand("Usp_Ed_DameDatosFacDig", CONEXION)
        COMANDO.CommandType = CommandType.StoredProcedure
        CoManDo.CommandTimeout = 0
        '@Clv_Factura Bigint,@Clv_FacturaCFD bigint,@Serie varchar(5)

        Dim PARAMETRO1 As New SqlParameter("@CLV_FACTURA", SqlDbType.BigInt)
        PARAMETRO1.Direction = ParameterDirection.Input
        PARAMETRO1.Value = oCLV_FActura
        COMANDO.Parameters.Add(PARAMETRO1)

        Dim PARAMETRO2 As New SqlParameter("@IDCOMPANIA", SqlDbType.Int)
        PARAMETRO2.Direction = ParameterDirection.Input
        PARAMETRO2.Value = oCompania
        COMANDO.Parameters.Add(PARAMETRO2)

        '@FORMADEPAGO VARCHAR(250) OUTPUT
        ',@CONDICIONESDEPAGO VARCHAR(250)OUTPUT,@MOTIVODESCUENTO VARCHAR(250)OUTPUT,@METODODEPAGO VARCHAR(250)OUTPUT,
        '@TIPODECOMPROBANTE VARCHAR(50) OUTPUT,@PAGOENPARCIALIDADES VARCHAR(50) OUTPUT,@LUGAREXPEDICION VARCHAR(250) OUTPUT
        ',@REGIMEN VARCHAR(400) OUTPUT,@MONEDA VARCHAR(15) OUTPUT,@VERSON

        Dim PARAMETRO3 As New SqlParameter("@FORMADEPAGO", SqlDbType.VarChar, 250)
        PARAMETRO3.Direction = ParameterDirection.Output
        PARAMETRO3.Value = ""
        COMANDO.Parameters.Add(PARAMETRO3)

        Dim PARAMETRO4 As New SqlParameter("@CONDICIONESDEPAGO", SqlDbType.VarChar, 250)
        PARAMETRO4.Direction = ParameterDirection.Output
        PARAMETRO4.Value = ""
        COMANDO.Parameters.Add(PARAMETRO4)

        Dim PARAMETRO5 As New SqlParameter("@MOTIVODESCUENTO", SqlDbType.VarChar, 250)
        PARAMETRO5.Direction = ParameterDirection.Output
        PARAMETRO5.Value = ""
        COMANDO.Parameters.Add(PARAMETRO5)

        Dim PARAMETRO6 As New SqlParameter("@METODODEPAGO", SqlDbType.VarChar, 250)
        PARAMETRO6.Direction = ParameterDirection.Output
        PARAMETRO6.Value = ""
        COMANDO.Parameters.Add(PARAMETRO6)

        Dim PARAMETRO7 As New SqlParameter("@TIPODECOMPROBANTE", SqlDbType.VarChar, 250)
        PARAMETRO7.Direction = ParameterDirection.Output
        PARAMETRO7.Value = ""
        COMANDO.Parameters.Add(PARAMETRO7)

        Dim PARAMETRO8 As New SqlParameter("@PAGOENPARCIALIDADES", SqlDbType.VarChar, 250)
        PARAMETRO8.Direction = ParameterDirection.Output
        PARAMETRO8.Value = ""
        COMANDO.Parameters.Add(PARAMETRO8)

        Dim PARAMETRO9 As New SqlParameter("@LUGAREXPEDICION", SqlDbType.VarChar, 250)
        PARAMETRO9.Direction = ParameterDirection.Output
        PARAMETRO9.Value = ""
        COMANDO.Parameters.Add(PARAMETRO9)

        Dim PARAMETRO10 As New SqlParameter("@REGIMEN", SqlDbType.VarChar, 400)
        PARAMETRO10.Direction = ParameterDirection.Output
        PARAMETRO10.Value = ""
        COMANDO.Parameters.Add(PARAMETRO10)

        Dim PARAMETRO11 As New SqlParameter("@MONEDA", SqlDbType.VarChar, 15)
        PARAMETRO11.Direction = ParameterDirection.Output
        PARAMETRO11.Value = ""
        COMANDO.Parameters.Add(PARAMETRO11)

        Dim PARAMETRO12 As New SqlParameter("@VERSON", SqlDbType.VarChar, 15)
        PARAMETRO12.Direction = ParameterDirection.Output
        PARAMETRO12.Value = ""
        COMANDO.Parameters.Add(PARAMETRO12)

        Dim PARAMETRO13 As New SqlParameter("@numCtaPago", SqlDbType.VarChar, 4)
        PARAMETRO13.Direction = ParameterDirection.Output
        PARAMETRO13.Value = ""
        COMANDO.Parameters.Add(PARAMETRO13)

        Dim PARAMETRO14 As New SqlParameter("@TIPOFACTOR", SqlDbType.VarChar, 4)
        PARAMETRO14.Direction = ParameterDirection.Output
        PARAMETRO14.Value = ""
        COMANDO.Parameters.Add(PARAMETRO14)

        Dim PARAMETRO15 As New SqlParameter("@USOCFD", SqlDbType.VarChar, 4)
        PARAMETRO15.Direction = ParameterDirection.Output
        PARAMETRO15.Value = ""
        COMANDO.Parameters.Add(PARAMETRO15)

        Dim PARAMETRO16 As New SqlParameter("@FECHAFACTURA", SqlDbType.VarChar, 20)
        PARAMETRO16.Direction = ParameterDirection.Output
        PARAMETRO16.Value = ""
        COMANDO.Parameters.Add(PARAMETRO16)

        Dim PARAMETRO17 As New SqlParameter("@TIPOCAMBIO", SqlDbType.VarChar, 4)
        PARAMETRO17.Direction = ParameterDirection.Output
        PARAMETRO17.Value = ""
        COMANDO.Parameters.Add(PARAMETRO17)

        Try
            '@FORMADEPAGO VARCHAR(250) OUTPUT
            ',@CONDICIONESDEPAGO VARCHAR(250)OUTPUT,@MOTIVODESCUENTO VARCHAR(250)OUTPUT,@METODODEPAGO VARCHAR(250)OUTPUT,
            '@TIPODECOMPROBANTE VARCHAR(50) OUTPUT,@PAGOENPARCIALIDADES VARCHAR(50) OUTPUT,@LUGAREXPEDICION VARCHAR(250) OUTPUT
            ',@REGIMEN VARCHAR(400) OUTPUT,@MONEDA VARCHAR(15) OUTPUT,@VERSON  

            CONEXION.Open()
            Dim i As Integer = COMANDO.ExecuteNonQuery
            GloFORMADEPAGO = PARAMETRO3.Value
            GloCONDICIONESDEPAGO = PARAMETRO4.Value
            GloMOTIVODESCUENTO = PARAMETRO5.Value
            GloMETODODEPAGO = PARAMETRO6.Value
            GloTIPODECOMPROBANTE = PARAMETRO7.Value
            GloPAGOENPARCIALIDADES = PARAMETRO8.Value
            GloLUGAREXPEDICION = PARAMETRO9.Value
            GloREGIMEN = PARAMETRO10.Value
            GloMONEDA = PARAMETRO11.Value
            GloVERSON = PARAMETRO12.Value
            GlonumCtaPago = PARAMETRO13.Value
            GloTIPOFACTOR = PARAMETRO14.Value
            GloUSOCFD = PARAMETRO15.Value
            GloFECHAVIGENCIA = PARAMETRO16.Value
            GloTIPOCAMBIO = PARAMETRO17.Value
        Catch ex As Exception
            If Graba = "0" Then
                Llenalog(ex.Source.ToString & " " & ex.Message)
            Else

                Llenalog(ex.Source.ToString & " " & ex.Message)
            End If
        Finally
            CONEXION.Close()
            CONEXION.Dispose()
        End Try
    End Sub

    Public Shared Sub Usp_Ed_DameDatosFacDigRefacturacion(ByVal oCLV_FActura As Long, ByVal oCompania As Integer, ByVal LocConexion As String)
        GloFORMADEPAGO = ""
        GloCONDICIONESDEPAGO = ""
        GloMOTIVODESCUENTO = ""
        GloMETODODEPAGO = ""
        GloTIPODECOMPROBANTE = ""
        GloPAGOENPARCIALIDADES = ""
        GloLUGAREXPEDICION = ""
        GloREGIMEN = ""
        GloMONEDA = ""
        GloVERSON = ""
        GloTIPOFACTOR = ""
        GloFORMAPAGO = ""
        GloTIPOCAMBIO = ""
        GloUSOCFD = ""
        GloFECHAVIGENCIA = ""
        GloUUIDRel = ""

        Dim CONEXION As New SqlConnection(LocConexion)
        Dim COMANDO As New SqlCommand("Usp_Ed_DameDatosFacDigRefacturacion", CONEXION)
        COMANDO.CommandType = CommandType.StoredProcedure
        COMANDO.CommandTimeout = 0
        '@Clv_Factura Bigint,@Clv_FacturaCFD bigint,@Serie varchar(5)

        Dim PARAMETRO1 As New SqlParameter("@CLV_FACTURA", SqlDbType.BigInt)
        PARAMETRO1.Direction = ParameterDirection.Input
        PARAMETRO1.Value = oCLV_FActura
        COMANDO.Parameters.Add(PARAMETRO1)

        Dim PARAMETRO2 As New SqlParameter("@IDCOMPANIA", SqlDbType.Int)
        PARAMETRO2.Direction = ParameterDirection.Input
        PARAMETRO2.Value = oCompania
        COMANDO.Parameters.Add(PARAMETRO2)

        '@FORMADEPAGO VARCHAR(250) OUTPUT
        ',@CONDICIONESDEPAGO VARCHAR(250)OUTPUT,@MOTIVODESCUENTO VARCHAR(250)OUTPUT,@METODODEPAGO VARCHAR(250)OUTPUT,
        '@TIPODECOMPROBANTE VARCHAR(50) OUTPUT,@PAGOENPARCIALIDADES VARCHAR(50) OUTPUT,@LUGAREXPEDICION VARCHAR(250) OUTPUT
        ',@REGIMEN VARCHAR(400) OUTPUT,@MONEDA VARCHAR(15) OUTPUT,@VERSON

        Dim PARAMETRO3 As New SqlParameter("@FORMADEPAGO", SqlDbType.VarChar, 250)
        PARAMETRO3.Direction = ParameterDirection.Output
        PARAMETRO3.Value = ""
        COMANDO.Parameters.Add(PARAMETRO3)

        Dim PARAMETRO4 As New SqlParameter("@CONDICIONESDEPAGO", SqlDbType.VarChar, 250)
        PARAMETRO4.Direction = ParameterDirection.Output
        PARAMETRO4.Value = ""
        COMANDO.Parameters.Add(PARAMETRO4)

        Dim PARAMETRO5 As New SqlParameter("@MOTIVODESCUENTO", SqlDbType.VarChar, 250)
        PARAMETRO5.Direction = ParameterDirection.Output
        PARAMETRO5.Value = ""
        COMANDO.Parameters.Add(PARAMETRO5)

        Dim PARAMETRO6 As New SqlParameter("@METODODEPAGO", SqlDbType.VarChar, 250)
        PARAMETRO6.Direction = ParameterDirection.Output
        PARAMETRO6.Value = ""
        COMANDO.Parameters.Add(PARAMETRO6)

        Dim PARAMETRO7 As New SqlParameter("@TIPODECOMPROBANTE", SqlDbType.VarChar, 250)
        PARAMETRO7.Direction = ParameterDirection.Output
        PARAMETRO7.Value = ""
        COMANDO.Parameters.Add(PARAMETRO7)

        Dim PARAMETRO8 As New SqlParameter("@PAGOENPARCIALIDADES", SqlDbType.VarChar, 250)
        PARAMETRO8.Direction = ParameterDirection.Output
        PARAMETRO8.Value = ""
        COMANDO.Parameters.Add(PARAMETRO8)

        Dim PARAMETRO9 As New SqlParameter("@LUGAREXPEDICION", SqlDbType.VarChar, 250)
        PARAMETRO9.Direction = ParameterDirection.Output
        PARAMETRO9.Value = ""
        COMANDO.Parameters.Add(PARAMETRO9)

        Dim PARAMETRO10 As New SqlParameter("@REGIMEN", SqlDbType.VarChar, 400)
        PARAMETRO10.Direction = ParameterDirection.Output
        PARAMETRO10.Value = ""
        COMANDO.Parameters.Add(PARAMETRO10)

        Dim PARAMETRO11 As New SqlParameter("@MONEDA", SqlDbType.VarChar, 15)
        PARAMETRO11.Direction = ParameterDirection.Output
        PARAMETRO11.Value = ""
        COMANDO.Parameters.Add(PARAMETRO11)

        Dim PARAMETRO12 As New SqlParameter("@VERSON", SqlDbType.VarChar, 15)
        PARAMETRO12.Direction = ParameterDirection.Output
        PARAMETRO12.Value = ""
        COMANDO.Parameters.Add(PARAMETRO12)

        Dim PARAMETRO13 As New SqlParameter("@numCtaPago", SqlDbType.VarChar, 4)
        PARAMETRO13.Direction = ParameterDirection.Output
        PARAMETRO13.Value = ""
        COMANDO.Parameters.Add(PARAMETRO13)

        Dim PARAMETRO14 As New SqlParameter("@TIPOFACTOR", SqlDbType.VarChar, 4)
        PARAMETRO14.Direction = ParameterDirection.Output
        PARAMETRO14.Value = ""
        COMANDO.Parameters.Add(PARAMETRO14)

        Dim PARAMETRO15 As New SqlParameter("@USOCFD", SqlDbType.VarChar, 4)
        PARAMETRO15.Direction = ParameterDirection.Output
        PARAMETRO15.Value = ""
        COMANDO.Parameters.Add(PARAMETRO15)

        Dim PARAMETRO16 As New SqlParameter("@FECHAFACTURA", SqlDbType.VarChar, 20)
        PARAMETRO16.Direction = ParameterDirection.Output
        PARAMETRO16.Value = ""
        COMANDO.Parameters.Add(PARAMETRO16)

        Dim PARAMETRO17 As New SqlParameter("@TIPOCAMBIO", SqlDbType.VarChar, 4)
        PARAMETRO17.Direction = ParameterDirection.Output
        PARAMETRO17.Value = ""
        COMANDO.Parameters.Add(PARAMETRO17)

        Dim PARAMETRO18 As New SqlParameter("@UUIDRel", SqlDbType.VarChar, 150)
        PARAMETRO18.Direction = ParameterDirection.Output
        PARAMETRO18.Value = ""
        COMANDO.Parameters.Add(PARAMETRO18)

        Try
            '@FORMADEPAGO VARCHAR(250) OUTPUT
            ',@CONDICIONESDEPAGO VARCHAR(250)OUTPUT,@MOTIVODESCUENTO VARCHAR(250)OUTPUT,@METODODEPAGO VARCHAR(250)OUTPUT,
            '@TIPODECOMPROBANTE VARCHAR(50) OUTPUT,@PAGOENPARCIALIDADES VARCHAR(50) OUTPUT,@LUGAREXPEDICION VARCHAR(250) OUTPUT
            ',@REGIMEN VARCHAR(400) OUTPUT,@MONEDA VARCHAR(15) OUTPUT,@VERSON  

            CONEXION.Open()
            Dim i As Integer = COMANDO.ExecuteNonQuery
            GloFORMADEPAGO = PARAMETRO3.Value
            GloCONDICIONESDEPAGO = PARAMETRO4.Value
            GloMOTIVODESCUENTO = PARAMETRO5.Value
            GloMETODODEPAGO = PARAMETRO6.Value
            GloTIPODECOMPROBANTE = PARAMETRO7.Value
            GloPAGOENPARCIALIDADES = PARAMETRO8.Value
            GloLUGAREXPEDICION = PARAMETRO9.Value
            GloREGIMEN = PARAMETRO10.Value
            GloMONEDA = PARAMETRO11.Value
            GloVERSON = PARAMETRO12.Value
            GlonumCtaPago = PARAMETRO13.Value
            GloTIPOFACTOR = PARAMETRO14.Value
            GloUSOCFD = PARAMETRO15.Value
            GloFECHAVIGENCIA = PARAMETRO16.Value
            GloTIPOCAMBIO = PARAMETRO17.Value
            GloUUIDRel = PARAMETRO18.Value
        Catch ex As Exception
            If Graba = "0" Then
                Llenalog(ex.Source.ToString & " " & ex.Message)
            Else

                Llenalog(ex.Source.ToString & " " & ex.Message)
            End If
        Finally
            CONEXION.Close()
            CONEXION.Dispose()
        End Try
    End Sub

    Public Shared Sub Usp_Ed_DameDatosFacDig_Almacen(ByVal oIdCompra As Long, ByVal oCompania As Integer, ByVal LocConexion As String)
        GloFORMADEPAGO = ""
        GloCONDICIONESDEPAGO = ""
        GloMOTIVODESCUENTO = ""
        GloMETODODEPAGO = ""
        GloTIPODECOMPROBANTE = ""
        GloPAGOENPARCIALIDADES = ""
        GloLUGAREXPEDICION = ""
        GloREGIMEN = ""
        GloMONEDA = ""
        GloVERSON = ""
        GloTIPOFACTOR = ""
        GloFORMAPAGO = ""
        GloTIPOCAMBIO = ""
        GloUSOCFD = ""
        GloFECHAVIGENCIA = ""

        Dim CONEXION As New SqlConnection(LocConexion)
        Dim COMANDO As New SqlCommand("Usp_Ed_DameDatosFacDig_Almacen", CONEXION)
        COMANDO.CommandType = CommandType.StoredProcedure
        COMANDO.CommandTimeout = 0
        '@Clv_Factura Bigint,@Clv_FacturaCFD bigint,@Serie varchar(5)

        Dim PARAMETRO1 As New SqlParameter("@IDCompra", SqlDbType.BigInt)
        PARAMETRO1.Direction = ParameterDirection.Input
        PARAMETRO1.Value = oIdCompra
        COMANDO.Parameters.Add(PARAMETRO1)

        Dim PARAMETRO2 As New SqlParameter("@IDCOMPANIA", SqlDbType.Int)
        PARAMETRO2.Direction = ParameterDirection.Input
        PARAMETRO2.Value = oCompania
        COMANDO.Parameters.Add(PARAMETRO2)

        '@FORMADEPAGO VARCHAR(250) OUTPUT
        ',@CONDICIONESDEPAGO VARCHAR(250)OUTPUT,@MOTIVODESCUENTO VARCHAR(250)OUTPUT,@METODODEPAGO VARCHAR(250)OUTPUT,
        '@TIPODECOMPROBANTE VARCHAR(50) OUTPUT,@PAGOENPARCIALIDADES VARCHAR(50) OUTPUT,@LUGAREXPEDICION VARCHAR(250) OUTPUT
        ',@REGIMEN VARCHAR(400) OUTPUT,@MONEDA VARCHAR(15) OUTPUT,@VERSON

        Dim PARAMETRO3 As New SqlParameter("@FORMADEPAGO", SqlDbType.VarChar, 250)
        PARAMETRO3.Direction = ParameterDirection.Output
        PARAMETRO3.Value = ""
        COMANDO.Parameters.Add(PARAMETRO3)

        Dim PARAMETRO4 As New SqlParameter("@CONDICIONESDEPAGO", SqlDbType.VarChar, 250)
        PARAMETRO4.Direction = ParameterDirection.Output
        PARAMETRO4.Value = ""
        COMANDO.Parameters.Add(PARAMETRO4)

        Dim PARAMETRO5 As New SqlParameter("@MOTIVODESCUENTO", SqlDbType.VarChar, 250)
        PARAMETRO5.Direction = ParameterDirection.Output
        PARAMETRO5.Value = ""
        COMANDO.Parameters.Add(PARAMETRO5)

        Dim PARAMETRO6 As New SqlParameter("@METODODEPAGO", SqlDbType.VarChar, 250)
        PARAMETRO6.Direction = ParameterDirection.Output
        PARAMETRO6.Value = ""
        COMANDO.Parameters.Add(PARAMETRO6)

        Dim PARAMETRO7 As New SqlParameter("@TIPODECOMPROBANTE", SqlDbType.VarChar, 250)
        PARAMETRO7.Direction = ParameterDirection.Output
        PARAMETRO7.Value = ""
        COMANDO.Parameters.Add(PARAMETRO7)

        Dim PARAMETRO8 As New SqlParameter("@PAGOENPARCIALIDADES", SqlDbType.VarChar, 250)
        PARAMETRO8.Direction = ParameterDirection.Output
        PARAMETRO8.Value = ""
        COMANDO.Parameters.Add(PARAMETRO8)

        Dim PARAMETRO9 As New SqlParameter("@LUGAREXPEDICION", SqlDbType.VarChar, 250)
        PARAMETRO9.Direction = ParameterDirection.Output
        PARAMETRO9.Value = ""
        COMANDO.Parameters.Add(PARAMETRO9)

        Dim PARAMETRO10 As New SqlParameter("@REGIMEN", SqlDbType.VarChar, 400)
        PARAMETRO10.Direction = ParameterDirection.Output
        PARAMETRO10.Value = ""
        COMANDO.Parameters.Add(PARAMETRO10)

        Dim PARAMETRO11 As New SqlParameter("@MONEDA", SqlDbType.VarChar, 15)
        PARAMETRO11.Direction = ParameterDirection.Output
        PARAMETRO11.Value = ""
        COMANDO.Parameters.Add(PARAMETRO11)

        Dim PARAMETRO12 As New SqlParameter("@VERSON", SqlDbType.VarChar, 15)
        PARAMETRO12.Direction = ParameterDirection.Output
        PARAMETRO12.Value = ""
        COMANDO.Parameters.Add(PARAMETRO12)

        Dim PARAMETRO13 As New SqlParameter("@numCtaPago", SqlDbType.VarChar, 4)
        PARAMETRO13.Direction = ParameterDirection.Output
        PARAMETRO13.Value = ""
        COMANDO.Parameters.Add(PARAMETRO13)

        Dim PARAMETRO14 As New SqlParameter("@TIPOFACTOR", SqlDbType.VarChar, 4)
        PARAMETRO14.Direction = ParameterDirection.Output
        PARAMETRO14.Value = ""
        COMANDO.Parameters.Add(PARAMETRO14)

        Dim PARAMETRO15 As New SqlParameter("@USOCFD", SqlDbType.VarChar, 4)
        PARAMETRO15.Direction = ParameterDirection.Output
        PARAMETRO15.Value = ""
        COMANDO.Parameters.Add(PARAMETRO15)

        Dim PARAMETRO16 As New SqlParameter("@FECHAFACTURA", SqlDbType.VarChar, 20)
        PARAMETRO16.Direction = ParameterDirection.Output
        PARAMETRO16.Value = ""
        COMANDO.Parameters.Add(PARAMETRO16)

        Dim PARAMETRO17 As New SqlParameter("@TIPOCAMBIO", SqlDbType.VarChar, 15)
        PARAMETRO17.Direction = ParameterDirection.Output
        PARAMETRO17.Value = ""
        COMANDO.Parameters.Add(PARAMETRO17)

        Try
            '@FORMADEPAGO VARCHAR(250) OUTPUT
            ',@CONDICIONESDEPAGO VARCHAR(250)OUTPUT,@MOTIVODESCUENTO VARCHAR(250)OUTPUT,@METODODEPAGO VARCHAR(250)OUTPUT,
            '@TIPODECOMPROBANTE VARCHAR(50) OUTPUT,@PAGOENPARCIALIDADES VARCHAR(50) OUTPUT,@LUGAREXPEDICION VARCHAR(250) OUTPUT
            ',@REGIMEN VARCHAR(400) OUTPUT,@MONEDA VARCHAR(15) OUTPUT,@VERSON  

            CONEXION.Open()
            Dim i As Integer = COMANDO.ExecuteNonQuery
            GloFORMADEPAGO = PARAMETRO3.Value
            GloCONDICIONESDEPAGO = PARAMETRO4.Value
            GloMOTIVODESCUENTO = PARAMETRO5.Value
            GloMETODODEPAGO = PARAMETRO6.Value
            GloTIPODECOMPROBANTE = PARAMETRO7.Value
            GloPAGOENPARCIALIDADES = PARAMETRO8.Value
            GloLUGAREXPEDICION = PARAMETRO9.Value
            GloREGIMEN = PARAMETRO10.Value
            GloMONEDA = PARAMETRO11.Value
            GloVERSON = PARAMETRO12.Value
            GlonumCtaPago = PARAMETRO13.Value
            GloTIPOFACTOR = PARAMETRO14.Value
            GloUSOCFD = PARAMETRO15.Value
            GloFECHAVIGENCIA = PARAMETRO16.Value
            GloTIPOCAMBIO = PARAMETRO17.Value
        Catch ex As Exception
            If Graba = "0" Then
                Llenalog(ex.Source.ToString & " " & ex.Message)
            Else

                Llenalog(ex.Source.ToString & " " & ex.Message)
            End If
        Finally
            CONEXION.Close()
            CONEXION.Dispose()
        End Try
    End Sub

    Public Function ups_NUEVAFACTGLOBALporcompania(ByVal oclvcompania As Long, ByVal oFecha As Date, ByVal oCajera As String, ByVal oSucursal As Integer, ByVal LocConexion As String) As Long
        ups_NUEVAFACTGLOBALporcompania = 0
        '@clvcompania int,@Fecha datetime,@Cajera varchar(11),@IdFactura bigint output

        Dim DT As New DataTable
        Class1.limpiaParametros()
        DT = Class1.ConsultaDT("UspDameClvCompañia", LocConexion)

        'oclvcompania = 1
        oclvcompania = CInt(DT.Rows(0)(0).ToString)
        GloNomReporteFiscal = ""
        GloNomReporteFiscalGlobal = ""
        Dim CONEXION As New SqlConnection(LocConexion)
        Dim COMANDO As New SqlCommand("ups_NUEVAFACTGLOBALporcompania", CONEXION)
        COMANDO.CommandType = CommandType.StoredProcedure
        COMANDO.CommandTimeout = 0
        '@Clv_Factura Bigint,@Clv_FacturaCFD bigint,@Serie varchar(5)
        Dim PARAMETRO1 As New SqlParameter("@clvcompania", SqlDbType.BigInt)
        PARAMETRO1.Direction = ParameterDirection.Input
        PARAMETRO1.Value = oclvcompania
        COMANDO.Parameters.Add(PARAMETRO1)

        Dim PARAMETRO2 As New SqlParameter("@Fecha", SqlDbType.DateTime)
        PARAMETRO2.Direction = ParameterDirection.Input
        PARAMETRO2.Value = oFecha
        COMANDO.Parameters.Add(PARAMETRO2)

        Dim PARAMETRO3 As New SqlParameter("@Cajera", SqlDbType.VarChar, 11)
        PARAMETRO3.Direction = ParameterDirection.Input
        PARAMETRO3.Value = oCajera
        COMANDO.Parameters.Add(PARAMETRO3)

        Dim PARAMETRO4 As New SqlParameter("@IdFactura", SqlDbType.BigInt)
        PARAMETRO4.Direction = ParameterDirection.Output
        PARAMETRO4.Value = ""
        COMANDO.Parameters.Add(PARAMETRO4)

        Dim PARAMETRO5 As New SqlParameter("@Sucursal", SqlDbType.Int)
        PARAMETRO5.Direction = ParameterDirection.Input
        PARAMETRO5.Value = oSucursal
        COMANDO.Parameters.Add(PARAMETRO5)


        Try
            CONEXION.Open()
            Dim i As Integer = COMANDO.ExecuteNonQuery
            ups_NUEVAFACTGLOBALporcompania = PARAMETRO4.Value

        Catch ex As Exception
            ups_NUEVAFACTGLOBALporcompania = 0
            If Graba = "0" Then
                Llenalog(ex.Source.ToString & " " & ex.Message)
            Else

                Llenalog(ex.Source.ToString & " " & ex.Message)
            End If
        Finally
            CONEXION.Close()
            CONEXION.Dispose()
        End Try
    End Function

    Public Shared Function USP_DameImportePorCompania(ByVal oFecha As Date, ByVal oclvcompania As Long, ByVal oSucursal As Long, ByVal LocConexion As String) As Double
        USP_DameImportePorCompania = 0
        Dim CON01 As New SqlConnection(LocConexion)
        Dim SQL As New SqlCommand()
        Try
            CON01.Open()
            SQL = New SqlCommand()
            With SQL
                .CommandText = "USP_DameImportePorCompania"
                .CommandTimeout = 0
                .Connection = CON01
                .CommandType = CommandType.StoredProcedure
                .CommandTimeout = 0

                Dim PRM As New SqlParameter("@Fecha", SqlDbType.DateTime)
                PRM.Direction = ParameterDirection.Input
                PRM.Value = oFecha
                .Parameters.Add(PRM)
                '@clvcompania
                Dim PRM2 As New SqlParameter("@clvcompania", SqlDbType.BigInt)
                PRM2.Direction = ParameterDirection.Input
                PRM2.Value = oclvcompania
                .Parameters.Add(PRM2)

                '@sucursal
                Dim PRM3 As New SqlParameter("@sucursal", SqlDbType.BigInt)
                PRM3.Direction = ParameterDirection.Input
                PRM3.Value = oSucursal
                .Parameters.Add(PRM3)

                Dim reader As SqlDataReader = .ExecuteReader()

                While (reader.Read)
                    USP_DameImportePorCompania = reader.GetValue(0)
                    'EMail_TextBox.Text = reader.GetValue(2)
                End While
            End With

        Catch ex As Exception
            'System.Windows.Forms.MessageBox.Show(ex.Message)
            Llenalog(ex.Source.ToString & " " & ex.Message)
        Finally
            CON01.Close()
        End Try
    End Function
    Public Shared Sub Usp_Ed_DameDatosFacDigGlobalSinIeps(ByVal oCLV_FActura As Long, ByVal oCompania As Integer, ByVal LocConexion As String)
        GloFORMADEPAGO = ""
        GloCONDICIONESDEPAGO = ""
        GloMOTIVODESCUENTO = ""
        GloMETODODEPAGO = ""
        GloTIPODECOMPROBANTE = ""
        GloPAGOENPARCIALIDADES = ""
        GloLUGAREXPEDICION = ""
        GloREGIMEN = ""
        GloMONEDA = ""
        GloVERSON = ""
        GloTIPOFACTOR = ""
        GloFORMAPAGO = ""
        GloTIPOCAMBIO = ""
        GloUSOCFD = ""
        GloFECHAVIGENCIA = ""
        GloOBSERVACION = ""

        Dim CONEXION As New SqlConnection(LocConexion)
        Dim COMANDO As New SqlCommand("Usp_Ed_DameDatosFacDigNotaGlobalSinIeps", CONEXION)
        COMANDO.CommandType = CommandType.StoredProcedure
        COMANDO.CommandTimeout = 0
        '@Clv_Factura Bigint,@Clv_FacturaCFD bigint,@Serie varchar(5)

        Dim PARAMETRO1 As New SqlParameter("@Id_CFD", SqlDbType.BigInt)
        PARAMETRO1.Direction = ParameterDirection.Input
        PARAMETRO1.Value = oCLV_FActura
        COMANDO.Parameters.Add(PARAMETRO1)

        Dim PARAMETRO2 As New SqlParameter("@IDCOMPANIA", SqlDbType.Int)
        PARAMETRO2.Direction = ParameterDirection.Input
        PARAMETRO2.Value = oCompania
        COMANDO.Parameters.Add(PARAMETRO2)

        '@FORMADEPAGO VARCHAR(250) OUTPUT
        ',@CONDICIONESDEPAGO VARCHAR(250)OUTPUT,@MOTIVODESCUENTO VARCHAR(250)OUTPUT,@METODODEPAGO VARCHAR(250)OUTPUT,
        '@TIPODECOMPROBANTE VARCHAR(50) OUTPUT,@PAGOENPARCIALIDADES VARCHAR(50) OUTPUT,@LUGAREXPEDICION VARCHAR(250) OUTPUT
        ',@REGIMEN VARCHAR(400) OUTPUT,@MONEDA VARCHAR(15) OUTPUT,@VERSON

        Dim PARAMETRO3 As New SqlParameter("@FORMADEPAGO", SqlDbType.VarChar, 250)
        PARAMETRO3.Direction = ParameterDirection.Output
        PARAMETRO3.Value = ""
        COMANDO.Parameters.Add(PARAMETRO3)

        Dim PARAMETRO4 As New SqlParameter("@CONDICIONESDEPAGO", SqlDbType.VarChar, 250)
        PARAMETRO4.Direction = ParameterDirection.Output
        PARAMETRO4.Value = ""
        COMANDO.Parameters.Add(PARAMETRO4)

        Dim PARAMETRO5 As New SqlParameter("@MOTIVODESCUENTO", SqlDbType.VarChar, 250)
        PARAMETRO5.Direction = ParameterDirection.Output
        PARAMETRO5.Value = ""
        COMANDO.Parameters.Add(PARAMETRO5)

        Dim PARAMETRO6 As New SqlParameter("@METODODEPAGO", SqlDbType.VarChar, 250)
        PARAMETRO6.Direction = ParameterDirection.Output
        PARAMETRO6.Value = ""
        COMANDO.Parameters.Add(PARAMETRO6)

        Dim PARAMETRO7 As New SqlParameter("@TIPODECOMPROBANTE", SqlDbType.VarChar, 250)
        PARAMETRO7.Direction = ParameterDirection.Output
        PARAMETRO7.Value = ""
        COMANDO.Parameters.Add(PARAMETRO7)

        Dim PARAMETRO8 As New SqlParameter("@PAGOENPARCIALIDADES", SqlDbType.VarChar, 250)
        PARAMETRO8.Direction = ParameterDirection.Output
        PARAMETRO8.Value = ""
        COMANDO.Parameters.Add(PARAMETRO8)

        Dim PARAMETRO9 As New SqlParameter("@LUGAREXPEDICION", SqlDbType.VarChar, 250)
        PARAMETRO9.Direction = ParameterDirection.Output
        PARAMETRO9.Value = ""
        COMANDO.Parameters.Add(PARAMETRO9)

        Dim PARAMETRO10 As New SqlParameter("@REGIMEN", SqlDbType.VarChar, 400)
        PARAMETRO10.Direction = ParameterDirection.Output
        PARAMETRO10.Value = ""
        COMANDO.Parameters.Add(PARAMETRO10)

        Dim PARAMETRO11 As New SqlParameter("@MONEDA", SqlDbType.VarChar, 15)
        PARAMETRO11.Direction = ParameterDirection.Output
        PARAMETRO11.Value = ""
        COMANDO.Parameters.Add(PARAMETRO11)

        Dim PARAMETRO12 As New SqlParameter("@VERSON", SqlDbType.VarChar, 15)
        PARAMETRO12.Direction = ParameterDirection.Output
        PARAMETRO12.Value = ""
        COMANDO.Parameters.Add(PARAMETRO12)

        Dim PARAMETRO13 As New SqlParameter("@numCtaPago", SqlDbType.VarChar, 4)
        PARAMETRO13.Direction = ParameterDirection.Output
        PARAMETRO13.Value = ""
        COMANDO.Parameters.Add(PARAMETRO13)

        Dim PARAMETRO14 As New SqlParameter("@TIPOFACTOR", SqlDbType.VarChar, 4)
        PARAMETRO14.Direction = ParameterDirection.Output
        PARAMETRO14.Value = ""
        COMANDO.Parameters.Add(PARAMETRO14)

        Dim PARAMETRO15 As New SqlParameter("@USOCFD", SqlDbType.VarChar, 4)
        PARAMETRO15.Direction = ParameterDirection.Output
        PARAMETRO15.Value = ""
        COMANDO.Parameters.Add(PARAMETRO15)

        Dim PARAMETRO16 As New SqlParameter("@FECHAFACTURA", SqlDbType.VarChar, 20)
        PARAMETRO16.Direction = ParameterDirection.Output
        PARAMETRO16.Value = ""
        COMANDO.Parameters.Add(PARAMETRO16)

        Dim PARAMETRO17 As New SqlParameter("@TIPOCAMBIO", SqlDbType.VarChar, 4)
        PARAMETRO17.Direction = ParameterDirection.Output
        PARAMETRO17.Value = ""
        COMANDO.Parameters.Add(PARAMETRO17)

        Dim PARAMETRO18 As New SqlParameter("@OBSERVACION", SqlDbType.VarChar, 8000)
        PARAMETRO18.Direction = ParameterDirection.Output
        PARAMETRO18.Value = ""
        COMANDO.Parameters.Add(PARAMETRO18)

        Try
            '@FORMADEPAGO VARCHAR(250) OUTPUT
            ',@CONDICIONESDEPAGO VARCHAR(250)OUTPUT,@MOTIVODESCUENTO VARCHAR(250)OUTPUT,@METODODEPAGO VARCHAR(250)OUTPUT,
            '@TIPODECOMPROBANTE VARCHAR(50) OUTPUT,@PAGOENPARCIALIDADES VARCHAR(50) OUTPUT,@LUGAREXPEDICION VARCHAR(250) OUTPUT
            ',@REGIMEN VARCHAR(400) OUTPUT,@MONEDA VARCHAR(15) OUTPUT,@VERSON  

            CONEXION.Open()
            Dim i As Integer = COMANDO.ExecuteNonQuery
            GloFORMADEPAGO = PARAMETRO3.Value
            GloCONDICIONESDEPAGO = PARAMETRO4.Value
            GloMOTIVODESCUENTO = PARAMETRO5.Value
            GloMETODODEPAGO = PARAMETRO6.Value
            GloTIPODECOMPROBANTE = PARAMETRO7.Value
            GloPAGOENPARCIALIDADES = PARAMETRO8.Value
            GloLUGAREXPEDICION = PARAMETRO9.Value
            GloREGIMEN = PARAMETRO10.Value
            GloMONEDA = PARAMETRO11.Value
            GloVERSON = PARAMETRO12.Value
            GlonumCtaPago = PARAMETRO13.Value
            GloTIPOFACTOR = PARAMETRO14.Value
            GloUSOCFD = PARAMETRO15.Value
            GloFECHAVIGENCIA = PARAMETRO16.Value
            GloTIPOCAMBIO = PARAMETRO17.Value
            GloOBSERVACION = PARAMETRO18.Value
        Catch ex As Exception
            If Graba = "0" Then
                Llenalog(ex.Source.ToString & " " & ex.Message)
            Else

                Llenalog(ex.Source.ToString & " " & ex.Message)
            End If
        Finally
            CONEXION.Close()
            CONEXION.Dispose()
        End Try
    End Sub
    Public Shared Sub Usp_Ed_DameDatosFacDigGlobal(ByVal oCLV_FActura As Long, ByVal oCompania As Integer, ByVal LocConexion As String)
        GloFORMADEPAGO = ""
        GloCONDICIONESDEPAGO = ""
        GloMOTIVODESCUENTO = ""
        GloMETODODEPAGO = ""
        GloTIPODECOMPROBANTE = ""
        GloPAGOENPARCIALIDADES = ""
        GloLUGAREXPEDICION = ""
        GloREGIMEN = ""
        GloMONEDA = ""
        GloVERSON = ""
        GloTIPOFACTOR = ""
        GloFORMAPAGO = ""
        GloTIPOCAMBIO = ""
        GloUSOCFD = ""
        GloFECHAVIGENCIA = ""
        GloOBSERVACION = ""

        Dim CONEXION As New SqlConnection(LocConexion)
        Dim COMANDO As New SqlCommand("Usp_Ed_DameDatosFacDigGlobal", CONEXION)
        COMANDO.CommandType = CommandType.StoredProcedure
        COMANDO.CommandTimeout = 0
        '@Clv_Factura Bigint,@Clv_FacturaCFD bigint,@Serie varchar(5)

        Dim PARAMETRO1 As New SqlParameter("@CLV_FACTURA", SqlDbType.BigInt)
        PARAMETRO1.Direction = ParameterDirection.Input
        PARAMETRO1.Value = oCLV_FActura
        COMANDO.Parameters.Add(PARAMETRO1)

        Dim PARAMETRO2 As New SqlParameter("@IDCOMPANIA", SqlDbType.Int)
        PARAMETRO2.Direction = ParameterDirection.Input
        PARAMETRO2.Value = oCompania
        COMANDO.Parameters.Add(PARAMETRO2)

        '@FORMADEPAGO VARCHAR(250) OUTPUT
        ',@CONDICIONESDEPAGO VARCHAR(250)OUTPUT,@MOTIVODESCUENTO VARCHAR(250)OUTPUT,@METODODEPAGO VARCHAR(250)OUTPUT,
        '@TIPODECOMPROBANTE VARCHAR(50) OUTPUT,@PAGOENPARCIALIDADES VARCHAR(50) OUTPUT,@LUGAREXPEDICION VARCHAR(250) OUTPUT
        ',@REGIMEN VARCHAR(400) OUTPUT,@MONEDA VARCHAR(15) OUTPUT,@VERSON

        Dim PARAMETRO3 As New SqlParameter("@FORMADEPAGO", SqlDbType.VarChar, 250)
        PARAMETRO3.Direction = ParameterDirection.Output
        PARAMETRO3.Value = ""
        COMANDO.Parameters.Add(PARAMETRO3)

        Dim PARAMETRO4 As New SqlParameter("@CONDICIONESDEPAGO", SqlDbType.VarChar, 250)
        PARAMETRO4.Direction = ParameterDirection.Output
        PARAMETRO4.Value = ""
        COMANDO.Parameters.Add(PARAMETRO4)

        Dim PARAMETRO5 As New SqlParameter("@MOTIVODESCUENTO", SqlDbType.VarChar, 250)
        PARAMETRO5.Direction = ParameterDirection.Output
        PARAMETRO5.Value = ""
        COMANDO.Parameters.Add(PARAMETRO5)

        Dim PARAMETRO6 As New SqlParameter("@METODODEPAGO", SqlDbType.VarChar, 250)
        PARAMETRO6.Direction = ParameterDirection.Output
        PARAMETRO6.Value = ""
        COMANDO.Parameters.Add(PARAMETRO6)

        Dim PARAMETRO7 As New SqlParameter("@TIPODECOMPROBANTE", SqlDbType.VarChar, 250)
        PARAMETRO7.Direction = ParameterDirection.Output
        PARAMETRO7.Value = ""
        COMANDO.Parameters.Add(PARAMETRO7)

        Dim PARAMETRO8 As New SqlParameter("@PAGOENPARCIALIDADES", SqlDbType.VarChar, 250)
        PARAMETRO8.Direction = ParameterDirection.Output
        PARAMETRO8.Value = ""
        COMANDO.Parameters.Add(PARAMETRO8)

        Dim PARAMETRO9 As New SqlParameter("@LUGAREXPEDICION", SqlDbType.VarChar, 250)
        PARAMETRO9.Direction = ParameterDirection.Output
        PARAMETRO9.Value = ""
        COMANDO.Parameters.Add(PARAMETRO9)

        Dim PARAMETRO10 As New SqlParameter("@REGIMEN", SqlDbType.VarChar, 400)
        PARAMETRO10.Direction = ParameterDirection.Output
        PARAMETRO10.Value = ""
        COMANDO.Parameters.Add(PARAMETRO10)

        Dim PARAMETRO11 As New SqlParameter("@MONEDA", SqlDbType.VarChar, 15)
        PARAMETRO11.Direction = ParameterDirection.Output
        PARAMETRO11.Value = ""
        COMANDO.Parameters.Add(PARAMETRO11)

        Dim PARAMETRO12 As New SqlParameter("@VERSON", SqlDbType.VarChar, 15)
        PARAMETRO12.Direction = ParameterDirection.Output
        PARAMETRO12.Value = ""
        COMANDO.Parameters.Add(PARAMETRO12)

        Dim PARAMETRO13 As New SqlParameter("@numCtaPago", SqlDbType.VarChar, 4)
        PARAMETRO13.Direction = ParameterDirection.Output
        PARAMETRO13.Value = ""
        COMANDO.Parameters.Add(PARAMETRO13)

        Dim PARAMETRO14 As New SqlParameter("@TIPOFACTOR", SqlDbType.VarChar, 4)
        PARAMETRO14.Direction = ParameterDirection.Output
        PARAMETRO14.Value = ""
        COMANDO.Parameters.Add(PARAMETRO14)

        Dim PARAMETRO15 As New SqlParameter("@USOCFD", SqlDbType.VarChar, 4)
        PARAMETRO15.Direction = ParameterDirection.Output
        PARAMETRO15.Value = ""
        COMANDO.Parameters.Add(PARAMETRO15)

        Dim PARAMETRO16 As New SqlParameter("@FECHAFACTURA", SqlDbType.VarChar, 20)
        PARAMETRO16.Direction = ParameterDirection.Output
        PARAMETRO16.Value = ""
        COMANDO.Parameters.Add(PARAMETRO16)

        Dim PARAMETRO17 As New SqlParameter("@TIPOCAMBIO", SqlDbType.VarChar, 4)
        PARAMETRO17.Direction = ParameterDirection.Output
        PARAMETRO17.Value = ""
        COMANDO.Parameters.Add(PARAMETRO17)

        Dim PARAMETRO18 As New SqlParameter("@OBSERVACION", SqlDbType.VarChar, 8000)
        PARAMETRO18.Direction = ParameterDirection.Output
        PARAMETRO18.Value = ""
        COMANDO.Parameters.Add(PARAMETRO18)

        Try
            '@FORMADEPAGO VARCHAR(250) OUTPUT
            ',@CONDICIONESDEPAGO VARCHAR(250)OUTPUT,@MOTIVODESCUENTO VARCHAR(250)OUTPUT,@METODODEPAGO VARCHAR(250)OUTPUT,
            '@TIPODECOMPROBANTE VARCHAR(50) OUTPUT,@PAGOENPARCIALIDADES VARCHAR(50) OUTPUT,@LUGAREXPEDICION VARCHAR(250) OUTPUT
            ',@REGIMEN VARCHAR(400) OUTPUT,@MONEDA VARCHAR(15) OUTPUT,@VERSON  

            CONEXION.Open()
            Dim i As Integer = COMANDO.ExecuteNonQuery
            GloFORMADEPAGO = PARAMETRO3.Value
            GloCONDICIONESDEPAGO = PARAMETRO4.Value
            GloMOTIVODESCUENTO = PARAMETRO5.Value
            GloMETODODEPAGO = PARAMETRO6.Value
            GloTIPODECOMPROBANTE = PARAMETRO7.Value
            GloPAGOENPARCIALIDADES = PARAMETRO8.Value
            GloLUGAREXPEDICION = PARAMETRO9.Value
            GloREGIMEN = PARAMETRO10.Value
            GloMONEDA = PARAMETRO11.Value
            GloVERSON = PARAMETRO12.Value
            GlonumCtaPago = PARAMETRO13.Value
            GloTIPOFACTOR = PARAMETRO14.Value
            GloUSOCFD = PARAMETRO15.Value
            GloFECHAVIGENCIA = PARAMETRO16.Value
            GloTIPOCAMBIO = PARAMETRO17.Value
            GloOBSERVACION = PARAMETRO18.Value
        Catch ex As Exception
            If Graba = "0" Then
                Llenalog(ex.Source.ToString & " " & ex.Message)
            Else

                Llenalog(ex.Source.ToString & " " & ex.Message)
            End If
        Finally
            CONEXION.Close()
            CONEXION.Dispose()
        End Try
    End Sub

    Public Shared Sub Usp_Ed_DameDatosFacDigGlobalReFacturacion(ByVal oCLV_FActura As Long, ByVal oCompania As Integer, ByVal LocConexion As String)
        GloFORMADEPAGO = ""
        GloCONDICIONESDEPAGO = ""
        GloMOTIVODESCUENTO = ""
        GloMETODODEPAGO = ""
        GloTIPODECOMPROBANTE = ""
        GloPAGOENPARCIALIDADES = ""
        GloLUGAREXPEDICION = ""
        GloREGIMEN = ""
        GloMONEDA = ""
        GloVERSON = ""
        GloTIPOFACTOR = ""
        GloFORMAPAGO = ""
        GloTIPOCAMBIO = ""
        GloUSOCFD = ""
        GloFECHAVIGENCIA = ""
        GloOBSERVACION = ""
        GloUUIDRel = ""

        Dim CONEXION As New SqlConnection(LocConexion)
        Dim COMANDO As New SqlCommand("Usp_Ed_DameDatosFacDigGlobalReFacturacion", CONEXION)
        COMANDO.CommandType = CommandType.StoredProcedure
        CoManDo.CommandTimeout = 0
        '@Clv_Factura Bigint,@Clv_FacturaCFD bigint,@Serie varchar(5)

        Dim PARAMETRO1 As New SqlParameter("@CLV_FACTURA", SqlDbType.BigInt)
        PARAMETRO1.Direction = ParameterDirection.Input
        PARAMETRO1.Value = oCLV_FActura
        COMANDO.Parameters.Add(PARAMETRO1)

        Dim PARAMETRO2 As New SqlParameter("@IDCOMPANIA", SqlDbType.Int)
        PARAMETRO2.Direction = ParameterDirection.Input
        PARAMETRO2.Value = oCompania
        COMANDO.Parameters.Add(PARAMETRO2)

        '@FORMADEPAGO VARCHAR(250) OUTPUT
        ',@CONDICIONESDEPAGO VARCHAR(250)OUTPUT,@MOTIVODESCUENTO VARCHAR(250)OUTPUT,@METODODEPAGO VARCHAR(250)OUTPUT,
        '@TIPODECOMPROBANTE VARCHAR(50) OUTPUT,@PAGOENPARCIALIDADES VARCHAR(50) OUTPUT,@LUGAREXPEDICION VARCHAR(250) OUTPUT
        ',@REGIMEN VARCHAR(400) OUTPUT,@MONEDA VARCHAR(15) OUTPUT,@VERSON

        Dim PARAMETRO3 As New SqlParameter("@FORMADEPAGO", SqlDbType.VarChar, 250)
        PARAMETRO3.Direction = ParameterDirection.Output
        PARAMETRO3.Value = ""
        COMANDO.Parameters.Add(PARAMETRO3)

        Dim PARAMETRO4 As New SqlParameter("@CONDICIONESDEPAGO", SqlDbType.VarChar, 250)
        PARAMETRO4.Direction = ParameterDirection.Output
        PARAMETRO4.Value = ""
        COMANDO.Parameters.Add(PARAMETRO4)

        Dim PARAMETRO5 As New SqlParameter("@MOTIVODESCUENTO", SqlDbType.VarChar, 250)
        PARAMETRO5.Direction = ParameterDirection.Output
        PARAMETRO5.Value = ""
        COMANDO.Parameters.Add(PARAMETRO5)

        Dim PARAMETRO6 As New SqlParameter("@METODODEPAGO", SqlDbType.VarChar, 250)
        PARAMETRO6.Direction = ParameterDirection.Output
        PARAMETRO6.Value = ""
        COMANDO.Parameters.Add(PARAMETRO6)

        Dim PARAMETRO7 As New SqlParameter("@TIPODECOMPROBANTE", SqlDbType.VarChar, 250)
        PARAMETRO7.Direction = ParameterDirection.Output
        PARAMETRO7.Value = ""
        COMANDO.Parameters.Add(PARAMETRO7)

        Dim PARAMETRO8 As New SqlParameter("@PAGOENPARCIALIDADES", SqlDbType.VarChar, 250)
        PARAMETRO8.Direction = ParameterDirection.Output
        PARAMETRO8.Value = ""
        COMANDO.Parameters.Add(PARAMETRO8)

        Dim PARAMETRO9 As New SqlParameter("@LUGAREXPEDICION", SqlDbType.VarChar, 250)
        PARAMETRO9.Direction = ParameterDirection.Output
        PARAMETRO9.Value = ""
        COMANDO.Parameters.Add(PARAMETRO9)

        Dim PARAMETRO10 As New SqlParameter("@REGIMEN", SqlDbType.VarChar, 400)
        PARAMETRO10.Direction = ParameterDirection.Output
        PARAMETRO10.Value = ""
        COMANDO.Parameters.Add(PARAMETRO10)

        Dim PARAMETRO11 As New SqlParameter("@MONEDA", SqlDbType.VarChar, 15)
        PARAMETRO11.Direction = ParameterDirection.Output
        PARAMETRO11.Value = ""
        COMANDO.Parameters.Add(PARAMETRO11)

        Dim PARAMETRO12 As New SqlParameter("@VERSON", SqlDbType.VarChar, 15)
        PARAMETRO12.Direction = ParameterDirection.Output
        PARAMETRO12.Value = ""
        COMANDO.Parameters.Add(PARAMETRO12)

        Dim PARAMETRO13 As New SqlParameter("@numCtaPago", SqlDbType.VarChar, 4)
        PARAMETRO13.Direction = ParameterDirection.Output
        PARAMETRO13.Value = ""
        COMANDO.Parameters.Add(PARAMETRO13)

        Dim PARAMETRO14 As New SqlParameter("@TIPOFACTOR", SqlDbType.VarChar, 4)
        PARAMETRO14.Direction = ParameterDirection.Output
        PARAMETRO14.Value = ""
        COMANDO.Parameters.Add(PARAMETRO14)

        Dim PARAMETRO15 As New SqlParameter("@USOCFD", SqlDbType.VarChar, 4)
        PARAMETRO15.Direction = ParameterDirection.Output
        PARAMETRO15.Value = ""
        COMANDO.Parameters.Add(PARAMETRO15)

        Dim PARAMETRO16 As New SqlParameter("@FECHAFACTURA", SqlDbType.VarChar, 20)
        PARAMETRO16.Direction = ParameterDirection.Output
        PARAMETRO16.Value = ""
        COMANDO.Parameters.Add(PARAMETRO16)

        Dim PARAMETRO17 As New SqlParameter("@TIPOCAMBIO", SqlDbType.VarChar, 4)
        PARAMETRO17.Direction = ParameterDirection.Output
        PARAMETRO17.Value = ""
        COMANDO.Parameters.Add(PARAMETRO17)

        Dim PARAMETRO18 As New SqlParameter("@OBSERVACION", SqlDbType.VarChar, 8000)
        PARAMETRO18.Direction = ParameterDirection.Output
        PARAMETRO18.Value = ""
        COMANDO.Parameters.Add(PARAMETRO18)

        Dim PARAMETRO19 As New SqlParameter("@UUIDRel", SqlDbType.VarChar, 150)
        PARAMETRO19.Direction = ParameterDirection.Output
        PARAMETRO19.Value = ""
        COMANDO.Parameters.Add(PARAMETRO19)

        Try
            '@FORMADEPAGO VARCHAR(250) OUTPUT
            ',@CONDICIONESDEPAGO VARCHAR(250)OUTPUT,@MOTIVODESCUENTO VARCHAR(250)OUTPUT,@METODODEPAGO VARCHAR(250)OUTPUT,
            '@TIPODECOMPROBANTE VARCHAR(50) OUTPUT,@PAGOENPARCIALIDADES VARCHAR(50) OUTPUT,@LUGAREXPEDICION VARCHAR(250) OUTPUT
            ',@REGIMEN VARCHAR(400) OUTPUT,@MONEDA VARCHAR(15) OUTPUT,@VERSON  

            CONEXION.Open()
            Dim i As Integer = COMANDO.ExecuteNonQuery
            GloFORMADEPAGO = PARAMETRO3.Value
            GloCONDICIONESDEPAGO = PARAMETRO4.Value
            GloMOTIVODESCUENTO = PARAMETRO5.Value
            GloMETODODEPAGO = PARAMETRO6.Value
            GloTIPODECOMPROBANTE = PARAMETRO7.Value
            GloPAGOENPARCIALIDADES = PARAMETRO8.Value
            GloLUGAREXPEDICION = PARAMETRO9.Value
            GloREGIMEN = PARAMETRO10.Value
            GloMONEDA = PARAMETRO11.Value
            GloVERSON = PARAMETRO12.Value
            GlonumCtaPago = PARAMETRO13.Value
            GloTIPOFACTOR = PARAMETRO14.Value
            GloUSOCFD = PARAMETRO15.Value
            GloFECHAVIGENCIA = PARAMETRO16.Value
            GloTIPOCAMBIO = PARAMETRO17.Value
            GloOBSERVACION = PARAMETRO18.Value
            GloUUIDRel = PARAMETRO19.Value
        Catch ex As Exception
            If Graba = "0" Then
                Llenalog(ex.Source.ToString & " " & ex.Message)
            Else

                Llenalog(ex.Source.ToString & " " & ex.Message)
            End If
        Finally
            CONEXION.Close()
            CONEXION.Dispose()
        End Try
    End Sub

    Public Shared Sub Inserta_Tbl_FacturasNoGeneradas(ByVal oCLV_FActura As Long, ByVal oTipo As String, ByVal LocObs As String, ByVal LocConexion As String)
        Try


            Dim CONEXION As New SqlConnection(LocConexion)
            Dim COMANDO As New SqlCommand("Inserta_Tbl_FacturasNoGeneradas", CONEXION)
            COMANDO.CommandType = CommandType.StoredProcedure
            COMANDO.CommandTimeout = 0
            '@Clv_Factura Bigint,@Clv_FacturaCFD bigint,@Serie varchar(5)
            Dim PARAMETRO1 As New SqlParameter("@Clv_Factura", SqlDbType.BigInt)
            PARAMETRO1.Direction = ParameterDirection.Input
            PARAMETRO1.Value = oCLV_FActura
            COMANDO.Parameters.Add(PARAMETRO1)

            Dim PARAMETRO4 As New SqlParameter("@TipoFact", SqlDbType.VarChar, 1)
            PARAMETRO4.Direction = ParameterDirection.Input
            PARAMETRO4.Value = oTipo
            COMANDO.Parameters.Add(PARAMETRO4)

            Dim PARAMETRO5 As New SqlParameter("@Obs", SqlDbType.VarChar, 8000)
            PARAMETRO5.Direction = ParameterDirection.Input
            PARAMETRO5.Value = LocObs
            COMANDO.Parameters.Add(PARAMETRO5)

            Try
                CONEXION.Open()
                Dim i As Integer = COMANDO.ExecuteNonQuery
            Catch ex As Exception
                ''MsgBox(ex.Message, MsgBoxStyle.Exclamation)
                If Graba = "0" Then
                    Llenalog(ex.Source.ToString & " " & ex.Message)
                Else

                    Llenalog(ex.Source.ToString & " " & ex.Message)
                End If
            Finally
                CONEXION.Close()
                CONEXION.Dispose()
            End Try
        Catch ex As Exception

        End Try
    End Sub

    Public Shared Sub UPS_Modificar_Rel_FacturasCFD(ByVal oClv_FacturaCFD As Long, LocConexion As String)
        Dim CONEXION As New SqlConnection(LocConexion)
        Dim COMANDO As New SqlCommand("UPS_Modificar_Rel_FacturasCFD", CONEXION)
        COMANDO.CommandType = CommandType.StoredProcedure
        CoMando.CommandTimeout = 0
        Dim PARAMETRO2 As New SqlParameter("@Clv_FacturaCFD", SqlDbType.BigInt)
        PARAMETRO2.Direction = ParameterDirection.Input
        PARAMETRO2.Value = oClv_FacturaCFD
        COMANDO.Parameters.Add(PARAMETRO2)

        Try
            CONEXION.Open()
            Dim i As Integer = COMANDO.ExecuteNonQuery
        Catch ex As Exception
            'MsgBox(ex.Message, MsgBoxStyle.Exclamation)
            If Graba = "0" Then
                Llenalog(ex.Source.ToString & " " & ex.Message)
            Else

                Llenalog(ex.Source.ToString & " " & ex.Message)
            End If
        Finally
            CONEXION.Close()
            CONEXION.Dispose()
        End Try
    End Sub

    Public Shared Sub UPS_Inserta_Rel_FacturasCFD(ByVal oCLV_FActura As Long, ByVal oClv_FacturaCFD As Long, ByVal oSerie As String, ByVal oTipo As String, ByVal LocConexion As String)
        Dim CONEXION As New SqlConnection(LocConexion)
        Dim COMANDO As New SqlCommand("UPS_Inserta_Rel_FacturasCFD", CONEXION)
        COMANDO.CommandType = CommandType.StoredProcedure
        COMANDO.CommandTimeout = 0
        '@Clv_Factura Bigint,@Clv_FacturaCFD bigint,@Serie varchar(5)
        Dim PARAMETRO1 As New SqlParameter("@Clv_Factura", SqlDbType.BigInt)
        PARAMETRO1.Direction = ParameterDirection.Input
        PARAMETRO1.Value = oCLV_FActura
        COMANDO.Parameters.Add(PARAMETRO1)

        Dim PARAMETRO2 As New SqlParameter("@Clv_FacturaCFD", SqlDbType.BigInt)
        PARAMETRO2.Direction = ParameterDirection.Input
        PARAMETRO2.Value = oClv_FacturaCFD
        COMANDO.Parameters.Add(PARAMETRO2)

        Dim PARAMETRO3 As New SqlParameter("@Serie", SqlDbType.VarChar, 5)
        PARAMETRO3.Direction = ParameterDirection.Input
        PARAMETRO3.Value = oSerie
        COMANDO.Parameters.Add(PARAMETRO3)

        Dim PARAMETRO4 As New SqlParameter("@tipo", SqlDbType.VarChar, 1)
        PARAMETRO4.Direction = ParameterDirection.Input
        PARAMETRO4.Value = oTipo
        COMANDO.Parameters.Add(PARAMETRO4)

        Try
            CONEXION.Open()
            Dim i As Integer = COMANDO.ExecuteNonQuery
        Catch ex As Exception
            'MsgBox(ex.Message, MsgBoxStyle.Exclamation)
            If Graba = "0" Then
                Llenalog(ex.Source.ToString & " " & ex.Message)
            Else

                Llenalog(ex.Source.ToString & " " & ex.Message)
            End If
        Finally
            CONEXION.Close()
            CONEXION.Dispose()
        End Try
    End Sub

    Public Shared Sub UspAltaEsNuevoRel_Cliente_AsociadoCFD(ByVal oContrato As Long, ByVal oIDASOCIADO As Long, ByVal LocConexion As String)
        Dim CON01 As New SqlConnection(LocConexion)
        Dim SQL As New SqlCommand()
        Try
            CON01.Open()
            SQL = New SqlCommand()
            With SQL
                .CommandText = "UspAltaEsNuevoRel_Cliente_AsociadoCFD"
                .Connection = CON01
                .CommandTimeout = 0
                .CommandType = CommandType.StoredProcedure
                .CommandTimeout = 0

                Dim prm As New SqlParameter("@CONTRATO", SqlDbType.BigInt)
                prm.Value = oContrato
                prm.Direction = ParameterDirection.Input
                .Parameters.Add(prm)

                prm = New SqlParameter("@IDASOCIADO", SqlDbType.BigInt)
                prm.Value = oIDASOCIADO
                prm.Direction = ParameterDirection.Input
                .Parameters.Add(prm)

                'prm = New SqlParameter("@Email", SqlDbType.VarChar, 100)
                'prm.Value = oEmail
                'prm.Direction = ParameterDirection.Input
                '.Parameters.Add(prm)

                Dim i As Integer = .ExecuteNonQuery()
            End With
            CON01.Close()
        Catch ex As Exception
            'System.Windows.Forms.MessageBox.Show(ex.Message)
             Llenalog(ex.Source.ToString & " " & ex.Message)
        End Try
    End Sub


    Public Shared Sub Alta_Datosfiscales_NEW(ByVal IdCompania As Long, ByVal oRazonSocial As String,
        ByVal oRFC As String,
        ByVal oCalle As String,
        ByVal oClaveAsociado As String,
        ByVal oCodigoPostal As String,
        ByVal oColonia As String,
        ByVal oEMail As String,
        ByVal oEntreCalles As String,
        ByVal oEstado As String,
        ByVal oIdAsociado As String,
        ByVal oLocalidad As String,
        ByVal oMunicipio As String,
        ByVal oNumeroExterior As String,
        ByVal oPais As String, ByVal oTel As String, ByVal oFax As String, ByVal oReferencia As String, ByVal ocontrato As Long, ByVal LocConexion As String)
        Dim OPRUEBA As New MizarCFDi.API.Efac

        Dim oAsociados As MizarCFD.BRL.Asociados
        Dim oId_AsociadoLlave As Long = 0
        Dim oIdDireccion As Long = 0

        If String.IsNullOrEmpty(oRazonSocial) = True Then oRazonSocial = ""
        If String.IsNullOrEmpty(oRFC) = True Then oRFC = ""
        If String.IsNullOrEmpty(oCalle) = True Then oCalle = ""
        If String.IsNullOrEmpty(oClaveAsociado) = True Then oClaveAsociado = ""
        If String.IsNullOrEmpty(oCodigoPostal) = True Then oCodigoPostal = ""
        If String.IsNullOrEmpty(oColonia) = True Then oColonia = ""
        If String.IsNullOrEmpty(oEMail) = True Then oEMail = ""
        If String.IsNullOrEmpty(oEntreCalles) = True Then oEntreCalles = ""
        If String.IsNullOrEmpty(oEstado) = True Then oEstado = ""
        If String.IsNullOrEmpty(oIdAsociado) = True Then oIdAsociado = ""
        If String.IsNullOrEmpty(oLocalidad) = True Then oLocalidad = ""
        If String.IsNullOrEmpty(oMunicipio) = True Then oMunicipio = ""
        If String.IsNullOrEmpty(oNumeroExterior) = True Then oNumeroExterior = ""
        If String.IsNullOrEmpty(oPais) = True Then oPais = ""
        If String.IsNullOrEmpty(oTel) = True Then oTel = ""
        If String.IsNullOrEmpty(oFax) = True Then oFax = ""
        If String.IsNullOrEmpty(oReferencia) = True Then oReferencia = ""
        '
        oAlta_Datosfiscales = True

        If Existe_Checalo(" Select * from asociados  where ID_asociado = " + oIdAsociado) = False Then


            'If OPRUEBA.AltaCliente(oIdAsociado, IdCompania, oRazonSocial, oCalle, oCodigoPostal, oColonia, oEMail, oEntreCalles, oEstado, oFax, oLocalidad, oMunicipio, oNumeroExterior, "", oPais, oReferencia, oRFC, oTel) = False Then
            '    'MsgBox(OPRUEBA.mensajeError, MsgBoxStyle.Information)
            '    oAlta_Datosfiscales = False
            '    Exit Sub
            'End If

            UspAltaEsNuevoRel_Cliente_AsociadoCFD(ocontrato, oIdAsociado, LocConexion)

        Else
            'Modificar

            oId_AsociadoLlave = oIdAsociado 'Existe_DevuelveValor(" Select id_asociado from asociados  where id_asociado = " + oIdAsociado)
            UspAltaEsNuevoRel_Cliente_AsociadoCFD(ocontrato, oIdAsociado, LocConexion)

            Using Cnx As New MizarCFD.DAL.DAConexion("HL", "sa", "sa")

                oAsociados = New MizarCFD.BRL.Asociados
                oAsociados.Inicializar()
                oAsociados.IdAsociado = oId_AsociadoLlave
                oAsociados.ClaveAsociado = oId_AsociadoLlave
                oAsociados.RazonSocial = oRazonSocial
                oAsociados.RFC = oRFC
                'oAsociados.IdCompania = IdCompania
                oAsociados.TiempoPago = 1
                oAsociados.CondicionesDePago = "Una Sola Exhibicion"
                oAsociados.PeriodoPago = 2
                oAsociados.IdMoneda = 1

                oAsociados.IdTipoAsociado = Asociados.TipoAsociado.Cliente

                If oAsociados.Validar(Cnx, TipoAfectacionBD.Modificar) = True Then

                    If Not oAsociados.Modificar(Cnx) Then
                        For Each oMensaje As DAMensajesSistema.RegistroMensaje In oAsociados.MensajesSistema.ListaMensajes
                            MessageBox.Show(oMensaje.TextoMensaje + " " + oMensaje.Contexto)
                            oAlta_Datosfiscales = False
                        Next
                        Return
                    End If
                Else
                    For Each oMensaje As DAMensajesSistema.RegistroMensaje In oAsociados.MensajesSistema.ListaMensajes
                        MessageBox.Show(oMensaje.TextoMensaje + " " + oMensaje.Contexto)
                        oAlta_Datosfiscales = False
                    Next
                    Return
                End If
            End Using

            'oIdDireccion = Existe_DevuelveValor(" Select id_direccion  from [asociados_direcciones] where id_asociado = " + oId_AsociadoLlave)
            oIdDireccion = 1
            'Guardamos la Dirreccion
            Using Cnx As New MizarCFD.DAL.DAConexion("HL", "sa", "sa")
                Dim odirecciones As New MizarCFD.BRL.AsociadosDirecciones
                Dim otipo As New MizarCFD.BRL.AsociadosDirecciones.TipoDireccion
                odirecciones.Inicializar()
                odirecciones.IdDireccion = oIdDireccion
                odirecciones.IdAsociado = oId_AsociadoLlave
                odirecciones.CodigoPostal = oCodigoPostal
                odirecciones.Calle = oCalle
                odirecciones.IdTipoDireccion = odirecciones.TipoDireccion.Principal
                odirecciones.Colonia = oColonia
                odirecciones.EMail = oEMail
                odirecciones.EntreCalles = oEntreCalles
                odirecciones.Estado = oEstado
                odirecciones.Localidad = oLocalidad
                odirecciones.Municipio = oMunicipio
                odirecciones.NumeroExterior = oNumeroExterior
                odirecciones.Telefono = oTel
                odirecciones.Fax = oFax
                odirecciones.Referencia = oReferencia
                odirecciones.Pais = oPais
                'If odirecciones.Validar(Cnx, TipoAfectacionBD.Modificar) = True Then
                If Not odirecciones.Modificar(Cnx) Then
                    For Each oMensaje As DAMensajesSistema.RegistroMensaje In odirecciones.MensajesSistema.ListaMensajes
                        MessageBox.Show(oMensaje.TextoMensaje + " " + oMensaje.Contexto)
                        oAlta_Datosfiscales = False
                    Next
                    Return
                End If
                'Else
                'For Each oMensaje As DAMensajesSistema.RegistroMensaje In odirecciones.MensajesSistema.ListaMensajes
                '    MessageBox.Show(oMensaje.TextoMensaje + " " + oMensaje.Contexto)
                '    oAlta_Datosfiscales = False
                'Next
                'Return
                'End If



            End Using


        End If





    End Sub


    Public Shared Function Existe_Checalo(ByVal oSql As String) As Boolean
        Existe_Checalo = False

        Using Cnx As New MizarCFD.DAL.DAConexion("HL", "sa", "sa")
            Cnx.DbCommand.CommandText = oSql
            Cnx.DbCommand.CommandTimeout = 0
            Cnx.DbCommand.CommandType = CommandType.Text
            Cnx.DbCommand.CommandTimeout = 0
            'Cnx.DbConnection.Open()
            'Dim CONEXION As New SqlConnection(Cnx.DbConnection.Database)
            'Dim COMANDO As New SqlCommand(oSql, CONEXION)
            'COMANDO.CommandType = CommandType.Text
            'Try
            '    CONEXION.Open()
            'Dim reader As OleDbDataReader = Cnx.DbCommand.ExecuteReader
            Dim reader As SqlDataReader = Cnx.DbCommand.ExecuteReader
            '    'Recorremos los Cargos obtenidos desde el Procedimiento
            While (reader.Read())
                If Len(reader(0).ToString) > 0 Then
                    Existe_Checalo = True
                End If
            End While
            'Cnx.DbConnection.Close()
            'Catch ex As Exception
            '    'MsgBox(ex.Message, MsgBoxStyle.Exclamation)
            'Finally
            '    CONEXION.Close()
            '    CONEXION.Dispose()
            'End Try
        End Using
    End Function

    Public Shared Function Existe_DevuelveValor(ByVal oSql As String) As Long
        Existe_DevuelveValor = 0

        Using Cnx As New MizarCFD.DAL.DAConexion("HL", "sa", "sa")
            Cnx.DbCommand.CommandText = oSql
            Cnx.DbCommand.CommandTimeout = 0
            Cnx.DbCommand.CommandType = CommandType.Text
            Cnx.DbCommand.CommandTimeout = 0
            'Cnx.DbConnection.Open()
            'Dim CONEXION As New SqlConnection(Cnx.DbConnection.Database)
            'Dim COMANDO As New SqlCommand(oSql, CONEXION)
            'COMANDO.CommandType = CommandType.Text
            'Try
            '    CONEXION.Open()
            Dim reader As SqlDataReader = Cnx.DbCommand.ExecuteReader
            '    'Recorremos los Cargos obtenidos desde el Procedimiento
            While (reader.Read())
                If Len(reader(0).ToString) > 0 Then
                    Existe_DevuelveValor = CLng(reader(0).ToString)
                End If
            End While
            'Cnx.DbConnection.Close()
            'Catch ex As Exception
            '    'MsgBox(ex.Message, MsgBoxStyle.Exclamation)
            'Finally
            '    CONEXION.Close()
            '    CONEXION.Dispose()
            'End Try
        End Using
    End Function

    Public Shared Sub Usp_ED_BorraRel_Cliente_AsociadoCFD(ByVal oContrato As Long, ByVal LocConexion As String)
        Dim cON_x As New SqlConnection(LocConexion)
        Try
            'Regresa La Session

            Dim cmd As New SqlCommand
            cON_x.Open()
            With cmd
                .CommandText = "Usp_ED_BorraRel_Cliente_AsociadoCFD"
                .CommandTimeout = 0
                .CommandType = CommandType.StoredProcedure
                .CommandTimeout = 0
                .Connection = cON_x
                Dim prm2 As New SqlParameter("@CONTRATO", SqlDbType.BigInt)
                prm2.Direction = ParameterDirection.Input
                prm2.Value = oContrato
                .Parameters.Add(prm2)
                Dim i As Integer = .ExecuteNonQuery
            End With
            cON_x.Close()
        Catch ex As Exception
            cON_x.Close()
        End Try
    End Sub

    Public Shared Function UspEsNuevoRel_Cliente_AsociadoCFD(ByVal RFC As String, ByVal LocConexion As String) As Long
        UspEsNuevoRel_Cliente_AsociadoCFD = 0
        Dim CON01 As New SqlConnection(LocConexion)
        Dim SQL As New SqlCommand()
        Try
            CON01.Open()
            SQL = New SqlCommand()
            With SQL
                .CommandText = "UspEsNuevoRel_Cliente_AsociadoCFD"
                .CommandTimeout = 0
                .Connection = CON01
                .CommandType = CommandType.StoredProcedure
                .CommandTimeout = 0

                Dim PRM As New SqlParameter("@RFC", SqlDbType.VarChar, 50)
                PRM.Direction = ParameterDirection.Input
                PRM.Value = RFC
                .Parameters.Add(PRM)


                Dim reader As SqlDataReader = .ExecuteReader()

                While (reader.Read)
                    UspEsNuevoRel_Cliente_AsociadoCFD = reader.GetValue(0)
                    'EMail_TextBox.Text = reader.GetValue(2)
                End While
            End With

        Catch ex As Exception
            'System.Windows.Forms.MessageBox.Show(ex.Message)
            Llenalog(ex.Source.ToString & " " & ex.Message)
        Finally
            CON01.Close()
        End Try
    End Function

    Public Shared Sub Borrar_Datosfiscales(ByVal orfc As String, ByVal Locconexion As String)

        'Modificar
        Dim DT As New DataTable
        Class1.limpiaParametros()
        DT = Class1.ConsultaDT("UspDameClvCompañia", Locconexion)

        Dim oId_AsociadoLlave As Long = 0
        Dim oIdDireccion As Long = 0
        oId_AsociadoLlave = UspEsNuevoRel_Cliente_AsociadoCFD(orfc, Locconexion)
        '--oId_AsociadoLlave = Existe_DevuelveValor(" Select id_asociado from asociados  where id_asociado = " + oContrato)
        oIdDireccion = 1
        'Guardamos la Dirreccion
        If oId_AsociadoLlave > 0 Then
            Using Cnx As New MizarCFD.DAL.DAConexion("HL", "sa", "sa")
                Dim odirecciones As New MizarCFD.BRL.AsociadosDirecciones
                Dim otipo As New MizarCFD.BRL.AsociadosDirecciones.TipoDireccion
                odirecciones.Inicializar()
                odirecciones.IdDireccion = oIdDireccion
                odirecciones.IdAsociado = oId_AsociadoLlave
                If Not odirecciones.BorrarPorIdAsociado(Cnx) Then
                    For Each oMensaje As DAMensajesSistema.RegistroMensaje In odirecciones.MensajesSistema.ListaMensajes
                        MessageBox.Show(oMensaje.TextoMensaje + " " + oMensaje.Contexto)
                        oAlta_Datosfiscales = False
                    Next
                    Return
                End If
            End Using

            Dim oAsociados As MizarCFD.BRL.Asociados
            Using Cnx As New MizarCFD.DAL.DAConexion("HL", "sa", "sa")
                oAsociados = New MizarCFD.BRL.Asociados
                oAsociados.Inicializar()
                oAsociados.IdAsociado = oId_AsociadoLlave
                oAsociados.ClaveAsociado = oId_AsociadoLlave
                'oAsociados.IdCompania = CInt(DT.Rows(0)(0).ToString)
                'oAsociados.IdCompania = 1
                If oAsociados.Validar(Cnx, TipoAfectacionBD.Borrar) = True Then
                    If Not oAsociados.Borrar(Cnx) Then
                        For Each oMensaje As DAMensajesSistema.RegistroMensaje In oAsociados.MensajesSistema.ListaMensajes
                            MessageBox.Show(oMensaje.TextoMensaje + " " + oMensaje.Contexto)
                        Next
                        Return
                    End If
                Else
                    For Each oMensaje As DAMensajesSistema.RegistroMensaje In oAsociados.MensajesSistema.ListaMensajes
                        MessageBox.Show(oMensaje.TextoMensaje + " " + oMensaje.Contexto)
                    Next
                    Return
                End If
            End Using
        End If


    End Sub

    Public Shared Function Query_Asociado(ByVal oSql As String) As Long
        Query_Asociado = 0
        Using Cnx As New MizarCFD.DAL.DAConexion("HL", "sa", "sa")
            Cnx.DbCommand.CommandText = oSql
            Cnx.DbCommand.CommandTimeout = 0
            Cnx.DbCommand.CommandType = CommandType.Text
            Cnx.DbCommand.CommandTimeout = 0
            'Cnx.DbConnection.Open()
            'Dim CONEXION As New SqlConnection(Cnx.DbConnection.Database)
            'Dim COMANDO As New SqlCommand(oSql, CONEXION)
            'COMANDO.CommandType = CommandType.Text
            'Try
            '    CONEXION.Open()
            Dim reader As SqlDataReader = Cnx.DbCommand.ExecuteReader
            '    'Recorremos los Cargos obtenidos desde el Procedimiento
            While (reader.Read())
                'If Len(reader(0).ToString) > 0 Then
                If IsNumeric(reader(0).ToString) = True Then
                    Query_Asociado = CLng(reader(0).ToString)
                End If
                'End If
            End While
            'Cnx.DbConnection.Close()
            'Catch ex As Exception
            '    'MsgBox(ex.Message, MsgBoxStyle.Exclamation)
            'Finally
            '    CONEXION.Close()
            '    CONEXION.Dispose()
            'End Try
        End Using
    End Function

    Public Shared Function Nuevo_Asociado(ByVal oSql As String) As Long
        Nuevo_Asociado = 0
        Using Cnx As New MizarCFD.DAL.DAConexion("HL", "sa", "sa")
            Cnx.DbCommand.CommandText = oSql
            Cnx.DbCommand.CommandTimeout = 0
            Cnx.DbCommand.CommandType = CommandType.Text
            Cnx.DbCommand.CommandTimeout = 0
            'Cnx.DbConnection.Open()
            'Dim CONEXION As New SqlConnection(Cnx.DbConnection.Database)
            'Dim COMANDO As New SqlCommand(oSql, CONEXION)
            'COMANDO.CommandType = CommandType.Text
            'Try
            '    CONEXION.Open()
            Dim reader As SqlDataReader = Cnx.DbCommand.ExecuteReader
            '    'Recorremos los Cargos obtenidos desde el Procedimiento
            While (reader.Read())
                If Len(reader(0).ToString) > 0 Then
                    If IsNumeric(reader(0).ToString) = False Then
                        Nuevo_Asociado = 1
                    Else
                        Nuevo_Asociado = reader(0).ToString + 1
                    End If
                End If
            End While
            'Cnx.DbConnection.Close()
            'Catch ex As Exception
            '    'MsgBox(ex.Message, MsgBoxStyle.Exclamation)
            'Finally
            '    CONEXION.Close()
            '    CONEXION.Dispose()
            'End Try
        End Using
    End Function

    Public Shared Sub SetDBReport(ByVal ds As DataSet, ByVal myReportDocument As ReportDocument)
        Try
            Dim myTables As Tables = myReportDocument.Database.Tables
            Dim myTable As CrystalDecisions.CrystalReports.Engine.Table
            For Each myTable In myTables
                myTable.SetDataSource(ds)
            Next
        Catch ex As System.Exception
            'System.Windows.Forms.MessageBox.Show(ex.Message)
            Llenalog(ex.Source.ToString & " " & ex.Message)
        End Try
    End Sub

    Public Shared Sub IMPRIMIR_Factura(ByVal oClv_Factura As Long)

        Try
            Dim oidcompania As Integer = 0

            'GloServerName = "TEAMEDGAR-PC"
            'GloDatabaseName = "NewSoftv"
            'GloUserID = "sa"
            'GloPassword = "06011975"
            '' ''
            'MiConexion = "Data Source=" & GloServerName & ";Initial Catalog=" & GloDatabaseName & " ;Persist Security Info=True;User ID=" & GloUserID & ";Password=" & GloPassword & ""
            'oClv_Factura = 76

            Dim CadenaOriginal As String = Nothing
            Dim oSerie As String = Nothing
            Dim Cantidad_Con_Letra As String = Nothing
            Using Cnx As New DAConexion("HL", "sa", "sa")
                'Dim oRreportes As New MizarCFD.Reportes.MizarCFDi
                'Dim archivoCFD As String = Nothing
                'Dim archivoPDF As String = Nothing
                Dim odas As New MizarCFD.DAL.DAUtileriasSQL
                Dim oCFD As New MizarCFD.BRL.CFD
                Dim oPath As String = ""
                oCFD.IdCFD = CStr(oClv_Factura)
                oCFD.IdCompania = "1"
                oCFD.Consultar(Cnx)
                oCFD.GenerarArchivosXMLPDF(Cnx)
                'oCFD.EnviarEmail(Cnx)
                oPath = DAUtileriasSQL.ObtenerArchivo(Cnx, "cfd_archivos", "archivo_pdf", "Id_cfd = " + CStr(oClv_Factura))
                Process.Start(oPath)
                'oreportes.ListaRDS_CFD(Cnx, CStr(oClv_Factura))
                'oCFD.Consultar(Cnx)
                'oSerie = oCFD.Serie
                'oidcompania = oCFD.IdCompania
                'Dim document As New System.Xml.XmlDocument
                'document = oEFAC.ObtenerXML(CStr(oClv_Factura)) 'oCFD.ObtenerXML()
                'document.Save(Class1.RutaReportes + "\MizarCFDi.xsd")
                'CadenaOriginal = oEFAC.ConsultarCadenaOriginal(CStr(oClv_Factura))


                'Cantidad_Con_Letra = Dame_Cantidad_Con_Letra(CDec(oCFD.Total))
                'oRreportes.cfd.ReadXml("C:\MizarCFD\abc.xml")

                ''Set RDL file. 
                '' Supply a DataTable corresponding to each report 
                '' data source. 



                'dataSet.ReadXml(Class1.RutaReportes + "\MizarCFDi.xsd")
                'Dim colString As DataColumn = New DataColumn("Cadena_Original")
                'colString.DataType = System.Type.GetType("System.String")
                'dataSet.Tables("Comprobante").Columns.Add(colString)
                'dataSet.Tables("Comprobante").Rows(0)("Cadena_Original") = CadenaOriginal

                'Dim colString2 As DataColumn = New DataColumn("Cantidad_Con_Letra")
                'colString2.DataType = System.Type.GetType("System.String")
                'dataSet.Tables("Comprobante").Columns.Add(colString2)
                'dataSet.Tables("Comprobante").Rows(0)("Cantidad_Con_Letra") = Cantidad_Con_Letra

                'Dim colString3 As DataColumn = New DataColumn("Serie")
                'colString3.DataType = System.Type.GetType("System.String")
                'dataSet.Tables("Comprobante").Columns.Add(colString3)
                'dataSet.Tables("Comprobante").Rows(0)("Serie") = oSerie

                'If (Not dataSet.Tables("Domicilio").Columns.Contains("localidad")) Then
                '    Dim colString4 As DataColumn = New DataColumn("localidad")
                '    colString4.DataType = System.Type.GetType("System.String")
                '    dataSet.Tables("Domicilio").Columns.Add(colString4)
                '    dataSet.Tables("Domicilio").Rows(0)("localidad") = ""
                'End If

                'Dim Report As New ReportDocument

                'If oidcompania = 1 Then
                'Report.Load(Class1.RutaReportes + "\RFDigital2012.rpt")
                ' Else
                'Report.Load(Class1.RutaReportes + "\RFDigital2012.rpt")
                'End If

                'Report.SetDataSource(oreportes)

                'SetDBReport(dataSet, Report)

                'CrystalReportViewer1.ReportSource = Report
            End Using

            'GloClv_Factura = 0
            'ClassCFDI.GloClv_FacturaCFD = 0
        Catch ex As Exception
            'MsgBox(ex.Message & " " & ex.Message)
            If Graba = "0" Then
                Llenalog(ex.Source.ToString & " " & ex.Message)
            Else

                Llenalog(ex.Source.ToString & " " & ex.Message)
            End If
        End Try
    End Sub

    Public Shared Function GENERAR_ARCHIVOS(ByVal oClv_Factura As Long) As Boolean

        Try
            GENERAR_ARCHIVOS = False
            Dim OPRUEBA As New MizarCFDi.API.Efac
            Dim oidcompania As Integer = 0
            Dim CadenaOriginal As String = Nothing
            Dim oSerie As String = Nothing
            Dim Cantidad_Con_Letra As String = Nothing
            Using Cnx As New DAConexion("HL", "sa", "sa")

                Dim odas As New MizarCFD.DAL.DAUtileriasSQL
                Dim oCFD As New MizarCFD.BRL.CFD
                Dim oPath As String = ""
                oCFD.IdCFD = CStr(oClv_Factura)
                oCFD.IdCompania = "1"
                OPRUEBA.IdCFD = CStr(oClv_Factura)
                oCFD.Consultar(Cnx)
                GENERAR_ARCHIVOS = OPRUEBA.GeneraCFD("c:\Xml")

                'oCFD.GenerarArchivosXMLPDF(Cnx)
                'oCFD.EnviarEmail(Cnx)
                'Try
                '    oPath = DAUtileriasSQL.ObtenerArchivo(Cnx, "cfd_archivos", "archivo_xml", "Id_cfd = " + CStr(oClv_Factura))
                'Catch ex As Exception
                '    oCFD.GenerarArchivosXMLPDF(Cnx)
                '    oPath = DAUtileriasSQL.ObtenerArchivo(Cnx, "cfd_archivos", "archivo_xml", "Id_cfd = " + CStr(oClv_Factura))
                'End Try

                'Process.Start(oPath)

            End Using

            'GloClv_Factura = 0
            'ClassCFDI.GloClv_FacturaCFD = 0
        Catch ex As Exception
            'MsgBox(ex.Message & " " & ex.Message)
            If Graba = "0" Then
                Llenalog(ex.Source.ToString & " " & ex.Message)
            Else

                Llenalog(ex.Source.ToString & " " & ex.Message)
            End If
        End Try
    End Function

    Public Shared Sub GENERAR_XML(ByVal oClv_Factura As Long)

        Try
            Dim OPRUEBA As New MizarCFDi.API.Efac
            OPRUEBA.GeneraCFD("c:\Xml")

            Dim oidcompania As Integer = 0
            Dim CadenaOriginal As String = Nothing
            Dim oSerie As String = Nothing
            Dim Cantidad_Con_Letra As String = Nothing
            Using Cnx As New DAConexion("HL", "sa", "sa")

                Dim odas As New MizarCFD.DAL.DAUtileriasSQL
                Dim oCFD As New MizarCFD.BRL.CFD
                Dim oPath As String = ""
                oCFD.IdCFD = CStr(oClv_Factura)
                oCFD.IdCompania = "1"
                oCFD.Consultar(Cnx)

                'oCFD.GenerarArchivosXMLPDF(Cnx)
                'oCFD.EnviarEmail(Cnx)
                Try
                    oPath = DAUtileriasSQL.ObtenerArchivo(Cnx, "cfd_archivos", "archivo_xml", "Id_cfd = " + CStr(oClv_Factura))
                Catch ex As Exception
                    oCFD.GenerarArchivosXMLPDF(Cnx)
                    oPath = DAUtileriasSQL.ObtenerArchivo(Cnx, "cfd_archivos", "archivo_xml", "Id_cfd = " + CStr(oClv_Factura))
                End Try

                Process.Start(oPath)

            End Using

            'GloClv_Factura = 0
            'ClassCFDI.GloClv_FacturaCFD = 0
        Catch ex As Exception
            'MsgBox(ex.Message & " " & ex.Message)
            If Graba = "0" Then
                Llenalog(ex.Source.ToString & " " & ex.Message)
            Else

                Llenalog(ex.Source.ToString & " " & ex.Message)
            End If
        End Try
    End Sub


    Public Shared Sub ACUSECACENCALACION_Factura(ByVal oClv_Factura As Long)

        Try
            Dim oidcompania As Integer = 0

            'GloServerName = "TEAMEDGAR-PC"
            'GloDatabaseName = "NewSoftv"
            'GloUserID = "sa"
            'GloPassword = "06011975"
            '' ''
            'MiConexion = "Data Source=" & GloServerName & ";Initial Catalog=" & GloDatabaseName & " ;Persist Security Info=True;User ID=" & GloUserID & ";Password=" & GloPassword & ""
            'oClv_Factura = 76
            Dim GloAcuse As String = ""
            Dim CadenaOriginal As String = Nothing
            Dim oSerie As String = Nothing
            Dim Cantidad_Con_Letra As String = Nothing
            Using Cnx As New DAConexion("HL", "sa", "sa")
                'Dim oRreportes As New MizarCFD.Reportes.MizarCFDi
                'Dim archivoCFD As String = Nothing
                'Dim archivoPDF As String = Nothing
                Dim odas As New MizarCFD.DAL.DAUtileriasSQL
                Dim oCFD As New MizarCFD.BRL.CFD
                Dim oPath As String = ""
                oCFD.IdCFD = CStr(oClv_Factura)
                oCFD.IdCompania = "1"
                oCFD.Consultar(Cnx)
                'oCFD.GenerarArchivosXMLPDF(Cnx)
                Try
                    GloAcuse = oCFD.AcuseCancelacion()
                Catch ex As Exception

                End Try

                'oPath = DAUtileriasSQL.ObtenerArchivo(Cnx, "cfd_archivos", "archivo_pdf", "Id_cfd = " + CStr(oClv_Factura))
                'Process.Start(oPath)
                'oreportes.ListaRDS_CFD(Cnx, CStr(oClv_Factura))
                'oCFD.Consultar(Cnx)
                'oSerie = oCFD.Serie
                'oidcompania = oCFD.IdCompania
                'Dim document As New System.Xml.XmlDocument
                'document = oEFAC.ObtenerXML(CStr(oClv_Factura)) 'oCFD.ObtenerXML()
                'document.Save(Class1.RutaReportes + "\MizarCFDi.xsd")
                'CadenaOriginal = oEFAC.ConsultarCadenaOriginal(CStr(oClv_Factura))


                'Cantidad_Con_Letra = Dame_Cantidad_Con_Letra(CDec(oCFD.Total))
                'oRreportes.cfd.ReadXml("C:\MizarCFD\abc.xml")

                ''Set RDL file. 
                '' Supply a DataTable corresponding to each report 
                '' data source. 



                'dataSet.ReadXml(Class1.RutaReportes + "\MizarCFDi.xsd")
                'Dim colString As DataColumn = New DataColumn("Cadena_Original")
                'colString.DataType = System.Type.GetType("System.String")
                'dataSet.Tables("Comprobante").Columns.Add(colString)
                'dataSet.Tables("Comprobante").Rows(0)("Cadena_Original") = CadenaOriginal

                'Dim colString2 As DataColumn = New DataColumn("Cantidad_Con_Letra")
                'colString2.DataType = System.Type.GetType("System.String")
                'dataSet.Tables("Comprobante").Columns.Add(colString2)
                'dataSet.Tables("Comprobante").Rows(0)("Cantidad_Con_Letra") = Cantidad_Con_Letra

                'Dim colString3 As DataColumn = New DataColumn("Serie")
                'colString3.DataType = System.Type.GetType("System.String")
                'dataSet.Tables("Comprobante").Columns.Add(colString3)
                'dataSet.Tables("Comprobante").Rows(0)("Serie") = oSerie

                'If (Not dataSet.Tables("Domicilio").Columns.Contains("localidad")) Then
                '    Dim colString4 As DataColumn = New DataColumn("localidad")
                '    colString4.DataType = System.Type.GetType("System.String")
                '    dataSet.Tables("Domicilio").Columns.Add(colString4)
                '    dataSet.Tables("Domicilio").Rows(0)("localidad") = ""
                'End If

                'Dim Report As New ReportDocument

                'If oidcompania = 1 Then
                'Report.Load(Class1.RutaReportes + "\RFDigital2012.rpt")
                ' Else
                'Report.Load(Class1.RutaReportes + "\RFDigital2012.rpt")
                'End If

                'Report.SetDataSource(oreportes)

                'SetDBReport(dataSet, Report)

                'CrystalReportViewer1.ReportSource = Report
            End Using

            'GloClv_Factura = 0
            'ClassCFDI.GloClv_FacturaCFD = 0
        Catch ex As Exception
            'MsgBox(ex.Message & " " & ex.Message)
            If Graba = "0" Then
                Llenalog(ex.Source.ToString & " " & ex.Message)
            Else

                Llenalog(ex.Source.ToString & " " & ex.Message)
            End If
        End Try
    End Sub

    Public Shared Sub ENVIAR_Factura(ByVal oClv_Factura As Long)

        Try
            'Class1.limpiaParametros()
            'Class1.CreateMyParameter("@Clv_Factura", SqlDbType.BigInt, oClv_Factura)
            'Class1.CreateMyParameter("@Envia", ParameterDirection.Output, SqlDbType.Int)
            'Class1.ProcedimientoOutPut("ValidaEnvioFacturasCFD", locconexion)

            'If Class1.dicoPar("@Envia") = 1 Then
            Dim oidcompania As Integer = 0

            'GloServerName = "TEAMEDGAR-PC"
            'GloDatabaseName = "NewSoftv"
            'GloUserID = "sa"
            'GloPassword = "06011975"
            '' ''
            'MiConexion = "Data Source=" & GloServerName & ";Initial Catalog=" & GloDatabaseName & " ;Persist Security Info=True;User ID=" & GloUserID & ";Password=" & GloPassword & ""
            'oClv_Factura = 76

            Dim CadenaOriginal As String = Nothing
            Dim oSerie As String = Nothing
            Dim Cantidad_Con_Letra As String = Nothing
            Using Cnx As New DAConexion("HL", "sa", "sa")
                'Dim oRreportes As New MizarCFD.Reportes.MizarCFDi
                'Dim archivoCFD As String = Nothing
                'Dim archivoPDF As String = Nothing
                Dim odas As New MizarCFD.DAL.DAUtileriasSQL
                Dim oCFD As New MizarCFD.BRL.CFD
                Dim oPath As String = ""
                oCFD.IdCFD = CStr(oClv_Factura)
                oCFD.IdCompania = "1"
                Try
                    oCFD.Consultar(Cnx)
                Catch ex As Exception

                End Try
                oCFD.GenerarArchivosXMLPDF(Cnx)
                Try
                    oCFD.EnviarEmail(Cnx)
                Catch ex As Exception

                End Try

                'oPath = DAUtileriasSQL.ObtenerArchivo(Cnx, "cfd_archivos", "archivo_pdf", "Id_cfd = " + CStr(oClv_Factura))
                'Process.Start(oPath)
                'oreportes.ListaRDS_CFD(Cnx, CStr(oClv_Factura))
                'oCFD.Consultar(Cnx)
                'oSerie = oCFD.Serie
                'oidcompania = oCFD.IdCompania
                'Dim document As New System.Xml.XmlDocument
                'document = oEFAC.ObtenerXML(CStr(oClv_Factura)) 'oCFD.ObtenerXML()
                'document.Save(Class1.RutaReportes + "\MizarCFDi.xsd")
                'CadenaOriginal = oEFAC.ConsultarCadenaOriginal(CStr(oClv_Factura))


                'Cantidad_Con_Letra = Dame_Cantidad_Con_Letra(CDec(oCFD.Total))
                'oRreportes.cfd.ReadXml("C:\MizarCFD\abc.xml")

                ''Set RDL file. 
                '' Supply a DataTable corresponding to each report 
                '' data source. 



                'dataSet.ReadXml(Class1.RutaReportes + "\MizarCFDi.xsd")
                'Dim colString As DataColumn = New DataColumn("Cadena_Original")
                'colString.DataType = System.Type.GetType("System.String")
                'dataSet.Tables("Comprobante").Columns.Add(colString)
                'dataSet.Tables("Comprobante").Rows(0)("Cadena_Original") = CadenaOriginal

                'Dim colString2 As DataColumn = New DataColumn("Cantidad_Con_Letra")
                'colString2.DataType = System.Type.GetType("System.String")
                'dataSet.Tables("Comprobante").Columns.Add(colString2)
                'dataSet.Tables("Comprobante").Rows(0)("Cantidad_Con_Letra") = Cantidad_Con_Letra

                'Dim colString3 As DataColumn = New DataColumn("Serie")
                'colString3.DataType = System.Type.GetType("System.String")
                'dataSet.Tables("Comprobante").Columns.Add(colString3)
                'dataSet.Tables("Comprobante").Rows(0)("Serie") = oSerie

                'If (Not dataSet.Tables("Domicilio").Columns.Contains("localidad")) Then
                '    Dim colString4 As DataColumn = New DataColumn("localidad")
                '    colString4.DataType = System.Type.GetType("System.String")
                '    dataSet.Tables("Domicilio").Columns.Add(colString4)
                '    dataSet.Tables("Domicilio").Rows(0)("localidad") = ""
                'End If

                'Dim Report As New ReportDocument

                'If oidcompania = 1 Then
                'Report.Load(Class1.RutaReportes + "\RFDigital2012.rpt")
                ' Else
                'Report.Load(Class1.RutaReportes + "\RFDigital2012.rpt")
                'End If

                'Report.SetDataSource(oreportes)

                'SetDBReport(dataSet, Report)

                'CrystalReportViewer1.ReportSource = Report
            End Using

            'GloClv_Factura = 0
            'ClassCFDI.GloClv_FacturaCFD = 0
            'End If
        Catch ex As Exception
            'MsgBox(ex.Message & " " & ex.Message)
            If Graba = "0" Then
                Llenalog(ex.Source.ToString & " " & ex.Message)
            Else

                Llenalog(ex.Source.ToString & " " & ex.Message)
            End If
        End Try
    End Sub

    'Public Shared Sub ENVIAR_Factura(ByVal oClv_Factura As Long, ByVal locconexion As String)

    '    Try
    '        Class1.limpiaParametros()
    '        Class1.CreateMyParameter("@Clv_Factura", SqlDbType.BigInt, oClv_Factura)
    '        Class1.CreateMyParameter("@Envia", ParameterDirection.Output, SqlDbType.Int)
    '        Class1.ProcedimientoOutPut("ValidaEnvioFacturasCFD", locconexion)

    '        If Class1.dicoPar("@Envia") = 1 Then
    '            Dim oidcompania As Integer = 0

    '            Dim CadenaOriginal As String = Nothing
    '            Dim oSerie As String = Nothing
    '            Dim Cantidad_Con_Letra As String = Nothing
    '            Using Cnx As New DAConexion("HL", "sa", "sa")
    '                Dim odas As New MizarCFD.DAL.DAUtileriasSQL
    '                Dim oCFD As New MizarCFD.BRL.CFD
    '                Dim oPath As String = ""
    '                oCFD.IdCFD = CStr(oClv_Factura)
    '                oCFD.IdCompania = "1"
    '                Try
    '                    oCFD.Consultar(Cnx)
    '                Catch ex As Exception

    '                End Try
    '                oCFD.GenerarArchivosXMLPDF(Cnx)
    '                Try
    '                    oCFD.EnviarEmail(Cnx)
    '                Catch ex As Exception

    '                End Try

    '            End Using

    '            'GloClv_Factura = 0
    '            'ClassCFDI.GloClv_FacturaCFD = 0
    '        End If
    '    Catch ex As Exception
    '        'MsgBox(ex.Message & " " & ex.Message)
    '        If Graba = "0" Then
    '            Llenalog(ex.Source.ToString & " " & ex.Message)
    '        Else

    '            Llenalog(ex.Source.ToString & " " & ex.Message)
    '        End If
    '    End Try
    'End Sub
End Class
