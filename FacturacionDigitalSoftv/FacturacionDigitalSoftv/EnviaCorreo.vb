﻿Imports System
Imports System.Collections.Generic
Imports System.ComponentModel
Imports System.Data
Imports System.Drawing
Imports System.Text
Imports System.Windows.Forms
Imports System.Xml
Imports System.Data.SqlClient
'Imports System.Data.OleDb
Imports MizarCFD.DAL
Imports MizarCFD.BRL
Imports MizarCFD.Reportes
Imports MizarCFDi.API.Efac
Imports CrystalDecisions.Shared
Imports CrystalDecisions.CrystalReports.Engine

Public Class EnviaCorreo

    Public Shared Sub EnviarCorreo(ByVal oClv_Factura As Long)

        Try
            Dim oidcompania As Integer = 0


            Dim CadenaOriginal As String = Nothing
            Dim oSerie As String = Nothing
            Dim Cantidad_Con_Letra As String = Nothing
            Using Cnx As New DAConexion("HL", "sa", "sa")

                Dim odas As New MizarCFD.DAL.DAUtileriasSQL
                Dim oCFD As New MizarCFD.BRL.CFD
                Dim oPath As String = ""
                oCFD.IdCFD = CStr(oClv_Factura)
                oCFD.IdCompania = "1"
                oCFD.Consultar(Cnx)
                oCFD.GenerarArchivosXMLPDF(Cnx)

                DAUtileriasSQL.ObtenerArchivo(Cnx, "cfd_archivos", "archivo_pdf", "Id_cfd = " + CStr(oClv_Factura))

                Try
                    oCFD.EnviarEmail(Cnx)
                    MsgBox("Enviado con Exitó", MsgBoxStyle.Information)

                Catch ex As Exception
                    Llenalog(ex.Source.ToString & " " & ex.Message)
                End Try
                'oPath = DAUtileriasSQL.ObtenerArchivo(Cnx, "cfd_archivos", "archivo_pdf", "Id_cfd = " + CStr(oClv_Factura))
                'Process.Start(oPath)

            End Using

            'GloClv_Factura = 0
            'ClassCFDI.GloClv_FacturaCFD = 0
        Catch ex As Exception
            'MsgBox(ex.Message & " " & ex.Source)
            Llenalog(ex.Source.ToString & " " & ex.Message)
        End Try
    End Sub

End Class
